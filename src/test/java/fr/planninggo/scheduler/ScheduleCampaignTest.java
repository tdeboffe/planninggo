package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

public class ScheduleCampaignTest {

    @Test
    public void testEqualsWithNullArg() {
        Assert.assertEquals(false, new ScheduleCampaign().equals(null));
    }

    @Test
    public void testNotEqualsDiffDefenseTypeInstances() {
        ScheduleCampaign scheduleCamp1 = new ScheduleCampaign();
        ScheduleCampaign scheduleCamp2 = new ScheduleCampaign();
        ScheduleCampaign scheduleCamp3 = new ScheduleCampaign();
        scheduleCamp1.setId(1L);
        scheduleCamp1.setScore(HardSoftScore.of(0, 0));
        scheduleCamp2.setId(2L);
        scheduleCamp2.setScore(HardSoftScore.of(0, 0));
        scheduleCamp3.setId(3L);
        scheduleCamp3.setScore(HardSoftScore.of(-8, 0));
        Assert.assertNotEquals(scheduleCamp1, scheduleCamp2);
        Assert.assertNotEquals(scheduleCamp2, scheduleCamp3);
        Assert.assertNotEquals(scheduleCamp3, scheduleCamp1);
    }

    @Test
    public void testEqualsDiffDefenseTypeInstances() {
        ScheduleCampaign scheduleCamp1 = new ScheduleCampaign();
        ScheduleCampaign scheduleCamp2 = new ScheduleCampaign();
        scheduleCamp1.setId(1L);
        scheduleCamp1.setScore(HardSoftScore.of(0, 0));
        scheduleCamp2.setId(1L);
        scheduleCamp2.setScore(HardSoftScore.of(0, 0));
        Assert.assertEquals(scheduleCamp1, scheduleCamp2);
    }

    @Test
    public void testNotEqualsHashcodeDiffDefenseTypeInstances() {
        ScheduleCampaign scheduleCamp1 = new ScheduleCampaign();
        ScheduleCampaign scheduleCamp2 = new ScheduleCampaign();
        ScheduleCampaign scheduleCamp3 = new ScheduleCampaign();
        scheduleCamp1.setId(1L);
        scheduleCamp1.setScore(HardSoftScore.of(0, 0));
        scheduleCamp2.setId(2L);
        scheduleCamp2.setScore(HardSoftScore.of(0, 0));
        scheduleCamp3.setId(3L);
        scheduleCamp3.setScore(HardSoftScore.of(-8, 0));
        Assert.assertNotEquals(scheduleCamp1.hashCode(), scheduleCamp2.hashCode());
        Assert.assertNotEquals(scheduleCamp2.hashCode(), scheduleCamp3.hashCode());
        Assert.assertNotEquals(scheduleCamp3.hashCode(), scheduleCamp1.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffDefenseTypeInstances() {
        ScheduleCampaign scheduleCamp1 = new ScheduleCampaign();
        ScheduleCampaign scheduleCamp2 = new ScheduleCampaign();
        scheduleCamp1.setId(1L);
        scheduleCamp1.setScore(HardSoftScore.of(0, 0));
        scheduleCamp2.setId(1L);
        scheduleCamp2.setScore(HardSoftScore.of(0, 0));
        Assert.assertEquals(scheduleCamp1.hashCode(), scheduleCamp2.hashCode());
    }

    @Test
    public void testGetters() {
        ScheduleCampaign scheduleCamp = new ScheduleCampaign();
        scheduleCamp.setId(1L);
        scheduleCamp.setScore(HardSoftScore.of(0, 0));
        Assert.assertTrue(scheduleCamp.getId().equals(1L));
        Assert.assertEquals(HardSoftScore.of(0, 0), scheduleCamp.getScore());
    }

    @Test
    public void testSetIdNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setId(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetIdNeg() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setId(-1L);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testSetScoreNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setScore(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDefenseAssignsNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setDefenseAssigns(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDefensesNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setDefenses(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetRoomsNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setRooms(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetTimeslotsNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setTimeslots(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDefenseTypesNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setDefenseTypes(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetOtherAttendeesNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setOtherAttendees(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetSupervisorsNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setSupervisors(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetStudentsNull() {
        try {
            ScheduleCampaign scheduleCamp = new ScheduleCampaign();
            scheduleCamp.setStudents(null);
        } catch (NullPointerException e) {

        }
    }
}
