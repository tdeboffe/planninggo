package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;

public class ConstraintWeightTest {
    @Test
    public void testToCreateConstWeightWithNegId() {
        try {
            new ConstraintWeight(-1, "", 1, true, true);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreateConstWeightWithNullDesc() {
        try {
            new ConstraintWeight(1, null, 1, true, true);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateConstWeightWithNegWeight() {
        try {
            new ConstraintWeight(1, "", -1, true, true);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testCreateConstWeightAndGetValues() {
        int id = 1;
        String desc = "";
        int weight = 1;
        boolean enabled = true;
        boolean valid = true;
        ConstraintWeight cw = new ConstraintWeight(id, desc, weight, enabled, valid);
        Assert.assertTrue(cw.getId() == id);
        Assert.assertTrue(cw.getDescription().equals(desc));
        Assert.assertTrue(cw.getWeight() == weight);
        Assert.assertTrue(cw.isEnabled() == enabled);
        Assert.assertTrue(cw.isValid() == valid);
    }

    @Test
    public void testSetNegScore() {
        ConstraintWeight cw = new ConstraintWeight(1, "", 1, true, true);
        try {
            cw.setScore(-1);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testSetAndGetScore() {
        ConstraintWeight cw = new ConstraintWeight(1, "", 1, true, true);
        int score = 1;
        cw.setScore(score);
        Assert.assertTrue(cw.getScore() == score);
    }
}
