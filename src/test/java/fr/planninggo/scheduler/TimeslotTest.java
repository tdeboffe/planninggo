package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;

public class TimeslotTest {
    @Test
    public void testToCreateTimeslotWithNullId() {
        try {
            new Timeslot(null, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateTimeslotWithNegId() {
        try {
            new Timeslot(-1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreateTimeslotWithNullStartDateTime() {
        try {
            new Timeslot(1L, 1, null,
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateTimeslotWithNullEndDateTime() {
        try {
            new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        Timeslot t = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertEquals(false, t.equals(null));
    }

    @Test
    public void testNotEqualsDiffTimeslotInstances() {
        Timeslot t1 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 02, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot t2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 02, 11, 30));
        Timeslot t3 = new Timeslot(3L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertNotEquals(t1, t2);
        Assert.assertNotEquals(t1, t3);
        Assert.assertNotEquals(t2, t3);
    }

    @Test
    public void testEqualsDiffTimeslotInstances() {
        Timeslot t1 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot t2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertEquals(t1, t2);
    }

    @Test
    public void testNotEqualsHashcodeDiffTimeslotInstances() {
        Timeslot t1 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 02, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot t2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 03, 11, 30));
        Timeslot t3 = new Timeslot(3L, 1, LocalDateTime.of(2019, 07, 04, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertNotEquals(t1.hashCode(), t2.hashCode());
        Assert.assertNotEquals(t1.hashCode(), t3.hashCode());
        Assert.assertNotEquals(t2.hashCode(), t3.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffTimeslotInstances() {
        Timeslot t1 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot t2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertEquals(t1.hashCode(), t2.hashCode());
    }

    @Test
    public void testGetters() {
        Long id = 1L;
        LocalDateTime ldt1 = LocalDateTime.of(2019, 07, 01, 10, 30);
        LocalDateTime ldt2 = LocalDateTime.of(2019, 07, 01, 11, 30);
        Timeslot timeslot = new Timeslot(1L, 1, ldt1, ldt2);
        Assert.assertTrue(timeslot.getId().equals(id));
        Assert.assertTrue(timeslot.getStartDateTime().equals(ldt1));
        Assert.assertTrue(timeslot.getEndDateTime().equals(ldt2));
    }

    @Test
    public void testSetIdNull() {
        try {
            Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            timeslot.setId(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetIdNeg() {
        try {
            Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            timeslot.setId(-1L);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testSetStartDateTimeNull() {
        try {
            Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            timeslot.setStartDateTime(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetEndDateTimeNull() {
        try {
            Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            timeslot.setEndDateTime(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDefenseTypesNull() {
        try {
            Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                    LocalDateTime.of(2019, 07, 01, 11, 30));
            timeslot.setDefenseTypes(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testOverlapsTimeslotNull() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertFalse(timeslot.overlaps(null));
    }

    @Test
    public void testOverlapsTimeslotBeginsBeforeStartTimeslotTrue() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 00),
                LocalDateTime.of(2019, 07, 01, 12, 00));
        Assert.assertTrue(timeslot.overlaps(timeslot2));
    }

    @Test
    public void testOverlapsTimeslotBeginsAfterStartTimeslotTrue() {
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 00),
                LocalDateTime.of(2019, 07, 01, 12, 00));
        Assert.assertTrue(timeslot2.overlaps(timeslot));
        Assert.assertTrue(timeslot2.overlaps(timeslot2));
    }

    @Test
    public void testOverlapsTimeslotBeginsAfterStartTimeslotFinishBeforeEndTimeslotTrue() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 15),
                LocalDateTime.of(2019, 07, 01, 11, 45));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 00),
                LocalDateTime.of(2019, 07, 01, 12, 00));
        Assert.assertTrue(timeslot.overlaps(timeslot2));
    }

    @Test
    public void testOverlapsTimeslotFalse() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 9, 00),
                LocalDateTime.of(2019, 07, 01, 10, 00));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Assert.assertFalse(timeslot.overlaps(timeslot2));
        Assert.assertFalse(timeslot2.overlaps(timeslot));
    }
}
