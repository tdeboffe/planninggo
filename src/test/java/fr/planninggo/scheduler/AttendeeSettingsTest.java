package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;

public class AttendeeSettingsTest {
    @Test
    public void testCreateAttendeeSettingsWithNullRole() {
        try {
            new AttendeeSettings(null, false, false, false);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        AttendeeSettings attSetts = new AttendeeSettings(new Role("Encadrant"), false, false, false);
        Assert.assertEquals(false, attSetts.equals(null));
    }

    @Test
    public void testNotEqualsDiffAttSettsInstances() {
        AttendeeSettings attSetts1 = new AttendeeSettings(new Role("Encadrant"), true, false, false);
        AttendeeSettings attSetts2 = new AttendeeSettings(new Role("Encadrant"), false, true, false);
        AttendeeSettings attSetts3 = new AttendeeSettings(new Role("Encadrant"), false, false, true);
        AttendeeSettings attSetts4 = new AttendeeSettings(new Role("Prof de com"), false, false, false);
        Assert.assertNotEquals(attSetts1, attSetts2);
        Assert.assertNotEquals(attSetts1, attSetts3);
        Assert.assertNotEquals(attSetts1, attSetts4);
        Assert.assertNotEquals(attSetts2, attSetts3);
        Assert.assertNotEquals(attSetts2, attSetts4);
        Assert.assertNotEquals(attSetts3, attSetts4);
    }

    @Test
    public void testEqualsDiffAttSettsInstances() {
        AttendeeSettings attSetts1 = new AttendeeSettings(new Role("Encadrant"), true, false, false);
        AttendeeSettings attSetts2 = new AttendeeSettings(new Role("Encadrant"), true, false, false);
        Assert.assertEquals(attSetts1, attSetts2);
    }

    @Test
    public void testNotEqualsHashcodeDiffAttSettsInstances() {
        AttendeeSettings attSetts1 = new AttendeeSettings(new Role("Encadrant"), true, true, false);
        AttendeeSettings attSetts2 = new AttendeeSettings(new Role("Encadrant"), false, true, false);
        Assert.assertNotEquals(attSetts1.hashCode(), attSetts2.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffAttSettsInstances() {
        AttendeeSettings attSetts1 = new AttendeeSettings(new Role("Encadrant"), true, false, false);
        AttendeeSettings attSetts2 = new AttendeeSettings(new Role("Encadrant"), true, false, false);
        Assert.assertEquals(attSetts1.hashCode(), attSetts2.hashCode());
    }

    @Test
    public void testGetters() {
        Role role = new Role("Encadrant");
        boolean isLinkedToStudent = true;
        boolean isTheSameCourseStudy = false;
        boolean isPresident = false;
        AttendeeSettings attSett = new AttendeeSettings(role, isLinkedToStudent, isTheSameCourseStudy, isPresident);
        Assert.assertEquals(role, attSett.getRole());
        Assert.assertEquals(isLinkedToStudent, attSett.isLinkedToStudent());
        Assert.assertEquals(isTheSameCourseStudy, attSett.isTheSameCourseStudy());
        Assert.assertEquals(isPresident, attSett.isPresident());
    }
}
