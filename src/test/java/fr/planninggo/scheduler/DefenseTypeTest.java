package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;
import java.util.HashSet;

public class DefenseTypeTest {
    @Test
    public void testToCreateDefenseTypeWithNullId() {
        try {
            new DefenseType(null, "Rapport de Situation Professionnelle", Duration.ofSeconds(3600), new HashSet<>());
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullDesc() {
        try {
            new DefenseType("RSP", null, Duration.ofSeconds(3600), new HashSet<>());
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullDuration() {
        try {
            new DefenseType("RSP", "Rapport de Situation Professionnelle", null, new HashSet<>());
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullAttSettings() {
        try {
            new DefenseType("RSP", "Rapport de Situation Professionnelle", Duration.ofSeconds(3600), null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullCode2() {
        try {
            new DefenseType(null, "Rapport de Situation Professionnelle", Duration.ofSeconds(3600));
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullDuration2() {
        try {
            new DefenseType("RSP", null, Duration.ofSeconds(3600));
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullAttSettings2() {
        try {
            new DefenseType("RSP", "Rapport de Situation Professionnelle", null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseTypeWithNullStrDuration() {
        try {
            DefenseType.create("RSP", "Rapport de Situation Professionnelle", null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        DefenseType dt = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        Assert.assertEquals(false, dt.equals(null));
    }

    @Test
    public void testNotEqualsDiffDefenseTypeInstances() {
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        DefenseType dt2 = DefenseType.create("RSP2", "Rapport de Situation Professionnelle", "1h00");
        DefenseType dt3 = DefenseType.create("RSP", "Rapport", "1h00");
        DefenseType dt4 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "2h00");
        Assert.assertNotEquals(dt1, dt2);
        Assert.assertNotEquals(dt1, dt3);
        Assert.assertNotEquals(dt1, dt4);
        Assert.assertNotEquals(dt2, dt3);
        Assert.assertNotEquals(dt2, dt4);
        Assert.assertNotEquals(dt3, dt4);
    }

    @Test
    public void testEqualsDiffDefenseTypeInstances() {
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        DefenseType dt2 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        Assert.assertEquals(dt1, dt2);
    }

    @Test
    public void testNotEqualsHashcodeDiffDefenseTypeInstances() {
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        DefenseType dt2 = DefenseType.create("RSP2", "Rapport de Situation Professionnelle", "1h00");
        Assert.assertNotEquals(dt1.hashCode(), dt2.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffDefenseTypeInstances() {
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        DefenseType dt2 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        Assert.assertEquals(dt1.hashCode(), dt2.hashCode());
    }

    @Test
    public void testGetters() {
        String code = "RSP";
        String desc = "Rapport de Situation Professionnelle";
        String strDuration = "1h00";
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        Assert.assertEquals(code, dt1.getCode());
        Assert.assertEquals(desc, dt1.getDescription());
        Assert.assertEquals(Duration.ofSeconds(3600), dt1.getDuration());
    }

    @Test
    public void testSetCodeNull() {
        try {
            DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
            dt1.setCode(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDescNull() {
        try {
            DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
            dt1.setDescription(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDurationNull() {
        try {
            DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
            dt1.setDuration(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testAddAttendeeSettingsNull() {
        try {
            DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
            dt1.addAttendeeSettings(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testAddAndRemoveAttendeeSettings() {
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        AttendeeSettings attSetts1 = new AttendeeSettings(new Role("Encadrant"), true, false, false);
        dt1.addAttendeeSettings(attSetts1);
        Assert.assertEquals(1, dt1.getAttendeeSettings().size());
        Assert.assertTrue(dt1.getAttendeeSettings().contains(attSetts1));
        dt1.removeAttendeeSettings(attSetts1);
        Assert.assertEquals(0, dt1.getAttendeeSettings().size());
        Assert.assertFalse(dt1.getAttendeeSettings().contains(attSetts1));
    }

    @Test
    public void testRemoveAttendeeSettingsNull() {
        try {
            DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
            dt1.removeAttendeeSettings(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testCompareToNull() {
        try {
            DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
            dt1.compareTo(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testCompareTo(){
        DefenseType dt1 = DefenseType.create("RSP", "Rapport de Situation Professionnelle", "1h00");
        DefenseType dt2 = DefenseType.create("MI", "Mémoire d'INgénieur", "2h00");
        Assert.assertTrue(dt1.compareTo(dt2) > 0);
        Assert.assertTrue(dt2.compareTo(dt1) < 0);
        Assert.assertTrue(dt1.compareTo(dt1) == 0);
    }
}
