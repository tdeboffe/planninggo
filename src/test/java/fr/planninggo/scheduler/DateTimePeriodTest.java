package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;

public class DateTimePeriodTest {
    private static int getNbSeconds(String strDuration) {
        String[] strDurations = strDuration.split("h");
        int nbHours = Integer.parseInt(strDurations[0]);
        int nbMinutes = Integer.parseInt(strDurations[1]);
        return nbHours * 3600 + nbMinutes * 60;
    }

    @Test
    public void testToCreateDateTimePeriodWithNullDateStartTime() {
        try {
            new DateTimePeriod(null, LocalDateTime.now());
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDateTimePeriodWithNullDateEndTime() {
        try {
            new DateTimePeriod(LocalDateTime.now(), null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testCreateDateTimePeriodAndGetValues() {
        LocalDateTime ldt1 = LocalDateTime.of(2019, 07, 01, 10, 30);
        LocalDateTime ldt2 = LocalDateTime.of(2019, 07, 01, 11, 30);
        DateTimePeriod dtp = new DateTimePeriod(ldt1, ldt2);
        Assert.assertTrue(ldt1.equals(dtp.getDateStartTime()));
        Assert.assertTrue(ldt2.equals(dtp.getDateEndTime()));
    }

    @Test
    public void testGetNbTimeslotWithNullArg() {
        LocalDateTime ldt1 = LocalDateTime.of(2019, 07, 01, 10, 30);
        LocalDateTime ldt2 = LocalDateTime.of(2019, 07, 01, 11, 30);
        DateTimePeriod dtp = new DateTimePeriod(ldt1, ldt2);
        try {
            dtp.getNbTimeslot(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testGetNbTimeslot() {
        LocalDateTime ldt1 = LocalDateTime.of(2019, 07, 01, 10, 30);
        LocalDateTime ldt2 = LocalDateTime.of(2019, 07, 01, 11, 30);
        DateTimePeriod dtp = new DateTimePeriod(ldt1, ldt2);
        Assert.assertEquals(2, dtp.getNbTimeslot(Duration.ofSeconds(getNbSeconds("0h30"))));
        Assert.assertEquals(1, dtp.getNbTimeslot(Duration.ofSeconds(getNbSeconds("1h00"))));
    }

    @Test
    public void testGetNbTimeslot2() {
        LocalDateTime ldt1 = LocalDateTime.of(2019, 07, 01, 9, 00);
        LocalDateTime ldt2 = LocalDateTime.of(2019, 07, 01, 12, 00);
        DateTimePeriod dtp = new DateTimePeriod(ldt1, ldt2);
        Assert.assertEquals(1, dtp.getNbTimeslot(Duration.ofSeconds(getNbSeconds("2h00"))));
    }

}
