package fr.planninggo.scheduler;

import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DefenseTest {

    private static Long id;
    private static Room room;
    private static boolean pinned;
    private static Person student;
    private static Timeslot timeslot;
    private static DefenseType defenseTypeLong;
    private static DefenseType defenseTypeShort;

    @BeforeClass
    public static void initParams(){
        id = 1L;
        room = new Room(1L, "A");
        pinned = false;
        student = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 02, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        defenseTypeShort = new DefenseType("RSP", "Rapport de Situation Professionnelle", Duration.ofSeconds(3600), new HashSet<>());
        defenseTypeLong = new DefenseType("MI", "Mémoire d'Ingénieur", Duration.ofSeconds(2*3600), new HashSet<>());
    }

    @Test
    public void testToCreateDefenseWithNegId() {
        try {
            new Defense(-1L, room, pinned, student, timeslot, defenseTypeLong);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreateDefenseWithNullId() {
        try {
            new Defense(null, room, pinned, student, timeslot, defenseTypeLong);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseWithNullRoom() {
        try {
            new Defense(id, null, pinned, student, timeslot, defenseTypeLong);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseWithNullStudent() {
        try {
            new Defense(id, room, pinned, null, timeslot, defenseTypeLong);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseWithNullTimeslot() {
        try {
            new Defense(id, room, pinned, student, null, defenseTypeLong);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateDefenseWithNullDefenseType() {
        try {
            new Defense(id, room, pinned, student, timeslot, null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testGetters() {
        Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
        Assert.assertEquals(id, d.getId());
        Assert.assertEquals(room, d.getRoom());
        Assert.assertEquals(pinned, d.isPinned());
        Assert.assertEquals(student, d.getStudent());
        Assert.assertEquals(timeslot, d.getTimeslot());
        Assert.assertEquals(defenseTypeLong, d.getDefenseType());
    }

    @Test
    public void testSetIdNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.setId(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetRoomNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.setRoom(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetStudentNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.setStudent(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDefenseTypeNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.setDefenseType(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetSupervisorPersonsNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.setSupervisorPersons(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testOverlapsNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.overlaps(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testOverlapsTimeslotBeginsBeforeStartTimeslotTrue() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 00),
                LocalDateTime.of(2019, 07, 01, 12, 00));
        Defense d1 = new Defense(id, room, pinned, student, timeslot, defenseTypeShort);
        Defense d2 = new Defense(id, room, pinned, student, timeslot2, defenseTypeShort);
        Assert.assertTrue(d1.overlaps(d2));
    }

    @Test
    public void testOverlapsTimeslotBeginsAfterStartTimeslotTrue() {
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 00),
                LocalDateTime.of(2019, 07, 01, 12, 00));
        Defense d1 = new Defense(id, room, pinned, student, timeslot, defenseTypeShort);
        Defense d2 = new Defense(id, room, pinned, student, timeslot2, defenseTypeShort);
        Assert.assertTrue(d2.overlaps(d1));
    }

    @Test
    public void testOverlapsTimeslotBeginsAfterStartTimeslotFinishBeforeEndTimeslotTrue() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 15),
                LocalDateTime.of(2019, 07, 01, 11, 45));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 00),
                LocalDateTime.of(2019, 07, 01, 12, 00));
        Defense d1 = new Defense(id, room, pinned, student, timeslot, defenseTypeShort);
        Defense d2 = new Defense(id, room, pinned, student, timeslot2, defenseTypeShort);
        Assert.assertTrue(d1.overlaps(d2));
    }

    @Test
    public void testOverlapsDefenseFalse() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 9, 00),
                LocalDateTime.of(2019, 07, 01, 10, 00));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Defense d1 = new Defense(id, room, pinned, student, timeslot, defenseTypeShort);
        Defense d2 = new Defense(id, room, pinned, student, timeslot2, defenseTypeShort);
        Assert.assertFalse(d1.overlaps(d2));
        Assert.assertFalse(d2.overlaps(d1));
        Assert.assertFalse(d2.overlaps(d2));
    }

    @Test
    public void testCompareToNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.compareTo(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testCompareTo(){
        Defense d1 = new Defense(id, room, pinned, student, timeslot, defenseTypeShort);
        Defense d2 = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
        Assert.assertTrue(d1.compareTo(d2) > 0);
        Assert.assertTrue(d2.compareTo(d1) < 0);
        Assert.assertTrue(d1.compareTo(d1) == 0);
    }

    @Test
    public void testHasSameRoomNull() {
        try {
            Defense d = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
            d.hasSameRoom(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testHasSameRoom() {
        Room room1 = new Room(1L, "A");
        Room room2 = new Room(1L, "B");
        Defense d1 = new Defense(id, room1, pinned, student, timeslot, defenseTypeLong);
        Defense d2 = new Defense(id, room2, pinned, student, timeslot, defenseTypeLong);
        Assert.assertFalse(d1.hasSameRoom(d2));
        d2.setRoom(room1);
        Assert.assertTrue(d1.hasSameRoom(d2));
    }

    @Test
    public void testOverlappingDefensesNull() {
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 9, 00),
                LocalDateTime.of(2019, 07, 01, 10, 00));
        Timeslot timeslot2 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        Timeslot timeslot3 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 15),
                LocalDateTime.of(2019, 07, 01, 11, 15));
        Timeslot timeslot4 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 11, 15),
                LocalDateTime.of(2019, 07, 01, 12, 15));
        Timeslot timeslot5 = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 45),
                LocalDateTime.of(2019, 07, 01, 11, 15));
        Defense d1 = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
        Defense d2 = new Defense(id, room, pinned, student, timeslot2, defenseTypeLong);
        Defense d3 = new Defense(id, room, pinned, student, timeslot3, defenseTypeLong);
        Defense d4 = new Defense(id, room, pinned, student, timeslot4, defenseTypeLong);
        Defense d5 = new Defense(id, room, pinned, student, timeslot5, defenseTypeLong);
        Assert.assertEquals(Set.of(d3, d4, d5), d2.overlappingDefenses(List.of(d1, d3, d4, d5)));
    }

    @Test
    public void testGetNbAttendeesSameDefense(){
        try{
            new Defense(id, room, pinned, student, timeslot, defenseTypeLong).getNbAttendeesSameDefense(null);
            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testSetDefenseAssignNull(){
        try{
            new Defense(id, room, pinned, student, timeslot, defenseTypeLong).setDefenseAssigns(null);
            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testNotSameAttendeeOnDefense(){
        Person supervisorTI = new Person(2, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person supervisorTE = new Person(3, "arthure.martin@gmail.com", "10 rue du repos", "martin", "arthure");
        Person guest = new Person(4, "camille.duval@gmail.com", "10 rue du repos", "duval", "camille");
        Defense defense = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
        defense.setSupervisorPersons(Lists.newArrayList(supervisorTE, supervisorTI));
        DefenseAssign assign = new DefenseAssign(1L, false, guest, defense);
        defense.setDefenseAssigns(Set.of(assign));
        Assert.assertEquals(defense.sameAttendee(),0);

    }

    @Test
    public void testSameAttendeeOnDefense(){
        Person supervisorTI = new Person(2, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person supervisorTE = new Person(3, "arthure.martin@gmail.com", "10 rue du repos", "martin", "arthure");

        Defense defense = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
        defense.setSupervisorPersons(Lists.newArrayList(supervisorTE, supervisorTI));
        DefenseAssign assign = new DefenseAssign(1L, false, supervisorTE, defense);
        defense.setDefenseAssigns(Set.of(assign));
        Assert.assertEquals(defense.sameAttendee(),1);

    }

    @Test
    public void testSameAttendeeTwoGuestSameOnDefense(){
        Person supervisorTI = new Person(2, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person supervisorTE = new Person(3, "arthure.martin@gmail.com", "10 rue du repos", "martin", "arthure");
        Person guest = new Person(4, "camille.duval@gmail.com", "10 rue du repos", "duval", "camille");
        Defense defense = new Defense(id, room, pinned, student, timeslot, defenseTypeLong);
        defense.setSupervisorPersons(Lists.newArrayList(supervisorTE, supervisorTI));
        DefenseAssign assign = new DefenseAssign(1L, true, guest, defense);
        DefenseAssign assign1 = new DefenseAssign(1L, true, guest, defense);
        defense.setDefenseAssigns(Set.of(assign, assign1));
        Assert.assertEquals(defense.sameAttendee(),1);
    }
}
