package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;

public class DefenseAssignTest {

    private static Long id;
    private static boolean pinned;
    private static Defense defense;
    private static Person person;

    private static DefenseType defenseTypeLong;

    @BeforeClass
    public static void initParams(){
        id = 1L;
        Room room = new Room(1L, "A");
        pinned = false;
        boolean pinned = false;
        person = new Person(2, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person student = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Timeslot timeslot = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 02, 10, 30),
                LocalDateTime.of(2019, 07, 01, 11, 30));
        DefenseType defenseTypeShort = new DefenseType("RSP", "Rapport de Situation Professionnelle", Duration.ofSeconds(3600), new HashSet<>());
        defenseTypeLong = new DefenseType("MI", "Mémoire d'Ingénieur", Duration.ofSeconds(2*3600), new HashSet<>());
        defense = new Defense(1L, room, pinned, student, timeslot, defenseTypeShort);
    }

    @Test
    public void testToCreateDefenseAssignWithNegId() {
        try {
            new DefenseAssign(-1L, pinned, person, defense);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreateDefenseAssignWithNullId() {
        try {
            new DefenseAssign(null, pinned, person, defense);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testGetters() {
        DefenseAssign da = new DefenseAssign(id, pinned, person, defense);
        Assert.assertEquals(id, da.getId());
        Assert.assertEquals(pinned, da.isPinned());
        Assert.assertEquals(person, da.getOtherAttendee());
        Assert.assertEquals(defense, da.getDefense());
    }

    @Test
    public void testSetIdNull() {
        try {
            DefenseAssign da = new DefenseAssign(id, pinned, person, defense);
            da.setId(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDefenseNull() {
        try {
            DefenseAssign da = new DefenseAssign(id, pinned, person, defense);
            da.setDefense(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetOtherAttendeeNull() {
        try {
            DefenseAssign da = new DefenseAssign(id, pinned, person, defense);
            da.setOtherAttendee(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testCompareToNull() {
        try {
            DefenseAssign da = new DefenseAssign(id, pinned, person, defense);
            da.compareTo(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testCompareTo(){
        DefenseAssign da1 = new DefenseAssign(id, pinned, person, defense);
        Defense defense2 = new Defense(defense.getId(), defense.getRoom(), defense.isPinned(), defense.getStudent(), defense.getTimeslot(), defenseTypeLong);
        DefenseAssign da2 = new DefenseAssign(id, pinned, person, defense2);
        Assert.assertTrue(da1.compareTo(da2) > 0);
        Assert.assertTrue(da2.compareTo(da1) < 0);
        Assert.assertTrue(da1.compareTo(da1) == 0);
    }
}
