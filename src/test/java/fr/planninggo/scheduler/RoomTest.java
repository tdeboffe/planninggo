package fr.planninggo.scheduler;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class RoomTest {
    @Test
    public void testToCreateRoomWithNullId() {
        try {
            new Room(null, "A");
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateRoomWithNegId() {
        try {
            new Room(-1L, "A");
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreateRoomWithNullName() {
        try {
            new Room(1L, null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        Room room = new Room(1L, "A");
        Assert.assertEquals(false, room.equals(null));
    }

    @Test
    public void testNotEqualsDiffRoomInstances() {
        Room room1 = new Room(1L, "A");
        Room room2 = new Room(1L, "B");
        Room room3 = new Room(1L, "A");
        Room room4 = new Room(2L, "A");
        Assert.assertNotEquals(room1, room2);
        Assert.assertNotEquals(room3, room4);
    }

    @Test
    public void testEqualsDiffRoomInstances() {
        Room room1 = new Room(1L, "A");
        Room room2 = new Room(1L, "A");
        Assert.assertEquals(room1, room2);
    }

    @Test
    public void testNotEqualsHashcodeDiffRoomInstances() {
        Room room1 = new Room(1L, "A");
        Room room2 = new Room(1L, "B");
        Room room3 = new Room(1L, "A");
        Room room4 = new Room(2L, "A");
        Assert.assertNotEquals(room1.hashCode(), room2.hashCode());
        Assert.assertNotEquals(room3.hashCode(), room4.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffRoomInstances() {
        Room room1 = new Room(1L, "A");
        Room room2 = new Room(1L, "A");
        Assert.assertEquals(room1.hashCode(), room2.hashCode());
    }

    @Test
    public void testGetters() {
        Set<LocalDate> availableDays = new HashSet<>();
        availableDays.add(LocalDate.of(2019, 07, 01));
        Long id = 1L;
        String name = "A";
        Room room1 = new Room(1L, "A");
        room1.addAvailableDay(LocalDate.of(2019, 07, 01));
        Assert.assertTrue(room1.getId().equals(id));
        Assert.assertTrue(room1.getName().equals(name));
        room1.getAvailableDays().forEach(av -> Assert.assertTrue(availableDays.contains(av)));
    }

    @Test
    public void testSetIdNull() {
        try {
            Room room = new Room(1L, "A");
            room.setId(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetIdNeg() {
        try {
            Room room = new Room(1L, "A");
            room.setId(-1L);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testSetRoomNull() {
        try {
            Room room = new Room(1L, "A");
            room.setAvailableDays(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetAndGetRoom() {
        Set<LocalDate> availableDays = new HashSet<>();
        availableDays.add(LocalDate.of(2019, 07, 01));
        Set<LocalDate> availableDays2 = new HashSet<>();
        availableDays.add(LocalDate.of(2019, 07, 02));
        Room room = new Room(1L, "A");
        Set<LocalDate> availableDays3 = new HashSet<>();
        availableDays3.addAll(availableDays);
        availableDays3.addAll(availableDays2);
        room.setAvailableDays(availableDays2);
        room.getAvailableDays().forEach(av -> Assert.assertTrue(availableDays3.contains(av)));
    }

    @Test
    public void testIsRoomWithNullDefense() {
        Room room = new Room(1L, "A");
        Assert.assertFalse(room.isAvailable(null));
    }

    @Test
    public void testAddAvailAndIsNotAvail() {
        Room room = new Room(1L, "A");
        room.addAvailableDay(LocalDate.of(2019, 07, 01));
        Defense d = new Defense();
        d.setTimeslot(new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 02, 10, 30)
                , LocalDateTime.of(2019, 07, 02, 11, 30)));
        Assert.assertFalse(room.isAvailable(d));
    }

    @Test
    public void testAddAvailAndIsAvail() {
        Room room = new Room(1L, "A");
        room.addAvailableDay(LocalDate.of(2019, 07, 01));
        Assert.assertFalse(room.isAvailable(null));
        Defense d = new Defense();
        d.setTimeslot(new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30)
                , LocalDateTime.of(2019, 07, 01, 11, 30)));
        Assert.assertTrue(room.isAvailable(d));
    }
}
