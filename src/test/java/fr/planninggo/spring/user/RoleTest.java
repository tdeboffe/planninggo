package fr.planninggo.spring.user;

import fr.planninggo.scheduler.Role;
import org.junit.Assert;
import org.junit.Test;

public class RoleTest {
    @Test
    public void testToCreateRoleWithNullName() {
        try {
            new Role(null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        Role role = new Role("Encadrant");
        Assert.assertEquals(false, role.equals(null));
    }

    @Test
    public void testNotEqualsDiffRoleInstances() {
        Role role1 = new Role("Encadrant");
        Role role2 = new Role("Etudiant");
        Assert.assertNotEquals(role1, role2);
    }

    @Test
    public void testEqualsDiffAvailInstances() {
        Role role1 = new Role("Encadrant");
        Role role2 = new Role("Encadrant");
        Assert.assertEquals(role1, role2);
    }

    @Test
    public void testNotEqualsHashcodeDiffAvailInstances() {
        Role role1 = new Role("Encadrant");
        Role role2 = new Role("Etudiant");
        Assert.assertNotEquals(role1.hashCode(), role2.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffAvailInstances() {
        Role role1 = new Role("Encadrant");
        Role role2 = new Role("Encadrant");
        Assert.assertEquals(role1.hashCode(), role2.hashCode());
    }

    @Test
    public void testGetName() {
        String roleName = "Encadrant";
        Role role1 = new Role(roleName);
        Assert.assertTrue(roleName.equals(role1.getName()));
    }
}
