package fr.planninggo.spring.user;

import fr.planninggo.scheduler.AttendeeRelation;
import fr.planninggo.scheduler.DefenseType;
import fr.planninggo.scheduler.OtherAttendee;
import fr.planninggo.scheduler.Person;
import fr.planninggo.scheduler.Role;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class OtherAttendeeTest {
    @Test
    public void testGetRelations() {
        OtherAttendee o = new OtherAttendee();
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Etudiant");
        Assert.assertTrue(o.getRelations().isEmpty());
    }

    @Test
    public void testGetRelatedPersons() {
        OtherAttendee o = new OtherAttendee();
        List<Person> persRelations = o.getRelatedPersons();
        Assert.assertTrue(persRelations.isEmpty());
    }

    @Test
    public void testAddAndGetPersonsByRole() {
        OtherAttendee o = new OtherAttendee();
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Etudiant");
        List<Person> pers = o.getRelatedPersonsByRole(role);
        Assert.assertTrue(pers.isEmpty());
    }

    @Test
    public void testIsAssignedDefenseTypeFalse() {
        OtherAttendee o = new OtherAttendee();
        Assert.assertFalse(o.isAssignedToDefenseType());
    }

    @Test
    public void testIsAssignedDefenseTypeFalse2() {
        OtherAttendee o = new OtherAttendee();
        o.setDefenseType(DefenseType.create("RMT", "Rapport de Situation Professionelle", "1h00"));
        Assert.assertFalse(o.isAssignedToDefenseType());
    }
}
