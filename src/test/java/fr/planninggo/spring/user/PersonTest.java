package fr.planninggo.spring.user;

import fr.planninggo.scheduler.AttendeeRelation;
import fr.planninggo.scheduler.Availability;
import fr.planninggo.scheduler.Defense;
import fr.planninggo.scheduler.DefenseType;
import fr.planninggo.scheduler.Person;
import fr.planninggo.scheduler.Role;
import fr.planninggo.scheduler.Student;
import fr.planninggo.scheduler.Timeslot;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PersonTest {
    @Test
    public void testToCreatePersonWithNegId() {
        try {
            new Person(-1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreatePersonWithNullEmail() {
        try {
            new Person(1, null, "10 rue du repos", "duval", "roger");
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreatePersonWithNullLastname() {
        try {
            new Person(1, "roger.duval@gmail.com", "10 rue du repos", null, "roger");
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreatePersonWithNullFirstname() {
        try {
            new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        Person p = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Assert.assertEquals(false, p.equals(null));
    }

    @Test
    public void testNotEqualsDiffPersonInstances() {
        List<Person> persons = new ArrayList();
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person p2 = new Person(1, "roger.lagrotte@gmail.com", "10 rue du repos", "duval", "roger");
        Person p4 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "dupont", "roger");
        Person p5 = new Person(5, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        persons.add(p1);
        persons.add(p2);
        persons.add(p4);
        persons.add(p5);
        for (Person p : persons) {
            for (Person pcomp : persons) {
                if (p != pcomp) {
                    Assert.assertNotEquals(p, pcomp);
                }
            }
        }
    }

    @Test
    public void testEqualsDiffPersonInstances() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person p2 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Assert.assertEquals(p1, p2);
    }

    @Test
    public void testNotEqualsHashcodeDiffPersonInstances() {
        List<Person> persons = new ArrayList();
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person p2 = new Person(1, "roger.lagrotte@gmail.com", "10 rue du repos", "duval", "roger");
        Person p4 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "dupont", "roger");
        Person p5 = new Person(5, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        persons.add(p1);
        persons.add(p2);
        persons.add(p4);
        persons.add(p5);
        for (Person p : persons) {
            for (Person pcomp : persons) {
                if (p != pcomp) {
                    Assert.assertNotEquals(p.hashCode(), pcomp.hashCode());
                }
            }
        }
    }

    @Test
    public void testEqualsHashcodeDiffPersonInstances() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person p2 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Assert.assertEquals(p1.hashCode(), p2.hashCode());
    }

    @Test
    public void testAddNullAvail() {
        try {
            Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
            p1.addAvailability(null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testRemoveNullAvail() {
        try {
            Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
            p1.removeAvailability(null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testIsAvailWithNullDefense() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Assert.assertFalse(p1.isAvailable(null));
    }

    @Test
    public void testAddAvailAndIsAvail() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        LocalDate date = LocalDate.of(2019, 07, 01);
        Availability avail = new Availability(1, date, true, true);
        p1.addAvailability(avail);
        Defense d = new Defense();
        d.setTimeslot(new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30)
                , LocalDateTime.of(2019, 07, 01, 11, 30)));
        Assert.assertTrue(p1.isAvailable(d));
        p1.removeAvailability(date);
        Assert.assertFalse(p1.isAvailable(d));
    }

    @Test
    public void testSetDefenseTypeToNull() {
        try {
            Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
            p1.setDefenseType(null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testIsAssignedDefenseTypeFalseWithoutAttendee() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Assert.assertFalse(p1.isAssignedToDefenseType());
    }

    @Test
    public void testIsAssignedDefenseTypeFalseWithAttendee() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        p1.setAttendee(new Student());
        Assert.assertFalse(p1.isAssignedToDefenseType());
    }

    @Test
    public void testIsAssignedDefenseType() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        p1.setAttendee(new Student());
        p1.setDefenseType(DefenseType.create("RMT", "Rapport de Situation Professionelle", "1h00"));
        Assert.assertTrue(p1.isAssignedToDefenseType());
    }

    @Test
    public void testAddAndGetRelations() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person p2 = new Person(2, "guillaume.dupont@gmail.com", "20 rue du carnaval", "dupont", "guillaume");
        Person p3 = new Person(3, "theo.nacache@gmail.com", "7 rue du commit", "nacache", "theo");
        Student s = new Student();
        p1.setAttendee(s);
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Encadrant");
        relations.add(new AttendeeRelation(p2, role));
        relations.add(new AttendeeRelation(p3, role));
        s.setSupervisors(relations);
        p1.getRelations().forEach(rel -> Assert.assertTrue(relations.contains(rel)));
        List<Person> persRelations = relations.stream().map(r -> r.getPerson()).collect(Collectors.toList());
        relations.stream().map(rel -> rel.getPerson()).forEach(p -> Assert.assertTrue(persRelations.contains(p)));
    }

    @Test
    public void testAddAndGetPersonsByRole() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Person p2 = new Person(2, "guillaume.dupont@gmail.com", "20 rue du carnaval", "dupont", "guillaume");
        Person p3 = new Person(3, "theo.nacache@gmail.com", "7 rue du commit", "nacache", "theo");
        Student s = new Student();
        p1.setAttendee(s);
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Encadrant");
        Role role2 = new Role("AutreRole");
        relations.add(new AttendeeRelation(p2, role));
        relations.add(new AttendeeRelation(p3, role2));
        s.setSupervisors(relations);
        List<Person> pers = p1.getRelatedPersonsByRole(role);
        Assert.assertTrue(pers.size() == 1);
        Assert.assertTrue(pers.get(0).equals(p2));
    }

}
