package fr.planninggo.spring.user;

import fr.planninggo.scheduler.AttendeeRelation;
import fr.planninggo.scheduler.DefenseType;
import fr.planninggo.scheduler.Person;
import fr.planninggo.scheduler.Role;
import fr.planninggo.scheduler.Supervisor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SupervisorTest {
    @Test
    public void testGetRelations() {
        Person p1 = new Person(1, "guillaume.dupont@gmail.com", "20 rue du carnaval", "dupont", "guillaume");
        Person p2 = new Person(2, "theo.nacache@gmail.com", "7 rue du commit", "nacache", "theo");
        Supervisor s = new Supervisor();
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Etudiant");
        relations.add(new AttendeeRelation(p1, role));
        relations.add(new AttendeeRelation(p2, role));
        s.setStudents(relations);
        s.getRelations().forEach(rel -> Assert.assertTrue(relations.contains(rel)));
        List<Person> persRelations = relations.stream().map(r -> r.getPerson()).collect(Collectors.toList());
        relations.stream().map(rel -> rel.getPerson()).forEach(p -> Assert.assertTrue(persRelations.contains(p)));
    }

    @Test
    public void testGetRelatedPersons() {
        Person p1 = new Person(1, "guillaume.dupont@gmail.com", "20 rue du carnaval", "dupont", "guillaume");
        Person p2 = new Person(2, "theo.nacache@gmail.com", "7 rue du commit", "nacache", "theo");
        Supervisor s = new Supervisor();
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Etudiant");
        relations.add(new AttendeeRelation(p1, role));
        relations.add(new AttendeeRelation(p2, role));
        s.setStudents(relations);
        List<Person> persRelations = s.getRelatedPersons();
        relations.stream().map(rel -> rel.getPerson()).forEach(p -> Assert.assertTrue(persRelations.contains(p)));
    }

    @Test
    public void testAddAndGetPersonsByRole() {
        Person p1 = new Person(1, "guillaume.dupont@gmail.com", "20 rue du carnaval", "dupont", "guillaume");
        Person p2 = new Person(2, "theo.nacache@gmail.com", "7 rue du commit", "nacache", "theo");
        Supervisor s = new Supervisor();
        List<AttendeeRelation> relations = new ArrayList<>();
        Role role = new Role("Etudiant");
        Role role2 = new Role("AutreRole");
        relations.add(new AttendeeRelation(p1, role));
        relations.add(new AttendeeRelation(p2, role2));
        s.setStudents(relations);
        List<Person> pers = s.getRelatedPersonsByRole(role);
        Assert.assertTrue(pers.size() == 1);
        Assert.assertTrue(pers.get(0).equals(p1));
    }

    @Test
    public void testIsAssignedDefenseTypeFalse() {
        Supervisor s = new Supervisor();
        Assert.assertFalse(s.isAssignedToDefenseType());
    }

    @Test
    public void testIsAssignedDefenseTypeFalse2() {
        Supervisor s = new Supervisor();
        s.setDefenseType(DefenseType.create("RMT", "Rapport de Situation Professionelle", "1h00"));
        Assert.assertFalse(s.isAssignedToDefenseType());
    }
}
