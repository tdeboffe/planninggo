package fr.planninggo.spring.user;

import fr.planninggo.scheduler.AttendeeRelation;
import fr.planninggo.scheduler.Person;
import fr.planninggo.scheduler.Role;
import org.junit.Assert;
import org.junit.Test;

public class AttendeeRelationTest {
    @Test
    public void testToCreateAttendeeRelationWithNullPerson() {
        try {
            new AttendeeRelation(null, new Role(""));
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testToCreateAttendeeRelationWithNullRole() {
        try {
            new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        AttendeeRelation attRel = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role(""));
        Assert.assertEquals(false, attRel.equals(null));
    }

    @Test
    public void testNotEqualsDiffAttRelInstances() {
        AttendeeRelation attRel1 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel2 = new AttendeeRelation(new Person(2, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel3 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel4 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Etudiant"));
        Assert.assertNotEquals(attRel1, attRel2);
        Assert.assertNotEquals(attRel3, attRel4);
    }

    @Test
    public void testEqualsDiffAttRelInstances() {
        AttendeeRelation attRel1 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel2 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        Assert.assertEquals(attRel1, attRel2);
    }

    @Test
    public void testNotEqualsHashcodeDiffAvailInstances() {
        AttendeeRelation attRel1 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel2 = new AttendeeRelation(new Person(2, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel3 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel4 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Etudiant"));
        Assert.assertNotEquals(attRel1.hashCode(), attRel2.hashCode());
        Assert.assertNotEquals(attRel3.hashCode(), attRel4.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffAvailInstances() {
        AttendeeRelation attRel1 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        AttendeeRelation attRel2 = new AttendeeRelation(new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger"), new Role("Encadrant"));
        Assert.assertEquals(attRel1.hashCode(), attRel2.hashCode());
    }

    @Test
    public void testGetters() {
        Person p1 = new Person(1, "roger.duval@gmail.com", "10 rue du repos", "duval", "roger");
        Role role = new Role("Encadrant");
        AttendeeRelation attRel1 = new AttendeeRelation(p1, role);
        Assert.assertTrue(attRel1.getPerson().equals(p1));
        Assert.assertTrue(attRel1.getRole().equals(role));
    }
}
