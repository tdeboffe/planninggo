package fr.planninggo.spring.user;


import fr.planninggo.scheduler.Availability;
import fr.planninggo.scheduler.Timeslot;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class AvailabilityTest {
    @Test
    public void testToCreateAvailWithNegId() {
        try {
            new Availability(-1, LocalDate.now(), true, true);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {

        }
    }

    @Test
    public void testToCreateAvailWithNullDate() {
        try {
            new Availability(1, null, true, true);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        Availability avail = new Availability(1, LocalDate.now(), true, true);
        Assert.assertEquals(false, avail.equals(null));
    }

    @Test
    public void testNotEqualsDiffAvailInstances() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), true, true);
        Availability avail2 = new Availability(2, LocalDate.of(2019, 07, 01), true, true);
        Availability avail3 = new Availability(1, LocalDate.of(2019, 07, 02), false, true);
        Availability avail4 = new Availability(2, LocalDate.of(2019, 07, 02), true, false);
        Assert.assertNotEquals(avail1, avail2);
        Assert.assertNotEquals(avail3, avail4);
    }

    @Test
    public void testEqualsDiffAvailInstances() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), true, true);
        Availability avail2 = new Availability(2, LocalDate.of(2019, 07, 02), true, true);
        Assert.assertEquals(avail1, avail2);
    }

    @Test
    public void testNotEqualsHashcodeDiffAvailInstances() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), true, true);
        Availability avail2 = new Availability(2, LocalDate.of(2019, 07, 01), true, true);
        Availability avail3 = new Availability(1, LocalDate.of(2019, 07, 02), false, true);
        Availability avail4 = new Availability(2, LocalDate.of(2019, 07, 02), false, false);
        Assert.assertNotEquals(avail1.hashCode(), avail2.hashCode());
        Assert.assertNotEquals(avail3.hashCode(), avail4.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffAvailInstances() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), true, true);
        Availability avail2 = new Availability(2, LocalDate.of(2019, 07, 02), true, true);
        Assert.assertEquals(avail1.hashCode(), avail2.hashCode());
    }

    @Test
    public void testSetNullDate() {
        try {
            Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), true, true);
            avail1.setDate(null);
            Assert.fail();
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testHasAvail() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), true, true);
        Availability avail2 = new Availability(2, LocalDate.of(2019, 07, 01), true, false);
        Availability avail3 = new Availability(3, LocalDate.of(2019, 07, 03), false, true);
        Assert.assertTrue(avail1.hasAvailabilities());
        Assert.assertTrue(avail2.hasAvailabilities());
        Assert.assertTrue(avail3.hasAvailabilities());
    }

    @Test
    public void testHasNotAvail() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 02), false, false);
        Assert.assertFalse(avail1.hasAvailabilities());
    }

    @Test
    public void testAvailNotMatchesTimeslot() {
        Timeslot t = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30)
                , LocalDateTime.of(2019, 07, 01, 11, 30));
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 01), false, false);
        Availability avail2 = new Availability(2, LocalDate.of(2019, 07, 01), false, true);
        Availability avail3 = new Availability(3, LocalDate.of(2019, 07, 03), true, true);
        Assert.assertFalse(avail1.availMatches(t));
        Assert.assertFalse(avail2.availMatches(t));
        Assert.assertFalse(avail3.availMatches(t));
    }

    @Test
    public void testAvailMatchesTimeslot() {
        Timeslot t = new Timeslot(1L, 1, LocalDateTime.of(2019, 07, 01, 10, 30)
                , LocalDateTime.of(2019, 07, 01, 11, 30));
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 01), true, false);
        Assert.assertTrue(avail1.availMatches(t));
    }

    @Test
    public void testAvailMatchesWithNullTimeslot() {
        Availability avail1 = new Availability(1, LocalDate.of(2019, 07, 01), true, false);
        Assert.assertFalse(avail1.availMatches(null));
    }
}
