package fr.planninggo.spring.campaign;


import fr.planninggo.csv.CsvParser;
import fr.planninggo.spring.attendee.repository.*;
import fr.planninggo.spring.attendee.service.AttendeeGuestService;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.attendee.service.AvailabilityService;
import fr.planninggo.spring.attendee.service.StudentService;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.repository.*;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.spring.campaign.service.DefenseTypeService;
import fr.planninggo.spring.campaign.service.JuryMemberService;
import fr.planninggo.spring.campaign.service.RoomService;
import fr.planninggo.spring.user.repository.AccountRepository;
import fr.planninggo.spring.user.repository.RoleRepository;
import fr.planninggo.spring.user.service.AccountService;
import fr.planninggo.utility.Password;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Date;
import java.time.Duration;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = {CsvParserTest.Initializer.class })
@Transactional
@Rollback(false)
public class CsvParserTest {




    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer =
            (PostgreSQLContainer) new PostgreSQLContainer("ggorret/planninggo_db:importp")
                    .withDatabaseName("planninggo_db")
                    .withUsername("postgres")
                    .withPassword("postgres")
                    .withStartupTimeout(Duration.ofSeconds(600));

    private File file0 = new File("src/test/resources/csv/participant.csv");
    private File attendeeForSchedulerImport = new File("src/test/resources/csv/attendeeForSchedulerImport.csv");

    private File file1 = new File("src/test/resources/csv/disponibilite/CSV_import_civilite.csv");
    private File file2 = new File("src/test/resources/csv/disponibilite/CSV_import_nom.csv");
    private File file3 = new File("src/test/resources/csv/disponibilite/CSV_import_prenom.csv");
    private File file4 = new File("src/test/resources/csv/disponibilite/CSV_import_mail.csv");
    private File file5 = new File("src/test/resources/csv/disponibilite/CSV_import_date.csv");
    private File file6 = new File("src/test/resources/csv/disponibilite/CSV_import_ouinon.csv");
    private File file7 = new File("src/test/resources/csv/disponibilite/CSV_import_mailFail.csv");
    private File file8 = new File("src/test/resources/csv/disponibilite/CSV_import.csv");
    private File file9 = new File("src/test/resources/csv/disponibilite/CSV_importAvailabilityAttendee.csv");
    private File correctFileDefenseType = new File("src/test/resources/csv/defense/confSoutenance.csv");
    private File badFileDefenseType1 = new File("src/test/resources/csv/defense/confSoutenanceBadHead1.csv");
    private File badFileDefenseType2 = new File("src/test/resources/csv/defense/confSoutenanceBadHead2.csv");
    private File badFileBadYesNo = new File("src/test/resources/csv/defense/confSoutenanceBadYesNo.csv");
    private File badFileBadFiliere = new File("src/test/resources/csv/defense/confSoutenanceBadFiliere.csv");
    private File importCampaignOK = new File("src/test/resources/csv/campaign/importCampaignOK.csv");
    private File importCampaigneBadOrganizer = new File("src/test/resources/csv/campaign/importCampaignBadOrganizer.csv");
    private File importPlanning = new File("src/test/resources/csv/planning.csv");

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private TestEntityManager entityManager;


    @Autowired
    private PcTownRepository pcTownRepository;
    @Autowired
    private CivilityRepository civilityRepository;
    @Autowired
    private StudyRepository studyRepository;
    @Autowired
    private CampaignRepository campaignRepository;
    @Autowired
    private AvailabilityRepository availabilityRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private DefenseRepository defenseRepository;
    @Autowired
    private CampaignStateRepository campaignStateRepository;
    @Autowired
    private ConstraintRepository contConstraintRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private JuryMemberService juryMemberService;



    @Autowired
    private AttendeeService attendeeService;

    @Autowired
    private AvailabilityService availabilityService;

    @Autowired
    private DefenseTypeService defenseTypeService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private AttendeeGuestService attendeeGuestService;

    @Autowired
    private CampaignService campaignService;



    @TestConfiguration
    static class AttendeeServiceImplTestContextConfiguration {
        @Autowired
        private AttendeeRepository attendeeRepository;
        @Autowired
        private  SupervisorRepository supervisorRepository;
        @Autowired
        private  AttendeeGuestRepository attendeeGuestRepository;
        @Autowired
        private  StudentRepository studentRepository;
        @Autowired
        private AvailabilityRepository availabilityRepository;
        @Autowired
        private SupervisorStudentRepository supervisorStudentRepository;
        @Autowired
        private AccountRepository accountRepository;
        @Autowired
        private CampaignRepository campaignRepository;
        @Autowired
        private DefenseTypeRepository defenseTypeRepository;
        @Autowired
        private RoomRepository roomRepository;
        @Autowired
        private PhaseRepository phaseRepository;
        @Autowired
        private DefenseRepository defenseRepository;
        @Autowired
        private CampaignStateRepository campaignStateRepository;
        @Autowired
        private RoleRepository roleRepository;
        @Autowired
        private JuryMemberRepository juryMemberRepository;

        @Bean
        public AttendeeService attendeeService() {
            return new AttendeeService(supervisorStudentRepository, attendeeRepository, supervisorRepository, attendeeGuestRepository, studentRepository, campaignRepository, accountRepository);
        }

        @Bean
        public AvailabilityService availabilityService(){
            return new AvailabilityService(availabilityRepository);
        }

        @Bean
        public DefenseTypeService defenseTypeService(){
            return new DefenseTypeService(defenseTypeRepository);
        }

        @Bean
        public RoomService roomService(){
            return new RoomService(roomRepository);
        }

        @Bean
        public StudentService studentService(){
            return new StudentService(studentRepository);
        }

        @Bean
        public AttendeeGuestService attendeeGuestService(){
            return new AttendeeGuestService(attendeeGuestRepository);
        }

        @Bean
        public CampaignService campaignService(){
            return new CampaignService(phaseRepository,campaignRepository,defenseRepository, campaignStateRepository);
        }

        @Bean
        public AccountService accountService(){
            return new AccountService(roleRepository,accountRepository);
        }

        @Bean
        public JuryMemberService juryMemberService(){
            return new JuryMemberService(juryMemberRepository, roleRepository);
        }
    }

    @Test()
    //@Sql(statements = "TRUNCATE campaign CASCADE")
    public void importAttendees() throws IOException {
        FileInputStream content = new FileInputStream(file0);
        CsvParser.saveAttendee(new MockMultipartFile("file0",content), 1, pcTownRepository, civilityRepository, campaignRepository, attendeeService, studyRepository, roleRepository,accountRepository);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    @Sql(statements = "update campaign SET room_number = 1")
    public void testCampaignNotExist() throws IOException{
        FileInputStream content = new FileInputStream(file1);
        CsvParser.saveAttendee(new MockMultipartFile("file1",content), -10, pcTownRepository, civilityRepository, campaignRepository, attendeeService, studyRepository, roleRepository, accountRepository);
        content.close();
    }

    @Test
    public void testPassword() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String pass = Password.generatePassword();
        for (int i = 0; i < 10; i++) {
            assertEquals("$2a$10", bCryptPasswordEncoder.encode(pass).substring(0, 6));
            pass = Password.generatePassword();
        }
    }


    @Test(expected = NullPointerException.class)
    public void noFile() throws Exception{
        CsvParser.saveAvailabilityCSV(null, null, null);
    }

    @Test
    public void importFile() throws IOException {
        FileInputStream content = new FileInputStream(file8);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file8", content),availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void civiliteFail() throws IOException {
        FileInputStream content = new FileInputStream(file1);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file1",content),availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void nomFail() throws IOException  {
        FileInputStream content = new FileInputStream(file2);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file2",content), availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void prenomFail() throws IOException {
        FileInputStream content = new FileInputStream(file3);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file3",content), availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void mailFail() throws IOException  {
        FileInputStream content = new FileInputStream(file4);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file4",content), availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void dateFail() throws IOException  {
        FileInputStream content = new FileInputStream(file5);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file5",content), availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void oneOuiNonFail() throws IOException {
        FileInputStream content = new FileInputStream(file6);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file6",content), availabilityService, attendeeService);
        content.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void mailDataFail() throws IOException {
        FileInputStream content = new FileInputStream(file7);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file7",content), availabilityService, attendeeService);
        content.close();
    }


    @Test()
    public void importAvailability() throws IOException {
        FileInputStream availability = new FileInputStream(file9);
        CsvParser.saveAvailabilityCSV(new MockMultipartFile("file9", availability), availabilityService, attendeeService);
        assertNotNull(availabilityRepository.findAll());
        availability.close();
    }

    @Test(expected = NullPointerException.class)
    public void noFileDefenseType() throws Exception{
        CsvParser.getDefenseSetting( null, null);
    }

    @Test
    public void getDefenseTypeWhithCorrectFile() throws Exception{
        FileInputStream filedata = new FileInputStream(correctFileDefenseType);
        List<DefenseTypeEntity> defenseType  = CsvParser.getDefenseSetting( new MockMultipartFile("correctFileDefenseType", filedata), roleRepository);
        assertEquals(defenseType.size(), 2);
        filedata.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDefenseTypeWhithBadHead1() throws Exception{
        FileInputStream filedata = new FileInputStream(badFileDefenseType1);
        CsvParser.getDefenseSetting( new MockMultipartFile("badFileDefenseType1", filedata),  roleRepository);
        filedata.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDefenseTypeWhithBadHead2() throws Exception{
        FileInputStream filedata = new FileInputStream(badFileDefenseType2);
        CsvParser.getDefenseSetting( new MockMultipartFile("badFileDefenseType2", filedata), roleRepository);
        filedata.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDefenseTypeWhithBadYesNo() throws Exception{
        FileInputStream filedata = new FileInputStream(badFileBadYesNo);
        CsvParser.getDefenseSetting(  new MockMultipartFile("badFileBadYesNo", filedata),  roleRepository);
        filedata.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDefenseTypeWhithBadFiliere() throws Exception{
        FileInputStream filedata = new FileInputStream(badFileBadFiliere);
        CsvParser.getDefenseSetting( new MockMultipartFile("badFileBadFiliere", filedata),  roleRepository);
        filedata.close();
    }

    @Test
    public void parseCampaignOk() throws Exception {
        FileInputStream filedata = new FileInputStream(importCampaignOK);
        CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaignOK", filedata), accountRepository);
        filedata.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseCampaignBadOrganizer() throws Exception{
        FileInputStream filedata = new FileInputStream(importCampaigneBadOrganizer);
        CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaigneBadOrganizer", filedata), accountRepository);
        filedata.close();
    }

    @Test(expected = IllegalArgumentException.class)
    public void importPlanning() throws Exception{
        FileInputStream importCampaign = new FileInputStream(importCampaignOK);
        CampaignEntity campaignEntity = CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaignOK", importCampaign),accountRepository);
        System.out.println(campaignEntity);
        CampaignEntity c = campaignRepository.findById(1).get();
        c.setDefenseDays(campaignEntity.getDefenseDays());
        c.setCampaignBegin(Date.valueOf("2019-03-11"));
        c.setCampaignEnd(Date.valueOf("2019-03-15"));
        campaignRepository.save(c);
        importCampaign.close();
        System.out.println(campaignRepository.findById(1).get());
        FileInputStream attendee = new FileInputStream(attendeeForSchedulerImport);
        CsvParser.saveAttendee(new MockMultipartFile("attendeeForSchedulerImport", attendee), 1,pcTownRepository,civilityRepository, campaignRepository, attendeeService, studyRepository, roleRepository,accountRepository);
        attendee.close();

        FileInputStream filedata = new FileInputStream(importPlanning);
        CsvParser csvParser = new CsvParser();

        csvParser.saveDefense(new MockMultipartFile("importPlanning", filedata),1,defenseRepository,defenseTypeService, roomService, campaignRepository, studentService,attendeeGuestService);

        filedata.close();
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void parseCampaignBadDateCampaign() throws Exception{
//        FileInputStream filedata = new FileInputStream(importCampaigneBadCampaign);
//        CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaigneBadCampaign", filedata), accountRepository);
//        filedata.close();
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void parseCampaignBadDatePhase() throws Exception{
//        FileInputStream filedata = new FileInputStream(importCampaigneBadPhase);
//        CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaigneBadPhase", filedata), accountRepository);
//        filedata.close();
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void parseCampaignBadDateTime() throws Exception{
//        FileInputStream filedata = new FileInputStream(importCampaigneBadTime);
//        CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaigneBadTime", filedata), accountRepository);
//        filedata.close();
//    }
}
