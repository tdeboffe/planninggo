package fr.planninggo.spring.campaign;

import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.campaign.entity.CampaignConstraintEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.attendee.entity.CivilityEntity;
import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.attendee.entity.PcTownEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.StudyEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.attendee.repository.AttendeeGuestRepository;
import fr.planninggo.spring.campaign.repository.CampaignRepository;
import fr.planninggo.spring.campaign.repository.CampaignStateRepository;
import fr.planninggo.spring.campaign.repository.ConstraintRepository;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import fr.planninggo.spring.campaign.repository.DefenseTypeRepository;
import fr.planninggo.spring.campaign.repository.RoomRepository;
import fr.planninggo.spring.attendee.repository.StudentRepository;
import fr.planninggo.scheduler.ScheduleCampaign;
import fr.planninggo.scheduler.SolverManager;
import fr.planninggo.scheduler.SolverProdParam;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.repository.AccountRepository;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.test.impl.score.buildin.hardsoft.HardSoftScoreVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = {ScheduleTest.Initializer.class })
@Transactional
@Rollback(false)
public class ScheduleTest {
    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer =
            (PostgreSQLContainer) new PostgreSQLContainer("ggorret/planninggo_db:app")
                    .withDatabaseName("planninggo_db")
                    .withUsername("postgres")
                    .withPassword("postgres")
                    .withStartupTimeout(Duration.ofSeconds(3000));

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DefenseRepository defenseRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CampaignStateRepository campaignStateRepository;

    @Autowired
    private ConstraintRepository constraintRepository;

    @Autowired
    private DefenseTypeRepository defenseTypeRepository;

    @Autowired
    private AttendeeGuestRepository attendeeGuestRepository;

    @Autowired
    private RoomRepository roomRepository;

    private ScheduleCampaign scheduleCampaign;

    private static final int SOLVABLE_CAMPAIGN_ID = 1;
    private static final int UNSOLVABLE_CAMPAIGN_ID = 2;

    private static final int NB_STUDENTS = 100;

    public static final HardSoftScoreVerifier<ScheduleCampaign> SCORE_VERIFIER = new HardSoftScoreVerifier<>(
            SolverFactory.createFromXmlResource(SolverManager.SOLVER_XML_RESSOURCE_PATH));

    private static final SimpleDateFormat DF = new SimpleDateFormat("dd/MM/yyyy");

    private void saveDataSetCSV() throws IOException {
        Map<Integer, AccountEntity> mapAccounts = new HashMap<>();
        Map<Integer, RoleEntity> mapRoles = new HashMap<>();
        Map<Integer, AvailabilityEntity> mapAvail = new HashMap<>();
        Map<Integer, StudyEntity> mapStudy = new HashMap<>();
        Map<String, DefenseTypeEntity> mapDefType = new HashMap<>();
        Map<Integer, StudentEntity> mapStudentEntity = new HashMap<>();
        Map<Integer, SupervisorEntity> mapSupervisorEntity = new HashMap<>();
        Map<Integer, CivilityEntity> mapCivilityEntity = new HashMap<>();
        Map<Integer, AttendeeEntity> mapAttendeeEntity = new HashMap<>();
        Optional<PcTownEntity> pcTownEntity = null;

        List<Object> listAccountRoleEntities = parseDataFile("src/test/resources/csv/account_roles.csv",AccountRole::create);
        List<Object> listAttendeeAvailEntities = parseDataFile("src/test/resources/csv/attendee_availabilities.csv", AvailabilityAttendee::create);

        List<Object> listDefTypeEntities = parseDataFile("src/test/resources/csv/defense_types.csv", r -> {
            DefenseTypeEntity dte = DefenseTypeEntity.create(r);
            mapDefType.put(dte.getCode(), dte);
            return dte;
        });
        List<Object> listCampStateEntities = parseDataFile("src/test/resources/csv/campaign_states.csv", CampaignStateEntity::create);
        List<Object> listAvailEntities = parseDataFile("src/test/resources/csv/availability.csv", r -> {
            AvailabilityEntity av = AvailabilityEntity.create(r);
            mapAvail.put(Integer.parseInt(r.get(0)), av);
            return av;
        });
        List<Object> listStudyEntities = parseDataFile("src/test/resources/csv/studys.csv", r -> {
            StudyEntity studyEntity = StudyEntity.create(r);
            mapStudy.put(Integer.parseInt(r.get(0)), studyEntity);
            return studyEntity;
        });
        List<Object> listRoleEntities = parseDataFile("src/test/resources/csv/roles.csv", r -> {
            RoleEntity roleEntity = RoleEntity.create(r);
            mapRoles.put(Integer.parseInt(r.get(0)), roleEntity);
            return roleEntity;
        });
//        List<Object> listConstEntities = parseDataFile("src/test/resources/csv/constraints.csv", ConstraintEntity::create);
        List<Object> listCivilEntities = parseDataFile("src/test/resources/csv/civility.csv", r -> {
            CivilityEntity civilityEntity = CivilityEntity.create(r);
            mapCivilityEntity.put(Integer.parseInt(r.get(0)), civilityEntity);
            return civilityEntity;
        });
        List<Object> listPcTownEntities = parseDataFile("src/test/resources/csv/pc_towns.csv", PcTownEntity::create);
        List<Object> listAccountEntities = parseDataFile("src/test/resources/csv/accounts.csv", r -> {
            AccountEntity a = AccountEntity.create(r);
            mapAccounts.put(Integer.parseInt(r.get(0)), a);
            return a;
        });


        List<Object> listStudentSup = parseDataFile("src/test/resources/csv/student_supervisor.csv", StudentSup::create);


        pcTownEntity = listPcTownEntities.stream()
                                         .map(o -> (PcTownEntity)o)
                                         .filter(pc -> pc.getPostalCode().equals("77410"))
                                         .findAny();

        Optional<PcTownEntity> finalPcTownEntity = pcTownEntity;
        List<Object> listAttendeeEntities = parseDataFile("src/test/resources/csv/attendees.csv", r -> {
            AttendeeEntity attendeeEntity = AttendeeEntity.create(r);
            attendeeEntity.setAccount(mapAccounts.get(Integer.parseInt(r.get(5))));
            attendeeEntity.setCivility(mapCivilityEntity.get(Integer.parseInt(r.get(6))));
            attendeeEntity.setPcTown(finalPcTownEntity.get());
            mapAttendeeEntity.put(Integer.parseInt(r.get(0)), attendeeEntity);
            return attendeeEntity;
        });

        listAttendeeAvailEntities.stream()
                                 .map(o -> (AvailabilityAttendee)o)
                                 .forEach(a -> {
                                     AvailabilityEntity av = mapAvail.get(a.getIdAvail());
                                     AttendeeEntity att = mapAttendeeEntity.get(a.getIdAttendee());
                                     if(av != null){
                                         att.getAvailabilities().add(av);
                                     }
                                 });

        List<Object> listStudentEntities = parseDataFile("src/test/resources/csv/students.csv", r -> {
            int idStudent = Integer.parseInt(r.get(0));
            AttendeeEntity attendeeEntity = mapAttendeeEntity.get(idStudent);
            StudentEntity studentEntity = StudentEntity.create(attendeeEntity);
            studentEntity.setStudy(mapStudy.get(Integer.parseInt(r.get(1))));
            studentEntity.setDefenseType(mapDefType.get(r.get(2)));
            mapStudentEntity.put(Integer.parseInt(r.get(0)), studentEntity);
            return studentEntity;
        });
        List<Object> listSupervisorEntities = parseDataFile("src/test/resources/csv/supervisors.csv", r -> {
            int idSupervisor = Integer.parseInt(r.get(0));
            AttendeeEntity attendeeEntity = mapAttendeeEntity.get(idSupervisor);
            SupervisorEntity supervisorEntity = SupervisorEntity.create(attendeeEntity);
            mapSupervisorEntity.put(idSupervisor, supervisorEntity);
            return supervisorEntity;
        });

        listStudentSup.stream()
                      .map(o -> (StudentSup)o)
                      .forEach(s -> {
                          StudentEntity studentEntity = mapStudentEntity.get(s.getIdStudent());
                          SupervisorEntity supervisorEntity = mapSupervisorEntity.get(s.getIdSupervisor());
                          if(supervisorEntity != null && studentEntity != null){
                              studentEntity.getSupervisors().add(new SupervisorStudentEntity(supervisorEntity, studentEntity, mapRoles.get(4)));
                          }
                      });
        List<Object> listOtherAttendeeEntities = parseDataFile("src/test/resources/csv/otherAttendees.csv", r -> {
            int idOtherAttendee = Integer.parseInt(r.get(0));
            AttendeeEntity attendeeEntity = mapAttendeeEntity.get(idOtherAttendee);
            AttendeeGuestEntity attendeeGuestEntity = AttendeeGuestEntity.create(attendeeEntity);
            return attendeeGuestEntity;
        });

        listAccountRoleEntities.stream()
                           .map(o -> (AccountRole)o)
                           .forEach(a -> {
                                AccountEntity accEntity = mapAccounts.get(a.getIdAccount());
                                RoleEntity roleEntity = mapRoles.get(a.getIdRole());
                                if(roleEntity != null){
                                    accEntity.getRoleEntities().add(roleEntity);
                                }
                           });

        listDefTypeEntities.forEach(dte -> entityManager.persistAndFlush((DefenseTypeEntity)dte));
        listCampStateEntities.forEach(dte -> entityManager.persistAndFlush((CampaignStateEntity)dte));
        listAvailEntities.forEach(dte -> entityManager.persistAndFlush((AvailabilityEntity)dte));
        listStudyEntities.forEach(dte -> entityManager.persistAndFlush((StudyEntity)dte));
        listRoleEntities.forEach(dte -> entityManager.persistAndFlush((RoleEntity)dte));
//        listConstEntities.forEach(dte -> entityManager.persistAndFlush((ConstraintEntity)dte));
        listCivilEntities.forEach(dte -> entityManager.persistAndFlush((CivilityEntity)dte));
        entityManager.persistAndFlush(finalPcTownEntity.get());
        listAccountEntities.forEach(a -> entityManager.persistAndFlush((AccountEntity)a));
        listAttendeeEntities.forEach(a -> entityManager.persistAndFlush((AttendeeEntity)a));

        listStudentEntities.forEach(a -> entityManager.persistAndFlush((StudentEntity)a));
        listSupervisorEntities.forEach(a -> entityManager.persistAndFlush((SupervisorEntity)a));
        listOtherAttendeeEntities.forEach(a -> entityManager.persistAndFlush((AttendeeGuestEntity)a));
    }

    private List<Object> parseDataFile(String filePath, Function<CSVRecord, Object> parseFunct) throws IOException {
        List<Object> list = new ArrayList<>();
        try (Reader in = new FileReader(filePath)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .parse(in);
            for (CSVRecord record : records) {
                list.add(parseFunct.apply(record));
            }
        }
        return list;
    }

    private void createAndSaveDataCampaign() throws IOException {
        saveDataSetCSV();
        Optional<AccountEntity> accountEntity = accountRepository.findById(1);
        CampaignStateEntity campaignStateEntity = campaignStateRepository.findById(1).get();
        DefenseTypeEntity defenseTypeEntity = defenseTypeRepository.findByCode("1");
        Iterator<StudentEntity> studentEntities = studentRepository.findAll().iterator();
        Iterable<ConstraintEntity> constraintEntities = constraintRepository.findAll();
        Iterable<AttendeeGuestEntity> attendeeGuestEntities = attendeeGuestRepository.findAll();

        CampaignEntity campaignEntity = new CampaignEntity("ESIPE RSP 2018/2019",
                Date.valueOf("2019-07-01"),
                Date.valueOf("2019-07-12"),
                Set.of(accountEntity.get()),
                campaignStateEntity, 20);

        CampaignEntity campaignEntity2 = new CampaignEntity("ESIPE RSP 2018/2019",
                Date.valueOf("2019-06-24"),
                Date.valueOf("2019-07-05"),
                Set.of(accountEntity.get()),
                campaignStateEntity, 20);

        Set<TimeslotEntity> timeslotEntities = new HashSet<>();
        Time time1 = Time.valueOf(LocalTime.of(9, 0));
        Time time2 = Time.valueOf(LocalTime.of(12, 0));
        Time time3 = Time.valueOf(LocalTime.of(13, 45));
        Time time4 = Time.valueOf(LocalTime.of(17, 0));
        TimeslotEntity timeslotEntity = new TimeslotEntity(time1, time2);
        TimeslotEntity timeslotEntity2 = new TimeslotEntity(time3, time4);
        timeslotEntities.add(timeslotEntity);
        timeslotEntities.add(timeslotEntity2);

        Set<DefenseDayEntity> defenseDayEntities = new HashSet<>();
        Set<DefenseDayEntity> defenseDayEntities2 = new HashSet<>();
        for(int d = 24; d <= 28; d++) {
            DefenseDayEntity day = new DefenseDayEntity(Date.valueOf(LocalDate.of(2019, 06, d)),
                    campaignEntity2);
            day.setSlots(timeslotEntities);
            defenseDayEntities2.add(day);
        }
        for(int d = 1; d <= 5; d++) {
            DefenseDayEntity day = new DefenseDayEntity(Date.valueOf(LocalDate.of(2019, 07, d)),
                    campaignEntity);
            day.setSlots(timeslotEntities);
            defenseDayEntities.add(day);
        }
        for(int d = 8; d <= 12; d++) {
            DefenseDayEntity day = new DefenseDayEntity(Date.valueOf(LocalDate.of(2019, 07, d)),
                    campaignEntity);
            day.setSlots(timeslotEntities);
            defenseDayEntities.add(day);
        }

        StudentEntity studentEntity = null;
        for(int i = 0; i < NB_STUDENTS; i++){
            if(studentEntities.hasNext()){
                studentEntity = studentEntities.next();
                campaignEntity.getStudents().add(studentEntity);
                campaignEntity2.getStudents().add(studentEntity);
            }
        }
        Set<CampaignConstraintEntity> campaignConstraintEntities = new HashSet<>();
        Set<CampaignConstraintEntity> campaignConstraintEntities2 = new HashSet<>();
        for(ConstraintEntity constraintEntity : constraintEntities){
            if(constraintEntity.getId() <= 4) {
                CampaignConstraintEntity c = new CampaignConstraintEntity(constraintEntity, campaignEntity);
                CampaignConstraintEntity c2 = new CampaignConstraintEntity(constraintEntity, campaignEntity2);
                c.setWeight(4);
                c.setActive(true);
                c.setValide(true);
                c.setOptimal(false);
                c.setScore(0);

                c2.setWeight(4);
                c2.setActive(true);
                c2.setValide(true);
                c2.setOptimal(false);
                c2.setScore(0);

                campaignEntity.getConstraintEntities().add(c);
                campaignConstraintEntities.add(c);
                campaignEntity2.getConstraintEntities().add(c2);
                campaignConstraintEntities2.add(c2);
            }
        }

        SortedSet<RoomEntity> roomEntities = new TreeSet<>();
        int number = 1;
        for (int i = 0; i < campaignEntity.getRoomNumber(); i++) {
            RoomEntity r = new RoomEntity(String.valueOf(number++));
            r.setId(i);
            roomEntities.add(r);
        }
        defenseDayEntities.forEach(d -> d.setRooms(roomEntities));
        defenseDayEntities2.forEach(d -> d.setRooms(roomEntities));

        campaignEntity.addDefenseType(defenseTypeEntity);
        campaignEntity.setDefenseDays(defenseDayEntities);

        campaignEntity2.addDefenseType(defenseTypeEntity);
        campaignEntity2.setDefenseDays(defenseDayEntities2);
        attendeeGuestEntities.forEach(a -> campaignEntity.getGuests().add(a));
        attendeeGuestEntities.forEach(a -> campaignEntity2.getGuests().add(a));
        entityManager.persistAndFlush(campaignEntity);
        entityManager.persistAndFlush(campaignEntity2);
    }

    @Test
    public void testExpectedCampaign() throws IOException {
        createAndSaveDataCampaign();
        Optional<CampaignEntity> optionalCampaignEntity = campaignRepository.findById(SOLVABLE_CAMPAIGN_ID);
        CampaignEntity campaignEntity = optionalCampaignEntity.get();

        ScheduleCampaign unsolvedScheduleCompaign = ScheduleCampaign.create(new SolverProdParam(campaignEntity, defenseRepository));

        Assert.assertEquals(campaignEntity.getName(), unsolvedScheduleCompaign.getCampaign().getName());
        Assert.assertEquals(getLocalDate(campaignEntity.getCampaignBegin()), unsolvedScheduleCompaign.getCampaign().getDateBegin());
        Assert.assertEquals(getLocalDate(campaignEntity.getCampaignEnd()), unsolvedScheduleCompaign.getCampaign().getDateEnd());
        Assert.assertEquals(campaignEntity.getDefenseDays().iterator().next().getSlots().size()*campaignEntity.getDefenseDays().size(), unsolvedScheduleCompaign.getCampaign().getDayTimePeriods().size());
        Assert.assertEquals(campaignEntity.getDefensesType().size(), unsolvedScheduleCompaign.getDefenseTypes().size());
        Assert.assertEquals(campaignEntity.getStudents().size(), unsolvedScheduleCompaign.getStudents().size());
        Assert.assertEquals(21, unsolvedScheduleCompaign.getOtherAttendees().size());
        Assert.assertEquals(campaignEntity.getStudents()
                                          .stream()
                                          .map(s -> s.getSupervisors())
                                          .flatMap(s -> s.stream())
                                          .collect(Collectors.toList())
                                          .size(), unsolvedScheduleCompaign.getSupervisors().size());
        Assert.assertEquals(campaignEntity.getRoomNumber(), unsolvedScheduleCompaign.getRooms().size());

        Assert.assertTrue(unsolvedScheduleCompaign.getScore() == null);
        campaignEntity.getConstraintEntities().forEach(c -> {
            ConstraintEntity constraintEntity = c.getConstraint();
            int weight = unsolvedScheduleCompaign.getConstraintWeight(constraintEntity.getId());
            Assert.assertEquals(c.getWeight(), weight);
        });

        Assert.assertFalse(unsolvedScheduleCompaign.getRooms()
                                                  .stream()
                                                  .anyMatch(r -> r.getAvailableDays().isEmpty()));

        int nbAvailStudents = unsolvedScheduleCompaign.getStudents()
                                                      .stream()
                                                      .map(s -> s.getLocalDatesAvailables())
                                                      .flatMap(s -> s.stream())
                                                      .collect(Collectors.toList())
                                                      .size();

        int nbAvailSupervisors = unsolvedScheduleCompaign.getSupervisors()
                                                         .stream()
                                                         .map(s -> s.getLocalDatesAvailables())
                                                         .flatMap(s -> s.stream())
                                                         .collect(Collectors.toList())
                                                         .size();

        Assert.assertTrue(nbAvailSupervisors != 0);

        Assert.assertEquals(campaignEntity.getStudents()
                                          .stream()
                                          .map(s -> s.getAvailabilities())
                                          .flatMap(s -> s.stream())
                                          .collect(Collectors.toList())
                                          .size(), nbAvailStudents);

        Assert.assertEquals(campaignEntity.getStudents()
                .stream()
                .map(StudentEntity::getSupervisors)
                .flatMap(Set<SupervisorStudentEntity>::stream)
                .map(SupervisorStudentEntity::getSupervisor)
                .map(s -> s.getAvailabilities())
                .flatMap(s -> s.stream())
                .collect(Collectors.toList())
                .size(), nbAvailSupervisors);

        Assert.assertTrue(!unsolvedScheduleCompaign.getOtherAttendees()
                                                  .stream()
                                                  .map(s -> s.getLocalDatesAvailables())
                                                  .flatMap(s -> s.stream())
                                                  .collect(Collectors.toList())
                                                  .isEmpty());

        Assert.assertTrue(!unsolvedScheduleCompaign.getDefenses().isEmpty());
        Assert.assertEquals(campaignEntity.getStudents().size(), unsolvedScheduleCompaign.getDefenses().size());
        Assert.assertTrue(!unsolvedScheduleCompaign.getDefenseAssigns().isEmpty());
        Assert.assertEquals(unsolvedScheduleCompaign.getDefenseAssigns().size(), unsolvedScheduleCompaign.getDefenses().size());

        scheduleCampaign = SolverManager.solve(ScheduleCampaign::create,
                new SolverProdParam(campaignEntity, defenseRepository));

        SCORE_VERIFIER.assertHardWeight("Room unavailable", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Room conflict", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Supervisor unavailable", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Supervisor conflict", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Other attendee unavailable", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Other attendee conflict", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Same attendee on defense", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Defense Type with good TimeSlot", 0, scheduleCampaign);
        Assert.assertEquals(HardSoftScore.of(SolverManager.checksSolution(scheduleCampaign, true), 0).getHardScore(), scheduleCampaign.getScore().getHardScore());
        Assert.assertEquals(HardSoftScore.of(0, 0).getHardScore(), scheduleCampaign.getScore().getHardScore());

        Assert.assertTrue(scheduleCampaign.getUnscheduledDefenses().isEmpty());

        scheduleCampaign = SolverManager.solve(ScheduleCampaign::create,
                new SolverProdParam(campaignEntity, defenseRepository));

        SCORE_VERIFIER.assertHardWeight("Room unavailable", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Room conflict", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Supervisor unavailable", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Supervisor conflict", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Other attendee unavailable", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Other attendee conflict", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Same attendee on defense", 0, scheduleCampaign);
        SCORE_VERIFIER.assertHardWeight("Defense Type with good TimeSlot", 0, scheduleCampaign);
        Assert.assertEquals(HardSoftScore.of(SolverManager.checksSolution(scheduleCampaign, true), 0).getHardScore(), scheduleCampaign.getScore().getHardScore());
        Assert.assertEquals(HardSoftScore.of(0, 0).getHardScore(), scheduleCampaign.getScore().getHardScore());

        Assert.assertTrue(scheduleCampaign.getUnscheduledDefenses().isEmpty());

        Optional<CampaignEntity> optionalCampaignEntity2 = campaignRepository.findById(UNSOLVABLE_CAMPAIGN_ID);
        CampaignEntity campaignEntity2 = optionalCampaignEntity2.get();
        scheduleCampaign = SolverManager.solve(ScheduleCampaign::create,
                new SolverProdParam(campaignEntity2, defenseRepository));

        Assert.assertTrue(scheduleCampaign.getScore().getHardScore() < 0);
        Assert.assertFalse(scheduleCampaign.getUnscheduledDefenses().isEmpty());
    }

    private static LocalDate getLocalDate(Date date){
        String strDate = DF.format(date);
        String[] dates = strDate.split("/");
        return LocalDate.of(Integer.parseInt(dates[2]),
                Integer.parseInt(dates[1]),
                Integer.parseInt(dates[0]));
    }

    public static class AccountRole{
        private final int idRole;
        private final int idAccount;

        public AccountRole(int idRole, int idAccount) {
            this.idRole = idRole;
            this.idAccount = idAccount;
        }

        public int getIdRole() {
            return idRole;
        }

        public int getIdAccount() {
            return idAccount;
        }

        public static AccountRole create(CSVRecord csvRecord) {
            return new AccountRole(Integer.parseInt(csvRecord.get(0)),
                    Integer.parseInt(csvRecord.get(1)));
        }
    }

    public static class AvailabilityAttendee {
        private final int idAttendee;
        private final int idAvail;

        public AvailabilityAttendee(int idAttendee, int idAvail) {
            this.idAttendee = idAttendee;
            this.idAvail = idAvail;
        }

        public static AvailabilityAttendee create(CSVRecord record) {
            return new AvailabilityAttendee(Integer.parseInt(record.get(0)),
                    Integer.parseInt(record.get(1)));
        }

        public int getIdAttendee() {
            return idAttendee;
        }

        public int getIdAvail() {
            return idAvail;
        }

        @Override
        public String toString() {
            return "AvailabilityAttendee{" +
                    "idAttendee=" + idAttendee +
                    ", idAvail=" + idAvail +
                    '}';
        }
    }

    public static class StudentSup {
        private final int idStudent;
        private final int idSupervisor;

        public StudentSup(int idStudent, int idSupervisor) {
            this.idStudent = idStudent;
            this.idSupervisor = idSupervisor;
        }

        public static StudentSup create(CSVRecord record) {
            return new StudentSup(Integer.parseInt(record.get(0)),
                    Integer.parseInt(record.get(1)));
        }

        public int getIdStudent() {
            return idStudent;
        }

        public int getIdSupervisor() {
            return idSupervisor;
        }
    }

}



