package fr.planninggo.spring.campaign;

import fr.planninggo.scheduler.Campaign;
import fr.planninggo.scheduler.ScheduleCampaign;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class CampaignTest {

    @Test
    public void testCreateCampaignWithNameNull(){
        try{
            new Campaign(1,
                    null,
                    LocalDate.of(2019, 07, 01),
                    LocalDate.of(2019, 07, 12));
            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testCreateCampaignWithDateBeginNull(){
        try{
            new Campaign(1, "ESIPE RSP 2018/2019",
                    null,
                    LocalDate.of(2019, 07, 12));
            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testCreateCampaignWithDateEndNull(){
        try{
            new Campaign(1,
                    "ESIPE RSP 2018/2019",
                    LocalDate.of(2019, 07, 01),
                    null);
            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testCreateCampaignWithTimePeriodsNull(){
        try{
            new Campaign(1,
                    "ESIPE RSP 2018/2019",
                    null,
                    null);
            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testCreateCampaignWithMapConstraintsNull(){
        try{
            new Campaign(1,
                    "ESIPE RSP 2018/2019",
                    LocalDate.of(2019, 07, 01),
                    LocalDate.of(2019, 07, 12));
//            Assert.fail();
        }catch(NullPointerException e){

        }
    }

    @Test
    public void testEqualsWithNullArg() {
        Assert.assertEquals(false, new ScheduleCampaign().equals(null));
    }

    @Test
    public void testNotEqualsDiffCampaignInstances() {
        Campaign c1 = new Campaign(1, "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Campaign c2 = new Campaign(2, "ESIPE RMT 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Campaign c3 = new Campaign(3, "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 06),
                LocalDate.of(2019, 07, 20));
        Assert.assertNotEquals(c1, c2);
        Assert.assertNotEquals(c2, c3);
        Assert.assertNotEquals(c3, c1);
    }

    @Test
    public void testEqualsDiffCampaignInstances() {
        Campaign c1 = new Campaign(1,
                "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Campaign c2 = new Campaign(1,
                "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Assert.assertEquals(c1, c2);
    }

    @Test
    public void testNotEqualsHashcodeDiffCampaignInstances() {
        Campaign c1 = new Campaign(1, "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Campaign c2 = new Campaign(2, "ESIPE RMT 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Campaign c3 = new Campaign(3, "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 06),
                LocalDate.of(2019, 07, 20));
        Assert.assertNotEquals(c1.hashCode(), c2.hashCode());
        Assert.assertNotEquals(c2.hashCode(), c3.hashCode());
        Assert.assertNotEquals(c3.hashCode(), c1.hashCode());
    }

    @Test
    public void testEqualsHashcodeDiffCampaignInstances() {
        Campaign c1 = new Campaign(1,
                "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Campaign c2 = new Campaign(1,
                "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Assert.assertEquals(c1.hashCode(), c2.hashCode());
    }

    @Test
    public void testGetters() {
        String name = "ESIPE RSP 2018/2019";
        LocalDate ld1 = LocalDate.of(2019, 07, 01);
        LocalDate ld2 = LocalDate.of(2019, 07, 12);
        Campaign c1 = new Campaign(1,
                "ESIPE RSP 2018/2019",
                LocalDate.of(2019, 07, 01),
                LocalDate.of(2019, 07, 12));
        Assert.assertEquals(name, c1.getName());
        Assert.assertEquals(ld1, c1.getDateBegin());
        Assert.assertEquals(ld2, c1.getDateEnd());
    }

    @Test
    public void testSetDateBeginNull() {
        try {
            Campaign c1 = new Campaign(1,
                    "ESIPE RSP 2018/2019",
                    LocalDate.of(2019, 07, 01),
                    LocalDate.of(2019, 07, 12));
            c1.setDateBegin(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetDateEndNull() {
        try {
            Campaign c1 = new Campaign(1,
                    "ESIPE RSP 2018/2019",
                    LocalDate.of(2019, 07, 01),
                    LocalDate.of(2019, 07, 12));
            c1.setDateEnd(null);
        } catch (NullPointerException e) {

        }
    }

    @Test
    public void testSetNameNull() {
        try {
            Campaign c1 = new Campaign(1, "ESIPE RSP 2018/2019",
                    LocalDate.of(2019, 07, 01),
                    LocalDate.of(2019, 07, 12));
            c1.setName(null);
        } catch (NullPointerException e) {

        }
    }
}
