package fr.planninggo.spring.campaign;

import fr.planninggo.csv.CSVWriter;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CsvWriterTest {
    List<String> l;
    CSVWriter cw;

    @Before
    public void setUp(){
        l = new ArrayList<>();
        l.add("id;name;lastname;role;email");
        l.add("1;idali;oumaima;etudiante;oumaima.idali@gmail.com");
        l.add("2;idali;chaima;etudiante;chaima.idali@gmail.com");
        l.add("3;idali;soufiane;etudiant;soufiane.idali@gmail.com");
        l.add("4;boxero;janine;tutrice;janine.boxero@gmail.com");
        l.add("5;alanou;isabelle;presidente;isabelle.alanou@gmail.com");

    }

    @Test
    public void csvWriteTestNameFile() throws IOException {
        Path fileName = Paths.get("File_ofjo.csv");
        cw = new CSVWriter(fileName, l);
        cw.getData();
    }

    @Test
    public void csvWriterTestNbColulmns(){
        String header = l.get(0);
        String[] headerTab = header.split(";");
        for(int i = 1; i < l.size(); i++){
            String line = l.get(i);
            String[] lineTab = line.split(";");
            assertTrue(lineTab.length == headerTab.length);
        }
    }
}

