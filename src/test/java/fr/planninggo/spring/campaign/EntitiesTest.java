package fr.planninggo.spring.campaign;



import fr.planninggo.spring.attendee.entity.*;
import fr.planninggo.spring.campaign.entity.*;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = {EntitiesTest.Initializer.class})
public class EntitiesTest {
    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer =
            (PostgreSQLContainer) new PostgreSQLContainer("ggorret/planninggo_db:1.0.1")
                    .withDatabaseName("planninggo_db")
                    .withUsername("postgres")
                    .withPassword("postgres")
                    .withStartupTimeout(Duration.ofSeconds(600));

    @Autowired
    private TestEntityManager entityManager;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private CivilityEntity civilityEntity;
    private AccountEntity accountEntity;
    private PcTownEntity pcTownEntity;
    private AttendeeEntity attendeeEntity;
    private RoleEntity roleEntity;
    private StudyEntity studyEntity;
    private RoomEntity roomEntity;
    private DefenseTypeEntity defenseTypeEntity;
    private CampaignStateEntity campaignStateEntity;
    private AttendeeGuestEntity attendeeGuestEntity;
    private StudentEntity studentEntity;
    private CampaignEntity campaignEntity;
    private DefenseEntity defenseEntity;
    private SupervisorEntity supervisorEntity;
    private ConstraintEntity constraintEntity;
    private CampaignConstraintEntity campaignConstraintEntity;
    private TimeslotEntity timeslotEntity;
    private PhaseEntity phaseEntity;
    private DefenseDayEntity defenseDayEntity;
    private AvailabilityEntity availabilityEntity;


    @Before
    public void setUp() {
        availabilityEntity = new AvailabilityEntity(LocalDate.now(), true, true);
        constraintEntity = new ConstraintEntity("Pas de soutenance au même moment");
        civilityEntity = new CivilityEntity("Mlle");
        roleEntity = new RoleEntity("newrole");new RoleEntity("newrole");
        accountEntity = new AccountEntity("email", "password", Set.of(roleEntity));
        pcTownEntity = new PcTownEntity("77165", "Gesvres-le-chapitre", true);
        attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                accountEntity,
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));
        studyEntity = new StudyEntity("IMAC");
        roomEntity = new RoomEntity("1");
        defenseTypeEntity = new DefenseTypeEntity("RAP", "Rapport Appelant Papa", "1h00");
        campaignStateEntity = new CampaignStateEntity("NoExist");
        attendeeGuestEntity = new AttendeeGuestEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(roleEntity)),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));

        campaignEntity = new CampaignEntity("Camp", Date.valueOf("2019-01-01"), Date.valueOf("2019-03-31"),
                Set.of(accountEntity), campaignStateEntity, 3);
        studentEntity = new StudentEntity("ggorret", "gorret", "guillaume", "address",
                accountEntity,
                civilityEntity, pcTownEntity,
                new StudyEntity("IMAC"),
                defenseTypeEntity, Set.of(campaignEntity));
        defenseEntity = new DefenseEntity(Timestamp.valueOf(LocalDateTime.of(2019, 2, 22, 9, 0)), studentEntity,
                defenseTypeEntity, campaignEntity, roomEntity, Set.of(attendeeGuestEntity));
        supervisorEntity = new SupervisorEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("role"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));
        campaignConstraintEntity = new CampaignConstraintEntity(constraintEntity, new CampaignEntity("Camp", Date.valueOf("2019-01-01"), Date.valueOf("2019-03-31"),
                Set.of(accountEntity), campaignStateEntity, 3));
        timeslotEntity = new TimeslotEntity(Time.valueOf(LocalTime.now()), Time.valueOf(LocalTime.now()));
        phaseEntity = new PhaseEntity(Date.valueOf("2019-01-03"), Date.valueOf("2019-01-03"), campaignEntity, 3, "TE", "TE");
        defenseDayEntity = new DefenseDayEntity(Date.valueOf("2019-01-01"), campaignEntity, new TreeSet<>(Set.of(roomEntity)));
    }

    @Test
    public void testAvailability() {
        AvailabilityEntity t = this.entityManager.persistAndFlush(availabilityEntity);
        assertEquals(t.getId(), availabilityEntity.getId());
        assertEquals(t.getDate(), availabilityEntity.getDate());
        assertEquals(t.isOnMorning(), availabilityEntity.isOnMorning());
        assertEquals(t.isOnAfternoon(), availabilityEntity.isOnAfternoon());
        assertTrue(availabilityEntity.hasAvailabilities());
        assertEquals(t, availabilityEntity);
    }

    @Test
    public void saveDefenseDay() {
        this.entityManager.persist(campaignStateEntity);
        this.entityManager.persist(accountEntity);
        this.entityManager.persist(campaignEntity);
        DefenseDayEntity saveDefenseDayEntity = this.entityManager.persistAndFlush(this.defenseDayEntity);
        assertEquals(Date.valueOf("2019-01-01"), saveDefenseDayEntity.getDate());
        assertEquals(campaignEntity, saveDefenseDayEntity.getCampaign());
        assertEquals(3, campaignEntity.getRoomNumber());
        campaignEntity.setStudents(Set.of(studentEntity));
        campaignEntity.setGuests(Set.of(attendeeGuestEntity));
    }

    @Test
    public void savePhase() {
        this.entityManager.persist(campaignStateEntity);
        this.entityManager.persist(accountEntity);
        this.entityManager.persist(campaignEntity);
        PhaseEntity savePhaseEntity = this.entityManager.persistAndFlush(this.phaseEntity);
        assertEquals(phaseEntity.getBegin(), savePhaseEntity.getBegin());
        assertEquals(phaseEntity.getEnd(), savePhaseEntity.getEnd());
        assertEquals(phaseEntity.getCampaign(), savePhaseEntity.getCampaign());
        assertEquals(3, savePhaseEntity.getAvailabilityNumber());
    }

    @Test
    public void saveTimeslot() {
        TimeslotEntity saveTimeslotEntity = this.entityManager.persistAndFlush(timeslotEntity);
        assertEquals(timeslotEntity.getBegin(), saveTimeslotEntity.getBegin());
        assertEquals(timeslotEntity.getEnd(), saveTimeslotEntity.getEnd());
    }

    @Test
    public void saveConstraint() {
        ConstraintEntity saveConstraintEntity = this.entityManager.persistAndFlush(this.constraintEntity);
        assertEquals("Pas de soutenance au même moment", saveConstraintEntity.getDesc());
    }

    @Test
    public void saveCampaignConstraint() {
        this.entityManager.persist(roleEntity);
        this.entityManager.persist(accountEntity);
        this.entityManager.persistAndFlush(campaignStateEntity);
        CampaignConstraintEntity saveCampaignConstraintEntity = this.entityManager.persistAndFlush(campaignConstraintEntity);
        assertEquals(constraintEntity, saveCampaignConstraintEntity.getConstraint());
        assertEquals(campaignEntity, saveCampaignConstraintEntity.getCampaign());
    }

    @Test
    public void saveCivility() {
        CivilityEntity saveCivilityEntity = this.entityManager.persistAndFlush(this.civilityEntity);
        assertEquals("Mlle", saveCivilityEntity.getName());
    }

    @Test
    public void saveRole() {
        RoleEntity saveRoleEntity = this.entityManager.persistAndFlush(this.roleEntity);
        assertEquals("newrole", saveRoleEntity.getName());
    }

    @Test
    public void saveAccount() {
        AccountEntity saveAccountEntity = this.entityManager.persistAndFlush(this.accountEntity);
        assertEquals("email", saveAccountEntity.getEmail());
        assertFalse(saveAccountEntity.isRgpdAccept());
        assertTrue(saveAccountEntity.getRoleEntities().containsAll(Set.of(roleEntity)));
    }

    @Test
    public void savePcTown() {
        PcTownEntity savePcTownEntity = this.entityManager.persistAndFlush(this.pcTownEntity);
        assertEquals("77165", savePcTownEntity.getPostalCode());
        assertEquals("Gesvres-le-chapitre", savePcTownEntity.getTown());
        assertTrue(savePcTownEntity.isParisArea());
    }

    @Test
    public void saveAttendeeGuest() {
        AttendeeGuestEntity saveAttendeeEntity = this.entityManager.persistAndFlush(this.attendeeGuestEntity);
        assertEquals(accountEntity, saveAttendeeEntity.getAccount());
        assertEquals(new PcTownEntity("77123", "Hello", false).getTown(), saveAttendeeEntity.getTown());
        assertEquals(new PcTownEntity("77123", "Hello", false).getPostalCode(), saveAttendeeEntity.getPostalCode());
        assertEquals(new CivilityEntity("cla").getName(), saveAttendeeEntity.getCivility());
        assertEquals("email", saveAttendeeEntity.getEmail());
        assertEquals("lastname", saveAttendeeEntity.getLastname());
        assertEquals("firstname", saveAttendeeEntity.getFirstname());
        assertEquals("address", saveAttendeeEntity.getAddress());
    }

    @Test
    public void saveStudent() {
        this.entityManager.persist(this.defenseTypeEntity);
        this.entityManager.persist(this.roleEntity);
        this.entityManager.persist(campaignStateEntity);
        this.entityManager.persist(this.campaignEntity);
        StudentEntity saveStudentEntity = this.entityManager.persistAndFlush(this.studentEntity);
        assertEquals(accountEntity, saveStudentEntity.getAccount());
        assertEquals(pcTownEntity.getTown(), saveStudentEntity.getTown());
        assertEquals(pcTownEntity.getPostalCode(), saveStudentEntity.getPostalCode());
        assertEquals(civilityEntity.getName(), saveStudentEntity.getCivility());
        assertEquals(defenseTypeEntity, saveStudentEntity.getDefenseType());
        assertEquals(new StudyEntity("IMAC").getName(), saveStudentEntity.getStudy());
        assertEquals("ggorret", saveStudentEntity.getEmail());
        assertEquals("gorret", saveStudentEntity.getLastname());
        assertEquals("guillaume", saveStudentEntity.getFirstname());
        assertEquals("address", saveStudentEntity.getAddress());
    }

    @Test
    public void saveSupervisor() {
        this.entityManager.persist(this.defenseTypeEntity);
        this.entityManager.persist(this.roleEntity);
        this.entityManager.persist(campaignStateEntity);
        this.entityManager.persist(this.campaignEntity);
        this.entityManager.persist(studentEntity);

        supervisorEntity.addStudent(studentEntity, roleEntity);
        SupervisorEntity saveSupervisorEntity = this.entityManager.persistAndFlush(this.supervisorEntity);
        assertEquals(new AccountEntity("email", "password", Set.of(new RoleEntity("role"))), saveSupervisorEntity.getAccount());
        assertEquals(new PcTownEntity("77123", "Hello", false).getTown(), saveSupervisorEntity.getTown());
        assertEquals(new PcTownEntity("77123", "Hello", false).getPostalCode(), saveSupervisorEntity.getPostalCode());
        assertEquals(new CivilityEntity("cla").getName(), saveSupervisorEntity.getCivility());
        assertEquals("email", saveSupervisorEntity.getEmail());
        assertEquals("lastname", saveSupervisorEntity.getLastname());
        assertEquals("firstname", saveSupervisorEntity.getFirstname());
        assertEquals("address", saveSupervisorEntity.getAddress());
        assertTrue(saveSupervisorEntity.getStudents().contains(studentEntity));
    }

    @Test
    public void saveAttendee() {
        AttendeeEntity saveAttendeeEntity = this.entityManager.persistAndFlush(this.attendeeEntity);
        assertEquals(accountEntity, saveAttendeeEntity.getAccount());
        assertEquals(new PcTownEntity("77123", "Hello", false).getTown(), saveAttendeeEntity.getTown());
        assertEquals(new PcTownEntity("77123", "Hello", false).getPostalCode(), saveAttendeeEntity.getPostalCode());
        assertEquals(new CivilityEntity("cla").getName(), saveAttendeeEntity.getCivility());
        assertEquals("email", saveAttendeeEntity.getEmail());
        assertEquals("lastname", saveAttendeeEntity.getLastname());
        assertEquals("firstname", saveAttendeeEntity.getFirstname());
        assertEquals("address", saveAttendeeEntity.getAddress());
    }

    @Test
    public void saveStudy() {
        StudyEntity saveStudyEntity = this.entityManager.persistAndFlush(this.studyEntity);
        assertEquals("IMAC", saveStudyEntity.getName());
    }

    @Test
    public void saveRoom() {
        RoomEntity saveRoomEntity = this.entityManager.persistAndFlush(this.roomEntity);
        assertEquals("1", saveRoomEntity.getName());
    }

    @Test
    public void saveDefenseType() {
        DefenseTypeEntity saveDefenseTypeEntity = this.entityManager.persistAndFlush(this.defenseTypeEntity);
        assertEquals("RAP", saveDefenseTypeEntity.getCode());
        assertEquals("Rapport Appelant Papa", saveDefenseTypeEntity.getDesc());
        assertEquals("1h00", saveDefenseTypeEntity.getDuration());
    }

    @Test
    public void saveCampaignState() {
        CampaignStateEntity saveCampaignStateEntity = this.entityManager.persistAndFlush(this.campaignStateEntity);
        assertEquals("NoExist", saveCampaignStateEntity.getName());
    }

    @Test
    public void saveCampaign() {
        this.entityManager.persist(campaignStateEntity);
        this.entityManager.persist(accountEntity);
        CampaignEntity saveCampaignEntity = this.entityManager.persistAndFlush(this.campaignEntity);
        assertEquals("Camp", saveCampaignEntity.getName());
        assertEquals(Date.valueOf("2019-01-01"), saveCampaignEntity.getCampaignBegin());
        assertEquals(Date.valueOf("2019-03-31"), saveCampaignEntity.getCampaignEnd());
        assertTrue(saveCampaignEntity.getOrganizers().contains(accountEntity));
        assertEquals("NoExist", saveCampaignEntity.getCampaignState());
    }

    @Test
    public void saveDefense() {
        this.entityManager.persist(defenseTypeEntity);
        this.entityManager.persist(roleEntity);
        this.entityManager.persist(campaignStateEntity);
        this.entityManager.persist(campaignEntity);
        this.entityManager.persist(studentEntity);
        this.entityManager.persist(roomEntity);
        this.entityManager.persist(attendeeGuestEntity);
        this.entityManager.persist(roleEntity);
        this.entityManager.persist(accountEntity);
        this.entityManager.persist(studentEntity);
        DefenseEntity saveDefenseEntity = this.entityManager.persistAndFlush(this.defenseEntity);
        assertEquals(Timestamp.valueOf(LocalDateTime.of(2019, 2, 22, 9, 0)), saveDefenseEntity.getDefenseDate());
        assertEquals(studentEntity, saveDefenseEntity.getStudent());
        assertEquals(defenseTypeEntity, saveDefenseEntity.getDefenseType());
        assertEquals(campaignEntity, saveDefenseEntity.getCampaign());
        assertEquals(roomEntity, saveDefenseEntity.getRoom());
        assertTrue(saveDefenseEntity.getAttendeeGuests().contains(attendeeGuestEntity));
        assertTrue(saveDefenseEntity.isHold());
    }

    @Test
    public void campaignEntity() {
        campaignEntity.removeDefenseTypeByCode(defenseTypeEntity.getCode());
        assertFalse(campaignEntity.getDefensesType().contains(defenseTypeEntity));
    }

    public void removePhaseNoExist() {
        campaignEntity.removePhaseByCode(phaseEntity.getCode());
    }


    @Test
    public void createDefenseWithIsHold(){
        DefenseEntity defenseEntity1 = new DefenseEntity(Timestamp.valueOf(LocalDateTime.of(2019, 2, 22, 9, 0)), studentEntity, false, defenseTypeEntity, campaignEntity,
                roomEntity, Set.of(attendeeGuestEntity));
        assertTrue(!defenseEntity1.isHold());
    }

    @Test
    public void defenseSetHold(){
        defenseEntity.setHold(false);
        assertTrue(!defenseEntity.isHold());
    }

    @Test
    public void defenseSetShare(){
        defenseEntity.setShare(false);
        assertTrue(!defenseEntity.isShare());
    }

    @Test
    public void defenseGetId(){
        System.out.println(defenseEntity.getFormattedDefenseDate());
        assertEquals(defenseEntity.getId(), 0);
    }

    @Test
    public void defenseGetFormattedDefenseDate(){
        assertEquals(defenseEntity.getFormattedDefenseDate(), "22/02/2019 09:00");
    }

    @Test
    public void createCampaignEntity(){
        CampaignEntity campaignEntity1 = new CampaignEntity(campaignEntity, campaignStateEntity);
        assertEquals(campaignEntity.getName(),campaignEntity1.getName());
    }

    @Test
    public void campaignEntitySetName(){
        campaignEntity.setName("toto");
        assertEquals(campaignEntity.getName(),"toto");
    }

    @Test
    public void campaignEntitySetRoomNumber(){
        campaignEntity.setRoomNumber(10);
        assertEquals(campaignEntity.getRoomNumber(),10);
    }


    @Test
    public void campaignEntitySetDateBegin(){
        Date date =  Date.valueOf("2019-02-20");
        campaignEntity.setCampaignBegin(date);
        assertEquals(campaignEntity.getCampaignBegin(),date);
    }

    @Test
    public void campaignEntitySetDateEnd(){
        Date date =  Date.valueOf("2019-02-20");
        campaignEntity.setCampaignEnd(date);
        assertEquals(campaignEntity.getCampaignEnd(),date);
    }

    @Test
    public void campaignEntityGetSuperviasor(){
        assertEquals(campaignEntity.getSupervisors(),Set.of());
    }


    @Test
    public void campaignEntitySetCampagnState(){
        campaignEntity.setCampaignState(campaignStateEntity);
        assertEquals(campaignEntity.getCampaignState(), campaignStateEntity.getName());
    }

    @Test
    public void campaignEntityGetAttendees(){
        assertEquals(campaignEntity.getAttendees(), List.of());
    }

    @Test
    public void campaignEntityGetDefense(){
        assertEquals(campaignEntity.getDefenses(), Set.of());
    }

    @Test
    public void addConstraint(){
        ConstraintEntity constraintEntity1 = new ConstraintEntity("test");
        campaignEntity.addConstraint(constraintEntity1);
        assertEquals(campaignEntity.getConstraintEntities().size(),1);
    }

}