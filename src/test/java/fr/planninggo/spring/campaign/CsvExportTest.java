package fr.planninggo.spring.campaign;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.csv.CsvExport;
import fr.planninggo.csv.CsvParser;
import fr.planninggo.spring.user.repository.AccountRepository;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = {CsvExportTest.Initializer.class })
@Transactional
public class CsvExportTest {

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer =
            (PostgreSQLContainer) new PostgreSQLContainer("ggorret/planninggo_db:importp")
                    .withDatabaseName("planninggo_db")
                    .withUsername("postgres")
                    .withPassword("postgres")
                    .withStartupTimeout(Duration.ofSeconds(600));

    private File importCampaignOK = new File("src/test/resources/csv/campaign/importCampaignOK.csv");

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void parseCampaignOk() throws Exception {
        FileInputStream filedata = new FileInputStream(importCampaignOK);
        CampaignEntity campaignEntity = CsvParser.parseCampaignSetting(new MockMultipartFile("importCampaignOK", filedata), accountRepository);
        CsvExport.exportCampaign(campaignEntity);
    }
}
