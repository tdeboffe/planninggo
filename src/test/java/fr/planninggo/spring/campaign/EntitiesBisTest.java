package fr.planninggo.spring.campaign;


import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.attendee.entity.CivilityEntity;
import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.attendee.entity.PcTownEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.StudyEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class EntitiesBisTest {

    private CivilityEntity civilityEntity;
    private AccountEntity accountEntity;
    private PcTownEntity pcTownEntity;
    private AttendeeEntity attendeeEntity;
    private RoleEntity roleEntity;
    private StudyEntity studyEntity;
    private RoomEntity roomEntity;
    private DefenseTypeEntity defenseTypeEntity;
    private CampaignStateEntity campaignStateEntity;
    private AttendeeGuestEntity attendeeGuestEntity;
    private StudentEntity studentEntity;
    private CampaignEntity campaignEntity;
    private SupervisorEntity supervisorEntity;
    private ConstraintEntity constraintEntity;
    private TimeslotEntity timeslotEntity;
    private PhaseEntity phaseEntity;
    private DefenseDayEntity defenseDayEntity;
    private AvailabilityEntity availabilityEntity;
    private JuryMemberEntity juryMemberEntity;
    private SupervisorStudentEntity supervisorStudentEntity;


    @Before
    public void setUp() {
        HashSet<RoleEntity> roleEntityHashSet = new HashSet<>();

        availabilityEntity = new AvailabilityEntity(LocalDate.now(), true, true);
        constraintEntity = new ConstraintEntity("Pas de soutenance au même moment");
        civilityEntity = new CivilityEntity("Mlle");
        roleEntity = new RoleEntity("newrole");new RoleEntity("newrole");
        roleEntityHashSet.add(roleEntity);
        accountEntity = new AccountEntity("email", "password",roleEntityHashSet);
        pcTownEntity = new PcTownEntity("77165", "Gesvres-le-chapitre", true);
        attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                accountEntity,
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));
        studyEntity = new StudyEntity("IMAC");
        roomEntity = new RoomEntity("SALA");
        defenseTypeEntity = new DefenseTypeEntity("RAP", "Rapport Appelant Papa", "1h00");
        campaignStateEntity = new CampaignStateEntity("NoExist");
        attendeeGuestEntity = new AttendeeGuestEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(roleEntity)),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));

        campaignEntity = new CampaignEntity("Camp", Date.valueOf("2019-01-01"), Date.valueOf("2019-03-31"),
                Set.of(accountEntity), campaignStateEntity, 3);
        studentEntity = new StudentEntity("ggorret", "gorret", "guillaume", "address",
                accountEntity,
                civilityEntity, pcTownEntity,
                new StudyEntity("IMAC"),
                defenseTypeEntity, Set.of(campaignEntity));
        supervisorEntity = new SupervisorEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("role"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));
        timeslotEntity = new TimeslotEntity(Time.valueOf(LocalTime.now()), Time.valueOf(LocalTime.now()));
        phaseEntity = new PhaseEntity(Date.valueOf("2019-01-03"), Date.valueOf("2019-01-03"), campaignEntity, 3, "TE", "TE");
        defenseDayEntity = new DefenseDayEntity(Date.valueOf("2019-01-01"), campaignEntity, Set.of(roomEntity));
        juryMemberEntity = new JuryMemberEntity(true, true, false, roleEntity);
        supervisorStudentEntity = new SupervisorStudentEntity(supervisorEntity, studentEntity, roleEntity);
    }

    @Test
    public void testAttendeeSetEmailEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));


        attendeeEntity.setEmail("t@gmail");
        assertEquals(attendeeEntity.getEmail(),"t@gmail" );

    }

    @Test
    public void testAttendeeSetNameEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));
        attendeeEntity.setFirstname("to");
        attendeeEntity.setLastname("ma");
        assertEquals(attendeeEntity.getFirstname() + attendeeEntity.getLastname(),"toma" );
        attendeeEntity.setAddress("new Adresse");
        attendeeEntity.setCivility(new CivilityEntity("Mme"));
        attendeeEntity.setPcTown( new PcTownEntity("77123", "Hello", true));

    }

    @Test
    public void testAttendeeSetAddressEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));
        attendeeEntity.setAddress("new Adresse");
        assertEquals(attendeeEntity.getAddress(),"new Adresse");

    }

    @Test
    public void testAttendeeSetCivilityEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));

        attendeeEntity.setCivility(new CivilityEntity("Mme"));
        assertEquals(attendeeEntity.getCivility(),"Mme");

    }

    @Test
    public void testAttendeeSetTownEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));

        attendeeEntity.setPcTown( new PcTownEntity("77123", "Hello", true));
        assertEquals(attendeeEntity.getPcTown(),new PcTownEntity("77123", "Hello", true));

    }

    @Test
    public void testAttendeeSetIdEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));

        attendeeEntity.setId(10);
        assertEquals(attendeeEntity.getId(), 10);

    }

    @Test
    public void testAttendeeSetAccountEntity(){
        AttendeeEntity attendeeEntity = new AttendeeEntity("email", "lastname", "firstname", "address",
                new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))),
                new CivilityEntity("cla"), new PcTownEntity("77123", "Hello", false));

        attendeeEntity.setAccount(new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))));
        assertEquals(attendeeEntity.getAccount(), new AccountEntity("email", "password", Set.of(new RoleEntity("newrole"))));

    }

    @Test
    public void testSetSuperviasorStudent(){
        studentEntity.setSupervisors(Set.of());
        assertEquals(studentEntity.getSupervisors().size(),0 );
    }

    @Test
    public void testCreateSuperviasorWithAnotherSuperviasor(){
        SupervisorEntity supervia = new SupervisorEntity(supervisorEntity);
        supervia.setId(supervisorEntity.getId());
        assertEquals(supervia.toString(), supervisorEntity.toString());
    }

    @Test
    public void testSetStudyStudent(){
        studentEntity.setStudy(new StudyEntity("IR"));
        assertEquals(studentEntity.getStudy(),"IR" );
    }

    @Test
    public void testGetStudyEntityStudent(){
        StudyEntity study = new StudyEntity("IR");
        studentEntity.setStudy(study);
        assertEquals(studentEntity.getStudyEntity(),study );
    }

    @Test
    public void testCreateStudentWithAnotherStudent(){
        StudentEntity student = new StudentEntity(studentEntity);
        assertEquals(studentEntity.toString(),student.toString() );
    }

    @Test
    public void testStudentSetDefenseType(){
        studentEntity.setDefenseType(defenseTypeEntity);
        assertEquals(studentEntity.getDefenseType(), defenseTypeEntity);
    }

    @Test
    public void testStudentGetCampaign(){
        assertTrue(studentEntity.getCampaigns().contains(campaignEntity));
    }

    @Test
    public void testAttendeeGuest(){
        AttendeeGuestEntity guest = new AttendeeGuestEntity(attendeeEntity);
        assertEquals(guest.getEmail(), attendeeEntity.getEmail());
    }

    @Test
    public void testAccountSetEmail(){
        accountEntity.setEmail("toto");
        assertEquals(accountEntity.getEmail(), "toto");
    }

    @Test
    public void testAccountSetRGPD(){
        accountEntity.setRgpdAccept(false);
        assertEquals(accountEntity.isRgpdAccept(), false);
    }

    @Test
    public void testAccountaddRole(){
        accountEntity.addRole(new RoleEntity("Orga"));
        assertTrue(accountEntity.getRoleEntities().contains(new RoleEntity("Orga")));
    }

    @Test
    public void testAccountgetId(){
        assertEquals(accountEntity.getId(), 0);
    }

    @Test
    public void testAccountSetPassword(){
        accountEntity.setPassword("test");
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertTrue(encoder.matches("test", accountEntity.getPassword()));
    }

    @Test
    public void testAccountToString(){
        accountEntity.toString();
    }

    @Test
    public void testPhaseEntity(){
        PhaseEntity phaseEntity = new PhaseEntity(1, Date.valueOf("2019-01-03"), Date.valueOf("2019-01-03"), campaignEntity, "TE", "TE", 3);
        assertEquals(phaseEntity.getCode(), "1");
    }



    @Test
    public void testPhaseEntityWrittenBy(){
         assertEquals(phaseEntity.getWrittenBy(), "TE");
    }

    @Test
    public void testPhaseEntityWrittenFor(){
        assertEquals(phaseEntity.getWrittenFor(), "TE");
    }

    @Test
    public void testJuryMemberisPresident(){
        assertEquals(juryMemberEntity.isPresident(), false);
    }

    @Test
    public void testJuryMemberStudentLink(){
        assertEquals(juryMemberEntity.isStudentLink(), true);
    }

    @Test
    public void testJuryMemberStudyLink(){
        assertEquals(juryMemberEntity.isStudyLink(), true);
    }

    @Test
    public void testJuryMemberRole(){
        assertEquals(juryMemberEntity.getRole(), roleEntity);
    }



    @Test
    public void testAvailabilityOnMorning(){
        assertEquals(availabilityEntity.isOnMorning(), true);
    }

    @Test
    public void testAvailabilityOnAfternoon(){
        assertEquals(availabilityEntity.isOnAfternoon(), true);
    }

    @Test
    public void testAvailabilityDate(){
        assertEquals(availabilityEntity.getDate(),  Date.valueOf(LocalDate.now()));
    }

    @Test
    public void testAvailabilitySetId(){
        availabilityEntity.setId(10);
        assertEquals(availabilityEntity.getId(),  10);
    }

    @Test
    public void testAvailabilitySetDate(){
        LocalDate date = LocalDate.of(2000, Month.FEBRUARY, 10);
        availabilityEntity.setDate(date);
        assertEquals(availabilityEntity.getDate().toLocalDate(), date);
    }

    @Test
    public void testCampaignTypeEntity(){
        campaignEntity.addDefenseType(defenseTypeEntity);
        assertEquals(campaignEntity.getDefensesType().size(), 1);
    }

    @Test
    public void testCampaignDefenseByCode(){
        campaignEntity.addDefenseType(defenseTypeEntity);
        campaignEntity.removeDefenseTypeByCode("RAP");
        assertEquals(campaignEntity.getDefensesType().size(), 0);
    }

    @Test
    public void testCampaignPhaseByCode(){
        PhaseEntity phaseEntity = new PhaseEntity(1, Date.valueOf("2019-01-03"), Date.valueOf("2019-01-03"), campaignEntity, "TE", "TE", 3);
        campaignEntity.addPhaseEntity(phaseEntity);
        campaignEntity.removePhaseByCode("1");
        assertEquals(campaignEntity.getPhases().size(), 0);
    }

    @Test
    public void testDefenseDayEntityGetSlot(){
        defenseDayEntity.setSlots(Set.of(timeslotEntity));
        assertTrue(defenseDayEntity.getSlots().contains(timeslotEntity));
    }

    @Test
    public void testDefenseDayEntityGetAndSetRoom(){
        RoomEntity room = new RoomEntity("10");
        SortedSet set = new TreeSet<RoomEntity>();
        set.add(room);
        defenseDayEntity.setRooms(set);
        assertTrue(defenseDayEntity.getRooms().contains(room));
    }

    @Test
    public void testDefenseDayEntityToString(){
        defenseDayEntity.toString();
    }

    @Test
    public void testDefenseTypeEntityToString(){
        defenseTypeEntity.toString();
    }

    @Test
    public void testDefenseTypeEntityAddAndGetJuryMember(){
        JuryMemberEntity jury = new JuryMemberEntity(false, false, true, roleEntity);
        defenseTypeEntity.addJuryMembers(Set.of(jury));
        assertTrue(defenseTypeEntity.getJuryMembers().contains(jury));
    }

    @Test
    public void testSuperviasorStudentEntityGetSuperviasor(){
        assertEquals(supervisorStudentEntity.getSupervisor(), supervisorEntity);
    }

    @Test
    public void testPhaseIsStart() {
        PhaseEntity phaseEntity = new PhaseEntity();
        phaseEntity.setBegin(Date.valueOf(LocalDate.now().minusDays(7)));
        phaseEntity.setEnd(Date.valueOf(LocalDate.now().plusDays(8)));
        assertTrue(phaseEntity.isStart());
    }

    @Test
    public void testPhaseIsStartWithBeginEquals() {
        PhaseEntity phaseEntity = new PhaseEntity();
        phaseEntity.setBegin(Date.valueOf(LocalDate.now()));
        phaseEntity.setEnd(Date.valueOf(LocalDate.now().plusDays(3)));
        assertTrue(phaseEntity.isStart());
    }

    @Test
    public void testPhaseIsStartWithEndEquals() {
        PhaseEntity phaseEntity = new PhaseEntity();
        phaseEntity.setBegin(Date.valueOf("2019-02-01"));
        phaseEntity.setEnd(Date.valueOf(LocalDate.now())

        );
        assertTrue(phaseEntity.isStart());
    }

    @Test
    public void testPhaseNotStarted() {
        PhaseEntity phaseEntity = new PhaseEntity();
        phaseEntity.setBegin(Date.valueOf("2019-02-21"));
        phaseEntity.setEnd(Date.valueOf("2019-02-22"));
        assertFalse(phaseEntity.isStart());
    }

    @Test
    public void testSupervisorStudentEntityToString(){
        assertEquals(supervisorStudentEntity.toString(),"ggorret : email");
    }

    @Test
    public void testSupervisorStudentEntityEquals(){
        SupervisorStudentEntity supervisorStudentEntity1 = new SupervisorStudentEntity(supervisorStudentEntity.getSupervisor(), supervisorStudentEntity.getStudent(), supervisorStudentEntity.getRoleEntity());
        assertEquals(supervisorStudentEntity,supervisorStudentEntity1);
    }

    @Test
    public void testGetIdRole(){
        assertEquals(roleEntity.getId(),0);
    }

    @Test
    public void testRoleComparaTo(){
        assertEquals(roleEntity.compareTo(roleEntity),0);
    }

    @Test
    public void testGetIdPhase(){
        assertEquals(phaseEntity.getId(),0);
    }

    @Test
    public void testPhaseComparaTo(){
        assertEquals(phaseEntity.compareTo(phaseEntity),0);
    }

    @Test
    public void testPhaseEquals(){
        assertTrue(phaseEntity.equals(phaseEntity));
    }

    @Test
    public void testPhaseGetCode(){
        assertEquals(phaseEntity.getCode(),"");
    }

    @Test
    public void testPhaseSetCampaign(){
        phaseEntity.setCampaign(campaignEntity);
        assertEquals(phaseEntity.getCampaign(),campaignEntity);
    }

    @Test
    public void testAvailabilitySetOnMorning(){
        availabilityEntity.setOnMorning(false);
        assertTrue(!availabilityEntity.isOnMorning());
    }

    @Test
    public void testAvailabilitySetOnAfternoon(){
        availabilityEntity.setOnAfternoon(false);
        assertTrue(!availabilityEntity.isOnAfternoon());
    }

    @Test
    public void testAvailabilityComparaTo(){
        assertEquals(availabilityEntity.compareTo(availabilityEntity),0);
    }
}
