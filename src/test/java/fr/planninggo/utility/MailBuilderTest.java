package fr.planninggo.utility;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MailBuilderTest {

    @Test
    public void buildMail(){
        Map<String,String> map = new HashMap<>();
        map.put("to","test@gmail.com");
        map.put("subject", "PlanningGo - Lien pour un nouveau mot de passe.");
        map.put("text", "Bonjour, Veuillez trouver ci-joint un lien pour générer un nouveau mot de passe pour votre compte planning Go : " + "http://test.com" + "\nCordialement, L'équipe Planning Go.");
        assertEquals(map, MailBuilder.createPasswordMail("http://test.com","test@gmail.com"));
    }

    @Test
    public void buildWrongMail(){
        Map<String,String> map = new HashMap<>();
        map.put("to","test11@gmail.com");
        map.put("subject", "PlanningGo - Lien pour un nouveau mot de passe.");
        map.put("text", "Bonjour, <p>Veuillez trouver ci-joint un lien pour générer un nouveau mot de passe pour votre compte planning Go : " + "http://test.com" + ".</p> " +
                "<p>Cordialement, L'équipe Planning Go.</p>");
        assertNotEquals(map, MailBuilder.createPasswordMail("http://test.com","test@gmail.com"));
    }

    @Test
    public void createPasswordAccount(){
        Map<String,String> map = new HashMap<>();
        map.put("to", "test@gmail.com");
        map.put("subject", "PlanningGo - Votre compte");
        map.put("text", "Bonjour, un administrateur vient de vous créer un nouveau compte sur l'application PlanningGo. Vous trouverez votre mot de passe ici : http://test.com\n" +
                "Cordialement, L'équipe Planning Go.");
        assertEquals(map, MailBuilder.createPasswordAccount("http://test.com",  "test@gmail.com"));
    }

    @Test
    public void createAvailabilitiesEmptyWrittenForMail(){
        Map<String, String> map = new HashMap<>();
        map.put("to", "test@gmail.com");
        map.put("subject", "PlanningGo - Saisie de disponibilités");
        map.put("text", "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir vos disponibilités avant le 01/03/2019. Pour cela rendez-vous à " +
                "cette adresse : http://test.com. Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !");
        assertEquals(map, MailBuilder.createAvailabilitiesMail("http://test.com",  "test@gmail.com", "", "01/03/2019"));
    }

    @Test
    public void createAvailabilitiesWrittenForMail(){
        Map<String, String> map = new HashMap<>();
        map.put("to", "test@gmail.com");
        map.put("subject", "PlanningGo - Saisie de disponibilités");

        map.put("text", "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir des disponibilités avant le 01/03/2019 pour votre TI. Pour cela rendez-vous à " +
                "cette adresse : http://test.com. Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !");
        assertEquals(map, MailBuilder.createAvailabilitiesMail("http://test.com",  "test@gmail.com", "TI", "01/03/2019"));
    }

    @Test
    public void shareMail(){
        String mail = "test@gmail.com";
        String objet = "test";
        String msg = "msg";
        String url = "http://test";
        Map<String, String> map = new HashMap<>();
        map.put("to", mail);
        map.put("subject", objet);
        map.put("text", msg + "\n" + url);
        assertEquals(map, MailBuilder.sharePlanning(url, mail, msg, objet));
    }
}
