package fr.planninggo.utility;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SymetricEncryptionTest {

    @Test
    public void encryption() throws Exception {
        assertEquals("GuvWXdTeNnsTTP_2SNxhUw", SymetricEncryption.encrypt("ceciestuntest"));
    }

    @Test
    public void encryption2() throws Exception {
        assertNotEquals("GuvWXdazTeNnsTTP_2SNxhUw", SymetricEncryption.encrypt("ceciestuntest"));
    }

    @Test
    public void decryption() throws Exception {
        assertEquals("ceciestuntest", SymetricEncryption.decrypt("GuvWXdTeNnsTTP_2SNxhUw"));
    }

    @Test
    public void decryption2() throws Exception {
        assertNotEquals("ceciplanningtest", SymetricEncryption.decrypt("GuvWXdTeNnsTTP_2SNxhUw"));
    }
}
