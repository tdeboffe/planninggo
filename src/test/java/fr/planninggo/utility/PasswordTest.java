package fr.planninggo.utility;


import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PasswordTest {

    @Test
    public void generatePassword(){
        String passWord = Password.generatePassword();
        assertEquals(8, passWord.length());
    }

    @Test
    public void encodePassword(){
        String passWord = Password.generatePassword();
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertTrue(encoder.matches(passWord, Password.encodePassword(passWord)));
    }

    @Test
    public void testEncode(){
        System.out.println(Password.encodePassword("licorne"));
    }
}
