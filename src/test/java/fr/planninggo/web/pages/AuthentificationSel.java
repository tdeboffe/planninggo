package fr.planninggo.web.pages;

import fr.planninggo.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AuthentificationSel implements AuthentificationInterf {
    private WebDriver webDriver;

    public AuthentificationSel(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void verifPage() throws InterruptedException {
        // Vérifier présence logo page d'accueil
        //SeleniumUtils.findElement(webDriver, By
        //       .xpath("//div[@id=\"logo\"]/center/a/img[@src=\"images/logo.png\"][@width=\"200\"][@height=\"150\"]"));

        SeleniumUtils.findElement(webDriver, By.xpath("//h2"));
        SeleniumUtils.findElement(webDriver, By.xpath("//input[@id='username']"));
        SeleniumUtils.findElement(webDriver, By.xpath("//input[@id='password']"));

        // Vérifier la présence de titre de la page d'accueil
        //SeleniumUtils.findElement(webDriver, By.xpath("//h3[contains(text(),\"La Consigne Numérique Solidaire\")]"));
        //SeleniumUtils.findElement(webDriver,
        //        By.xpath("//p/i/b/font[contains(text(), \"'' Mon essentiel à l'abri en toute confiance '' .\")]"));
    }

    @Override
    public void go(String url) {
        SeleniumUtils.go(webDriver, url);
    }

    @Override
    public void writeIdOnPageAuthentif(String id) throws InterruptedException {
        SeleniumUtils.findElement(webDriver, By.xpath("//input[@id='username']")).sendKeys(id);
    }

    @Override
    public void writeMdpOnPageAuthentif(String mdp) throws InterruptedException {
        SeleniumUtils.findElement(webDriver, By.xpath("//input[@id='password']")).sendKeys(mdp);
    }

    @Override
    public void clickToConnect() throws InterruptedException {
        SeleniumUtils.findElement(webDriver, By.xpath("//input[@id='password']")).click();
    }

    @Override
    public void checkMessage() throws InterruptedException {
        Thread.sleep(60000);
        SeleniumUtils.findElement(webDriver, By.xpath("//p[@id='newMessage']"));
    }
}
