package fr.planninggo.web.pages;

public interface PlanningInterf {
    void verifPage() throws InterruptedException;
    void go(String url);
}
