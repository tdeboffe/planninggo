package fr.planninggo.web.pages;

public interface AuthentificationInterf {

    void verifPage() throws InterruptedException;

    void go(String url);

    void writeIdOnPageAuthentif(String id) throws InterruptedException;

    void writeMdpOnPageAuthentif(String id) throws InterruptedException;

    void clickToConnect() throws InterruptedException;

    void checkMessage() throws InterruptedException;
}
