package fr.planninggo.web.pages;

import fr.planninggo.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PlanningSel implements PlanningInterf {
    private WebDriver webDriver;

    public PlanningSel(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    @Override
    public void verifPage() throws InterruptedException {
        SeleniumUtils.findElement(webDriver, By.xpath("//div[@id=\"scheduler_here\"]"));
    }

    @Override
    public void go(String url) {
        SeleniumUtils.go(webDriver, url);
    }
}
