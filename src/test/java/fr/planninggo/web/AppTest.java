package fr.planninggo.web;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import fr.planninggo.utils.SeleniumUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {
        "html:./cucumber-html" }, features = "src/test/resources/features")
public class AppTest {
    private static Logger LOG = Logger.getLogger(AppTest.class.getName());
    private static WebDriver driver;

    /**
     * Before est appelé avant lancer le test
     *
     */
    @BeforeClass
    public static void setUpClass() throws IOException {
        setLogger();
        execScriptSQL("postgres","planninggo_db","localhost", "/home/planninggodata/selenium_post_test.sql");
        execScriptSQL("postgres","planninggo_db","localhost", "/home/planninggodata/selenium_pre_test.sql");
        driver = SeleniumUtils.setUpChromeDriver();
        LOG.info("Web interface tests : START");
    }

    /**
     * AfterClass est appelé à la fin du test
     *
     */
    @AfterClass
    public static void tearDown() {
        LOG.info("Web interface tests : STOP");
        driver.close();
    }

    public static WebDriver getDriver() {
        return driver;
    }

    private static void setLogger() {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
    }

    private static void execScriptSQL(String dbUsername, String dbName, String dbServer, String sqlFile) throws IOException {
        Process p = Runtime.getRuntime().exec("psql -U " + dbUsername + " -d " + dbName + " -h " + dbServer +" -f" + sqlFile);
        try (BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
            LOG.info("SQL script execution : " + sqlFile);
            String line;
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
}
