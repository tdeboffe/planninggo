package fr.planninggo.web.steps;

import com.google.common.collect.ImmutableMap;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;

public class WebDriverFactory {

    private static Logger logger = Logger.getLogger(StepGlobalUtils.class.getName());

    public static WebDriver get() {
        try {
            WebDriverManager.chromedriver().setup();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless", "window-size=1024,768", "--no-sandbox", "--disable-dev-shm-usage");

            ChromeDriverService service = new ChromeDriverService.Builder()
                    .usingAnyFreePort()
                    .withEnvironment(ImmutableMap.of("DISPLAY", ":1"))
                    .withVerbose(true)
                    .usingDriverExecutable(new File(System.getProperty("webdriver.chrome.driver")))
                    .build();

            service.start();

            return new ChromeDriver(service, options);
        } catch (Exception e) {
            logger.error("Impossible de lancer ChromeDriverService", e);
        }
        return null;
    }

}
