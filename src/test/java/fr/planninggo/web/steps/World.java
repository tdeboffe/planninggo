package fr.planninggo.web.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class World {

    void findAndClickOnElementWithXPath(WebDriver driver, String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
        element.click();
    }

    void findAndSendKeyToElementWithXPath(WebDriver driver, String xpath, String value) {
        WebElement element = driver.findElement(By.xpath(xpath));
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
        element.sendKeys(value);
    }

    void findAndSetKeyToElementWithXPath(WebDriver driver, String xpath, String value) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.clear();
        ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
        element.sendKeys(value);
    }

    void findAndMoveMouseTo(WebDriver driver, String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).build().perform();
    }

}
