package fr.planninggo.web.steps;

import fr.planninggo.web.pages.PlanningInterf;
import fr.planninggo.web.pages.PlanningSel;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class PlanningStep {

    private static Logger logger = Logger.getLogger(PlanningStep.class.getName());

    private PlanningInterf planning;

    public PlanningStep() {}

    /**
     * Avant chaque scenario
     */
    @Before
    public void prepare() {
        logger.debug("Debut PlanningStep prepare @Before");
        WebDriver webDriver = StepGlobalUtils.getWebDriver();
        planning = new PlanningSel(webDriver);
        logger.debug("Fin PlanningStep prepare @Before");
    }

    // ************ FORMULAIRE DE D'AUTHENTIFICATION ******************
    @Given("^Sur la page 'Planning' URL '(.*)'")
    public void goPlanning(String url) {
        planning.go(url);
    }

    @Then("^La page 'Planning' s'affiche$")
    public void verifPagePlanning() throws InterruptedException {
        planning.verifPage();
    }
}
