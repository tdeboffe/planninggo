package fr.planninggo.web.steps;

import cucumber.api.java.en.And;
import fr.planninggo.web.pages.AuthentificationInterf;
import fr.planninggo.web.pages.AuthentificationSel;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AuthentificationStep {

    private static Logger logger = Logger.getLogger(AuthentificationStep.class.getName());

    private AuthentificationInterf authentification;

    public AuthentificationStep() {

    }


    /**
     * Avant chaque scenario
     */
    @Before
    public void prepare() {
        logger.debug("Debut AuthentificationStep prepare @Before");
        WebDriver webDriver = StepGlobalUtils.getWebDriver();
        authentification = new AuthentificationSel(webDriver);
        logger.debug("Fin   AuthentificationStep prepare @Before");
    }

    // ************ FORMULAIRE DE D'AUTHENTIFICATION ******************
    @Given("^Sur la page 'Authentification' URL '(.*)'")
    public void goAuthentification(String url) {
        authentification.go(url);
    }

    @Given("^AUTH01 Sur la page 'Authentification Portail'$")
    public void verifPageAuthentifArrivee() throws InterruptedException {
        authentification.verifPage();
    }

    @Then("^AUTH01 La page 'Authentification Portail' s'affiche$")
    public void verifPageAuthentif() throws InterruptedException {
        authentification.verifPage();
    }

    @When("^AUTH01 Dans le champ 'Identifiant' saisir '(.*)'$")
    public void writeIdOnPageAuthentif(String id) throws InterruptedException {
        authentification.writeIdOnPageAuthentif(id);
    }

    @And("^AUTH01 Dans le champ 'Mot de passe' saisir '(.*)'$")
    public void writeMdpOnPageAuthentif(String mdp) throws InterruptedException {
        authentification.writeMdpOnPageAuthentif(mdp);
    }

    @And("^AUTH01 Cliquer sur le bouton 'Se connecter'$")
    public void clickToConnect() throws InterruptedException {
        authentification.clickToConnect();
    }

    @And("^AUTH01 Le message d'erreur apparait$")
    public void connexionFailure() throws InterruptedException {
        authentification.checkMessage();
    }
}
