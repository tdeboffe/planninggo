package fr.planninggo.web.steps;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {
        "html:./cucumber-html" }, features = "src/test/resources/fr/planninggo/web/steps")

public class AppTest {
    private static Logger LOG = Logger.getLogger(AppTest.class.getName());

    /**
     * BeforeClass est appelé avant lancer le test
     *
     * @throws ParserConfigurationException
     */
    @BeforeClass
    public static void setUpClass() throws ParserConfigurationException {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);

        LOG.info("Debut setUpClass dans log4j");
        System.out.println("Master setUpClass");
    }

    /**
     * AfterClass est appelé à la fin du test
     *
     * @throws IOException
     */
    @AfterClass
    public static void tearDownClass() throws IOException {
        System.out.println("Master tearDown");
    }

}
