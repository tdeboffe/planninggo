package fr.planninggo.web.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.planninggo.web.AppTest;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

public class OrganisationStep {
    private WebDriver driver = AppTest.getDriver();
    private World world = new World();

    @And("^La campagne du '(.+)' au '(.+)' doit apparaître$")
    public void confirmCampaignPageWithCampaign(String startDate, String endDate) {
        //TODO
    }

    @Then("^La page d'importation de campagne doit apparaître$")
    public void confirmCampaignImportPage() {
        String currentURL = driver.getCurrentUrl();
        assertTrue(currentURL.matches(".*/app/campaign/import/campaign"));
    }

    @Then("^La page d'importation de participants doit apparaître$")
    public void confirmAttendeeImportPage() {
        //TODO
    }

    @Then("^La page d'importation de type de soutenances doit apparaître$")
    public void confirmDefenseImportPage() {
        //TODO
    }

    @Then("^La page de création de phase doit apparaître$")
    public void confirmPhasePage() {
        //TODO
    }

    @Then("^La page de création de phase doit apparaître en étant remplie$")
    public void confirmPhasePagePrefilled() {
        //TODO
    }

    @Then("^La page de création d'un type de soutenance doit apparaître$")
    public void confirmDefenseTypePage() {
        //TODO
    }

    @Then("^La page de création d'un type de soutenance doit apparaître en étant préremplie$")
    public void confirmDefenseTypePagePrefilled() {
        //TODO
    }

    @Then("^La page de création d'une campagne doit apparaître$")
    public void confirmCampaignCreationPage() {
        //TODO
    }

    @Then("^La page de création d'une campagne doit apparaître en étant préremplie$")
    public void confirmCampaignCreationPagePrefilled() {
        //TODO
    }

    @Then("^La page de définition d'une nouvelle phase doit apparaître$")
    public void confirmPhaseConfigPage() {
        //TODO
    }

    @Then("^La page de la campagne '(.+)' doit apparaître$")
    public void confirmSpecificCampaignPage(String campaignName) {
        //TODO
    }

    @Then("^La page de mes campagnes doit apparaître$")
    public void confirmCampaignPage() {
        //TODO
    }

    @Then("^Le page du planning doit apparaître$")
    public void confirmPlanningPage() {
        //TODO
    }

    @When("^Je choisis le fichier '(.+)'$")
    public void choseFileToImport(String path) {
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='file']", path);
    }

    @When("^Je remplie le formulaire de création de campagne avec '(.+)' '(.+)' '(.+)' '(.+)'$")
    public void sendCampaignCreationForm(String name, String startDate, String endDate, String nRooms) {
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='text' and @name='campaignName']", name);
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='date' and @name='campaignBegin']", startDate);
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='date' and @name='campaignEnd']", endDate);
        world.findAndSetKeyToElementWithXPath(driver, "//input[@type='number' and @name='roomNumber']", nRooms);
    }

    @When("^Je complète le formulaire de création de campagne avec '(.+)' '(.+)' '(.+)'$")
    public void sendCampaignCreationPrefilledForm(String startDate, String endDate, String nRooms) {
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='date' and @name='campaignBegin']", startDate);
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='date' and @name='campaignEnd']", endDate);
        world.findAndSetKeyToElementWithXPath(driver, "//input[@type='number' and @name='roomNumber']", nRooms);
    }

    @When("^Je complète le formulaire de création de phase avec '(.+)' '(.+)' '(.+)' '(.+)' '(.+)'$")
    public void sendPhaseCreationForm(String startDate, String endDate, String forWho, String byWho, String minAvailabilities) {
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='date' and @name='dateBegin']", startDate);
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='date' and @name='dateEnd']", endDate);
        world.findAndSendKeyToElementWithXPath(driver, "//select[@id='rolePour']", forWho);
        world.findAndSendKeyToElementWithXPath(driver, "//select[@id='rolePar']", byWho);
        world.findAndSendKeyToElementWithXPath(driver, "//input[@type='number' and @id='availabilityNumber']", minAvailabilities);
    }
}
