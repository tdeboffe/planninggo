package fr.planninggo.web.steps;

import cucumber.api.java.Before;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

/**
 * Le glue code, il fait la liaison entre la description Gherkin et l'appel au métier (ici le pilotage de Selenium)
 */
public class StepGlobalUtils {

    public static Map<String, String> contextSave = new HashMap<>();
    private static Logger logger = Logger.getLogger(StepGlobalUtils.class.getName());

    // permet de sauvegarder des valeurs dans un contexte, ce contexte a utilisable a l'interieur d'un meme scenario
    private static WebDriver webDriver = null;

    public StepGlobalUtils() {
    }

    public static WebDriver getWebDriver() {
        if (webDriver == null) {
            webDriver = new SharedDriver();
        }
        return webDriver;
    }

    public static Map<String, String> getContext() {
        return contextSave;
    }

    /**
     * Avant chaque scenario
     */
    @Before
    public void prepare() {
        logger.info("Debut StepGlobalUtils prepare @Before");

        webDriver = getWebDriver();

        // a chaque scenario, on efface le contexte.
        contextSave.clear();

        logger.info("Fin   StepGlobalUtils prepare @Before");
    }

}
