package fr.planninggo.web.steps;

import cucumber.api.java.en.When;
import fr.planninggo.web.AppTest;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class GeneralStep {
    private WebDriver driver = AppTest.getDriver();
    private World world = new World();

    @When("^J'accède à la page (.+)$")
    public void doGet(String url) {
        driver.get(url);
    }

    @When("^J'envoie le formulaire$")
    public void sendForm() {
        world.findAndClickOnElementWithXPath(driver, "//button[@type='submit']");
    }

    @When("^Je clique sur '(.+)'$")
    public void clickOn(String text) {
        /* Check for perfect match */
        try { clickOnButton(text); } catch (Exception e) { /* try next */ }
        try { clickOnLink(text); } catch (Exception e) { /* try next */ }
        try { clickOnButtonLink(text); } catch (Exception e) { /* try next */ }
        /* Check for permissive match */
        try { world.findAndClickOnElementWithXPath(driver, "//button[contains(text(), '" + text + "')]"); } catch (Exception e) { /* try next */ }
        try { world.findAndClickOnElementWithXPath(driver, "//a[contains(text(), '" + text + "')]"); } catch (Exception e) { /* try next */ }
        try { world.findAndClickOnElementWithXPath(driver, "//a/button[contains(text(), '" + text + "')]"); } catch (Exception e) { /* try next */ }
        /* Check for ultra permissive match */
        world.findAndClickOnElementWithXPath(driver, "//*[contains(text(), '" + text + "')]");
    }

    @When("^J'ouvre le menu déroulant '(.+)' et je clique sur '(.+)'$")
    public void clickOnElementInDisplayedDropdownMenu(String superText, String text) {
        clickOn(superText);
        world.findAndClickOnElementWithXPath(driver, "//*[@class='dropdown-menu show']/*[contains(text(), '" + text +"')]");
    }

    @When("^Je clique sur le bouton '(.+)'$")
    public void clickOnButton(String buttonText) {
        world.findAndClickOnElementWithXPath(driver, "//button[text()='" + buttonText + "']");
    }

    @When("^Je clique sur le lien '(.+)'$")
    public void clickOnLink(String linkText) {
        world.findAndClickOnElementWithXPath(driver, "//a[text()=\"" + linkText + "\"]");
    }

    @When("^Je clique sur le lien/bouton '(.+)'$")
    public void clickOnButtonLink(String buttonText) {
        world.findAndClickOnElementWithXPath(driver, "//a/button[text()='" + buttonText + "']");
    }

    @When("^Je clique sur le lien vers '(.+)'$")
    public void clickOnLinkTo(String href) {
        world.findAndClickOnElementWithXPath(driver, "//a[@href='" + href + "']");
    }

    @When("^Je coche la case avec l'ID '(.+)'$")
    public void checkBoxByID(String id) {
        world.findAndClickOnElementWithXPath(driver, "//input[@type='checkbox' and @id=\'" + id + "\']");
    }

    @When("^Je déplace la souris sur le lien '(.+)'$")
    public void goToLink(String linkText) {
        world.findAndMoveMouseTo(driver, "//a[text()=\"" + linkText + "\"]");
    }
}
