package fr.planninggo.web.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import fr.planninggo.web.AppTest;
import org.openqa.selenium.WebDriver;

public class AccessStep {
    private WebDriver driver = AppTest.getDriver();
    private World world = new World();

    @Then("^La page de confirmation de nouveau mot de passe doit apparaître$")
    public void confirmNewPasswordSendPage() {
        //TODO
    }

    @Then("^La page de demande de mot de passe doit apparaître$")
    public void confirmNewPasswordRequestPage() {
        //TODO
    }

    @Then("^La page de login doit apparaître$")
    public void confirmLoginPage() {
        //TODO
    }

    @Then("^La page de RGPD doit apparaître$")
    public void confirmRGPDPage() {
        //TODO
    }

    @When("^J'envoie le formulaire de demande de mot de passe pour '(.+)'$")
    public void sendPasswordRequest(String email) {
        world.findAndClickOnElementWithXPath(driver, "//button[@type='submit']");
        world.findAndSendKeyToElementWithXPath(driver, "//input[@id='inputEmail']", email);
    }

    @When("^J'envoie le formulaire de login avec '(.+)' '(.+)'$")
    public void sendLoginForm(String email, String passwd) {
        world.findAndSendKeyToElementWithXPath(driver, "//input[@id='inputEmail']", email);
        world.findAndSendKeyToElementWithXPath(driver, "//input[@id='inputPassword']", passwd);
        world.findAndClickOnElementWithXPath(driver, "//button[@type='submit']");
    }
}
