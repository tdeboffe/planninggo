package fr.planninggo.utils;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class SeleniumUtils {
    private static final Logger LOG = Logger.getLogger(SeleniumUtils.class.getName());

    public static void go(WebDriver webDriver, String url) {
        if (url.startsWith("http") || url.startsWith("HTTP")) {
            LOG.info("url indiquee dans le step (commence par http)");
            webDriver.get(url);
        } else if (url.startsWith("env:") || url.startsWith("ENV:")) {
            LOG.info("recherche de l'url par variable d'environnement (ENV:PARAM)");
            // like env:HOME_SITE or ENV:ACCUEIL_HOME
            String sParam = url.substring(4);

            LOG.info("sParam = " + sParam);
            String urlEnv = getConfigurationProperty(sParam, sParam, "localhost");

            org.junit.Assert.assertNotNull(urlEnv);
            LOG.info("urlEnv = " + urlEnv);
            webDriver.get(urlEnv);
        } else {
            org.junit.Assert.fail(
                    "l'url n'est pas de type http ou ENV:URL_HOME avec la variable apres le env: declare en variable d'environnement avec mv -DURL_HOME=http://site_integ/page ");
        }
    }

    public static String getConfigurationProperty(
            String envKey, String sysKey, String defValue) {
        String retValue = defValue;
        String envValue = System.getenv(envKey);
        String sysValue = System.getProperty(sysKey);
        // system property prevails over environment variable
        if (sysValue != null) {
            retValue = sysValue;
        } else if (envValue != null) {
            retValue = envValue;
        }
        return retValue;
    }

    public static void goBack(WebDriver webDriver) {
        webDriver.navigate().back();
    }

    public static WebElement findElementWithTimeout(WebDriver webDriver, final By locator, int timeout,
                                                    int checkTime) {
        WebElement w = SeleniumUtils.getElementByGraduallyWait(webDriver, locator, timeout, checkTime);
        if (w == null) {
            throw new NoSuchElementException("Not found : " + locator.toString());
        }
        return w;
    }

    public static WebElement findElement(WebDriver webDriver, final By locator) throws InterruptedException {
        if (webDriver.findElements(locator).size() == 0) {
            Thread.sleep(5000);
        }
        return findElementWithTimeout(webDriver, locator, 20, 5);
    }

    public static WebElement getElementByGraduallyWait(WebDriver webDriver, final By locator, int timeout,
                                                       int checkTime) {
        Wait<WebDriver> wait = new FluentWait<>(webDriver).withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(checkTime,
                        TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(locator);
            }
        });
    }

    public static WebDriver setUpChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        return new ChromeDriver(options);
    }
}
