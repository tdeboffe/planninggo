@portail
Feature: Authentification

	Background:
		Given Sur la page 'Authentification' URL 'ENV:URL_HOME'
		Then AUTH01 La page 'Authentification Portail' s'affiche

	Scenario Outline:
		Given AUTH01 Sur la page 'Authentification Portail'
		When AUTH01 Dans le champ 'Identifiant' saisir '<uid>'
		And AUTH01 Dans le champ 'Mot de passe' saisir '<mdp>'
		And AUTH01 Cliquer sur le bouton 'Se connecter'
		#And AUTH01 Le message d'erreur apparait
		Examples:
			| uid  | mdp |
			| Alexis.Pons@gmail.com | azerty |
