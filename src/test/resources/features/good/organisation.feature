@organisation

Feature: Gestion de campagne


  ###
  Background: Connexion en tant qu'organisateur
    When J'accède à la page http://localhost:8080/app/login
    Then La page de login doit apparaître
    When J'envoie le formulaire de login avec 'selenium.organisateur@planninggo.fr' 'licorne'
    Then La page de mes campagnes doit apparaître

  ###
  Scenario Outline: Création d'une campagne (à base de fichiers d'importations)
    When Je clique sur le lien/bouton 'Importer une campagne'
    Then La page d'importation de campagne doit apparaître
    When Je choisis le fichier '<cheminDuFichierCampagne>'
    When J'envoie le formulaire
    Then La page de création d'une campagne doit apparaître en étant préremplie
    When Je complète le formulaire de création de campagne avec '<dateDébutCampagne>' '<dateFinCampagne>' '<nbSalle>'
    When J'envoie le formulaire
    Then La page de création d'un type de soutenance doit apparaître
    When Je clique sur le lien/bouton 'Importer'
    Then La page d'importation de type de soutenances doit apparaître
    When Je choisis le fichier '<cheminDuFichierSoutenances>'
    When J'envoie le formulaire
    Then La page de création d'un type de soutenance doit apparaître
    When Je clique sur le bouton 'Précédent'
    Then La page de création d'une campagne doit apparaître en étant préremplie
    When Je clique sur le bouton 'Suivant'
    Then La page de création d'un type de soutenance doit apparaître en étant préremplie
    When Je clique sur le bouton 'Suivant'
    Then La page de création de phase doit apparaître
    When Je clique sur le bouton 'Nouvelle phase'
    Then La page de définition d'une nouvelle phase doit apparaître
    When Je clique sur le bouton 'Annuler'
    Then La page de création de phase doit apparaître
    When Je clique sur le bouton 'Nouvelle phase'
    When Je complète le formulaire de création de phase avec '<dateDébutPhase>' '<dateFinPhase>' '<pour>' '<par>' '<nbDispoMin>'
    When Je clique sur le bouton 'Créer'
    Then La page de création de phase doit apparaître en étant remplie
    When Je clique sur le bouton 'Précédent'
    Then La page de création d'un type de soutenance doit apparaître en étant préremplie
    When Je clique sur le bouton 'Précédent'
    Then La page de création d'une campagne doit apparaître en étant préremplie
    When Je clique sur le bouton 'Suivant'
    Then La page de création d'un type de soutenance doit apparaître en étant préremplie
    When Je clique sur le bouton 'Suivant'
    Then La page de création de phase doit apparaître
    When Je clique sur le bouton 'Terminer'
    Then La page de la campagne 'Campagne de test Selenium N°1' doit apparaître
    When Je clique sur le lien 'Se déconnecter'
    Then La page de login doit apparaître
    Examples:
    | cheminDuFichierCampagne | dateDébutCampagne | dateFinCampagne | nbSalle | cheminDuFichierSoutenances | dateDébutPhase | dateFinPhase | pour | par | nbDispoMin |
    | /home/planninggo/Documents/planninggo/src/test/resources/csv/selenium/campagnes_ok.csv | 01/09/2050 | 15/09/2050 | 10 | /home/planninggo/Documents/planninggo/src/test/resources/csv/selenium/soutenances_ok.csv | 01/07/2050 | 01/08/2050 | Etudiant | Etudiant | 4 |

    ###
  Scenario Outline: Création d'une campagne (sans phase, peu de participants) et génération du planning
    When J'accède à la page http://localhost:8080/app/campaign
    Then La page de mes campagnes doit apparaître
    When Je clique sur le lien/bouton 'Nouvelle campagne'
    Then La page de création d'une campagne doit apparaître
    When Je remplie le formulaire de création de campagne avec '<nomCampagne>' '<dateDébutCampagne>' '<dateFinCampagne>' '<nbSalle>'
    When J'envoie le formulaire
    Then La page de création d'un type de soutenance doit apparaître
    When Je clique sur le lien/bouton 'Importer'
    Then La page d'importation de type de soutenances doit apparaître
    When Je choisis le fichier '<cheminDuFichierSoutenances>'
    When J'envoie le formulaire
    Then La page de création d'un type de soutenance doit apparaître en étant préremplie
    When Je clique sur le bouton 'Suivant'
    Then La page de création de phase doit apparaître
    When Je clique sur le bouton 'Terminer'
    Then La page de la campagne '<nomCampagne>' doit apparaître
    #When J'ouvre le menu déroulant 'Importer' et je clique sur 'Participants'
    #Then La page d'importation de participants doit apparaître
    #When Je choisis le fichier '<cheminDuFichierDeParticipants>'
    #When J'envoie le formulaire
    #Then La page de la campagne '<nomCampagne>' doit apparaître
    #When Je clique sur le lien/bouton 'Générer le planning'
    #Then Le page du planning doit apparaître
    #When Je clique sur le lien 'Se déconnecter'
    #Then La page de login doit apparaître
    Examples:
      | nomCampagne | dateDébutCampagne | dateFinCampagne | nbSalle | cheminDuFichierSoutenances | cheminDuFichierDeParticipants |
      | Campagne de test Selenium N°2 | 01/09/2050 | 02/09/2050 | 2 | /home/planninggo/Documents/planninggo/src/test/resources/csv/selenium/soutenances_ok_bis.csv | /home/planninggo/Documents/planninggo/src/test/resources/csv/selenium/participants_ok_bis.csv |