@login

Feature: Connexion à l'application

  ###
  Background: Accès à la page de login
    When J'accède à la page http://localhost:8080/app/login
    Then La page de login doit apparaître

  ###
  Scenario Outline: Connexion avec un compte administrateur
    When J'envoie le formulaire de login avec '<email>' '<password>'
    Then La page de gestion de comptes doit apparaître
    When Je clique sur le lien 'Se déconnecter'
    Then La page de login doit apparaître
  Examples:
    | email | password |
    | selenium.administrateur@planninggo.fr | licorne |

  ###
  Scenario Outline: Connexion avec un compte organisateur
    When J'accède à la page http://localhost:8080/app/login
    When J'envoie le formulaire de login avec '<email>' '<password>'
    Then La page de mes campagnes doit apparaître
    When Je clique sur le lien 'Se déconnecter'
    Then La page de login doit apparaître
    Examples:
      | email | password |
      | selenium.organisateur@planninggo.fr | licorne |

  ###
  Scenario Outline: Connexion avec un compte etudiant ou invité qui a déjà accepté la RGPD
    When J'envoie le formulaire de login avec '<email>' '<password>'
    Then La page d'accueil doit apparaître
    When Je déplace la souris sur le lien 'Mon profil'
    When Je clique sur le lien 'Deconnexion'
    Then La page de login doit apparaître
    Examples:
      | email | password |
      | selenium.etudiant@planninggo.fr | licorne |

  ###
  Scenario Outline: Connexion avec un compte étudiant qui n'a pas accepté la RGPD
    When J'envoie le formulaire de login avec '<email>' '<password>'
    Then La page de RGPD doit apparaître
    When Je coche la case avec l'ID 'accept'
    When Je clique sur le bouton 'Accepter'
    Then La page de mes campagnes doit apparaître
    When Je déplace la souris sur le lien 'Mon profil'
    When Je clique sur le lien 'Deconnexion'
    Then La page de login doit apparaître
    Examples:
      | email | password |
      | selenium.norgpd@planninggo.fr | licorne |

  ###
  Scenario Outline: Demande de nouveau mot de passe pour compte existant
    When Je clique sur le lien 'Des problèmes pour vous connecter ?'
    Then La page de demande de mot de passe doit apparaître
    When Je clique sur le lien 'Retour à l'écran de connexion'
    Then La page de login doit apparaître
    When Je clique sur le lien 'Des problèmes pour vous connecter ?'
    Then La page de demande de mot de passe doit apparaître
    When J'envoie le formulaire de demande de mot de passe pour '<email>'
    Then La page de confirmation de nouveau mot de passe doit apparaître
    When Je clique sur le lien 'Retour à l'écran de connexion'
    Then La page de login doit apparaître
    Examples:
    | email |
    | selenium.norgpd@planninggo.fr |