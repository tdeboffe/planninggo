INSERT INTO account (email,password,rgpd_accept) VALUES ('selenium.administrateur@planninggo.fr','$2a$10$ebozkwth8kBiHOU63LbOze0wkGt/ubTGqj2..n5bwNqAkb0kWwBsW','true');
INSERT INTO account (email,password,rgpd_accept) VALUES ('selenium.organisateur@planninggo.fr','$2a$10$ebozkwth8kBiHOU63LbOze0wkGt/ubTGqj2..n5bwNqAkb0kWwBsW','true');
INSERT INTO account (email,password,rgpd_accept) VALUES ('selenium.etudiant@planninggo.fr','$2a$10$ebozkwth8kBiHOU63LbOze0wkGt/ubTGqj2..n5bwNqAkb0kWwBsW','true');
INSERT INTO account (email,password,rgpd_accept) VALUES ('selenium.norgpd@planninggo.fr','$2a$10$ebozkwth8kBiHOU63LbOze0wkGt/ubTGqj2..n5bwNqAkb0kWwBsW','false');

INSERT INTO account_role (role_id,user_id) VALUES (2,(SELECT user_id FROM account WHERE email LIKE 'selenium.administrateur@planninggo.fr'));
INSERT INTO account_role (role_id,user_id) VALUES (1,(SELECT user_id FROM account WHERE email LIKE 'selenium.organisateur@planninggo.fr'));
INSERT INTO account_role (role_id,user_id) VALUES (3,(SELECT user_id FROM account WHERE email LIKE 'selenium.etudiant@planninggo.fr'));
INSERT INTO account_role (role_id,user_id) VALUES (3,(SELECT user_id FROM account WHERE email LIKE 'selenium.norgpd@planninggo.fr'));
