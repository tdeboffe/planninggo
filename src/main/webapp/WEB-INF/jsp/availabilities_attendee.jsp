<%@ page import="java.util.Map" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Saisie des disponibilités</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-combobox/1.1.8/css/bootstrap-combobox.min.css">
    <link rel="stylesheet" href="/app/css/availabilities.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/app/availability/attendee">Saisie de disponibilités</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/app/defense/schedule">Mon planning</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container-fluid mt-3">
    <h1 class="text-center" id="title">Saisie des disponibilités</h1>
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>Message d'explication !</strong> Pour saisir des disponibilités vous devez cliquer
        sur les demi-journées (rouge pour indisponible, vert pour disponible). Les informations affichées en blanc
        dans les cases représentent les disponibilités actuelles de votre homologue.
        <br/><strong>ATTENTION : Les disponibilités sont enregistrées à chaque sélection/désélection</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <hr class="my-4">
</div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-combobox/1.1.8/js/bootstrap-combobox.min.js"></script>
<script src="/app/js/utility.js"></script>
<script src="/app/js/availabilities.js"></script>
<script>
    getAvailabilitiesPerso("<%= session.getAttribute("map") %>")
</script>
</html>
