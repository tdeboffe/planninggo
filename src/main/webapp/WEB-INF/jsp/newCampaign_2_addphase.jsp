<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.DefenseTypeEntity" %>
<%@ page import="java.util.Collection" %>
<%@ page import="fr.planninggo.spring.campaign.entity.JuryMemberEntity" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fr.planninggo.spring.user.entity.RoleEntity" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.TreeSet" %>
<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Ajout d'une phase</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Ajout d'une phase</h1>
    <hr class="my-4">
    <%= message(session) %>
    <form method="post" action="createPhase">
        <input type="hidden" name="campaign" value="${campaign}">
        <input type="hidden" name="phase" value="${phase}">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputBegin">Du :</label>
                <input type="date" name="dateBegin" class="form-control" id="inputBegin" value="${phase.getBegin()}" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputEnd">Au :</label>
                <input type="date" name="dateEnd" class="form-control" id="inputEnd" value="${phase.getEnd()}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="rolePour">Pour :</label>
                <select name="rolePour" id="rolePour" required class='form-control'>
                    <%= getRoles(request.getSession()) %>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="rolePar">Par :</label>
                <select name="rolePar" id="rolePar" required class='form-control'></select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="availabilityNumber">Nombre de disponibilités minimum :</label>
                <input class="form-control" type="number" min="3" id="availabilityNumber" name="availabilityNumber" value="${phase.getAvailabilityNumber()}" required>
            </div>
        </div>
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Créer</button>
    </form>
    <form class="mt-2" method="post" action="backToPhase">
        <input type="hidden" name="campaign" value="${campaign}">
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Annuler</button>
    </form>
</div>
<%! private String getRoles(HttpSession session) {
        StringBuilder sb = new StringBuilder();
        CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
        if (campaignEntity != null && campaignEntity.getDefensesType() != null) {
            Set<RoleEntity> roleEntitySet = campaignEntity.getDefensesType().stream()
                    .map(DefenseTypeEntity::getJuryMembers).flatMap(Collection::stream)
                    .map(JuryMemberEntity::getRole).collect(Collectors.toSet());
            roleEntitySet.forEach(member -> sb.append("<option value='").append(member.getName()).append("'>")
                            .append(member.getName()).append("</option>"));
        }
        sb.append("<option value='Etudiant'>Etudiant</option>");
        return sb.toString();
    }
%>
<%! private String message(HttpSession session)  {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("msg") != null) {
        if (((String)session.getAttribute("msg")).startsWith("ERREUR : ")) {
            sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        }
        sb.append(session.getAttribute("msg"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("msg");
    }
    return sb.toString();
}%>
<script src="/app/js/newCampaign.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
