<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.PhaseEntity" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DateFormat" %>
<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Phases</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="/app/css/newCampaign3.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>

    </div>
</nav>
<div class="container-fluid mt-3">
    <h1 class="text-center">Phases</h1>
    <hr class="my-4">
    <form class="form-line" method="post" action="addStep">
        <input type="hidden" name="campaign" value="${campaign}">
        <button class="btn btn-outline-success" type="submit">Nouvelle phase</button>
    </form>
    <table class="table text-center table-striped mt-3"><%= outPutPhase(request.getSession()) %>
    </table>
    <hr class="my-4">
    <div class="navDiv">
        <form class="btn-duo" method="post" action="defensetype">
            <button class="btn btn-primary btn-duo btn-right text-uppercase" type="submit">Précédent</button>
        </form>
        <form class="btn-duo" method="post" action="createNewCampaign">
            <button class="btn btn-primary btn-duo btn-right text-uppercase" type="submit">Terminer</button>
        </form>
    </div>
</div>

<%! String outPutPhase(HttpSession session) {
    StringBuilder stringBuilder = new StringBuilder();
    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    if (campaignEntity != null && campaignEntity.getPhases() != null && !campaignEntity.getPhases().isEmpty()) {
        stringBuilder.append("<thead><th scope='col'>N°</th><th scope='col'>De</th><th scope='col'>Jusqu'à</th><th scope='col'>Pour</th><th scope='col'>Par</th><th scope='col'>Nombre de disponibilités minimums</th></thead>");
        int cpt = 0;
        for (PhaseEntity p : campaignEntity.getPhases()) {
            stringBuilder.append("<tr id='ligne" + cpt + "'><td>").append(p.getCode()).append("</td>");
            stringBuilder.append("<td>").append(formatter.format(p.getBegin())).append("</td>");
            stringBuilder.append("<td>").append(formatter.format(p.getEnd())).append("</td>");
            stringBuilder.append("<td>").append(p.getWrittenFor()).append("</td>");
            stringBuilder.append("<td>").append(p.getWrittenBy()).append("</td>");
            stringBuilder.append("<td>").append(p.getAvailabilityNumber()).append("</td>");
            stringBuilder.append("<td>");
            stringBuilder.append("<form method='post' onclick='this.submit()'; action='/app/campaign/removeTempPhase'><input type='hidden' name='code' value='" + p.getCode() + "'/><i class='fas fa-trash-alt'></i></form></td></tr>");
            cpt++;
        }
    }
    return stringBuilder.toString();
} %>
</body>
</html>
