<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Générer un nouveau mot de passe</title>
    <link rel="icon" href="/app/logo.png">
    <link rel="stylesheet" href="/app/css/login.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <div class="text-center"><img alt="LOGO" src="/app/images/logo.png"/></div>
                    <h5 class="card-title text-center">Informations</h5>
                    <%= message(session) %>
                </div>
            </div>
            <a class="btn btn-lg btn-primary btn-block text-uppercase" href="/app/login">Retour à la page de connexion</a>
        </div>
    </div>
</div>
<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("mail") != null) {
        if (((String) session.getAttribute("mail")).startsWith("ERREUR : ")) {
            sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
            sb.append(session.getAttribute("mail"));
        } else {
            sb.append("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">");
            sb.append("Mail envoyé avec succès");
        }
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("mail");
    }
    return sb.toString();
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
