<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="/app/logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>404 - Direction la lune</title>
    <link rel="stylesheet" type="text/css" href="/app/css/404.css"/>
</head>
<body class="bg-purple">
<div class="stars">
    <div class="central-body">
        <img class="image-404" src="/app/images/404.svg" width="300px">
        <a href="/app" class="btn-go-home">Retour sur Terre</a>
    </div>
    <div class="objects">
        <img class="object_rocket" src="/app/images/rocket.svg" width="40px">
        <div class="earth-moon">
            <img class="object_earth" src="/app/images/earth.svg" width="100px">
            <img class="object_moon" src="/app/images/moon.svg" width="80px">
        </div>
        <div class="box_astronaut">
            <img class="object_astronaut" src="/app/images/astronaut.svg" width="140px">
        </div>
    </div>
    <div class="glowing_stars">
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
    </div>
</div>

</body>
</html>
