<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="java.sql.Date" %>
<%@ page import="org.springframework.transaction.annotation.Transactional" %>
<%@ page import="java.util.TreeSet" %>
<%@ page import="fr.planninggo.spring.attendee.entity.AttendeeEntity" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Modification des disponibilités</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-combobox/1.1.8/css/bootstrap-combobox.min.css">
    <link rel="stylesheet" href="/app/css/availabilities.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign/<%= getId(session) %>">Accueil campagne <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container-fluid mt-3">
    <h1 class="text-center" id="title">Modification des disponibilités</h1>
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <strong>Message d'explication !</strong> Pour saisir des disponibilités vous devez cliquer
        sur les demi-journées (rouge pour indisponible, vert pour disponible). Les informations affichées en blanc
        dans les cases représentent les disponibilités actuelles de votre homologue.
        <br/><strong>ATTENTION : Les disponibilités sont enregistrées à chaque sélection/désélection</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <hr class="my-4">
    <label for="attendee">Sélectionner le participant</label>
    <select id="attendee" class="form-control" onchange="getAvailabilities(this.value, <%= getId(session) %>)">
        <option></option>
        <%= getList(session) %>
    </select>
    <hr class="my-4">
    <div class="tab"></div>
    <div class="content"></div>
</div>
    <%! @Transactional String getList(HttpSession session) {
        CampaignEntity campaignEntity = ((CampaignEntity) session.getAttribute("campaign"));
        StringBuilder stringBuilder = new StringBuilder();
        TreeSet<AttendeeEntity> set = new TreeSet<>(campaignEntity.getSupervisors());
        set.addAll(campaignEntity.getGuests());
        set.forEach(s -> stringBuilder.append("<option value='").append(s.getId()).append("'>").append(s.getEmail()).append("</option>"));
        return stringBuilder.toString();
    } %>
    <%! int getId(HttpSession session) { return ((CampaignEntity) session.getAttribute("campaign")).getId(); } %>

    <%! private Date getDateBegin(HttpSession session) {
        CampaignEntity campaignEntity = ((CampaignEntity) session.getAttribute("campaign"));
        return campaignEntity.getCampaignBegin();
    } %>

    <%! private Date getDateEnd(HttpSession session) {
        CampaignEntity campaignEntity = ((CampaignEntity) session.getAttribute("campaign"));
        return campaignEntity.getCampaignEnd();
    } %>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-combobox/1.1.8/js/bootstrap-combobox.min.js"></script>
<script src="/app/js/utility.js"></script>
<script src="/app/js/availabilities.js"></script>
<script>
    var nbDayDisplay=5;
    var nbDay = dayDiff(toDate("<%=getDateBegin(session)%>"),toDate("<%=getDateEnd(session)%>"));
    for(var i = 1; i<=Math.floor(nbDay/nbDayDisplay);i++){
        $('.tab').append("<button class=\"tablink\" onclick=openCalendar(event,'week"+i+"') style='display:none'>Semaine " + i + "</button>");
    }


 function dayDiff(d1,d2) {
     var timeDiff = Date.parse(d2) - Date.parse(d1);
     daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
     return daysDiff;
    }

    function toDate(str){
        var infos = str.split('-');
        return new Date(infos[0], infos[1], infos[2]);
    }
</script>
</html>
