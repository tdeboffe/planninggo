<%@ page import="fr.planninggo.spring.campaign.entity.DefenseEntity" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fr.planninggo.spring.attendee.entity.SupervisorStudentEntity" %>
<%@ page import="java.util.Collection" %>
<%@ page import="fr.planninggo.spring.attendee.entity.AttendeeEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.JuryMemberEntity" %>
<%@ page import="fr.planninggo.spring.user.entity.RoleEntity" %>
<%@ page import="java.util.HashSet" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/campaign_home.css">
    <link rel="stylesheet" href="/app/css/navBar.css">
    <title>Mon planning</title>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/app/availability/attendee">Saisie de disponibilités</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/app/defense/schedule">Mon planning</a>
            </li>
            <%=getButtons(session)%>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="/app/profil" class="dropdown-toggle nav-link">Mon profil</a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a class="dropdown-item" href="/app/logout">Deconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </form>
    </div>
</nav>
<% if (session.getAttribute("campaign") != null) { session.removeAttribute("campaign"); } %>
<div class="container-fluid mt-3">
    <h1 class="text-center">Mon planning</h1>
    <hr class="my-4">
    <table class="table table-striped mt-3">
        <thead>
        <tr>
            <th scope="col">&Eacute;tudiant</th>
            <th scope="col">Exercice</th>
            <th scope="col">Date de Début</th>
            <th scope="col">Durée</th>
            <th scope="col">Nom de la Salle</th>
            <th scope="col">Membres du jury</th>
            <th scope="col">Président du jury</th>
            <th scope="col">Adresse du président du jury</th>
        </tr>
        </thead>
        <tbody id="content"><%= getDefensesByPerson(session) %></tbody>
    </table>
</div>
<%! private String getDefensesByPerson(HttpSession session) {
    Set<DefenseEntity> defenseEntities = (Set<DefenseEntity>) session.getAttribute("defenses");
    StringBuilder stringBuilder = new StringBuilder();
    if(defenseEntities != null) {
        for (DefenseEntity defenseEntity : defenseEntities) {
            //On récupère les attendees de la soutenance
            Set<AttendeeEntity> attendees = new HashSet<>(defenseEntity.getAttendeeGuests());
            attendees.addAll(defenseEntity.getStudent().getSupervisorsSet());
            String attendeeEntities = attendees.stream().map(attendeeEntity -> "<a href='mailto:" + attendeeEntity.getEmail() + "'> " + attendeeEntity.getFirstname()
                    + " " + attendeeEntity.getLastname() + "</a>").collect(Collectors.joining(", "));

            //On récupère les attendees qui sont présidents
            Set<RoleEntity> rolePresident = defenseEntity.getDefenseType().getJuryMembers().stream().filter(JuryMemberEntity::isPresident).map(JuryMemberEntity::getRole).collect(Collectors.toSet());

            Set<AttendeeEntity> presidents = new HashSet<>();
            for(AttendeeEntity attendeeEntity : attendees) {
                if(attendeeEntity.getAccount().getRoleEntities().stream().anyMatch(rolePresident::contains)) {
                    presidents.add(attendeeEntity);
                }
            }
            String presidentsEntities = presidents.stream().map(attendeeEntity -> "<a href='mailto:" + attendeeEntity.getEmail() + "'> " + attendeeEntity.getFirstname()
                    + " " + attendeeEntity.getLastname() + "</a>").collect(Collectors.joining(", "));
            String adresses = presidents.stream().map(attendeeEntity -> (attendeeEntity.getAddress().equals("null") ? "Adresse non renseignée": attendeeEntity.getAddress()) + ", " +
                    (attendeeEntity.getPostalCode().equals("null") ? "Code postal non renseigné": attendeeEntity.getPostalCode()) + " " +
                    (attendeeEntity.getTown().equals("null") ? "Ville non renseignée": attendeeEntity.getTown())).collect(Collectors.joining(", "));

            //On affiche tout
            stringBuilder.append("<tr><td><a href='mailto:").append(defenseEntity.getStudent().getEmail()).append("'>").append(defenseEntity.getStudent().getFirstname()).append(" ").append(defenseEntity.getStudent().getLastname()).append("</a></td>")
                    .append("<td>").append(defenseEntity.getDefenseType().getDesc()).append("</td>")
                    .append("<td>").append(defenseEntity.getFormattedDefenseDate()).append("</td>")
                    .append("<td>").append(defenseEntity.getDefenseType().getDuration()).append("</td>")
                    .append("<td>").append(defenseEntity.getRoom().toString()).append("</td>")
                    .append("<td>").append(attendeeEntities).append("</td>")
                    .append("<td>").append(presidentsEntities).append("</td>")
                    .append("<td>").append(adresses).append("</td></tr>");
        }
    }
    return stringBuilder.toString();
}%>
<%! private String getButtons(HttpSession session){
    String buttons ="";
    if(session.getAttribute("buttons") != null){
        buttons = session.getAttribute("buttons").toString();
    }
    return buttons;
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
