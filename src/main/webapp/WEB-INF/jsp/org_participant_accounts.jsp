<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/searchBar.css">
    <link rel="stylesheet" href="/app/css/campaign_home.css">
    <title>Supprimer des participants</title>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign/<%= getId(session) %>">Accueil campagne<span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>

<div class="container-fluid mt-3">
    <h1 class="text-center">Supprimer des participants</h1>
    <hr class="my-4">
    <%= message(session) %>
    <div class="form-group pull-right">
        <input type="text" class="search form-control col-md-3" placeholder="Rechercher...">
    </div>
    <span class="counter pull-right"></span>
    <form method ="post" action="/app/campaign/<%= getId(session) %>/attendees/delete"  >
    <a href="#" style="display:inline-block"><button class="btn btn-outline-success">Supprimer</button></a>
    <table class="table table-striped mt-3 table-hover table-bordered results">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Prénom</th>
                <th scope="col">Nom</th>
                <th scope="col">Adresse email</th>
                <th scope="col">Rôle(s)</th>
            </tr>
            </thead>
            <tbody id="content">
            <tr class="warning no-result">
                <td colspan="5"><i class="fa fa-warning"></i> Pas de résultat</td>
            </tr>
            <%= session.getAttribute("accounts") %>
            </tbody>
        </table>
    </form>
</div>
<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if(session.getAttribute("msg")!=null){
       sb.append(session.getAttribute("msg"));
        session.removeAttribute("msg");
    }
    return sb.toString();
}%>
<%! int getId(HttpSession session) { return ((CampaignEntity) session.getAttribute("campaign")).getId(); } %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="/app/js/searchBar.js"></script>
</body>
</html>
