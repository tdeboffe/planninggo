<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Ajout d'un type de soutenance</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Définition des rôles</h1>
    <hr class="my-4">
    <button id="add" class="btn btn-outline-success">Ajouter un rôle</button>
    <input type="text" id="roleNew" class="form-control" style="display: none" onclick="return false;">
    <button id="validateNewRole" class="btn btn-outline-success" style="display: none" onclick="return false;">Ajouter</button>
    <span id="errorRole" class="text-danger" style="display: none">Impossible de créer un role vide</span>
    <form method="post" action="createType" onsubmit="return beforeSubmit();">
        <input type="hidden" name="campaign" value="${campaign}">
        <input type="hidden" name="typeCode" value="${typeCode}">
        <input type="hidden" name="typeDesc" value="${typeDesc}">
        <input type="hidden" name="typeMin" value="${typeMin}">
        <input type="hidden" name="typeHour" value="${typeHour}">
        <input type="hidden" name="juryNumber" value="${juryNumber}">
        <table class="table text-center table-striped mt-3" id="roles">
            <thead>
            <tr>
                <th scope="col">Rôle</th>
                <th scope="col" data-toggle="tooltip" data-placement="top" title="Encadrant direct de l'étudiant">Lié</th>
                <th scope="col" data-toggle="tooltip" data-placement="top" title="Encadrant d'un autre étudiant de la même filière">Filière</th>
                <th scope="col" data-toggle="tooltip" data-placement="top" title="Président de la soutenance">Président</th>
            </tr>
            </thead>
            <tbody><%= printTable((Integer) session.getAttribute("juryMember")) %>
            </tbody>
        </table>
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Créer</button>
    </form>
    <form class="mt-2" method="post" action="backToAddType">
        <input type="hidden" name="campaign" value="${campaign}">
        <input type="hidden" name="typeCode" value="${typeCode}">
        <input type="hidden" name="typeDesc" value="${typeDesc}">
        <input type="hidden" name="typeMin" value="${typeMin}">
        <input type="hidden" name="typeHour" value="${typeHour}">
        <input type="hidden" name="juryNumber" value="${juryNumber}">
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Précédent</button>
    </form>
</div>
<%! String printTable(int number) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < number; i++) {
        stringBuilder.append("<tr><td><select class='form-control' id=\"roles\" type=\"text\" name=\"roleName\" required/><option></option><option v-for=\"option in options\" v-bind:value=\"option.name\">{{ option.name }}</option></select></td>");
        stringBuilder.append(" <td><input onclick=\"updateValue(this, this.value);\" type=\"checkbox\" value=\"false\" name=\"isStudent\"></td>\n" +
                "                <td><input onclick=\"updateValue(this, this.value);\" type=\"checkbox\" value=\"false\" name=\"isStudy\"></td>\n" +
                "                <td><input onclick=\"updateValue(this, this.value);\" type=\"checkbox\" value=\"false\" name=\"isPresident\"></td></tr>");
    }
    return stringBuilder.toString();
}%>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.2/vue.js"></script>
<script src="/app/js/addRole.js"></script>
<script src="/app/js/utility.js"></script>
<script src="/app/js/getRole.js"></script>
</html>
