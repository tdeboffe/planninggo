<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/home.css">
    <link rel="stylesheet" href="/app/css/navBar.css">
    <title>Accueil</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/app/availability/attendee">Saisie de disponibilités</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/app/defense/schedule">Mon planning</a>
            </li>
            <%=getButtons(session)%>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="/app/profil" class="dropdown-toggle nav-link">Mon profil</a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a class="dropdown-item" href="/app/logout">Deconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container-fluid mt-3">
    <hr class="my-4">
    <%= message(session) %>
    <h1 class="text-center">Bienvenue sur PlanningGo !</h1>
    <div class="pub">Cette application permet la gestion des soutenances de l'UPEM. Elle a été développée dans le cadre du LAST PROJECT de la filière INFORMATIQUE de l'ESIPE.
        <br/>Les développeurs sont :
        <div class="text-center list-group">
            <a href="mailto:guillaume.gorret77@gmail.com" class="list-group-item list-group-item-action">Guillaume GORRET (Chef de projet)</a>
            <a href="mailto:oumaima.idali@gmail.com" class="list-group-item list-group-item-action">Oumaïma IDALI</a>
            <a href="mailto:deboffe.theo@gmail.com" class="list-group-item list-group-item-action">Théo DEBOFFE</a>
            <a href="mailto:llopis.kevin@outlook.fr" class="list-group-item list-group-item-action">Kévin LLOPIS</a>
            <a href="mailto:anthony.nagibsa@gmail.om" class="list-group-item list-group-item-action">Anthony NAGIB SALEEB</a>
            <a href="mailto:kevingn.ngu@gmail.com" class="list-group-item list-group-item-action">Kévin NGUYEN</a>
            <a href="mailto:vincent.vivier.95@gmail.com" class="list-group-item list-group-item-action">Vincent VIVIER</a>
        </div>
    </div>
</>
<% if (session.getAttribute("idDispo") != null) { session.removeAttribute("idDispo"); } if (session.getAttribute("mailDispo") != null) { session.removeAttribute("mailDispo"); } %>
<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("errorDispo") != null) {
        sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        sb.append(session.getAttribute("errorDispo"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("errorDispo");
    }
    return sb.toString();
} %>

<%! private String getButtons(HttpSession session){
    String buttons ="";
    if(session.getAttribute("buttons") != null){
        buttons = session.getAttribute("buttons").toString();
    }
    return buttons;
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
