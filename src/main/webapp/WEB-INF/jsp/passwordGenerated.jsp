<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="images/logo.png"/>
    <title>Nouveau mot de passe</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <div class="text-center"><img alt="LOGO" src="images/logo.png"/></div>
                        <h5 class="card-title text-center">Votre nouveau mot de passe !</h5>
                        <div class="text-center">
                            <p class="h6">Pensez à le noter : <br/><b id="password">{{Toscreen}}</b></p>
                        </div>
                        <hr class="my-4">
                        <form class="form-signin">
                            <a class="btn btn-primary btn-block text-uppercase" href="login">Retour à l'écran de connexion</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.2/vue.js"></script>
    <script src="js/utility.js"></script>
    <script src="js/passwordGenerated.js"></script>
</body>
</html>
