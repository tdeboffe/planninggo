<%@ page import="fr.planninggo.spring.user.entity.AccountEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="java.util.stream.Collectors" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nouvelle campagne</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="/app/css/paramg.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Paramètres généraux</h1>
    <hr class="my-4">
    <%= message(session) %>
    <form method="post" action="/app/campaign/type">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputCampaign">Nom de la campagne :</label>
                <input type="text" name="campaignName" class="form-control" id="inputCampaign" placeholder="Soutenances A3 ESIPE 2018/2019" value="${campaign.getName()}" required autofocus>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputBegin">Du :</label>
                <input type="date" name="campaignBegin" class="form-control" id="inputBegin" value="${campaign.getCampaignBegin()}" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputEnd">Au :</label>
                <input type="date" name="campaignEnd" class="form-control" id="inputEnd" value="${campaign.getCampaignEnd()}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label for="inputRoomNumber">Nombre de salles :</label>
                <input type="number" name="roomNumber" class="form-control" id="inputRoomNumber" min="1" max="30" value="${campaign.getRoomNumber()}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-10">
                <label for="inputNewOrga">Ajouter d'autre(s) organisateur(s)</label>
                <input type="email" class="form-control" id="inputNewOrga" placeholder="mail@gmail.com">
            </div>
            <div class="form-group col-md-1">
                <i class="fas fa-plus" id="add"></i>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <textarea class="form-control" name="orgList" id="listOrga" rows="1" cols="50" readonly><%! private String output(HttpSession session) {
                        if (session.getAttribute("campaign") != null) {
                            return ((CampaignEntity) session.getAttribute("campaign")).getOrganizers().stream().map(AccountEntity::getEmail).collect(Collectors.joining(","));
                        }
                        return "";
                    }%><%= output(session) %></textarea>
            </div>
        </div>
        <button class="btn btn-primary btn-duo text-uppercase" onclick="window.location.href = '/app/campaign/${campaign.getId()}'; return false;">Annuler</button>
        <button class="btn btn-primary btn-duo btn-right text-uppercase" type="submit">Suivant</button>
    </form>
</div>
<%! String message(HttpSession session)  {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("msg") != null) {
        if (((String)session.getAttribute("msg")).startsWith("ERREUR : ")) {
            sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        }
        sb.append(session.getAttribute("msg"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("msg");
    }
    return sb.toString();
}%>
<script src="/app/js/newCampaign.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="/app/js/addOrg.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
