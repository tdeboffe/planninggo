<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="images/logo.png"/>
    <meta http-equiv="Content-Type" content="text/html">
    <title>RGPD</title>
    <link rel="stylesheet" href="css/rgpd.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-11 col-md-11 col-lg-11 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <div class="text-center"><img alt="LOGO" src="images/logo.png"/></div>
                    <h3 class="card-title text-center">Règlement général sur la protection de vos données
                        personnelles</h3>
                    <p>
                        L’ESIPE prend toutes les précautions nécessaires pour s’assurer que vos données personnelles
                        sont traitées en toute sécurité et en conformité avec la réglementation.
                        Vous trouverez ci-dessous des informations détaillées sur le traitement de vos données
                        personnelles dans le cadre de l’utilisation de cette application.
                    </p>
                    <h4 class="card-title text-center">Qui sont les responsables du traitement de vos données
                        personnelles ?</h4>
                    <p>
                        Vos données personnelles sont traitées par l’ESIPE (Ecole Supérieure d’Ingénieurs de
                        l’université Paris-Est Marne-la-Vallée).
                        L’ESIPE est située au 5 boulevard Descartes 77420 Champs-sur-Marne. Les coordonnées de son
                        délégué à la protection des données sont les suivantes : 5 boulevard Descartes 77420
                        Champs-sur-Marne et etienne.duris@u-pem.fr.
                    </p>

                    <h4 class="card-title text-center">Pourquoi vos données personnelles sont-elles utilisées ?</h4>
                    <p>
                        Nous collectons vos données personnelles lorsque vous créez un compte sur notre application, et
                        lorsque vous l’utilisez pour renseigner vos disponibilités ou toute autre information.
                        Nous utilisons vos données personnelles afin de :
                    <ul>Gérer vos demandes (demande d’information par exemple)</ul>
                    <ul>Gérer votre compte utilisateur</ul>
                    <ul>Gérer l'enregistrement de vos disponibilités et la gestion des soutenances</ul>
                    <ul>Répondre à vos demandes éventuelles d’exercice de droits en relation avec vos données
                        personnelles
                    </ul>
                    De manière générale, nous nous engageons à collecter uniquement les données personnelles qui sont
                    nécessaires à chacune des finalités pour lesquelles nous traitons vos données.
                    </p>

                    <h4 class="card-title text-center" >Combien de temps sont conservées vos données ?</h4>
                    <p>Conformément à la réglementation, nous nous engageons à ne conserver vos données personnelles que
                        pour la durée nécessaire à l’accomplissement des finalités.
                        Afin de calculer ces durées de conservation, nous nous appuyons notamment sur les critères
                        suivants :
                    <ul>Le temps nécessaire pour traiter votre demande,</ul>
                    <ul>La durée pendant laquelle votre compte utilisateur est ouvert, sauf en cas d’inactivité pendant
                        3 ans,
                    </ul>
                    <ul>La durée pendant laquelle se déroule la campagne,</ul>
                    <ul>La nécessité de conserver un certain historique de vos interactions avec nous, pour la bonne
                        gestion de notre relation commerciale.
                    </ul>
                    </p>

                    <h4 class="card-title text-center">Qui a accès à vos données ?</h4>
                    <p>
                        Nous pouvons être amenés à partager vos données personnelles avec d’autres entités, afin de
                        traiter tout ou partie de vos données personnelles, dans la limite nécessaire à
                        l’accomplissement des tâches qui leur sont confiées (par exemple, pour l’hébergement de notre
                        application mobile, pour la fourniture de services liés à l’exploitation ou la maintenance du
                        site internet).
                        Vos données personnelles seront partagées uniquement avec les services habilités, en raison de
                        leurs fonctions et dans la limite de leurs attributions respectives.
                        Vos données sont hébergées sur des serveurs situés dans l’Espace Economique Européen (EEE).
                    </p>

                    <h4 class="card-title text-center">Vos droits</h4>
                    <p>
                        Vous bénéficiez de plusieurs droits concernant le traitement de vos données personnelles :
                    <ul>Un droit d'accès à vos données personnelles, c’est-à-dire le droit d’en recevoir une copie,</ul>
                    <ul>Un droit de rectification, si vos données s’avéraient erronées ou périmées, ce qui nous
                        permettra de nous conformer à notre obligation d’avoir des données à jour vous concernant,
                    </ul>
                    <ul>Un droit à la portabilité de vos données, c’est-à-dire le droit de recevoir les données
                        personnelles que nous vous avez fournies, dans un format informatique structuré, couramment
                        utilisé.
                    </ul>
                    Pour exercer n’importe lequel de vos droits, vous pouvez nous contacter, en justifiant de votre
                    identité, par email à l’adresse etienne.duris@u-pem.fr ou par courrier postal à l’adresse 5
                    boulevard Descartes 77420 Champs-sur-Marne.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <form method="post" action="account/acceptRgpd">
        <input type="checkbox" id="accept" onchange="enableButton()"/> En cochant cette case, vous acceptez les
        conditions d'utilisations de ce site.
        <br/>
        <button class="btn btn-lg btn-primary btn-block text-uppercase" id="accept_button" disabled=true>Accepter</button>
    </form>
</div>
<script src="js/rgpd.js"></script>
</body>
</html>
