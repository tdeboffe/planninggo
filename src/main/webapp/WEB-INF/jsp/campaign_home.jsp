<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="fr.planninggo.spring.campaign.repository.CampaignRepository" %>
<%@ page import="fr.planninggo.spring.user.entity.AccountEntity" %>
<%@ page import="fr.planninggo.spring.user.repository.RoleRepository" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<%@ page import="fr.planninggo.spring.user.entity.RoleEntity" %>
<%@ page import="java.util.Set" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/campaign_home.css">
    <title>Mes campagnes</title>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <%=getButtons(session)%>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<% if (session.getAttribute("campaign") != null) { session.removeAttribute("campaign"); }
   if (session.getAttribute("defenseTypes") != null) { session.removeAttribute("defenseTypes"); };
%>
<div class="container-fluid mt-3">
    <h1 class="text-center">Mes campagnes</h1>
    <hr class="my-4">
    <%= message(session) %>
    <a href="/app/campaign/create"><button class="btn btn-outline-success">Nouvelle campagne</button></a>
    <a href="/app/campaign/import/campaign"><button class="btn btn-outline-success">Importer une campagne</button></a>
    <table class="table table-striped mt-3">
        <thead>
        <tr>
            <th scope="col"></th>
            <th scope="col">Nom de la campagne</th>
            <th scope="col">Date de début</th>
            <th scope="col">Date de fin</th>
            <th scope="col">&Eacute;tat de la campagne</th>
            <th scope="col">Créer à partir de ...</th>
        </tr>
        </thead>
        <tbody id="content"><%= getCampaigns(session) %></tbody>
    </table>
</div>

<%! private String getButtons(HttpSession session) {
    String buttons = "";
    if(session.getAttribute("buttons")!=null){
        buttons = session.getAttribute("buttons").toString();
    }
    return buttons;
}
    %>

<%! private String getCampaigns(HttpSession session) {
    CampaignRepository campaignRepository = (CampaignRepository) session.getAttribute("repo");
    AccountEntity account = (AccountEntity) session.getAttribute("accountEntity");
    List<CampaignEntity> list = (List<CampaignEntity>) campaignRepository.findAll();
    StringBuilder stringBuilder = new StringBuilder();
    for (CampaignEntity l : list) {
        if (l.getOrganizers().stream().map(AccountEntity::getEmail).collect(Collectors.toList()).contains(account.getEmail())) {
            stringBuilder.append("<tr><th scope=\"row\"><a href='/app/campaign/" + l.getId() + "'><i class=\"fas fa-eye\"></i></a></th>");
            stringBuilder.append("<td>").append(l.getName()).append("</td>")
                    .append("<td>").append(new SimpleDateFormat("dd/MM/yyyy").format(l.getCampaignBegin())).append("</td>")
                    .append("<td>").append(new SimpleDateFormat("dd/MM/yyyy").format(l.getCampaignEnd())).append("</td>")
                    .append("<td>").append(l.getCampaignState()).append("</td>");
            stringBuilder.append("<td><a href='campaign/createFrom/" + l.getId() + "'><i class=\"fas fa-plus\"></i></a></td></tr>");
        }
    }
    return stringBuilder.toString();
}%>
<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("msgImportCampaign") != null) {
        sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        sb.append(session.getAttribute("msgImportCampaign"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("msgImportCampaign");
    }
    return sb.toString();
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
