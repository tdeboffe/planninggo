<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Import planning</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="/app/css/errorCsv.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign/<%= getId(session) %>">Accueil campagne <span class="sr-only">(current)</span></a>
            </li>
        </ul>

    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Import du planning</h1>
    <hr class="my-4">
    <form method="post" action="/app/campaign/<%= getId(session) %>/importPlanning" enctype="multipart/form-data">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputCampaign">Fichier à importer</label>
                <input type="file" name="file" class="form-control-file" id="inputCampaign" required>
            </div>
        </div>
        <button class="btn btn-primary btn-right text-uppercase" type="submit">Importer</button>
    </form>
    <span>Le fichier csv doit être encodé en UTF-8</span>
</div>
<%! int getId(HttpSession session) { return ((CampaignEntity) session.getAttribute("campaign")).getId(); } %>
</body>
</html>
