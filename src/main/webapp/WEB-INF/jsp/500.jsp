<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <%@ page contentType="text/html; charset=UTF-8" %>
    <title>500 - Erreur interne</title>
    <link rel="icon" href="/app/logo.png">
    <link rel="stylesheet" href="/app/css/500.css">
</head>
<body>
<p class="info">Une erreur interne est survenue :(</p>
<h1 class="internal">
    <span class="five">5</span>
    <span class="zero">0</span>
    <span class="zero">0</span>

</h1>
<footer class="link">
    <a href="/app">Retour à l'accueil</a>
</footer>
</body>
</html>
