<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="images/logo.png"/>
    <title>Générer un nouveau mot de passe</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="card card-signin my-5">
                    <div class="card-body">
                        <div class="text-center"><img alt="LOGO" src="images/logo.png"/></div>
                        <h5 class="card-title text-center">Demander un nouveau mot de passe</h5>
                        <form class="form-signin" action="account/generateURL" method="post">
                            <div class="form-label-group">
                                <input type="email" id="inputEmail" name="mail" class="form-control" placeholder="Adresse mail" required autofocus>
                                <label for="inputEmail">Adresse mail</label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Générer un nouveau mot de passe</button>
                            <a class="btn btn-primary btn-block text-uppercase" href="login">Retour à l'écran de connexion</a>
                            <hr class="my-4">
                            <div id="errorMessage"></div>
                            <i>*Après avoir demandé un nouveau mot de passe, vous recevrez un lien dans votre boîte mail qui vous fournira un nouveau mot de passe.</i>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/newPassword.js"></script>
</body>
</html>
