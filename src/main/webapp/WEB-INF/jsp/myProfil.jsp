<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Modification du profil</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/navBar.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <a class="nav-link" href="/app/">Accueil participant <span class="sr-only">(current)</span></a>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="/app/profil" class="dropdown-toggle nav-link">Mon profil</a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a class="dropdown-item" href="/app/logout">Deconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Mon profil</h1>
    <hr class="my-4">
    <form method="post" action="profil/change">
        <input type="hidden" name="campaign" value="${campaign}">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputFirstname">Prénom</label>
                <input type="text" name="firstname" class="form-control" id="inputFirstname" value="${firstname}"
                       readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputLastname">Nom</label>
                <input type="text" name="lastname" class="form-control" id="inputLastname" value="${lastname}" readonly>

            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="inputAddress">Adresse postale </label>
                <input type="text" name="address" class="form-control" id="inputAddress" value="${address}" readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="zipcode">Code postale </label>
                <input type="number" min="0" max="99999" name="zipcode" class="form-control" id="zipcode"
                       value="${zipcode}" readonly>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="city">Ville</label>
                <input type="text" name="city" class="form-control" id="city" value="${city}" readonly>
            </div>
        </div>

        <button class="btn btn-block btn-primary text-uppercase" type="submit">Modifier</button>
    </form>

    <div class="form-row mt-2">
        <div class="form-group col-md-12">
            <input type="hidden" name="campaign" value="${campaign}">
            <a href="/app/">
                <button class="btn btn-block btn-primary text-uppercase" type="submit">Annuler</button>
            </a>
        </div>
    </div>
</div>
</body>
</html>
