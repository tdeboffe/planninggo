<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.DefenseTypeEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.JuryMemberEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.stream.Collectors" %>
<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">

    <title>Types de soutenances</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="/app/css/newCampaign3.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container-fluid mt-3">
    <h1 class="text-center">Types de soutenances</h1>
    <%= message(session) %>
    <hr class="my-4">
    <form class="form-line" method="post" action="/app/campaign/addType">
        <button class="btn btn-outline-success" type="submit">Nouveau type</button>
    </form>
    <a href="/app/campaign/import/defenseType">
        <button class="btn btn-outline-success">Importer</button>
    </a>
    <%= getAllDefenseType(session) %>
    <table class="table text-center table-striped mt-3"><%= outputDefenseType(request.getSession()) %>
    </table>
    <hr class="my-4">
    <div class="navDiv">
        <form class="btn-duo" method="post" action="/app/campaign/create">
            <button class="btn btn-primary btn-duo btn-right text-uppercase" type="submit">Précédent</button>
        </form>
        <%= outputNext(session) %>
    </div>
</div>

<%! String outputNext(HttpSession session) {
    if (!((CampaignEntity) session.getAttribute("campaign")).getDefensesType().isEmpty()) {
        return "<form class=\"btn-duo\" method=\"post\" action=\"/app/campaign/steps\">\n" +
                "            <input type=\"hidden\" name=\"campaign\" value=\"${campaign}\">\n" +
                "            <button class=\"btn btn-primary btn-duo btn-right text-uppercase\" type=\"submit\">Suivant</button>\n" +
                "        </form>";
    }
    return "";
}%>

<%! private String getAllDefenseType(HttpSession session) {
    List<DefenseTypeEntity> defenseTypeEntitySet = (List<DefenseTypeEntity>) session.getAttribute("defenseTypes");
    StringBuilder stringBuilder = new StringBuilder();
    if (defenseTypeEntitySet.isEmpty()) {
        return "";
    }
    stringBuilder.append("<button class=\"btn btn-outline-success\" id='addexisting' onclick='showForm()'>Ajouter un type existant</button>\n");
    stringBuilder.append("<br/><label id='label' class='mt-3' style='display:none' for='select'>Sélectionnez un type : </label><select id='select' name='select' class='form-control col-sm-3 ml-1 mr-3' style='display:none'><option></option>");
    for (DefenseTypeEntity defenseTypeEntity : defenseTypeEntitySet) {
        stringBuilder.append("<option value='").append(defenseTypeEntity.getCode()).append("'>").append(defenseTypeEntity.getCode()).append(" : ").append(defenseTypeEntity.getDesc()).append("</option>");
    }
    stringBuilder.append("</select>");
    stringBuilder.append("<button id='select_btn' style='display:none;' class='btn btn-outline-success' onclick='addDefenseType()'>Ajouter</button>");
    return stringBuilder.toString();
} %>

<%! String outputDefenseType(HttpSession session) {
    StringBuilder stringBuilder = new StringBuilder();
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    if (campaignEntity != null && campaignEntity.getDefensesType() != null && !campaignEntity.getDefensesType().isEmpty()) {
        stringBuilder.append("<thead><th scope='col'>Code</th><th scope='col'>Description</th><th scope='col'>Durée</th><th scope='col'>Membres du jury</th><th scope='col'>Action</th></thead>");
        int cpt = 0;
        for (DefenseTypeEntity d : campaignEntity.getDefensesType()) {
            stringBuilder.append("<tr id='ligne" + cpt + "'><td>").append(d.getCode()).append("</td>");
            stringBuilder.append("<td>").append(d.getDesc()).append("</td>");
            stringBuilder.append("<td>").append(d.getDuration()).append("</td>");
            stringBuilder.append("<td>");
            stringBuilder.append(d.getJuryMembers().stream().map(JuryMemberEntity::getRolename).collect(Collectors.joining(", ")));
            stringBuilder.append("</td><td><form method='post' onclick='this.submit()'; action='/app/campaign/removeTempDType'><input type='hidden' name='code' value='" + d.getCode() + "'/><i class='fas fa-trash-alt'></form></td></tr>");
            cpt++;
        }
    }
    return stringBuilder.toString();
} %>

<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("msgImport") != null) {
        sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        sb.append(session.getAttribute("msgImport"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("msgImport");
    }
    return sb.toString();
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="/app/js/newCampaign3.js"></script>

</body>
</html>
