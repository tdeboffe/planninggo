<%@ page import="fr.planninggo.spring.attendee.repository.SupervisorRepository" %>
<%@ page import="fr.planninggo.spring.attendee.entity.SupervisorEntity" %>
<%@ page import="fr.planninggo.spring.attendee.entity.StudentEntity" %>
<%@ page import="java.util.Set" %>
<%@ page import="fr.planninggo.spring.attendee.entity.SupervisorStudentEntity" %>
<%@ page import="fr.planninggo.spring.attendee.repository.SupervisorStudentRepository" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/campaign_home.css">
    <title>Mes étudiants</title>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/">Accueil participant<span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="/app/profil" class="dropdown-toggle nav-link">Mon profil</a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a class="dropdown-item" href="/app/logout">Deconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </form>
    </div>
</nav>
<% if (session.getAttribute("campaign") != null) { session.removeAttribute("campaign"); } %>
<div class="container-fluid mt-3">
    <h1 class="text-center">Mes étudiants</h1>
    <table class="table table-striped mt-3">
        <thead>
        <tr>
            <th scope="col">Civilité</th>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Email</th>
            <th scope="col">Année</th>
            <th scope="col">Rôle</th>

        </tr>
        </thead>
        <tbody id="content"><%= getStudents(session) %></tbody>
    </table>
</div>
<%! private String getStudents(HttpSession session) {
    String students ="";
    if(session.getAttribute("students") != null){
        students = session.getAttribute("students").toString();
    }
    return students;
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
