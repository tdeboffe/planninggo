<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Modification d'un compte</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/accounts">Accueil administrateur <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<form method="get" action="<%=getId(session)%>/save">
    <div class="container pt-3 bg-light">
        <h1 class="text-center">Modification d'un compte</h1>
        <hr class="my-4">
        <%= message(session) %>
        <input type="hidden" name="campaign" value="${campaign}">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail">Adresse mail</label>
                <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Adresse e-mail" value="<%= getEmail(session)%>"
                       readonly>
            </div>
        </div>
        <div class="form-row">
           <%=getButtons(session)%>
        </div>
    </div>
    <button class="btn btn-block btn-primary text-uppercase" type="submit">Modifier</button>
    <button class="btn btn-block btn-primary text-uppercase" onclick="window.location.href = '/app/accounts'; return false;">Retour</button>
</form>
<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if(session.getAttribute("msg")!=null){
        sb.append(session.getAttribute("msg"));
        session.removeAttribute("msg");
    }
    return sb.toString();
}%>

<%! private String getEmail(HttpSession session){
    StringBuilder sb = new StringBuilder();
    if(session.getAttribute("email")!=null){
        sb.append(session.getAttribute("email"));
        session.removeAttribute("email");
    }
    return sb.toString();
}%>


<%! private String getButtons(HttpSession session){
    String buttons ="";
    if(session.getAttribute("buttons") != null){
        buttons = session.getAttribute("buttons").toString();
    }
    return buttons;
}%>

<%! private String getId(HttpSession session){
   return (session.getAttribute("id")!=null)?session.getAttribute("id").toString():"";

}%>
</body>
</html>
