<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="fr.planninggo.spring.configuration.State" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Planning</title>
    <link rel="icon" href="/app/images/logo.png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/codebase/dhtmlxscheduler.css" type="text/css"/>
    <link rel="stylesheet" href="/app/codebase/dhtmlxtree.css" type="text/css"/>
    <link rel="stylesheet" href="/app/css/scheduler.css" type="text/css"/>
</head>
<body>

<input type="hidden" id="authenticated" value='<%=request.getSession().getAttribute("authenticated")%>'/>

<% if (!(Boolean) request.getSession().getAttribute("authenticated")) { %>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
</nav>
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-sm-12">
            <input class="form-control col-sm-3" style="display: inline-block" type="text" placeholder="Filtrer"
                   onkeyup="highlight(this.value); return false;">
        </div>
    </div>
    <div class="row">
        <div id="scheduler_here" class="dhx_cal_container" style="width:100%; height:100%; position:absolute;">
            <div class="dhx_cal_navline">
                <div class="dhx_cal_prev_button">&nbsp;</div>
                <div class="dhx_cal_next_button">&nbsp;</div>
                <div class="dhx_cal_today_button">&nbsp;</div>
                <div class="dhx_cal_date">&nbsp;</div>
                <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
                <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
                <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
                <div class="dhx_cal_tab" name="agenda_tab" style="right:280px;"></div>
            </div>
            <div class="dhx_cal_header"></div>
            <div class="dhx_cal_data"></div>
        </div>
    </div>
</div>

<% } else { %>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign/<%= getId(session) %>">Accueil campagne <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-sm-12">
            <%= setButton(session) %>
            <input class="form-control col-sm-3" style="display: inline-block" id="diff" type="text" placeholder="Filtrer"
                   onkeyup="highlight(this.value); return false;">
            <%= getMessage(session) %>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3" id="treeBox" style="display: none;"></div>
        <div id="scheduler_here" class="dhx_cal_container col-sm-12">
            <div class="dhx_cal_navline">
                <div class="dhx_cal_prev_button">&nbsp;</div>
                <div class="dhx_cal_next_button">&nbsp;</div>
                <div class="dhx_cal_today_button">&nbsp;</div>
                <div class="dhx_cal_date">&nbsp;</div>
                <div class="dhx_cal_tab" name="day_tab" style="right:204px;"></div>
                <div class="dhx_cal_tab" name="week_tab" style="right:140px;"></div>
                <div class="dhx_cal_tab" name="month_tab" style="right:76px;"></div>
                <div class="dhx_cal_tab" name="agenda_tab" style="right:280px;"></div>
            </div>
            <div class="dhx_cal_header"></div>
            <div class="dhx_cal_data"></div>
        </div>
    </div>
</div>

<input type="hidden" id="roomNumber" value='<%=getRoom(request.getSession())%>'/>


<%! int getId(HttpSession session) {
    return ((CampaignEntity) session.getAttribute("campaign")).getId();
} %>
<%! private int getRoom(HttpSession session) {
    return ((CampaignEntity) session.getAttribute("campaign")).getRoomNumber();
} %>

<% } %>

<%!
    private String setButton(HttpSession session) {
        StringBuilder stringBuilder = new StringBuilder();
        if (session.getAttribute("campaign") != null && !((CampaignEntity) session.getAttribute("campaign")).getCampaignState().equals(State.ARCHIVE)) {
            stringBuilder.append("<button id=\"indicator\" onclick=\"return false\" class=\"btn\"></button>\n" +
                    "            <button id=\"figer\" onclick=\"changeMode()\" class=\"btn btn-outline-success\">Figer</button>\n" +
                    "            <a id='diffBtn' href=\"/app/campaign/diffusion\">\n" +
                    "                <button class=\"btn btn-outline-success\">Diffuser</button>\n" +
                    "            </a>");
        }
        return stringBuilder.toString();
    }

    private String getMessage(HttpSession session) {
        if (session.getAttribute("campaign") != null && !((CampaignEntity) session.getAttribute("campaign")).getCampaignState().equals(State.ARCHIVE)) {
            return "";
        }
        if (session.getAttribute("errorList") != null) {
            return session.getAttribute("errorList").toString();
        }
        if (session.getAttribute("errorBroadcast") != null) {
            List<String> errors = (List<String>) session.getAttribute("errorBroadcast");
            StringBuilder sb = new StringBuilder();
            if (errors.size() == 1 && errors.contains("true")) {
                sb.append("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">");
                sb.append("Mails de diffusion envoyés avec succès");
                sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
                sb.append("<span aria-hidden='true'>&times;</span>");
                sb.append("</button>");
                sb.append("</div><hr class='my-4'>");
                session.removeAttribute("errorBroadcast");
            } else {
                for (String error : errors) {
                    sb.append("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">");
                    sb.append(error);
                    sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
                    sb.append("<span aria-hidden='true'>&times;</span>");
                    sb.append("</button>");
                    sb.append("</div><hr class='my-4'>");
                }
            }
            session.removeAttribute("errorBroadcast");
            return sb.toString();
        }
        return "";
    }%>


<style type="text/css" media="screen">
    html, body {
        margin: 0px;
        padding: 0px;
        height: 98%;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.2/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="/app/codebase/dhtmlxtree.js"></script>
<script src="/app/codebase/dhtmlxscheduler.js"></script>
<script src="/app/codebase/ext/dhtmlxscheduler_collision.js"></script>
<script src="/app/codebase/ext/dhtmlxscheduler_outerdrag.js"></script>
<script src="/app/codebase/ext/dhtmlxscheduler_agenda_view.js"></script>
<script src="/app/codebase/ext/dhtmlxscheduler_limit.js"></script>
<script src="/app/codebase/sources/locale/locale_fr.js"></script>
<script src="/app/js/utility.js"></script>
<script src="/app/js/scheduler.js"></script>
</body>
</html>
