<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/campaign_home.css">
    <title>Gestion des comptes</title>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <%=getButtons(session)%>

        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<% if (session.getAttribute("campaign") != null) {
    session.removeAttribute("campaign");
} %>
<div class="container-fluid mt-3">
    <h1 class="text-center">Gestion des comptes</h1>
    <hr class="my-4">
    <%= message(session,"msg") %>
    <%= message(session,"msgUpdate") %>
    <%= message(session,"msgNoDelete") %>
    <form method="post" action="accounts/delete">
        <a href="/app/accounts/addAccount" class="btn btn-outline-success">Ajouter un compte</a>
        <a href="/app/accounts/delete" style="display:inline-block">
            <button class="btn btn-outline-success">Supprimer</button>
        </a>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Adresse email</th>
                <th scope="col">Rôle(s)</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody id="content"><%= session.getAttribute("accounts") %>
            </tbody>
        </table>
    </form>
</div>
<%! String message(HttpSession session, String name) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute(name) != null) {
        sb.append(session.getAttribute(name));
        session.removeAttribute(name);
    }
    return sb.toString();
}%>

<%! private String getButtons(HttpSession session) {
    String buttons = "";
    if (session.getAttribute("buttons") != null) {
        buttons = session.getAttribute("buttons").toString();
    }
    return buttons;
}%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
