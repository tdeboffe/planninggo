<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="images/logo.png"/>
    <meta http-equiv="Content-Type" content="text/html">
    <title>Login</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <div class="text-center"><img alt="LOGO" src="images/logo.png"/></div>
                    <h5 class="card-title text-center">Connexion</h5>
                    <div id="errorMessage" class="text-center"></div>
                    <form class="form-signin" action="login" method="post">
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" name="username" class="form-control" placeholder="Adresse mail" required autofocus>
                            <label for="inputEmail">Adresse mail</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required>
                            <label for="inputPassword">Mot de passe</label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Se connecter</button>
                        <hr class="my-4">
                        <div class="text-center"><a href="newPassword">Des problèmes pour vous connecter ?</a></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<body>
    <script src="js/login.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
