<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.PhaseEntity" %>
<%@ page import="fr.planninggo.spring.configuration.State" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="fr.planninggo.spring.attendee.entity.AttendeeEntity" %>
<%@ page import="fr.planninggo.spring.attendee.entity.StudentEntity" %>
<%@ page import="fr.planninggo.spring.attendee.entity.SupervisorStudentEntity" %>
<%@ page import="fr.planninggo.spring.attendee.entity.AttendeeGuestEntity" %>
<%@ page import="java.util.TreeSet" %>
<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Campagne</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/app/css/campaign.css"/>
    <link rel="stylesheet" href="/app/css/errorCsv.css"/>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Importer
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/app/campaign/<%= getId(session) %>/import/attendee">Participants</a>
                    <a class="dropdown-item" href="/app/campaign/<%= getId(session) %>/import/availability">Disponibilités</a>
                    <a class="dropdown-item" href="/app/campaign/<%= getId(session) %>/import/planning">Planning</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Exporter
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                    <a class="dropdown-item"
                       href="/app/attendee/<%= getId(session) %>/exportParticipants">Participants</a>
                    <a class="dropdown-item" href="/app/availability/<%= getId(session) %>/exportAvailabilities">Disponibilités</a>
                    <a class="dropdown-item" href="/app/campaign/<%= getId(session) %>/exportCampaign">Paramètres</a>
                    <a class="dropdown-item" href="/app/defenseType/<%= getId(session) %>/exportConfigSoutenances">Configurations de soutenance</a>
                    <a class="dropdown-item" href="/app/defense/<%= getId(session) %>/exportPlanning">Planning</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Modifier
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                    <a class="dropdown-item" href="/app/campaign/<%= getId(session) %>/availability/change">Disponibilités</a>
                    <a class="dropdown-item" href="/app/campaign/create">Paramètres</a>
                </div>
            </li>
            <li class='nav-item'>
                <a  class="nav-link"  href="/app/campaign/<%= getId(session) %>/attendees">Supprimer des participants</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto float-right">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div id="cover-spin">
    <span class="text-center">Génération du planning en cours. Veuillez patienter</span>
</div>
<div class="container pt-3 bg-light">

    <h1 class="text-center"><%= getName(session) %></h1>
    <hr class="my-4" id="errorAfter">
    <%= message(session) %>
    <%= setButton(session) %>
    <div class="row-fluid">
        <table class="table text-center">
            <tr>
                <td colspan="4" style="border-top: none"><div class="font-weight-bold">&Eacute;tat d'avancement : <%= getState(session) %></div></td>
            </tr>
            <tr>
                <td colspan="2"><div class="d-inline-block font-weight-bold">Début des soutenances : <%= getBegin(session) %></div></td>
                <td colspan="2"><div class="d-inline-block font-weight-bold">Fin des soutenances : <%= getEnd(session) %></div></td>
            </tr>
            <tr>
                <td>Nombre d'étudiants : <%= getStudentNumber(session) %></td>
                <td>Nombre de superviseurs : <%= getSupervisorNumber(session) %></td>
                <td>Nombre d'invités : <%= getAttendeeGuestNumber(session) %></td>
                <td>Nombre de salles : <%= getRoomNumber(session) %></td>
            </tr>
        </table>
        <table class="table table-striped text-center">
            <thead>
                <%= setHeader(session) %>
            </thead>
            <tbody>
                <%= getPhases(session) %>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmer l'envoi des mails de relance</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ATTENTION ! Vous êtes sur le point d'envoyer des mails de relance. Merci de confirmer ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" id="launchRelance" data-dismiss="modal" class="btn btn-primary" onclick="relance(this.title, this.name)">Envoyer</button>
            </div>
        </div>
    </div>
</div>

<%! private String setHeader(HttpSession session) {
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    StringBuilder stringBuilder = new StringBuilder();
    if (!campaignEntity.getPhases().isEmpty()) {
        stringBuilder.append("<tr>")
                .append("<th scope='row'>Date de début</th>")
                .append("<th scope=\"row\">Date de fin</th>")
                .append("<th scope=\"row\">&Eacute;tat</th>")
                .append("<th scope=\"row\">Participant interrogé</th>")
                .append("<th scope=\"row\">Participant concerné</th>")
                .append("<th scope=\"row\">Participants ayant saisi</th>")
                .append("<th scope=\"row\">% de saisie</th>")
                .append("<th scope=\"row\"></th>")
                .append("</tr>");
    }
    return stringBuilder.toString();
}%>

<%! private String getPhases(HttpSession session) {
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    StringBuilder stringBuilder = new StringBuilder();
    Set<PhaseEntity> phases = new TreeSet<>(campaignEntity.getPhases());
    String currentState;
    String currentWrittenBy;
    String currentWrittenFor;
    int currentAvailabilities;
    int currentTotalPerson;
    for (PhaseEntity phase : phases) {
        currentAvailabilities = 0;
        currentTotalPerson = 0;
        currentState = "Pas commencée";
        if (phase.isStart()) { currentState = "En cours"; }
        if (phase.getEnd().toLocalDate().isBefore(LocalDate.now())) { currentState = "Terminée"; }

        currentWrittenBy = phase.getWrittenBy();
        currentWrittenFor = phase.getWrittenFor();

        if (currentWrittenBy.equals("Etudiant")) {
            currentTotalPerson += campaignEntity.getStudents().size();
            currentAvailabilities += (int) campaignEntity.getStudents().stream().filter(AttendeeEntity::isAvailabilityGiven).count();
        } else {
            for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                for (SupervisorStudentEntity supervisorStudentEntity : studentEntity.getSupervisors()) {
                    if (supervisorStudentEntity.getRoleEntity().getName().equals(currentWrittenBy)) {
                        currentAvailabilities++;
                    }
                    currentTotalPerson++;
                }
            }
            if (currentAvailabilities == 0) {
                for (AttendeeGuestEntity attendeeGuestEntity : campaignEntity.getGuests()) {
                    final String cw = currentWrittenBy;
                    if (attendeeGuestEntity.getAccount().getRoleEntities().stream().anyMatch(r -> r.getName().equals(cw))) {
                        currentAvailabilities++;
                    }
                    currentTotalPerson++;
                }
            }
        }

        stringBuilder.append("<tr>")
                .append("<td>").append(new SimpleDateFormat("dd/MM/yyyy").format(phase.getBegin())).append("</td>")
                .append("<td>").append(new SimpleDateFormat("dd/MM/yyyy").format(phase.getEnd())).append("</td>")
                .append("<td>").append(currentState).append("</td>")
                .append("<td>").append(phase.getWrittenBy()).append("</td>")
                .append("<td>").append(phase.getWrittenFor()).append("</td>")
                .append("<td>").append(currentAvailabilities).append("</td>");
                if (currentTotalPerson != 0) {
                    float value = ((float)currentAvailabilities / (float)currentTotalPerson) * 100;
                    stringBuilder.append("<td>").append(value).append("%</td>");
                } else {
                    stringBuilder.append("<td>0%</td>");
                }
                stringBuilder.append("<td>");

        if (currentState.equals("En cours")) {
            stringBuilder.append("<button class='btn btn-outline-success' data-toggle=\"modal\" data-target=\"#exampleModalCenter\" onclick='setRelance(\"").append(phase.getId()).append("\", \"").append(campaignEntity.getId()).append("\")'>Relancer</button>");
        }

        stringBuilder.append("</td>")
                .append("</tr>");
    }

    return stringBuilder.toString();
} %>

<%! private int getStudentNumber(HttpSession session) {
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    return campaignEntity.getStudents().size();
}%>
<%! private int getSupervisorNumber(HttpSession session) {
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    return campaignEntity.getSupervisors().size();
}%>
<%! private int getAttendeeGuestNumber(HttpSession session) {
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    return campaignEntity.getGuests().size();
}%>
<%! private int getRoomNumber(HttpSession session) {
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    return campaignEntity.getRoomNumber();
}%>


<%! private String setButton(HttpSession session) {
    String state = getState(session);
    StringBuilder stringBuilder = new StringBuilder();
    if (state.equals(State.READY_GENERATE) || state.equals(State.READY_GENERATE_DEFAULT) || state.equals(State.GENERATE) || state.equals(State.BROADCAST) || state.equals(State.PARTIAL_BROADCAST) || state.equals(State.READY_GENERATE_ORG)) {
        stringBuilder.append("<a onclick='$(\"#cover-spin\").show();' href=\"/app/campaign/" + getId(session) + "/solve\"><button class=\"btn btn-outline-success\">Générer le planning</button></a>\n");
        stringBuilder.append("<a href=\"/app/campaign/" + getId(session) + "/scheduler\"><button class=\"btn btn-outline-success\">Voir le planning</button></a>\n");
    }
    if (state.equals(State.BROADCAST)) {
        stringBuilder.append("<a href=\"/app/campaign/" + getId(session) + "/archive\"><button class=\"btn btn-outline-success\">Archiver la campagne</button></a>\n");
    }
    if (state.equals(State.ARCHIVE)) {
        stringBuilder.append("<a href=\"/app/campaign/" + getId(session) + "/scheduler\"><button class=\"btn btn-outline-success\">Voir le planning</button></a>\n");
    }
    return stringBuilder.toString();
} %>
<%! String message(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("msg") != null) {
        if (((String) session.getAttribute("msg")).startsWith("ERREUR : ")) {
            sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        } else {
            sb.append("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">");
        }
        sb.append(session.getAttribute("msg"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("msg");
    }
    return sb.toString();
}%>
<%! int getId(HttpSession session) { return ((CampaignEntity) session.getAttribute("campaign")).getId(); } %>
<%! String getName(HttpSession session) { return ((CampaignEntity) session.getAttribute("campaign")).getName(); } %>
<%! String getState(HttpSession session) { return ((CampaignEntity) session.getAttribute("campaign")).getCampaignState(); } %>
<%! private String getBegin(HttpSession session) {
    return new SimpleDateFormat("dd/MM/yyyy").format(((CampaignEntity) session.getAttribute("campaign")).getCampaignBegin());
} %>
<%! private String getEnd(HttpSession session) {
    return new SimpleDateFormat("dd/MM/yyyy").format(((CampaignEntity) session.getAttribute("campaign")).getCampaignEnd());
} %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
<script src="/app/js/utility.js"></script>
<script src="/app/js/campaign.js"></script>
</body>
</html>
