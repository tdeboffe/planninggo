<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Ajout d'un type de soutenance</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Création d'un type de soutenance</h1>
    <hr class="my-4">
    <%= message(session) %>
    <form method="post" action="addTypeNext">
        <input type="hidden" name="campaign" value="${campaign}">
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputCode">Code du type :</label>
                <input type="text" name="typeCode" class="form-control" id="inputCode" placeholder="RMT2018"
                       value="${typeCode}" required autofocus>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputDesc">Description du type :</label>
                <input type="text" name="typeDesc" class="form-control" id="inputDesc"
                       placeholder="Rapport de Mission Technique Professionnelle 2018" value="${typeDesc}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-1">
                <label for="inputHour">Heures : </label>
                <input type="number" name="typeHour" class="form-control" id="inputHour" min="0" max="8"
                       value="${typeHour}" required>
            </div>
            <div class="form-group col-md-1">
                <label for="inputMin">Minutes :</label>
                <input type="number" name="typeMin" class="form-control" id="inputMin" min="0" max="59"
                       value="${typeMin}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="inputNumber">Nombre de jurys :</label>
                <input type="number" name="juryNumber" min="1" max="10" class="form-control" id="inputNumber" value="${juryNumber}"
                       required>
            </div>
        </div>
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Suivant</button>
    </form>
    <form class="mt-2" method="post" action="backToType">
        <input type="hidden" name="campaign" value="${campaign}">
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Annuler</button>
    </form>
</div>
<%! String message(HttpSession session)  {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("msg") != null) {
        if (((String)session.getAttribute("msg")).startsWith("ERREUR : ")) {
            sb.append("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\">");
        }
        sb.append(session.getAttribute("msg"));
        sb.append("<button type='button' class='close' data-dismiss='alert' aria-label='Close'>");
        sb.append("<span aria-hidden='true'>&times;</span>");
        sb.append("</button>");
        sb.append("</div><hr class='my-4'>");
        session.removeAttribute("msg");
    }
    return sb.toString();
}%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js"></script>
</body>
</html>
