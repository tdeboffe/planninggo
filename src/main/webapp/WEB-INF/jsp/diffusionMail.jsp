<%@ page import="fr.planninggo.spring.campaign.entity.CampaignEntity" %>
<%@ page import="fr.planninggo.spring.campaign.entity.JuryMemberEntity" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="fr.planninggo.spring.campaign.entity.DefenseTypeEntity" %>
<!DOCTYPE html>
<html lang="en">
<%@ page contentType="text/html; charset=UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Diffusion</title>
    <link rel="icon" href="/app/images/logo.png"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <style>
        .tabcontent{
            display:none;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><img src="/app/images/logo.png" width="30" height="30" alt="planninggo"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/app/campaign">Mes campagnes <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/app/logout">Se déconnecter</a>
                </li>
            </ul>
        </form>
    </div>
</nav>
<div class="container pt-3 bg-light">
    <h1 class="text-center">Diffusion</h1>
    <hr class="my-4">
    <form method="post" action="sendMail">
        <input type="hidden" name="campaign" value="${campaign}">
        <div class="form-row">
            <div class="form-group col-md-6" >
                <div class="tab">
                    <button class="tablinks btn btn-secondary btn-sm" onclick="openDiv(event, 'roles'); return false;">Roles &darr;</button>
                    <button class="tablinks btn btn-secondary btn-sm" onclick="openDiv(event, 'defenseType'); return false;">Types de soutenance &darr;	</button>
                </div>
                <div id="roles" class="tabcontent" style="flex-direction: column">
                    <%= getRoles(request.getSession()) %>
                </div>

                <div id="defenseType" class="tabcontent"  style="flex-direction: column">
                    <%= getDefenseType(request.getSession()) %>
                </div>

            </div>
            <div class="form-group col-md-6">
                <label for="other">Autre(s)</label>
                <input type="text" name="otherMails" class="form-control" id="other" >
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="objet">Objet</label>
                <input type="text" name="objet" class="form-control" id="objet" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="message">Message</label>
                <textarea id="message" name="message" class="form-control"></textarea>
            </div>
        </div>

        <button class="btn btn-block btn-primary text-uppercase" type="submit">Envoyer</button>
    </form>
    <form class="mt-2" method="post" action="backToHome">
        <input type="hidden" name="campaign" value="${campaign}">
        <button class="btn btn-block btn-primary text-uppercase" type="submit">Annuler</button>
    </form>
</div>
<%! String getRoles(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
    if (campaignEntity != null && campaignEntity.getDefensesType() != null) {
        Iterator<DefenseTypeEntity> it = campaignEntity.getDefensesType().iterator();
        while(it.hasNext()) {
            Set<JuryMemberEntity> set = it.next().getJuryMembers();
            set.forEach(member -> sb.append("<div><input type='checkbox' checked name='role' value='").append(member.getRolename()).append("'>").append("</input><label>").append(member.getRolename()).append("</label></div>"));
        }
    }
    sb.append("<div><input type='checkbox' checked name='role' value='Etudiant'></input><label>Etudiant</label></div>");
    return sb.toString();
}
%>


<%! String getDefenseType(HttpSession session) {
    StringBuilder sb = new StringBuilder();
    if (session.getAttribute("campaign") != null) {
        CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
        Set<DefenseTypeEntity> set = campaignEntity.getDefensesType();
        if (set != null) {
            set.forEach(type -> sb.append("<div>").append("<input type='checkbox' checked name='type_code' value='").append(type.getCode()).append("'>").append("</input><label>").append(type.getCode()).append("</label></div>"));
        }
    }
    return sb.toString();
}
%>
</body>
<script>
    function openDiv(evt, div) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(div).style.display = "flex";
        evt.currentTarget.className += " active";
    }

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
</html>
