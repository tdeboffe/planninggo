package fr.planninggo.utility;

public class Utils {
    private Utils() {}

    public static String message(String type, String message) {
        return "<div class=\"alert " + type +
                " alert-dismissible fade show\" role=\"alert\">" + message +
                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                "<span aria-hidden='true'>&times;</span>" +
                "</button>" +
                "</div><hr class='my-4'>";
    }
}
