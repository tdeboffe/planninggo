package fr.planninggo.utility;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Random;

public class Password {

    private static final char[] ALPHA_UPPER_CHARACTERS = {'A', 'B', 'C', 'D',
            'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private static final char[] ALPHA_LOWER_CHARACTERS = {'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
            'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static final char[] NUMERIC_CHARACTERS = {'0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9'};
    private static final char[] SPECIAL_CHARACTERS = {'@', '#'};
    private static final char[][] CHARACTERES = {ALPHA_UPPER_CHARACTERS, ALPHA_LOWER_CHARACTERS,
            NUMERIC_CHARACTERS, SPECIAL_CHARACTERS};
    private Password() {
    }

    /**
     * Cette méthode permet de générer de nouveaux mots aléatoirement
     * il contient 8 caractères dont un caractère spéciaux
     *
     * @return le mot de passe
     */
    public static String generatePassword() {

        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random(System.currentTimeMillis());
        int specialCharactere = 0;
        while (stringBuilder.length() < 8) {
            char[] charactere;
            if (specialCharactere == 0) {
                charactere = CHARACTERES[random.nextInt(4)];
                specialCharactere++;
            } else {
                charactere = CHARACTERES[random.nextInt(3)];
            }

            stringBuilder.append(charactere[random.nextInt(charactere.length)]);
        }

        return stringBuilder.toString();
    }

    /**
     * cette méthode permet d'encripter un mot de passe
     *
     * @param password le mot de passe à encrypter
     * @return le mot de passe encrypté
     */
    public static String encodePassword(String password) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }
}
