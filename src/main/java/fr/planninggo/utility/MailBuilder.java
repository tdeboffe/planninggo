package fr.planninggo.utility;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;


public class MailBuilder {

    private MailBuilder() {
    }

    /**
     * La méthode construit les informations du mail spécifique à la génération d'un nouveau mot de passe.
     *
     * @param url  L'URL pour générér le nouveau mot de passe
     * @param mail Le mail de l'utilisateur
     * @return une map avec les informations.
     */
    public static Map<String, String> createPasswordMail(String url, String mail) {
        Map<String, String> map = new HashMap<>();
        map.put("to", mail);
        map.put("subject", "ESIPE - PlanningGo - Lien pour un nouveau mot de passe.");
        map.put("text", "Bonjour, Veuillez trouver ci-joint un lien pour générer un nouveau mot de passe pour votre compte planning Go : " + url + "\n" +
                "Cordialement, L'équipe Planning Go.");
        return map;
    }

    /**
     * Crétation d'un mail pour la création d'un compte
     * @param url le lien pour accéder à son mot de passe
     * @param mail le mail du compte
     * @return une map contenant toutes les informations du mail
     */
    public static Map<String, String> createPasswordAccount(String url, String mail) {
        Map<String, String> map = new HashMap<>();
        map.put("to", mail);
        map.put("subject", "ESIPE - PlanningGo - Votre compte");
        map.put("text", "Bonjour, un administrateur vient de vous créer un nouveau compte sur l'application PlanningGo. Vous trouverez votre mot de passe ici : " + url + "\n" +
                "Cordialement, L'équipe Planning Go.");
        return map;
    }

    /**
     * création du mail de début d'une phase de dissponibilité
     * @param url l'url pour accéder à l'application
     * @param mail le mail du participant
     * @param writtenFor saisie par
     * @param dateEnd fin de la date de saisie
     * @return une map contenant toutes les informations du mail
     */
    public static Map<String, String> createAvailabilitiesMail(String url, String mail, String writtenFor, String dateEnd) {
        Map<String, String> map = new HashMap<>();
        map.put("to", mail);
        map.put("subject", "ESIPE - PlanningGo - Saisie de disponibilités");
        if (writtenFor.isEmpty()) {
            map.put("text", "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir vos disponibilités avant le " + dateEnd + ". Pour cela rendez-vous à " +
                    "cette adresse : " + url + ". Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !");
            return map;
        }
        map.put("text", "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir des disponibilités avant le " + dateEnd + " pour votre " + writtenFor + ". Pour cela rendez-vous à " +
                "cette adresse : " + url + ". Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !");
        return map;
    }

    /**
     * Mail de diffusion du planning
     * @param url l'url pour accéder à l'application
     * @param mail le mail du participant
     * @param msg le message
     * @param objet l'objet du mail
     * @return une map contenant toutes les informations du mail
     */
    public static Map<String, String> sharePlanning(String url, String mail, String msg, String objet ){
        Map<String, String> map = new HashMap<>();
        map.put("to", mail);
        map.put("subject", objet);
        map.put("text", msg + "\n" + url);
        return map;
    }

    /**
     * création du mail de relance
     * @param s url pour accéder à l'application
     * @param email l'email du participant
     * @param writtenFor saisie par un role spécifique
     * @param dateEnd date de fin de saisie
     * @return une map contenant toutes les informations du mail
     */
    public static Map<String, String> createAvailabilitiesMailRelance(String s, String email, String writtenFor, String dateEnd) {
        Map<String, String> map = new HashMap<>();
        String text;
        try (FileInputStream inputStream = new FileInputStream("/home/planninggodata/relance.txt")) {
            text = IOUtils.toString(inputStream, Charset.forName("UTF-8"));
            text = text.replace("<URL>", s);
            text = text.replace("<DATEEND>", dateEnd);
        } catch (IOException e) {
            if (writtenFor.isEmpty()) {
                text = "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir vos disponibilités avant le " + dateEnd + ". Pour cela rendez-vous à " +
                        "cette adresse : " + s + ". Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !";
            } else {
                text = "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir des disponibilités avant le " + dateEnd + " pour votre " + writtenFor + ". Pour cela rendez-vous à " +
                        "cette adresse : " + s + ". Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !";
            }

        }
        map.put("text", text);
        map.put("to", email);
        map.put("subject", "ESIPE - PlanningGo - Relance Saisie de disponibilités");
       return map;
    }
}
