package fr.planninggo.utility;

import com.google.common.base.Charsets;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Classe pour le chiffrement symétrique du mail.
 * NE PAS MODIFIER.
 */
public class SymetricEncryption {

    private static final byte[] key = "16planninggo2019".getBytes(Charsets.UTF_8);

    private SymetricEncryption() {
    }

    /**
     * Encrypte un message
     *
     * @param msg message
     * @return un hash
     */
    public static String encrypt(String msg) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"));
        return Base64.encodeBase64URLSafeString(cipher.doFinal(msg.getBytes(Charsets.UTF_8)));
    }

    /**
     * Decrypte un hash
     *
     * @param hash hash
     * @return le message déchiffré
     */
    public static String decrypt(String hash) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"));
        return new String(cipher.doFinal(Base64.decodeBase64(hash)), Charsets.UTF_8);
    }
}
