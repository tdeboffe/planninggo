package fr.planninggo.utility;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Map;
import java.util.Properties;

/**
 * Classe pour envoyer un mail
 * NE PAS MODIFIER
 */
public class SendMail {
    private SendMail() {
    }

    /**
     * Envoi un mail grâce aux paramètres fournis en arguments.
     *
     * @param params arguments
     */
    public static void sendAnEmail(Map<String, String> params) {
        try {
            SendMail.sendMail(params);
        } catch (MessagingException e) {
            // IgnoreException
        }
    }

    public static String sendAnEmailWithResponse(Map<String, String> params) {
        try {
            SendMail.sendMail(params);
            return "true";
        } catch (MessagingException e) {
            return  "ERREUR : Erreur lors de l'envoi du mail. : " + e.getMessage();
        }
    }

    private static void sendMail(Map<String, String> params) throws MessagingException {
        final String username = "noreply.planninggo@gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, "gologoesipe");
            }
        });

        Message mm = new MimeMessage(session);
        mm.setFrom(new InternetAddress(username));
        mm.setRecipients(Message.RecipientType.TO, InternetAddress.parse(params.get("to")));
        mm.setSubject(params.get("subject"));
        mm.setText(params.get("text"));
        Transport.send(mm);
    }
}
