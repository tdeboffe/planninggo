package fr.planninggo.scheduler;

import fr.planninggo.spring.campaign.entity.JuryMemberEntity;

import java.util.Objects;

/**
 * Classe permettant de paramétrer différentes informations d'un participant
 * au regard de la configuration des soutenances
 */
public class AttendeeSettings {
    private final Role role;
    private final boolean isLinkedToStudent;
    private final boolean isTheSameCourseStudy;
    private final boolean isPresident;

    AttendeeSettings(Role role, boolean isLinkedToStudent, boolean isTheSameCourseStudy, boolean isPresident) {
        this.role = Objects.requireNonNull(role);
        this.isLinkedToStudent = isLinkedToStudent;
        this.isTheSameCourseStudy = isTheSameCourseStudy;
        this.isPresident = isPresident;
    }

    /**
     * Permet de créer une nouvelle instance de AttendeeSettings.
     *
     * @param jme JuryMemberEntity
     * @return AttendeeSettings
     */
    public static AttendeeSettings create(JuryMemberEntity jme) {
        return new AttendeeSettings(Role.create(jme.getRole()),
                jme.isStudentLink(),
                jme.isStudyLink(),
                jme.isPresident());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AttendeeSettings)) {
            return false;
        }
        AttendeeSettings attSetts = (AttendeeSettings) o;
        return role.equals(attSetts.role) &&
                isLinkedToStudent == attSetts.isLinkedToStudent &&
                isTheSameCourseStudy == attSetts.isTheSameCourseStudy &&
                isPresident == attSetts.isPresident;
    }

    @Override
    public int hashCode() {
        return role.hashCode() ^
                Boolean.hashCode(isLinkedToStudent) ^
                Boolean.hashCode(isTheSameCourseStudy) ^
                Boolean.hashCode(isPresident);
    }

    public Role getRole() {
        return role;
    }

    public boolean isPresident() {
        return isPresident;
    }

    boolean isLinkedToStudent() {
        return isLinkedToStudent;
    }

    boolean isTheSameCourseStudy() {
        return isTheSameCourseStudy;
    }
}
