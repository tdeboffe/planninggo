package fr.planninggo.scheduler;

import java.util.Comparator;

/**
 * Classe représentant le Comparator appelé par le solveur
 * pour déterminer les Defense les plus difficiles à résoudre.
 */
public class DefenseDifficultyComparator implements Comparator<Defense> {

    @Override
    public int compare(Defense d1, Defense d2) {
        return d1.compareTo(d2);
    }

}
