package fr.planninggo.scheduler;

import org.apache.commons.csv.CSVRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Classe représentant un étudiant
 */
public class Student implements Attendee {
    private List<AttendeeRelation> supervisors = new ArrayList<>();
    private DefenseType defenseType;

    @Override
    public List<AttendeeRelation> getRelations() {
        return supervisors;
    }

    public void setSupervisors(List<AttendeeRelation> supervisors) {
        this.supervisors = supervisors;
    }

    @Override
    public List<Person> getRelatedPersons() {
        return supervisors.stream()
                .map(AttendeeRelation::getPerson)
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> getRelatedPersonsByRole(Role role) {
        return supervisors.stream()
                .filter(rel -> rel.getRole().equals(role))
                .map(AttendeeRelation::getPerson)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isAssignedToDefenseType() {
        return (defenseType != null);
    }

    @Override
    public DefenseType getDefenseType() {
        return defenseType;
    }

    public void setDefenseType(DefenseType defenseType) {
        this.defenseType = Objects.requireNonNull(defenseType);
    }

    @Override
    public String toString() {
        return "Student{" +
                "supervisors=" + supervisors +
                ", defenseType=" + defenseType +
                '}';
    }
}
