package fr.planninggo.scheduler;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Classe représentant un jour de soutenance
 */
public class Day {
    private final LocalDate localDate;

    public Day(LocalDate localDate) {
        this.localDate = Objects.requireNonNull(localDate);
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Day)){
            return false;
        }
        Day d = (Day) o;
        return localDate.equals(d.localDate);
    }

    @Override
    public int hashCode() {
        return localDate.hashCode();
    }

    @Override
    public String toString() {
        return "Day{" +
                "localDate=" + localDate +
                '}';
    }
}
