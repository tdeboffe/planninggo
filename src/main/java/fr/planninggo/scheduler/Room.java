package fr.planninggo.scheduler;

import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Classe représentant une salle où aura lieu une soutenance.
 */
public class Room {
    private static final SimpleDateFormat DF = new SimpleDateFormat("dd/MM/yyyy");
    private Long id;
    private String name;
    private Set<LocalDate> availableDays;

    public Room(Long id, String name) {
        Objects.requireNonNull(id);
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        this.id = id;
        this.name = Objects.requireNonNull(name);
        this.availableDays = new HashSet<>();
    }

    /**
     * Permet de créer une nouvelle instance Room
     * à partir de l'entité RoomEntity
     * @param roomEntity RoomeEntity
     * @param defenseDayEntities Set<DefenseDayEntity> jours de disponibilités de la salle
     * @return nouvelle instance de Room
     */
    public static Room create(RoomEntity roomEntity, Set<DefenseDayEntity> defenseDayEntities) {
        Room room = new Room(Long.valueOf(Integer.toString(roomEntity.getId())),
                roomEntity.getName());
        defenseDayEntities.forEach(r -> room.addAvailableDay(getLocalDate(r.getDate())));
        return room;
    }

    private static LocalDate getLocalDate(Date date) {
        String strDate = DF.format(date);
        String[] dates = strDate.split("/");
        return LocalDate.of(Integer.parseInt(dates[2]),
                Integer.parseInt(dates[1]),
                Integer.parseInt(dates[0]));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Room)) {
            return false;
        }
        Room r = (Room) o;
        return id.equals(r.id) && name.equals(r.name);
    }

    @Override
    public int hashCode() {
        return id.hashCode() ^ name.hashCode();
    }

    @PlanningId
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Objects.requireNonNull(id);
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public Set<LocalDate> getAvailableDays() {
        return availableDays;
    }

    void setAvailableDays(Set<LocalDate> availableDays) {
        this.availableDays = Objects.requireNonNull(availableDays);
    }

    /**
     * Permet de vérifier si Room est disponible
     * pour une soutenance (Defense)
     *
     * @param defense Defense soutenance dont on veut déterminer si Room est disponible
     * @return boolean indiquant si Room est disponible
     */
    public boolean isAvailable(Defense defense) {
        if (defense == null) {
            return false;
        }
        Timeslot timeslot = defense.getTimeslot();
        if (timeslot == null) {
            return false;
        }
        LocalDate defenseLocalDate = timeslot.getStartDateTime().toLocalDate();
        return availableDays.contains(defenseLocalDate);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        for (LocalDate ld : availableDays) {
            sb.append(ld.format(format)).append("--");
        }
        return "[id=" + id + ",name=" + name + ",availDays=" + sb.toString() + "]";
    }

    /**
     * Ajoute une disponibilité
     * @param localDate LocalDate
     */
    public void addAvailableDay(LocalDate localDate) {
        availableDays.add(localDate);
    }
}
