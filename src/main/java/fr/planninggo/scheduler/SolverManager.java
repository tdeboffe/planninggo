package fr.planninggo.scheduler;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

/**
 * Classe permettant de résoudre notre planning
 */
public class SolverManager {

    public static final String SOLVER_XML_RESSOURCE_PATH = "fr/planninggo/scheduler/ScheduleCampainSolverConfig.xml";

    private static final int NB_SUP_MAX_SOLVING = 2;

    private SolverManager() {

    }

    /**
     * Soumet un planning à résoudre au solveur de satisfaction de contraintes (OptaPlanner)
     *
     * @param createScheduleCampaign Function<SolverParam, ScheduleCampaign>
     * @return Planning ScheduleCampaign résultat du solveur
     * @throws IOException
     * @throws ParseException
     */
    public static ScheduleCampaign solve(Function<SolverParam, ScheduleCampaign> createScheduleCampaign, SolverParam solverParam) {

        ScheduleCampaign unsolvedScheduleCampaign = createScheduleCampaign.apply(solverParam);
        SolverFactory<ScheduleCampaign> solverFactory = SolverFactory.createFromXmlResource(SOLVER_XML_RESSOURCE_PATH);
        Solver<ScheduleCampaign> solver = solverFactory.buildSolver();

        solver.addEventListener(event -> {
            if (event.getNewBestSolution().getScore().getHardScore() >= 0 &&
                event.getNewBestSolution().getScore().getSoftScore() >= 0) {
                solver.terminateEarly();
            }
        });

        int consWeightMinOtherAttHalfDay = unsolvedScheduleCampaign.getConstraintWeight(ConstraintWeight.MINIMIZE_OTHER_ATTENDEE_PER_HALF_DAY);
        int consWeightMinOverlappedDefs = unsolvedScheduleCampaign.getConstraintWeight(ConstraintWeight.MINIMIZE_OVERLAPPED_DEFENSES);
        int consWeightMinDipersedOtherAttDefsDay = unsolvedScheduleCampaign.getConstraintWeight(ConstraintWeight.MINIMIZE_DISPERSED_OTHER_ATTENDEE_DEFENSES_DAY);
        int consWeightMiddleDayDefs = unsolvedScheduleCampaign.getConstraintWeight(ConstraintWeight.MIDDLE_DAY_DEFENSES);

        unsolvedScheduleCampaign.updateConstraintWeight(ConstraintWeight.UNAVAILABLE_OTHER_ATTENDEE, 0);
        unsolvedScheduleCampaign.updateConstraintWeight(ConstraintWeight.CONFLICT_OTHER_ATTENDEE, 0);
        unsolvedScheduleCampaign.updateConstraintWeight(ConstraintWeight.MINIMIZE_OTHER_ATTENDEE_PER_HALF_DAY, 0);
        unsolvedScheduleCampaign.updateConstraintWeight(ConstraintWeight.MINIMIZE_OVERLAPPED_DEFENSES, 0);
        unsolvedScheduleCampaign.updateConstraintWeight(ConstraintWeight.MINIMIZE_DISPERSED_OTHER_ATTENDEE_DEFENSES_DAY, 0);
        unsolvedScheduleCampaign.updateConstraintWeight(ConstraintWeight.MIDDLE_DAY_DEFENSES, 0);

        ScheduleCampaign initScc = unsolvedScheduleCampaign.createClone();
        ScheduleCampaign solution = solver.solve(unsolvedScheduleCampaign);

        if(retrySolvingIfNeeded(solver, solution, false) < 0) {
            retrySolvingIfNeeded(solver, solution, false);
        }

        solution.updateConstraintWeight(ConstraintWeight.UNAVAILABLE_OTHER_ATTENDEE, solution.getConstraintWeight(ConstraintWeight.UNAVAILABLE_ATTENDEE));
        solution.updateConstraintWeight(ConstraintWeight.CONFLICT_OTHER_ATTENDEE, solution.getConstraintWeight(ConstraintWeight.CONFLICT_ATTENDEE));
        solution.updateConstraintWeight(ConstraintWeight.MINIMIZE_OTHER_ATTENDEE_PER_HALF_DAY, consWeightMinOtherAttHalfDay);
        solution.updateConstraintWeight(ConstraintWeight.MINIMIZE_OVERLAPPED_DEFENSES, consWeightMinOverlappedDefs);
        solution.updateConstraintWeight(ConstraintWeight.MINIMIZE_DISPERSED_OTHER_ATTENDEE_DEFENSES_DAY, consWeightMinDipersedOtherAttDefsDay);
        solution.updateConstraintWeight(ConstraintWeight.MIDDLE_DAY_DEFENSES, consWeightMiddleDayDefs);
        solution = solver.solve(solution);

        int hardScore = checksSolution(solution, true);
        for(int i = 0; i < NB_SUP_MAX_SOLVING; i++) {
            int newHardScore;
            if((newHardScore = retrySolvingIfNeeded(solver, solution, true)) >= 0) {
                break;
            }
            if(newHardScore < 0 && hardScore == newHardScore){
                solution = solver.solve(initScc);
                break;
            }
        }
        solution = solver.solve(solution);
        solution.extractUnscheduledDefenses();
        return solution;
    }

    private static int retrySolvingIfNeeded(Solver<ScheduleCampaign> solver, ScheduleCampaign solution, boolean solveDefenseAssign){
        int hardScore = checksSolution(solution, solveDefenseAssign);
        if (hardScore < 0) {
            solver.solve(solution);
        }
        return hardScore;
    }

    /**
     * Permet de vérifier la solution
     * et calcule un score indépendemment du sovleur.
     *
     * @param solution ScheduleCampaign
     * @return score int des contraintes
     */
    public static int checksSolution(ScheduleCampaign solution, boolean solveDefenseAssign) {
        Objects.requireNonNull(solution);
        int hardScore = 0;
        List<Defense> defenses = solution.getDefenses();
        defenses.addAll(solution.getUnscheduledDefenses());
        hardScore += checksConstraintsOnDefense(solution, defenses, solveDefenseAssign);
        return hardScore;
    }

    private static int checksConstraintsOnDefense(ScheduleCampaign solution, List<Defense> defenses, boolean solveDefenseAssign) {
        int hardScore = 0;
        for (Defense d : defenses) {
            hardScore += d.getHardScoreDefense(solution, defenses, solveDefenseAssign);
        }
        return hardScore;
    }
}
