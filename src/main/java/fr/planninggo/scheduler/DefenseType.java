package fr.planninggo.scheduler;

import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import org.jetbrains.annotations.NotNull;
import org.optaplanner.core.api.domain.lookup.PlanningId;

import java.time.Duration;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Classe permettant de définir le type de soutenance.
 */
public class DefenseType implements Comparable<DefenseType> {
    private String code;
    private String description;
    private Duration duration;
    private Set<AttendeeSettings> attendeeSettings;

    public DefenseType(String code, String description, Duration duration, Set<AttendeeSettings> attendeeSettings) {
        this.code = Objects.requireNonNull(code);
        this.description = Objects.requireNonNull(description);
        this.duration = Objects.requireNonNull(duration);
        this.attendeeSettings = Objects.requireNonNull(attendeeSettings);
    }

    public DefenseType(String code, String description, Duration duration) {
        this.code = Objects.requireNonNull(code);
        this.description = Objects.requireNonNull(description);
        this.duration = Objects.requireNonNull(duration);
        this.attendeeSettings = new HashSet<>();
    }

    /**
     * Permet de créer une nouvelle instance de DefenseType
     * @param code String code du DefenseType
     * @param description String description du DefenseType
     * @param strDuration String durée du DefenseType
     * @return nouvelle instance de DefenseType
     */
    public static DefenseType create(String code, String description, String strDuration) {
        return new DefenseType(code, description, Duration.ofSeconds(getNbSeconds(Objects.requireNonNull(strDuration))));
    }

    /**
     * Permet de créer une nouvelle instance de DefenseType
     * à partir de l'entité DefenseTypeEntity
     * @param dte DefenseTypeEntity
     * @return DefenseType
     */
    public static DefenseType create(DefenseTypeEntity dte) {
        DefenseType dt = DefenseType.create(dte.getCode(), dte.getDesc(), dte.getDuration());
        dte.getJuryMembers().forEach(j -> dt.addAttendeeSettings(AttendeeSettings.create(j)));
        return dt;
    }

    /**
     * Calcule et renvoie le nombre de secondes
     * pour la durée du DefenseType
     * @param strDuration String durée
     * @return nombre de secondes
     */
    private static int getNbSeconds(String strDuration) {
        String[] strDurations = strDuration.split("h");
        int nbHours = Integer.parseInt(strDurations[0]);
        int nbMinutes = Integer.parseInt(strDurations[1].trim());
        return nbHours * 3600 + nbMinutes * 60;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof DefenseType)) {
            return false;
        }
        DefenseType dt = (DefenseType) o;
        return code.equals(dt.code) &&
                description.equals(dt.description) &&
                duration.equals(dt.duration);
    }

    @Override
    public int hashCode() {
        return code.hashCode() ^
                description.hashCode() ^
                duration.hashCode();
    }

    @PlanningId
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = Objects.requireNonNull(code);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = Objects.requireNonNull(description);
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = Objects.requireNonNull(duration);
    }

    public Set<AttendeeSettings> getAttendeeSettings() {
        return Collections.unmodifiableSet(attendeeSettings);
    }

    @Override
    public String toString() {
        return "[code=" + code + ",description=" + description + ", duration=" + duration + "]";
    }

    /**
     * Ajout de AttendeeSettings
     * @param attendeeSettings
     */
    public void addAttendeeSettings(AttendeeSettings attendeeSettings) {
        Objects.requireNonNull(attendeeSettings);
        this.attendeeSettings.add(attendeeSettings);
    }

    /**
     * Suppression de AttendeeSettings
     * @param attendeeSettings
     */
    public void removeAttendeeSettings(AttendeeSettings attendeeSettings) {
        Objects.requireNonNull(attendeeSettings);
        this.attendeeSettings.remove(attendeeSettings);
    }

    @Override
    public int compareTo(DefenseType dt) {
        int res = duration.compareTo(dt.duration);
        if (res != 0) {
            return -res;
        }
        return res;
    }
}
