package fr.planninggo.scheduler;

import java.util.Objects;

/**
 * Représente une relation de tutorat entre participants (entre un étudiant et ses superviseurs)
 */
public class AttendeeRelation {
    private final Person person;
    private final Role role;

    public AttendeeRelation(Person person, Role role) {
        this.person = Objects.requireNonNull(person);
        this.role = Objects.requireNonNull(role);
    }

    public Person getPerson() {
        return person;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AttendeeRelation)) {
            return false;
        }
        AttendeeRelation attRel = (AttendeeRelation) o;
        return person.equals(attRel.person) && role.equals(attRel.role);
    }

    @Override
    public String toString() {
        return "AttendeeRelation{" +
                "person=" + person +
                ", role=" + role +
                '}';
    }

    @Override
    public int hashCode() {
        return person.hashCode() ^ role.hashCode();
    }
}
