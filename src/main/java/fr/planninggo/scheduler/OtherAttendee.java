package fr.planninggo.scheduler;

import java.util.ArrayList;
import java.util.List;

public class OtherAttendee implements Attendee {

    @Override
    public List<AttendeeRelation> getRelations() {
        return new ArrayList<>();
    }

    @Override
    public List<Person> getRelatedPersons() {
        return new ArrayList<>();
    }

    @Override
    public List<Person> getRelatedPersonsByRole(Role role) {
        return new ArrayList<>();
    }

    @Override
    public boolean isAssignedToDefenseType() {
        return false;
    }

    @Override
    public DefenseType getDefenseType() {
        return null;
    }

    @Override
    public void setDefenseType(DefenseType defenseType) {
        //Non supporté par OtherAttendee
    }
}
