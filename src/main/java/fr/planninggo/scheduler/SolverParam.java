package fr.planninggo.scheduler;

/**
 * Interface à implémenter par les paramètres à transmettre à la méthode
 * de résolution du solveur (méthode solve de SolverManager)
 */
public interface SolverParam {
}
