package fr.planninggo.scheduler;

import org.optaplanner.core.api.domain.lookup.PlanningId;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Classe réprésentant le créneau pour une éventuelle soutenance à placer
 */
public class Timeslot {
    private Long id;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private Set<DefenseType> defenseTypes;
    private final int indexTimeslot;
    private Day day;

    public Timeslot(Long id, int indexTimeslot, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        Objects.requireNonNull(id);
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        this.id = id;
        Objects.checkIndex(indexTimeslot, indexTimeslot + 1);
        this.indexTimeslot = indexTimeslot;
        this.startDateTime = Objects.requireNonNull(startDateTime);
        this.endDateTime = Objects.requireNonNull(endDateTime);
        this.defenseTypes = new HashSet<>();
    }

    public Timeslot(Long id, Day day, int indexTimeslot, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this(id, indexTimeslot, startDateTime, endDateTime);
        this.day = Objects.requireNonNull(day);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Timeslot)) {
            return false;
        }
        Timeslot t = (Timeslot) o;
        return startDateTime.equals(t.startDateTime) &&
                endDateTime.equals(t.endDateTime);
    }

    @Override
    public int hashCode() {
        return startDateTime.hashCode() ^
                endDateTime.hashCode();
    }

    @PlanningId
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Objects.requireNonNull(id);
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        this.id = id;
    }

    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = Objects.requireNonNull(startDateTime);
    }

    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = Objects.requireNonNull(endDateTime);
    }

    public Set<DefenseType> getDefenseTypes() {
        return defenseTypes;
    }

    public void setDefenseTypes(Set<DefenseType> defenseTypes) {
        this.defenseTypes = Objects.requireNonNull(defenseTypes);
    }

    public void addDefenseType(DefenseType defenseType) {
        defenseTypes.add(Objects.requireNonNull(defenseType));
    }

    /**
     * Permet de vérifier si la plage horaires du Timeslot
     * se chevauche avec l'instance actuelle du Timeslot
     *
     * @param other Timeslot
     * @return boolean indiquant le Timeslot possède une plage horaires en commun
     */
    public boolean overlaps(Timeslot other) {
        if (other == null) {
            return false;
        }
        return !(this.startDateTime.compareTo(other.endDateTime) >= 0 || this.endDateTime.compareTo(other.startDateTime) <= 0);
    }

    public LocalDate getDate(){
        return startDateTime.toLocalDate();
    }

    boolean isMorning(){
        return startDateTime.toLocalTime().isBefore(Availability.NOON_HOUR);
    }

    public int getIndexTimeslot() {
        return indexTimeslot;
    }

    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return "[id=" + id + ",start=" + startDateTime.format(format) + ",end=" + endDateTime.format(format) + ",defenseTypes=" + defenseTypes + "]";
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }
}
