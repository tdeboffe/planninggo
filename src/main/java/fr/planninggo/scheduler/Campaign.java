package fr.planninggo.scheduler;

import fr.planninggo.spring.campaign.entity.CampaignConstraintEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.TimeslotEntity;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Classe représentant une campagne de planification de soutenances
 */
public class Campaign {
    private static final SimpleDateFormat DF = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat TF = new SimpleDateFormat("HH:mm");

    private final int id;
    private final Map<Integer, ConstraintWeight> mapConsWeight;
    private final SortedMap<LocalDate, TreeSet<DateTimePeriod>> dayTimePeriods;
    private String name;
    private LocalDate dateBegin;
    private LocalDate dateEnd;

    public Campaign(int id, String name, LocalDate dateBegin, LocalDate dateEnd) {
        Objects.checkIndex(id, id + 1);
        this.id = id;
        this.name = Objects.requireNonNull(name);
        this.dateBegin = Objects.requireNonNull(dateBegin);
        this.dateEnd = Objects.requireNonNull(dateEnd);
        this.dayTimePeriods = new TreeMap<>();
        this.mapConsWeight = new HashMap<>();
    }

    /**
     * Permet de créer une nouvelle instance de Campaign
     * à partir d'une entité CampaignEntity
     * @param campaignEntity CampaignEntity
     * @return Campaign
     */
    public static Campaign create(CampaignEntity campaignEntity) {
        Set<DefenseDayEntity> defenseDayEntities = campaignEntity.getDefenseDays();
        Campaign campaign = new Campaign(campaignEntity.getId(),
                campaignEntity.getName(),
                getLocalDate(campaignEntity.getCampaignBegin()),
                getLocalDate(campaignEntity.getCampaignEnd()));
        defenseDayEntities.forEach(d -> campaign.addDateTimePeriods(d.getDate(), d.getSlots()));
        campaignEntity.getConstraintEntities()
                .forEach(campaign::addConstraint);
        campaign.addConstraint(new ConstraintWeight(ConstraintWeight.UNAVAILABLE_OTHER_ATTENDEE, "", campaign.getConstraintWeight(ConstraintWeight.UNAVAILABLE_ATTENDEE), true, true));
        campaign.addConstraint(new ConstraintWeight(ConstraintWeight.CONFLICT_OTHER_ATTENDEE, "", campaign.getConstraintWeight(ConstraintWeight.CONFLICT_ATTENDEE), true, true));
        return campaign;
    }

    private static LocalDate getLocalDate(Date date) {
        String strDate = DF.format(date);
        String[] dates = strDate.split("/");
        return LocalDate.of(Integer.parseInt(dates[2]),
                Integer.parseInt(dates[1]),
                Integer.parseInt(dates[0]));
    }

    private static LocalTime getLocalTime(Time time) {
        String strDate = TF.format(time);
        String[] dates = strDate.split(":");
        return LocalTime.of(Integer.parseInt(dates[0]),
                Integer.parseInt(dates[1]),
                0);
    }

    private static LocalDateTime getLocalDateTime(LocalDate localDate, Time time) {
        return LocalDateTime.of(localDate, getLocalTime(time));
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Campaign)) {
            return false;
        }
        Campaign c = (Campaign) o;
        return name.equals(c.name) &&
                dateBegin.equals(c.dateBegin) &&
                dateEnd.equals(c.dateEnd);
    }

    @Override
    public int hashCode() {
        return name.hashCode() ^
                dateBegin.hashCode() ^
                dateEnd.hashCode();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public LocalDate getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(LocalDate dateBegin) {
        this.dateBegin = Objects.requireNonNull(dateBegin);
    }

    public LocalDate getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDate dateEnd) {
        this.dateEnd = Objects.requireNonNull(dateEnd);
    }

    /**
     * Renvoie les plages horaires définies
     * @return Set<DateTimePeriod>
     */
    public Set<DateTimePeriod> getDayTimePeriods() {
        return dayTimePeriods.values()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    /**
     * Renvoie les entrées de la Map dayTimePeriods pour le stockage des
     * plages horaires par jour de soutenance
     * @return Set<Map.Entry<LocalDate, TreeSet<DateTimePeriod>>>
     */
    Set<Map.Entry<LocalDate, TreeSet<DateTimePeriod>>> getEntriesDayTimePeriods(){
        return dayTimePeriods.entrySet();
    }

    /**
     * Renvoie le poids (weight) de la contrainte
     * répertoriée dans mapConsWeight.
     * Autrement, si l'id de la contrainte n'est pas répertoriée,
     * -1 est renvoyé à la place
     *
     * @param id int id de la contrainte
     * @return weight correspondant au poids de la contrainte (-1 si non répertoriée)
     */
    public int getConstraintWeight(int id) {
        ConstraintWeight cw = mapConsWeight.getOrDefault(id, null);
        if (cw != null) {
            return cw.getWeight();
        }
        return -1;
    }

    private void addDateTimePeriods(Date date, Set<TimeslotEntity> timeslotEntities) {
        LocalDate localDate = getLocalDate(date);
        Set<DateTimePeriod> dateTimePeriods = timeslotEntities.stream()
                .map(t -> new DateTimePeriod(getLocalDateTime(localDate, t.getBegin()),
                        getLocalDateTime(localDate, t.getEnd())))
                .collect(Collectors.toSet());
        TreeSet<DateTimePeriod> dtpSorted = new TreeSet<>(DateTimePeriod::compareTo);
        dtpSorted.addAll(dateTimePeriods);
        dayTimePeriods.merge(localDate, dtpSorted, (k, v) -> {
            v.addAll(dtpSorted);
            return v;
        });
    }

    private void addConstraint(ConstraintWeight cw) {
        Objects.requireNonNull(cw);
        mapConsWeight.put(cw.getId(), cw);
    }

    private void addConstraint(CampaignConstraintEntity campaignConstraintEntity) {
        Objects.requireNonNull(campaignConstraintEntity);
        mapConsWeight.put(campaignConstraintEntity.getConstraint().getId(),
                ConstraintWeight.create(campaignConstraintEntity));
    }

    /**
     * Modifie le poids d'une contrainte
     *
     * @param id int identifiant de la contrainte à mettre à jour
     * @param weight int nouveau poids de la contrainte
     */
    public void updateConstraintWeight(int id, int weight){
        mapConsWeight.computeIfPresent(id, (k, v) -> {
            v.setWeight(weight);
            return v;
        });
    }

}
