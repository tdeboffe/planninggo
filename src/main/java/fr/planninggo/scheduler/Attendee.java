package fr.planninggo.scheduler;

import java.util.List;

/**
 * Interface des participants
 */
public interface Attendee {
    /**
     * @return La liste des relations List<AttendeeRelation>
     */
    List<AttendeeRelation> getRelations();

    /**
     * @return la liste des personnes en relations List<Person>
     */
    List<Person> getRelatedPersons();

    /**
     * Permet d'obtenir la liste des Person en relation avec le rôle qui convient
     *
     * @param role Role des personnes à avoir parmi les relations
     * @return la liste des personnes en relations List<Person> avec le rôle correspondant
     */
    List<Person> getRelatedPersonsByRole(Role role);

    /**
     * Vérifie si un type de soutenance (DefenseType) est affecté
     *
     * @return boolean indiquant si le type de soutenance est assigné
     */
    boolean isAssignedToDefenseType();

    /**
     * Renvoie le type de soutenance d'un participant
     *
     * @return DefenseType
     */
    DefenseType getDefenseType();

    /**
     * Assigne un type de soutenance spécifique
     *
     * @param defenseType DefenseType
     */
    void setDefenseType(DefenseType defenseType);
}
