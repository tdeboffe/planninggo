package fr.planninggo.scheduler;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.repository.DefenseRepository;

/**
 * Classe représentant les paramètres du solveur
 */
public class SolverProdParam implements SolverParam {
    private final CampaignEntity campaignEntity;
    private final DefenseRepository defenseRepository;

    public SolverProdParam(CampaignEntity campaignEntity, DefenseRepository defenseRepository) {
        this.campaignEntity = campaignEntity;
        this.defenseRepository = defenseRepository;
    }

    public CampaignEntity getCampaignEntity() {
        return campaignEntity;
    }

    DefenseRepository getDefenseRepository() {
        return defenseRepository;
    }
}
