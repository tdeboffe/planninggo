package fr.planninggo.scheduler;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.entity.PlanningPin;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Classe représentant la correspondance entre un Defense et un OtherAttendee
 */
@PlanningEntity(difficultyComparatorClass = DefenseAssignDifficultyComparator.class)
public class DefenseAssign implements Comparable<DefenseAssign> {

    private Long id;
    private Defense defense;

    @PlanningPin
    private boolean pinned;

    private Person otherAttendee;

    public DefenseAssign(Long id, boolean pinned, Person otherAttendee, Defense defense) {
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        Objects.requireNonNull(id);
        this.id = id;
        this.pinned = pinned;
        this.otherAttendee = otherAttendee;
        this.defense = Objects.requireNonNull(defense);
    }

    public DefenseAssign() {

    }

    @PlanningId
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        Objects.requireNonNull(id);
        this.id = id;
    }

    boolean isPinned() {
        return pinned;
    }

    @PlanningVariable(valueRangeProviderRefs = "guestAttendeeRange")
    public Person getOtherAttendee() {
        return otherAttendee;
    }

    public void setOtherAttendee(Person otherAttendee) {
        this.otherAttendee = Objects.requireNonNull(otherAttendee);
    }

    public Defense getDefense() {
        return defense;
    }

    public void setDefense(Defense defense) {
        this.defense = Objects.requireNonNull(defense);
    }

    public Day getDay(){
        return defense.getDay();
    }

    private int getIndexTimeslot(){
        return defense.getTimeslot().getIndexTimeslot();
    }

    /**
     * Vérifie si l'instance actuelle de DefenseAssign se déroule le même jour
     * et même demi-journée (matin ou après-midi) que l'autre DefenseAssign
     * et détermine si leur OtherAttendee est différent
     * @param defenseAssign DefenseAssign
     * @return boolean
     */
    public boolean hasDifferentOtherAttendeeFromHalfDayDefenses(DefenseAssign defenseAssign){
        if(this == defenseAssign){
            return false;
        }
        Timeslot currentTimeslot = defense.getTimeslot();
        Timeslot otherDefenseTimeslot = defenseAssign.defense.getTimeslot();
        if(currentTimeslot.getDate().equals(otherDefenseTimeslot.getDate()) &&
           currentTimeslot.isMorning() == otherDefenseTimeslot.isMorning()){
           return !otherAttendee.equals(defenseAssign.otherAttendee);
        }
        return false;
    }

    /**
     * Vérifie si des soutenances ne sont pas adjacentes
     * @param defenseAssign DefenseAssign
     * @return boolean
     */
    public boolean isNotAdjacentDefense(DefenseAssign defenseAssign){
        if(this == defenseAssign){
            return false;
        }
        int currentIndexTimeslot = getIndexTimeslot();
        int otherDefenseIndexTimeslot = defenseAssign.getIndexTimeslot();
        return currentIndexTimeslot == otherDefenseIndexTimeslot - 1 ||
                currentIndexTimeslot == otherDefenseIndexTimeslot + 1;
    }

    /**
     * Vérifie si l'OtherAttendee habite en province
     * @return boolean
     */
    public boolean hasNotOtherAttendeeLivingParisArea() {
        if (otherAttendee == null) {
            return true;
        }
        return !otherAttendee.isLivesParisArea();
    }

    /**
     * Calcule et renvoie le nombre d'heures d'écarts
     * par rapport à la plage horaires préférable pour un participant venant de province.
     * @return nombre d'heures d'écarts
     */
    public int getTimeslotDistanceWithNoonHour(){
        Timeslot currentTimeslot = defense.getTimeslot();
        int referenceHour = currentTimeslot.isMorning()? Availability.NOON_HOUR.getHour() : (Availability.NOON_HOUR.getHour() + 1);
        int res = currentTimeslot.getStartDateTime().toLocalTime().getHour() - referenceHour;
        res = Math.abs(res);
        if(res <= 2){
            return 0;
        }
        return res - 2;
    }

    /**
     * Permet de vérifier si des participants à la soutenance (Person)
     * sont indisponibles
     * @return boolean
     */
    public boolean hasUnavailableAttendee() {
        return !otherAttendee.isAvailable(defense);
    }

    /**
     * Permet de vérifier si la soutenance (instance actuelle de Defense dans DefenseAssign)
     * se chevauche avec une autre soutenance au niveau de la plage horaires
     *
     * @param defenseAssign DefenseAssign
     * @return boolean
     */
    public boolean overlaps(DefenseAssign defenseAssign) {
        Objects.requireNonNull(defenseAssign);
        Defense d = defenseAssign.defense;
        if (d == null || defense == null || this == defenseAssign) {
            return false;
        }
        return defense.overlaps(d);
    }

    /**
     * Permet de vérifier si l'instance actuelle de DefenseAssign
     * n'a pas un Person commun avec un autre DefenseAssign
     * @param defenseAssign DefenseAssign
     * @return boolean
     */
    public boolean hasSameAttendee(DefenseAssign defenseAssign) {
        Objects.requireNonNull(defenseAssign);
        Person otherAtt = defenseAssign.otherAttendee;
        if (otherAtt == null || this.otherAttendee == null || this == defenseAssign) {
            return false;
        }
        return this.otherAttendee.equals(otherAtt);
    }

    /**
     * Retourne les soutenances (Defense) qui se chevauchent avec l'instance actuelle de Defense dans DefenseAssign
     * @param defenseAssigns Set<DefenseAssign>
     * @return Set<DefenseAssign>
     */
    private Set<DefenseAssign> overlappingDefenses(Set<DefenseAssign> defenseAssigns) {
        Objects.requireNonNull(defenseAssigns);
        return defenseAssigns.stream()
                .filter(da -> da.defense != this.defense && this.defense.overlaps(da.defense))
                .collect(Collectors.toSet());
    }

    /**
     * Renvoie le score pour les contraintes de validité appliquées
     * sur l'instance actuelle de DefenseAssign
     * @param solution
     * @return
     */
    int getHardScoreDefenseAssign(ScheduleCampaign solution) {
        Set<DefenseAssign> defenseAssigns = solution.getDefenseAssigns();
        int hardScore = 0;
        if (this.otherAttendee == null) { return hardScore; }
        if (this.hasUnavailableAttendee()) {
            hardScore -= solution.getConstraintWeight(ConstraintWeight.UNAVAILABLE_ATTENDEE);
        }
        for (DefenseAssign otherDefenseAssign : this.overlappingDefenses(defenseAssigns)) {
            if (this.hasSameAttendee(otherDefenseAssign)) {
                hardScore -= solution.getConstraintWeight(ConstraintWeight.CONFLICT_ATTENDEE);
            }
        }
        return hardScore;
    }

    @Override
    public String toString() {
        return "[id=" + getId() + ",pinned=" + pinned + ",otherAttendee=" + otherAttendee + ",defense=" + defense + "]\n " + defense;
    }

    @Override
    public int compareTo(DefenseAssign defenseAssign) {
        int res = defense.compareTo(defenseAssign.defense);
        if (res != 0) {
            return res;
        }
        return 0;
    }
}
