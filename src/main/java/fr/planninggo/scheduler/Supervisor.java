package fr.planninggo.scheduler;

import org.apache.commons.csv.CSVRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe représentant un tuteur (Encadrant école ou entreprise)
 */
public class Supervisor implements Attendee {
    private List<AttendeeRelation> students = new ArrayList<>();

    public static Person create(CSVRecord record) {
        return new Person(Integer.parseInt(record.get(0)),
                true,
                record.get(1),
                record.get(4),
                record.get(2),
                record.get(3));
    }

    public void setStudents(List<AttendeeRelation> students) {
        this.students = students;
    }

    void addAttendeeRelation(AttendeeRelation attendeeRelation) {
        if (!students.contains(attendeeRelation)) {
            students.add(new AttendeeRelation(attendeeRelation.getPerson(), attendeeRelation.getRole()));
        }
    }

    @Override
    public List<AttendeeRelation> getRelations() {
        return students;
    }

    @Override
    public List<Person> getRelatedPersons() {
        return students.stream()
                .map(AttendeeRelation::getPerson)
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> getRelatedPersonsByRole(Role role) {
        return students.stream()
                .filter(rel -> rel.getRole().equals(role))
                .map(AttendeeRelation::getPerson)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isAssignedToDefenseType() {
        return false;
    }

    @Override
    public String toString() {
        return "Supervisor{" +
                "students=" + students +
                '}';
    }

    @Override
    public DefenseType getDefenseType() {
        return null;
    }

    @Override
    public void setDefenseType(DefenseType defenseType) {
        //Non supporté par Supervisor
    }
}
