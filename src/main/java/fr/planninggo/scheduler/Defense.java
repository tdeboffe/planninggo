package fr.planninggo.scheduler;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.entity.PlanningPin;
import org.optaplanner.core.api.domain.lookup.PlanningId;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Classe représentant une soutenance
 */
@PlanningEntity(difficultyComparatorClass = DefenseDifficultyComparator.class)
public class Defense implements Comparable<Defense> {
    private Long id;
    private Person student;
    private DefenseType defenseType;
    private List<Person> supervisorPersons;
    private Set<DefenseAssign> defenseAssigns = new HashSet<>();
    private final SortedMap<Long, Integer> mapNbCommonDefenseSup = new TreeMap<>();

    @PlanningPin
    private boolean pinned;

    // Planning variables: change durant la résolutino du planning, entre les calculs de scores.
    private Room room;
    private Timeslot timeslot;

    public Defense(Long id, Room room, boolean pinned, Person student, Timeslot timeslot, DefenseType defenseType) {
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        Objects.requireNonNull(id);
        this.id = id;
        this.room = Objects.requireNonNull(room);
        this.pinned = pinned;
        this.student = Objects.requireNonNull(student);
        this.timeslot = Objects.requireNonNull(timeslot);
        this.defenseType = Objects.requireNonNull(defenseType);
    }

    public Defense() {

    }

    @PlanningId
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        Objects.requireNonNull(id);
        this.id = id;
    }

    public Set<DefenseAssign> getDefenseAssigns() {
        return defenseAssigns;
    }

    void setDefenseAssigns(Set<DefenseAssign> defenseAssigns) {
        this.defenseAssigns = Objects.requireNonNull(defenseAssigns);
    }

    @PlanningVariable(valueRangeProviderRefs = "roomRange")
    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = Objects.requireNonNull(room);
    }

    public boolean isPinned() {
        return pinned;
    }

    public Person getStudent() {
        return student;
    }

    public void setStudent(Person student) {
        this.student = Objects.requireNonNull(student);
    }

    private List<Person> getSupervisorPersons() {
        return supervisorPersons;
    }

    void setSupervisorPersons(List<Person> supervisorPersons) {
        this.supervisorPersons = Objects.requireNonNull(supervisorPersons);
    }

    @PlanningVariable(valueRangeProviderRefs = "timeslotRange")
    public Timeslot getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(Timeslot timeslot) {
        this.timeslot = timeslot;
    }

    public DefenseType getDefenseType() {
        return defenseType;
    }

    public void setDefenseType(DefenseType defenseType) {
        this.defenseType = Objects.requireNonNull(defenseType);
    }

    /**
     * Permet de vérifier si la soutenance (instance actuelle de Defense)
     * se chevauche avec une autre soutenance au niveau de la plage horaires
     *
     * @param otherDefense Defense
     * @return boolean
     */
    public boolean overlaps(Defense otherDefense) {
        Objects.requireNonNull(otherDefense);
        if(this == otherDefense){
            return false;
        }
        return timeslot.overlaps(otherDefense.getTimeslot());
    }

    /**
     * Retourne les soutenances (Defense) qui se chevauchent avec l'instance actuelle
     *
     * @param defenses Set<Defense>
     * @return Set<Defense>
     */
    public Set<Defense> overlappingDefenses(List<Defense> defenses) {
        Objects.requireNonNull(defenses);
        return defenses.stream().filter(v -> v != this && this.overlaps(v)).collect(Collectors.toSet());
    }

    /**
     * Permet de renvoyer le nombre de participants (Supervisor)
     * qui ne sont pas disponibles
     *
     * @return nombre (int) de participants indisponibles
     */
    public int getNbUnavailableAttendees() {
        int nbUnavailable = 0;
        for (Person p : supervisorPersons) {
            if (!p.isAvailable(this)) {
                nbUnavailable++;
            }
        }
        return nbUnavailable;
    }

    /**
     * Permet de renvoyer le nombre de participants (Supervisor)
     * qui doivent assister à la fois à la soutenance actuelle (instance actuelle de Defense)
     * et à une autre (otherDefense)
     *
     * @param otherDefense Defense autre soutenance
     * @return nombre de participants assistant aux deux soutenances
     */
    int getNbAttendeesSameDefense(Defense otherDefense) {
        Objects.requireNonNull(otherDefense);
        int nbAttendeesSameDefense = 0;
        if(this == otherDefense){
            return nbAttendeesSameDefense;
        }
        for (Person p : supervisorPersons) {
            if (otherDefense.getSupervisorPersons().contains(p)) {
                nbAttendeesSameDefense++;
            }
        }
        return nbAttendeesSameDefense;
    }

    /**
     * Renvoie le nombre de participants en commun entre les deux soutenances
     * (entre l'instance actuelle de Defense et le paramètre de la méthode)
     * @param otherDefense Defense
     * @return nombre de participants communs
     */
    public int getNbAttendeesSameDefenseFromCache(Defense otherDefense) {
        Objects.requireNonNull(otherDefense);
        if(this == otherDefense){
            return 0;
        }
        return mapNbCommonDefenseSup.getOrDefault(otherDefense.getId(), 0);
    }

    /**
     * Permet de vérifier si l'instance actuelle de la soutenance (Defense)
     * n'a pas une salle (Room) commune avec une autre soutenance
     *
     * @param otherDefense Defense
     * @return boolean
     */
    boolean hasSameRoom(Defense otherDefense) {
        Objects.requireNonNull(otherDefense);
        if(this == otherDefense){
            return false;
        }
        return room.equals(otherDefense.getRoom());
    }

    /**
     * Permet de vérifier si des salles (Room)
     * sont indisponibles
     *
     * @return boolean
     */
    public boolean hasUnavailableRoom() {
        return room != null && !room.isAvailable(this);
    }

    public Defense getDefense() {
        return this;
    }

    @Override
    public String toString() {
        return "Defense{" +
                "id=" + id +
                "timeslot=" + timeslot +
                ", room=" + room +
                ", student=" + student +
                '}';
    }

    @Override
    public int compareTo(Defense d) {
        int res = defenseType.compareTo(d.defenseType);
        if (res != 0) {
            return res;
        }
        return 0;
    }

    /**
     * Permet d'ajouter un DefenseAssign qui seraient associé
     * à l'instance actuelle de Defense
     * @param defenseAssign DefenseAssign
     */
    public void addDefenseAssign(DefenseAssign defenseAssign) {
        Objects.requireNonNull(defenseAssign);
        defenseAssigns.add(defenseAssign);
    }

    /**
     * Permet de construire la map de Defense dont
     * les participants sont en communs avec l'instance actuelle.
     * @param defenses List<Defense>
     */
    public void setNbCommonDefenseSup(List<Defense> defenses){
        for(Defense d : defenses){
            if(this != d) {
                for (Person p : d.getSupervisorPersons()) {
                    if (supervisorPersons.contains(p)) {
                        mapNbCommonDefenseSup.merge(d.getId(), 1, (k, v) -> v + 1);
                    }
                }
            }
        }
    }

    /**
     * Vérifie si l'instance actuelle de Defense
     * est une soutenance non planifiée (ne respecte pas toutes les contraintes de validité).
     * @param solution ScheduleCampaign solution générée par le solveur
     * @param defenses List<Defense> Liste de toutes les soutenances
     * @return boolean indiquant si la soutenance est non planifiée
     */
    public boolean isUnscheduledDefense(ScheduleCampaign solution, List<Defense> defenses){
        return (getHardScoreDefense(solution, defenses, true) < 0);
    }

    /**
     * Renvoie le score pour les contraintes de validité appliquées
     * sur l'instance actuelle de Defense
     * @param solution ScheduleCampaign solution générée par le solveur
     * @param defenses List<Defense> Liste de toutes les soutenances
     * @param solveDefenseAssign boolean applique les contraintes de validité sur les DefenseAssign
     * @return score des contraintes de validité appliquées
     */
    public int getHardScoreDefense(ScheduleCampaign solution, List<Defense> defenses, boolean solveDefenseAssign){
        int hardScore = 0;
        hardScore -= getNbUnavailableAttendees() * solution.getConstraintWeight(ConstraintWeight.UNAVAILABLE_ATTENDEE);
        if (!room.isAvailable(this)) {
            hardScore -= solution.getConstraintWeight(ConstraintWeight.UNAVAILABLE_ROOM);
        }

        if (!goodTimeSlot()) {
            hardScore -= solution.getConstraintWeight(ConstraintWeight.DIFFERENTE_DEFENSE_TYPE);
        }
        hardScore -= solution.getConstraintWeight(ConstraintWeight.SAME_ATTENDEE) * sameAttendee();

        for (Defense otherDefense : overlappingDefenses(defenses)) {
            hardScore -= getNbAttendeesSameDefense(otherDefense) * solution.getConstraintWeight(ConstraintWeight.CONFLICT_ATTENDEE);

            if (hasSameRoom(otherDefense)) {
                hardScore -= solution.getConstraintWeight(ConstraintWeight.CONFLICT_ROOM);
            }
        }
        if(solveDefenseAssign) {
            for (DefenseAssign da : defenseAssigns) {
                hardScore += da.getHardScoreDefenseAssign(solution);
            }
        }
        return hardScore;
    }

    /**
     * Renvoie le jour de soutenance (Day)
     * pour le timeslot correspondant au Defense.
     * @return Day
     */
    public Day getDay() {
        return timeslot.getDay();
    }

    /**
     * Permet de renvoyer le nombre de participants identiques
     * dans l'instance actuelle de Defense.
     * @return nombre de participants identiques au sein de la soutenance
     */
    public int sameAttendee(){
        List<Person> attendees = defenseAssigns.stream().map(DefenseAssign::getOtherAttendee).collect(Collectors.toList());

        int same = 0;
        for(Person p : supervisorPersons){
            if(attendees.contains(p)){
                same++;
            }
        }

        Set<Person> setPerson = new HashSet<>();

        for(Person p : attendees){
            if(!setPerson.add(p)){
                same++;
            }
        }

        return same;
    }

    /**
     * Vérifie si le timeslot affecté est conforme au type de la soutenance.
     * @return boolean
     */
    public boolean goodTimeSlot(){
        return timeslot.getDefenseTypes().contains(student.getDefenseType());
    }
}
