package fr.planninggo.scheduler;

import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import org.apache.log4j.Logger;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Classe mère du solveur, contient au final l'ensemble des soutenances
 */
@PlanningSolution
public class ScheduleCampaign {
    private static Logger logger = Logger.getLogger(ScheduleCampaign.class.getName());
    private Long id;
    private Campaign campaign;
    private Set<Person> otherAttendees;
    private Set<Room> rooms;
    private Set<Person> students;
    private Set<Timeslot> timeslots;
    private Set<Day> days;
    private Set<Person> supervisors;
    private List<Defense> defenses;
    private List<Defense> unscheduledDefenses = new ArrayList<>();
    private Set<DefenseAssign> defenseAssigns;
    private Set<DefenseType> defenseTypes;
    private HardSoftScore score = null;

    /**
     * Permet de créer une nouvelle instance de ScheduleCampaign
     * @param solverParam SolverParam paramètres du solveur
     * @return ScheduleCampaign
     */
    public static ScheduleCampaign create(SolverParam solverParam) {
        ScheduleCampaign scheduleCampaign = new ScheduleCampaign();
        scheduleCampaign.id = 1L;
        setProblemFacts(scheduleCampaign, (SolverProdParam) solverParam);
        return scheduleCampaign;
    }

    /**
     * Créé une nouvelle instance de ScheduleCampaign
     * avec les mêmes valeurs que l'instance actuelle
     * @return instance clonée ScheduleCampaign
     */
    public ScheduleCampaign createClone(){
        ScheduleCampaign cloneScc = new ScheduleCampaign();
        cloneScc.id = this.id;
        cloneScc.campaign = this.campaign;
        cloneScc.otherAttendees = new HashSet<>();
        cloneScc.rooms = new HashSet<>();
        cloneScc.students = new HashSet<>();
        cloneScc.timeslots = new HashSet<>();
        cloneScc.days = new HashSet<>();
        cloneScc.supervisors = new HashSet<>();
        cloneScc.defenses = new ArrayList<>();
        cloneScc.unscheduledDefenses = new ArrayList<>();
        cloneScc.defenseAssigns = new HashSet<>();
        cloneScc.defenseTypes = new HashSet<>();

        cloneScc.otherAttendees.addAll(this.otherAttendees);
        cloneScc.rooms.addAll(this.rooms);
        cloneScc.students.addAll(this.students);
        cloneScc.timeslots.addAll(this.timeslots);
        cloneScc.days.addAll(this.days);
        cloneScc.supervisors.addAll(this.supervisors);
        cloneScc.defenses.addAll(this.defenses);
        cloneScc.unscheduledDefenses.addAll(this.unscheduledDefenses);
        cloneScc.defenseAssigns.addAll(this.defenseAssigns);
        cloneScc.defenseTypes.addAll(this.defenseTypes);

        return cloneScc;
    }

    /**
     * Permet de créer une nouvelle instance de ScheduleCampaign
     * représentant une partition du jeu de données à résoudre
     * (utilisé par le partitioner)
     * @param numPartition int numéro de partition
     * @param originalSolution ScheduleCampaign planning originale (sans partitionnement)
     * @param sizePartition int taille de la partition (nombre de soutenances)
     * @return partition ScheduleCampaign
     */
    public static ScheduleCampaign createPartition(int numPartition, ScheduleCampaign originalSolution, int sizePartition){
        ScheduleCampaign scheduleCampaign = new ScheduleCampaign();
        scheduleCampaign.id = (long) numPartition;
        scheduleCampaign.campaign = originalSolution.getCampaign();
        scheduleCampaign.otherAttendees = originalSolution.getOtherAttendees();
        scheduleCampaign.rooms = originalSolution.getRooms();
        scheduleCampaign.timeslots = originalSolution.getTimeslots();
        scheduleCampaign.defenseTypes = originalSolution.getDefenseTypes();
        if(numPartition == sizePartition){
            scheduleCampaign.defenses = originalSolution.getDefenses()
                    .stream()
                    .filter(d -> d.getId().compareTo((long) ((numPartition - 1) * sizePartition)) > 0)
                    .collect(Collectors.toList());
        }else {
            scheduleCampaign.defenses = originalSolution.getDefenses()
                    .stream()
                    .filter(d -> d.getId().compareTo((long) ((numPartition - 1) * sizePartition)) > 0 &&
                            d.getId().compareTo((long) (numPartition * sizePartition)) <= 0)
                    .collect(Collectors.toList());
        }
        scheduleCampaign.defenseAssigns = scheduleCampaign.defenses
                .stream()
                .map(Defense::getDefenseAssigns)
                .flatMap(Set<DefenseAssign>::stream)
                .collect(Collectors.toSet());
        scheduleCampaign.students = scheduleCampaign.defenses
                .stream()
                .map(Defense::getStudent)
                .collect(Collectors.toSet());
        scheduleCampaign.supervisors = scheduleCampaign.students
                .stream()
                .map(Person::getRelatedPersons)
                .flatMap(List<Person>::stream)
                .collect(Collectors.toSet());
        return scheduleCampaign;
    }

    private static void setProblemFacts(ScheduleCampaign scheduleCampaign,
                                        SolverProdParam solverProdParam) {

        CampaignEntity campaignEntity = solverProdParam.getCampaignEntity();
        DefenseRepository defenseRepository = solverProdParam.getDefenseRepository();

        Map<Integer, Person> mapStudents = new HashMap<>();
        Map<Long, Room> mapRooms = new HashMap<>();
        Map<Timestamp, Timeslot> mapTimeslot = new HashMap<>();
        SortedMap<Integer, Person> mapSupervisors = new TreeMap<>();
        SortedMap<String, DefenseType> mapDefenseTypes = new TreeMap<>();
        Map<Integer, Person> mapOtherAttendees;

        scheduleCampaign.campaign = Campaign.create(campaignEntity);
        scheduleCampaign.defenseTypes = campaignEntity.getDefensesType()
                .stream()
                .map(dte -> DefenseType.create(dte.getCode(), dte.getDesc(), dte.getDuration()))
                .collect(Collectors.toSet());

        scheduleCampaign.defenseTypes.forEach(dt -> mapDefenseTypes.put(dt.getCode(), dt));
        Set<StudentEntity> studentsEntity = campaignEntity.getStudents();

        scheduleCampaign.supervisors = studentsEntity.stream()
                .map(StudentEntity::getSupervisors)
                .flatMap(Set<SupervisorStudentEntity>::stream)
                .map(SupervisorStudentEntity::getSupervisor)
                .map(sup -> Person.create(sup, new Supervisor(), Optional.empty()))
                .collect(Collectors.toSet());

        scheduleCampaign.supervisors.forEach(p -> mapSupervisors.put(p.getId(), p));


        scheduleCampaign.students = studentsEntity.stream()
                .map(s -> Person.create(s, new Student(), Optional.of(a -> {
                    Student student = (Student) a.getAttendee();
                    DefenseType dt = null;
                    if ((dt = mapDefenseTypes.get(s.getDefenseType().getCode())) != null) {
                        student.setDefenseType(dt);
                    }
                    Set<Person> supervisors = s.getSupervisors()
                            .stream()
                            .map(SupervisorStudentEntity::getSupervisor)
                            .filter(sup -> mapSupervisors.get(sup.getId()) != null)
                            .map(sup -> mapSupervisors.get(sup.getId()))
                            .collect(Collectors.toSet());

                    student.setSupervisors(supervisors.stream()
                            .map(p -> new AttendeeRelation(p, s.getSupervisors().stream()
                                    .filter(sup -> sup.getSupervisor().getId() == p.getId())
                                    .map(sup -> Role.create(sup.getRoleEntity())).findAny().get()))
                            .collect(Collectors.toList()));

                    supervisors.stream().map(sup -> (Supervisor) sup.getAttendee())
                            .forEach(sup -> sup.addAttendeeRelation(new AttendeeRelation(a, Role.create(s.getAccount().getRoleEntities().iterator().next()))));
                })))
                .collect(Collectors.toSet());

        scheduleCampaign.students.forEach(p -> mapStudents.put(p.getId(), p));


        scheduleCampaign.otherAttendees = campaignEntity.getGuests()
                .stream()
                .map(g -> Person.create(g, new OtherAttendee(), Optional.empty()))
                .collect(Collectors.toSet());

        mapOtherAttendees = getMapOtherAttendees(scheduleCampaign);

        scheduleCampaign.rooms = campaignEntity.getDefenseDays()
                .stream()
                .map(DefenseDayEntity::getRooms)
                .flatMap(Set<RoomEntity>::stream)
                .map(r -> Room.create(r, campaignEntity.getDefenseDays()))
                .collect(Collectors.toSet());

        scheduleCampaign.rooms.forEach(r -> mapRooms.put(r.getId(), r));

        createDefenseTimeslots(scheduleCampaign);

        scheduleCampaign.timeslots.forEach(t -> mapTimeslot.put(Timestamp.valueOf(t.getStartDateTime()), t));

        createDefensesAndDefenseAssigns(scheduleCampaign, defenseRepository, mapRooms, mapTimeslot, mapStudents, mapDefenseTypes, mapOtherAttendees);
        for(Defense d : scheduleCampaign.defenses) {
            d.setNbCommonDefenseSup(scheduleCampaign.defenses);
        }
    }

    private static Map<Integer, Person> getMapOtherAttendees(ScheduleCampaign scheduleCampaign) {
        Map<Integer, Person> mapOtherAttendees = new HashMap<>();
        scheduleCampaign.otherAttendees.forEach(o -> mapOtherAttendees.put(o.getId(), o));
        return mapOtherAttendees;
    }

    private static void createDefenseTimeslots(ScheduleCampaign scheduleCampaign) {
        Long numTimeslot = 0L;
        scheduleCampaign.timeslots = new HashSet<>();
        scheduleCampaign.days = new HashSet<>();
        for (Map.Entry<LocalDate, TreeSet<DateTimePeriod>> entry : scheduleCampaign.campaign.getEntriesDayTimePeriods()) {
            for(DateTimePeriod dateTimePeriod : entry.getValue()) {
                for (DefenseType dt : scheduleCampaign.defenseTypes) {
                    int indexTimeslot = 0;
                    long nbTimeslot = dateTimePeriod.getNbTimeslot(dt.getDuration());
                    Day day = new Day(dateTimePeriod.getDateStartTime().toLocalDate());
                    LocalDateTime lastEndLocalDateTime = null;
                    for (Long i = numTimeslot; i < nbTimeslot + numTimeslot; i++) {
                        LocalDateTime startDateTime = (lastEndLocalDateTime == null) ? dateTimePeriod.getDateStartTime() : lastEndLocalDateTime;
                        lastEndLocalDateTime = startDateTime.plusSeconds(dt.getDuration().getSeconds());
                        if(!lastEndLocalDateTime.isAfter(dateTimePeriod.getDateEndTime())) {
                            Timeslot timeslot = new Timeslot(i, day, indexTimeslot++, startDateTime, lastEndLocalDateTime);
                            timeslot.addDefenseType(dt);
                            scheduleCampaign.timeslots.add(timeslot);
                        }
                        scheduleCampaign.days.add(day);
                    }
                    numTimeslot += nbTimeslot;
                }
            }
        }

        System.out.println(scheduleCampaign.timeslots);
    }

    private static void createDefensesAndDefenseAssigns(ScheduleCampaign scheduleCampaign,
                                                        DefenseRepository defenseRepository,
                                                        Map<Long, Room> mapRooms,
                                                        Map<Timestamp, Timeslot> mapTimeslots,
                                                        Map<Integer, Person> mapStudents,
                                                        SortedMap<String, DefenseType> mapDefenseTypes,
                                                        Map<Integer, Person> mapOtherAttendees) {
        scheduleCampaign.defenses = new ArrayList<>();
        Iterator<Room> iterRooms = scheduleCampaign.rooms.iterator();
        Iterator<Timeslot> iterTimeslot = scheduleCampaign.timeslots.iterator();
        List<DefenseEntity> defenseEntities = defenseRepository.findAllWithCampaignId(scheduleCampaign.campaign.getId());
        if(defenseEntities.isEmpty()){
            createDefenses(scheduleCampaign, iterRooms, iterTimeslot);
            createDefenseAssigns(scheduleCampaign);
        }else{
            Long i = 1L;
            scheduleCampaign.defenseAssigns = new HashSet<>();
            for(DefenseEntity de : defenseEntities){
                Defense d = createExistingDefense(scheduleCampaign, mapRooms, mapTimeslots, mapStudents, mapDefenseTypes, de);
                i = createExistingDefenseAssigns(scheduleCampaign, mapOtherAttendees, i, de, d);
            }
        }
    }

    private static void createDefenses(ScheduleCampaign scheduleCampaign, Iterator<Room> iterRooms, Iterator<Timeslot> iterTimeslot) {
        Long id = 1L;
        Room room = null;
        Timeslot timeslot = null;
        for (Person p : scheduleCampaign.students) {
            if (iterRooms.hasNext()) {
                room = iterRooms.next();
            }
            if (iterTimeslot.hasNext()) {
                timeslot = iterTimeslot.next();
            }
            Defense d = new Defense(id++, room, false, p, timeslot, p.getDefenseType());
            d.setSupervisorPersons(p.getRelatedPersons());
            scheduleCampaign.defenses.add(d);
        }
    }

    private static Long createExistingDefenseAssigns(ScheduleCampaign scheduleCampaign, Map<Integer, Person> mapOtherAttendees, Long i, DefenseEntity de, Defense d) {
        for(AttendeeGuestEntity attendeeGuestEntity : de.getAttendeeGuests()) {
            DefenseAssign defenseAssign = new DefenseAssign(i++,
                    de.isHold(),
                    mapOtherAttendees.get(attendeeGuestEntity.getId()),
                    d);
            d.addDefenseAssign(defenseAssign);
            scheduleCampaign.defenseAssigns.add(defenseAssign);
        }
        return i;
    }

    private static Defense createExistingDefense(ScheduleCampaign scheduleCampaign, Map<Long, Room> mapRooms, Map<Timestamp, Timeslot> mapTimeslots, Map<Integer, Person> mapStudents, SortedMap<String, DefenseType> mapDefenseTypes, DefenseEntity de) {
        Timeslot timeslot;
        if (de.getDefenseDate() != null) {
            timeslot = mapTimeslots.get(de.getDefenseDate());
        } else {
            Random r = new Random();
            Object[] timeslots = mapTimeslots.keySet().toArray();
            timeslot = (Timeslot) timeslots[r.nextInt(timeslots.length)];
        }
        if (timeslot == null) {
            timeslot = mapTimeslots.values().stream().collect(Collectors.toList()).get(0);
        }
        Person p = mapStudents.get(de.getStudent().getId());
        Defense d = new Defense(Long.valueOf(Integer.toString(de.getId())),
                mapRooms.get(Long.valueOf(Integer.toString(de.getRoom().getId()))),
                de.isHold(),
                p,
                timeslot,
                mapDefenseTypes.get(de.getDefenseType().getCode()));
        d.setSupervisorPersons(p.getRelatedPersons());
        scheduleCampaign.defenses.add(d);
        return d;
    }

    private static void createDefenseAssigns(ScheduleCampaign scheduleCampaign) {
        Long i = 1L;
        scheduleCampaign.defenseAssigns = new HashSet<>();
        Iterator<Person> iterOtherAttendees = scheduleCampaign.otherAttendees.iterator();
        Person otherAttendee = null;
        for (Defense defense : scheduleCampaign.defenses) {
            if (iterOtherAttendees.hasNext()) {
                otherAttendee = iterOtherAttendees.next();
            }
            DefenseAssign defenseAssign = new DefenseAssign(i++, defense.isPinned(), otherAttendee, defense);
            defense.addDefenseAssign(defenseAssign);
            scheduleCampaign.defenseAssigns.add(defenseAssign);
        }
    }

    public static List<LocalDate> getDefenseLocalDates(LocalDate toLocalDate, LocalDate toLocalDate1) {
        List<LocalDate> localDates = new ArrayList<>();
        LocalDate d1 = toLocalDate;
        LocalDate exclusive = toLocalDate1.plusDays(1);
        do {
            DayOfWeek dow = d1.getDayOfWeek();
            if (dow != DayOfWeek.SUNDAY && dow != DayOfWeek.SATURDAY) {
                localDates.add(d1);
            }
            d1 = d1.plusDays(1);
        } while (d1.isBefore(exclusive));
        return localDates;
    }

    /**
     * Permet d'extraire les soutenances non planifiées
     * du reste des soutenances et les affecter
     * dans le champ unscheduledDefenses.
     */
    public void extractUnscheduledDefenses(){
        List<Defense> scheduledDefensesList = new ArrayList<>();
        List<Defense> unscheduledDefensesList = new ArrayList<>();
        for(Defense d : defenses){
            if(d.isUnscheduledDefense(this, defenses)){
                d.setTimeslot(null);
                unscheduledDefensesList.add(d);
            }else{
                scheduledDefensesList.add(d);
            }
        }
        defenses = scheduledDefensesList;
        this.unscheduledDefenses = unscheduledDefensesList;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ScheduleCampaign)) {
            return false;
        }
        ScheduleCampaign s = (ScheduleCampaign) o;
        return id.equals(s.id) &&
                score.equals(s.score);
    }

    @Override
    public int hashCode() {
        return id.hashCode() ^
                score.hashCode();
    }

    @ProblemFactProperty
    public Campaign getCampaign() {
        return campaign;
    }

    /**
     * Renvoie le poids (weight) de la contrainte
     * correspondant à l'id
     * @param id int id de la contrainte
     * @return weight correspondant au poids de la contrainte (-1 si non répertoriée)
     */
    public int getConstraintWeight(int id) {
        return campaign.getConstraintWeight(id);
    }

    /**
     * Modifie le poids d'une contrainte
     *
     * @param id int identifiant de la contrainte à mettre à jour
     * @param weight int nouveau poids de la contrainte
     */
    void updateConstraintWeight(int id, int weight){
        campaign.updateConstraintWeight(id, weight);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Objects.checkIndex(id.intValue(), id.intValue() + 1);
        Objects.requireNonNull(id);
        this.id = id;
    }

    @ValueRangeProvider(id = "defenseRange")
    @PlanningEntityCollectionProperty
    public List<Defense> getDefenses() {
        return defenses;
    }

    public List<Defense> getUnscheduledDefenses() {
        return unscheduledDefenses;
    }

    public void setDefenses(List<Defense> defenses) {
        this.defenses = Objects.requireNonNull(defenses);
    }

    @ValueRangeProvider(id = "defenseAssignRange")
    @PlanningEntityCollectionProperty
    public Set<DefenseAssign> getDefenseAssigns() {
        return defenseAssigns;
    }

    void setDefenseAssigns(Set<DefenseAssign> defenseAssigns) {
        this.defenseAssigns = Objects.requireNonNull(defenseAssigns);
    }

    @ProblemFactCollectionProperty
    public Set<DefenseType> getDefenseTypes() {
        return defenseTypes;
    }

    void setDefenseTypes(Set<DefenseType> defenseTypes) {
        this.defenseTypes = Objects.requireNonNull(defenseTypes);
    }

    @ValueRangeProvider(id = "timeslotRange")
    @ProblemFactCollectionProperty
    private Set<Timeslot> getTimeslots() {
        return timeslots;
    }

    void setTimeslots(Set<Timeslot> timeslots) {
        this.timeslots = Objects.requireNonNull(timeslots);
    }

    @ValueRangeProvider(id = "guestAttendeeRange")
    @ProblemFactCollectionProperty
    public Set<Person> getOtherAttendees() {
        return otherAttendees;
    }

    void setOtherAttendees(Set<Person> otherAttendees) {
        this.otherAttendees = Objects.requireNonNull(otherAttendees);
    }

    @ValueRangeProvider(id = "roomRange")
    @ProblemFactCollectionProperty
    public Set<Room> getRooms() {
        return rooms;
    }

    void setRooms(Set<Room> rooms) {
        this.rooms = Objects.requireNonNull(rooms);
    }

    @ProblemFactCollectionProperty
    public Set<Person> getStudents() {
        return students;
    }

    public void setStudents(Set<Person> students) {
        this.students = Objects.requireNonNull(students);
    }

    @ProblemFactCollectionProperty
    public Set<Person> getSupervisors() {
        return supervisors;
    }

    void setSupervisors(Set<Person> supervisors) {
        this.supervisors = Objects.requireNonNull(supervisors);
    }

    @ProblemFactCollectionProperty
    public Set<Day> getDays() {
        return days;
    }

    public void setDays(Set<Day> days) {
        this.days = days;
    }

    @PlanningScore
    public HardSoftScore getScore() {
        return score;
    }

    public void setScore(HardSoftScore score) {
        this.score = Objects.requireNonNull(score);
    }

}
