package fr.planninggo.scheduler;

import fr.planninggo.spring.user.entity.RoleEntity;

import java.util.Objects;

/**
 * Classe définissant le rôle d'un utilisateur.
 */
public class Role {
    private final String name;

    public Role(String name) {
        this.name = Objects.requireNonNull(name);
    }

    /**
     * Permet de créer une nouvelle instance de Role
     * à partir de l'entité RoleEntity
     * @param role RoleEntity
     * @return Role
     */
    public static Role create(RoleEntity role) {
        return new Role(role.getName());
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Role)) {
            return false;
        }
        Role r = (Role) o;
        return name.equals(r.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                '}';
    }
}
