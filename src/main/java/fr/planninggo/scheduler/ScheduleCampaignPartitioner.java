package fr.planninggo.scheduler;

import org.optaplanner.core.impl.partitionedsearch.partitioner.SolutionPartitioner;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant de partitioner le jeu de données du plannnig à résoudre.
 */
public class ScheduleCampaignPartitioner implements SolutionPartitioner<ScheduleCampaign> {

    private static final int NB_DEFENSES_PARTITION = 100;

    @Override
    public List<ScheduleCampaign> splitWorkingSolution(ScoreDirector<ScheduleCampaign> scoreDirector, Integer runnablePartThreadLimit) {
        ScheduleCampaign originalUnsolvedSolution = scoreDirector.getWorkingSolution();
        int nbTotalDefenses = originalUnsolvedSolution.getDefenses().size();
        int nbPartitions =  nbTotalDefenses / NB_DEFENSES_PARTITION;
        if(nbPartitions == 0){
            return List.of(originalUnsolvedSolution);
        }
        if(nbPartitions < 1){
            nbPartitions =  nbTotalDefenses / 2;
        }
        int sizePartition = nbTotalDefenses / nbPartitions;
//        System.out.println("----------------------------------------------------");
//        //System.out.println("Defenses : " + originalUnsolvedSolution.getDefenses());
//        System.out.println("Defenses : " + originalUnsolvedSolution.getDefenses().size());
//        //System.out.println("Defense Assigns : " + originalUnsolvedSolution.getDefenseAssigns());
//        System.out.println("Defense Assigns : " + originalUnsolvedSolution.getDefenseAssigns().size());
//        //System.out.println("Students : " + originalUnsolvedSolution.getStudents());
//        System.out.println("Students : " + originalUnsolvedSolution.getStudents().size());
//        //System.out.println("Supervisors : " + originalUnsolvedSolution.getSupervisors());
//        System.out.println("Supervisors : " + originalUnsolvedSolution.getSupervisors().size());
//        //System.out.println("Other Attendees : " + originalUnsolvedSolution.getOtherAttendees());
//        System.out.println("Other Attendees : " + originalUnsolvedSolution.getOtherAttendees().size());
//        System.out.println("Defense Assigns from Defenses : " + originalUnsolvedSolution.getDefenses()
//                                                                             .stream()
//                                                                             .map(Defense::getDefenseAssigns)
//                                                                             .flatMap(Set<DefenseAssign>::stream)
//                                                                             .collect(Collectors.toSet()).size());
//        System.out.println("----------------------------------------------------");
        List<ScheduleCampaign> scheduleCampaignPartitions = new ArrayList<>(nbPartitions);
        for(int i = 0; i < nbPartitions; i++){
            ScheduleCampaign schCampPart = ScheduleCampaign.createPartition(i + 1, originalUnsolvedSolution, sizePartition);
//            //System.out.println("Defenses : " + schCampPart.getDefenses());
//            System.out.println("Defenses : " + schCampPart.getDefenses().size());
//            //System.out.println("Defense Assigns : " + schCampPart.getDefenseAssigns());
//            System.out.println("Defense Assigns : " + schCampPart.getDefenseAssigns().size());
//            //System.out.println("Students : " + schCampPart.getStudents());
//            System.out.println("Students : " + schCampPart.getStudents().size());
//            //System.out.println("Supervisors : " + schCampPart.getSupervisors());
//            System.out.println("Supervisors : " + schCampPart.getSupervisors().size());
//            //System.out.println("Other Attendees : " + schCampPart.getOtherAttendees());
//            System.out.println("Other Attendees : " + schCampPart.getOtherAttendees().size());
            scheduleCampaignPartitions.add(schCampPart);
        }
//        System.out.println("Nb partitions : " + nbPartitions);
//        System.out.println("Size partition : " + sizePartition);
        return scheduleCampaignPartitions;
    }
}
