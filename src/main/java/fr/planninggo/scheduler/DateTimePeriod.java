package fr.planninggo.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Classe représentant les bornes de début et de fin d'une plage horaire
 */
public class DateTimePeriod implements Comparable<DateTimePeriod> {
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    public DateTimePeriod(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        this.startDateTime = Objects.requireNonNull(startDateTime);
        this.endDateTime = Objects.requireNonNull(endDateTime);
    }

    /**
     * Renvoie le nombre de timeslot pouvant exister
     * pour l'instance actuelle de DateTimePeriod et le Duration du timeslot requis.
     * @param durationTimeslot Duration du timeslot
     * @return nombre de timeslots
     */
    public long getNbTimeslot(Duration durationTimeslot) {
        Objects.requireNonNull(durationTimeslot);
        Duration d = Duration.between(startDateTime, endDateTime);
        return (d.getSeconds() / durationTimeslot.getSeconds());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof DateTimePeriod)) {
            return false;
        }
        DateTimePeriod dtp = (DateTimePeriod) o;
        return startDateTime.equals(dtp.startDateTime) && endDateTime.equals(dtp.endDateTime);
    }

    @Override
    public int hashCode() {
        return startDateTime.hashCode() ^ endDateTime.hashCode();
    }

    public LocalDateTime getDateStartTime() {
        return startDateTime;
    }

    public LocalDateTime getDateEndTime() {
        return endDateTime;
    }

    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return "start=" + startDateTime.format(format) + ",end=" + endDateTime.format(format);
    }

    @Override
    public int compareTo(DateTimePeriod dtp) {
        int res = startDateTime.compareTo(dtp.startDateTime);
        if(res == 0){
            return endDateTime.compareTo(dtp.endDateTime);
        }
        return res;
    }
}
