package fr.planninggo.scheduler;

import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import org.optaplanner.core.api.domain.lookup.PlanningId;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;


/**
 * Classe représentant une personne
 */
public class Person implements Attendee {
    private Integer id;
    private Attendee attendee;
    private String address;
    private SortedMap<LocalDate, Availability> availabilities = new TreeMap<>();
    private String lastname;
    private String firstname;
    private String email;
    private boolean livesParisArea;

    public Person(int id, String email, String address, String lastname, String firstname) {
        this.id = Objects.checkIndex(id, id + 1);
        this.address = address;
        this.lastname = Objects.requireNonNull(lastname);
        this.firstname = Objects.requireNonNull(firstname);
        this.email = Objects.requireNonNull(email);
    }

    public Person(int id, boolean livesParisArea, String email, String address, String lastname, String firstname) {
        this(id, email, address, lastname, firstname);
        this.livesParisArea = livesParisArea;
    }

    /**
     * Permet de créer une nouvelle instance de Person
     * à partir de l'entité AttendeeEntity
     * @param attendeeEntity AttendeeEntity
     * @param attendee Attendee instance de la classe concrète
     * @param optConsumer Optional<Consumer<Person>> fonction Consumer facultative
     * @return Person
     */
    public static Person create(AttendeeEntity attendeeEntity, Attendee attendee, Optional<Consumer<Person>> optConsumer) {
        Person p = new Person(attendeeEntity.getId(),
                attendeeEntity.getPcTown().isParisArea(),
                attendeeEntity.getEmail(),
                attendeeEntity.getAddress(),
                attendeeEntity.getLastname(),
                attendeeEntity.getFirstname());
        p.setAttendee(attendee);
        attendeeEntity.getAvailabilities()
                .stream()
                .map(Availability::create)
                .forEach(p::addAvailability);
        optConsumer.ifPresent(c -> c.accept(p));
        return p;
    }

    public Attendee getAttendee() {
        return attendee;
    }

    public void setAttendee(Attendee attendee) {
        this.attendee = attendee;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    @PlanningId
    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Person)) {
            return false;
        }
        Person p = (Person) o;
        return id.equals(p.id) &&
                firstname.equals(p.firstname) &&
                lastname.equals(p.lastname) &&
                email.equals(p.email);
    }

    @Override
    public int hashCode() {
        return id.hashCode() ^
                lastname.hashCode() ^
                firstname.hashCode() ^
                email.hashCode();
    }

    /**
     * Ajoute une disponibilité (Availability)
     * @param availability Availability
     */
    public void addAvailability(Availability availability) {
        Objects.requireNonNull(availability);
        availabilities.put(availability.getDate(), availability);
    }

    /**
     * Retire une disponibilité (Availability)
     * @param dateAvail LocalDate date de disponibilité
     */
    public void removeAvailability(LocalDate dateAvail) {
        availabilities.remove(Objects.requireNonNull(dateAvail));
    }

    /**
     * Permet de vérifier si Person est disponible
     * pour une soutenance (Defense)
     *
     * @param d Defense soutenance dont on veut déterminer si Person est disponible
     * @return boolean indiquant si Person est disponible
     */
    public boolean isAvailable(Defense d) {
        if (d == null) {
            return false;
        }
        Timeslot timeslot = d.getTimeslot();
        if (timeslot == null) {
            return false;
        }
        Availability av = availabilities.get(timeslot.getStartDateTime().toLocalDate());
        return (av != null && av.availMatches(timeslot));
    }

    @Override
    public List<AttendeeRelation> getRelations() {
        return Collections.unmodifiableList(attendee.getRelations());
    }

    @Override
    public List<Person> getRelatedPersons() {
        return Collections.unmodifiableList(attendee.getRelatedPersons());
    }

    @Override
    public List<Person> getRelatedPersonsByRole(Role role) {
        return attendee.getRelatedPersonsByRole(role);
    }

    @Override
    public boolean isAssignedToDefenseType() {
        if (attendee != null) {
            return attendee.isAssignedToDefenseType();
        }
        return false;
    }

    @Override
    public DefenseType getDefenseType() {
        return attendee.getDefenseType();
    }

    @Override
    public void setDefenseType(DefenseType defenseType) {
        Objects.requireNonNull(defenseType);
        if (attendee != null) {
            attendee.setDefenseType(defenseType);
        }
    }

    /**
     * Vérifie si le participant habite en province
     * @return boolean
     */
    public boolean isLivesParisArea() {
        return livesParisArea;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Availability av : availabilities.values()) {
            sb.append(av.toString());
        }
        return "Person{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", " + sb.toString() +
                '}';
    }

    /**
     * Renvoie un Set des dates de jours de soutenance
     * @return Set<LocalDate>
     */
    public Set<LocalDate> getLocalDatesAvailables() {
        return availabilities.keySet();
    }
}
