package fr.planninggo.scheduler;

import java.util.Comparator;

/**
 * Classe représentant le Comparator appelé par le solveur
 * pour déterminer les DefenseAssign les plus difficiles à résoudre.
 */
public class DefenseAssignDifficultyComparator implements Comparator<DefenseAssign> {

    @Override
    public int compare(DefenseAssign da1, DefenseAssign da2) {
        return da1.compareTo(da2);
    }

}
