package fr.planninggo.scheduler;

import fr.planninggo.spring.campaign.entity.CampaignConstraintEntity;
import fr.planninggo.spring.campaign.entity.ConstraintEntity;

import java.util.Objects;

/**
 * Classe permettant de représenter une contrainte pour la résolution du scheduler.
 */
public class ConstraintWeight {
    static final int UNAVAILABLE_ROOM = 3;
    static final int CONFLICT_ROOM = 4;
    static final int UNAVAILABLE_ATTENDEE = 1;
    static final int CONFLICT_ATTENDEE = 2;
    static final int UNAVAILABLE_OTHER_ATTENDEE = 7;
    static final int CONFLICT_OTHER_ATTENDEE = 8;
    static final int SAME_ATTENDEE = 9;
    static final int DIFFERENTE_DEFENSE_TYPE = 10;
    public static final int MINIMIZE_OTHER_ATTENDEE_PER_HALF_DAY = 11;
    public static final int MINIMIZE_OVERLAPPED_DEFENSES = 12;
    public static final int MINIMIZE_DISPERSED_OTHER_ATTENDEE_DEFENSES_DAY = 5;
    public static final int MIDDLE_DAY_DEFENSES = 6;
    private final int id;
    private final String description;
    private int weight;
    private final boolean enabled;
    private final boolean valid;
    private int score;

    public ConstraintWeight(int id, String description, int weight, boolean enabled, boolean valid) {
        this.id = Objects.checkIndex(id, id + 1);
        this.description = Objects.requireNonNull(description);
        this.weight = Objects.checkIndex(weight, weight + 1);
        this.enabled = enabled;
        this.valid = valid;
    }

    public ConstraintWeight(int id, String description, int weight, boolean enabled, boolean valid, int score) {
        this(id, description, weight, enabled, valid);
        this.score = score;
    }

    /**
     * Peremt de créer une nouvelle instance de ConstraintWeight
     * @param campaignConstraintEntity CampaignConstraintEntity
     * @return ConstraintWeight
     */
    public static ConstraintWeight create(CampaignConstraintEntity campaignConstraintEntity) {
        ConstraintEntity constraintEntity = campaignConstraintEntity.getConstraint();
        return new ConstraintWeight(constraintEntity.getId(),
                constraintEntity.getDesc(),
                campaignConstraintEntity.getWeight(),
                campaignConstraintEntity.isActive(),
                campaignConstraintEntity.isValide(),
                campaignConstraintEntity.getScore());
    }

    public int getId() {
        return id;
    }

    public int getWeight() {
        return weight;
    }

    public String getDescription() {
        return description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isValid() {
        return valid;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = Objects.checkIndex(score, score + 1);
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
