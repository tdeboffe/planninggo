package fr.planninggo.scheduler;

import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import org.apache.commons.csv.CSVRecord;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

/**
 * Classe formalisant une disponibilité de participant
 */
public class Availability implements Comparable<Availability> {
    public static final LocalTime NOON_HOUR = LocalTime.of(12, 0);
    private static final SimpleDateFormat DF = new SimpleDateFormat("dd/MM/yyyy");
    private int id;
    private LocalDate date;
    private boolean onMorning;
    private boolean onAfternoon;

    public Availability(int id, LocalDate date, boolean onMorning, boolean onAfternoon) {
        this.id = Objects.checkIndex(id, id + 1);
        this.date = Objects.requireNonNull(date);
        this.onMorning = onMorning;
        this.onAfternoon = onAfternoon;
    }

    /**
     * Permet de créer une nouvelle instance Availability à partir
     * de l'entité AvailabilityEntity.
     * @param availabilityEntity AvailabilityEntity
     * @return Availability
     */
    public static Availability create(AvailabilityEntity availabilityEntity) {
        return new Availability(availabilityEntity.getId(),
                getLocalDate(availabilityEntity.getDate()),
                availabilityEntity.isOnMorning(),
                availabilityEntity.isOnAfternoon());
    }

    /**
     * Permet de créer une nouvelle instance Availability à partir
     * du parsing de CSV.
     * @param record CSVRecord entrée de CSV
     * @return Availability
     */
    public static Availability create(CSVRecord record) {
        return new Availability(Integer.parseInt(record.get(0)),
                strToLocalDate(record.get(1)),
                strToBoolean(record.get(2)),
                strToBoolean(record.get(3)));
    }

    private static LocalDate getLocalDate(Date date) {
        String strDate = DF.format(date);
        String[] dates = strDate.split("/");
        return LocalDate.of(Integer.parseInt(dates[2]),
                Integer.parseInt(dates[1]),
                Integer.parseInt(dates[0]));
    }

    private static LocalDate strToLocalDate(String strDate) {
        String[] dates = strDate.split("-");
        return LocalDate.of(Integer.parseInt(dates[0]), Integer.parseInt(dates[1]), Integer.parseInt(dates[2]));
    }

    private static boolean strToBoolean(String strBoolean) {
        return strBoolean.equals("t");
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Availability)) {
            return false;
        }
        Availability av = (Availability) o;
        return (date.equals(av.date) &&
                onMorning == av.onMorning &&
                onAfternoon == av.onAfternoon);
    }

    @Override
    public int hashCode() {
        return date.hashCode() ^ Boolean.hashCode(onMorning) ^ Boolean.hashCode(onAfternoon);
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = Objects.requireNonNull(date);
    }

    /**
     * Vérifie si le créneau (Timeslot) est placé dans la plage horaires
     * de la disponibilité (Availability)
     *
     * @param timeslot Timeslot créneau
     * @return boolean indiquant si le créneau intègre la plage horaires de Availability
     */
    public boolean availMatches(Timeslot timeslot) {
        if (timeslot == null) {
            return false;
        }
        LocalDate localDateTimeslot = timeslot.getStartDateTime().toLocalDate();
        LocalTime localTimeTimeslot = timeslot.getStartDateTime().toLocalTime();
        if (date.equals(localDateTimeslot)) {
            boolean condAvMatch1 = onMorning && onAfternoon;
            boolean condAvMatch2 = onMorning && localTimeTimeslot.isBefore(NOON_HOUR);
            boolean condAvMatch3 = onAfternoon && localTimeTimeslot.compareTo(NOON_HOUR) >= 0;
            return condAvMatch1 || condAvMatch2 || condAvMatch3;
        }
        return false;
    }

    /**
     * Renvoie un booléen indiquant si une disponibiliité est présente
     * (soit le matin -> onMorning, ou soit l'après-midi -> onAfternoon)
     *
     * @return boolean correspondant à la présence de disponibilité
     */
    public boolean hasAvailabilities() {
        return (onMorning || onAfternoon);
    }

    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String strDate = date.format(format);
        return "[date=" + strDate + ",onMorning=" + onMorning + ",onAfternoon=" + onAfternoon + "]";
    }

    @Override
    public int compareTo(@NotNull Availability av) {
        int compMorning = Boolean.compare(onMorning, av.onMorning);
        if (compMorning != 0) {
            return -compMorning;
        }
        int compAfternoon = Boolean.compare(onAfternoon, av.onAfternoon);
        if (compAfternoon != 0) {
            return -compAfternoon;
        }
        return 0;
    }

}
