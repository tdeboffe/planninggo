package fr.planninggo;

import fr.planninggo.csv.CsvParser;
import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.attendee.entity.PcTownEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.repository.AttendeeGuestRepository;
import fr.planninggo.spring.campaign.repository.CampaignRepository;
import fr.planninggo.spring.campaign.repository.CampaignStateRepository;
import fr.planninggo.spring.attendee.repository.CivilityRepository;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import fr.planninggo.spring.attendee.repository.PcTownRepository;
import fr.planninggo.spring.attendee.repository.StudyRepository;
import fr.planninggo.spring.attendee.repository.SupervisorRepository;
import fr.planninggo.spring.attendee.repository.SupervisorStudentRepository;
import fr.planninggo.spring.attendee.service.AttendeeGuestService;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.attendee.service.AvailabilityService;
import fr.planninggo.spring.campaign.service.DefenseTypeService;
import fr.planninggo.spring.campaign.service.RoomService;
import fr.planninggo.spring.attendee.service.StudentService;
import fr.planninggo.spring.configuration.State;
import fr.planninggo.scheduler.Defense;
import fr.planninggo.scheduler.DefenseAssign;
import fr.planninggo.scheduler.ScheduleCampaign;
import fr.planninggo.scheduler.SolverManager;
import fr.planninggo.scheduler.SolverProdParam;
import fr.planninggo.scheduler.Person;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.repository.AccountRepository;
import fr.planninggo.spring.user.repository.RoleRepository;
import fr.planninggo.spring.user.service.AccountService;
import fr.planninggo.utility.MailBuilder;
import fr.planninggo.utility.Password;
import fr.planninggo.utility.SendMail;
import fr.planninggo.utility.SymetricEncryption;
import fr.planninggo.utility.Utils;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class ApplicationController implements ErrorController {
    private final PcTownRepository pcTownRepository;
    private final CivilityRepository civilityRepository;
    private final CampaignRepository campaignRepository;
    private final StudyRepository studyRepository;
    private final RoleRepository roleRepository;
    private final AttendeeService attendeeService;
    private final DefenseTypeService defenseTypeService;
    private final CampaignStateRepository campaignStateRepository;
    private final AccountRepository accountRepository;
    private final AccountService accountService;
    private final RoomService roomService;
    private final AvailabilityService availabilityService;
    private final DefenseRepository defenseRepository;
    private final SupervisorRepository supervisorRepository;
    private final AttendeeGuestRepository attendeeGuestRepository;
    private final StudentService studentService;
    private final AttendeeGuestService attendeeGuestService;
    private final SupervisorStudentRepository supervisorStudentRepository;

    @Inject
    public ApplicationController(PcTownRepository pcTownRepository, CivilityRepository civilityRepository, CampaignRepository campaignRepository, StudyRepository studyRepository, RoleRepository roleRepository, AttendeeService attendeeService, DefenseTypeService defenseTypeService, CampaignStateRepository campaignStateRepository, AccountRepository accountRepository, AccountService accountService, RoomService roomService, AvailabilityService availabilityService, DefenseRepository defenseRepository, SupervisorRepository supervisorRepository, AttendeeGuestRepository attendeeGuestRepository, StudentService studentService, AttendeeGuestService attendeeGuestService, SupervisorStudentRepository supervisorStudentRepository) {
        this.pcTownRepository = pcTownRepository;
        this.civilityRepository = civilityRepository;
        this.campaignRepository = campaignRepository;
        this.studyRepository = studyRepository;
        this.roleRepository = roleRepository;
        this.attendeeService = attendeeService;
        this.defenseTypeService = defenseTypeService;
        this.campaignStateRepository = campaignStateRepository;
        this.accountRepository = accountRepository;
        this.accountService = accountService;
        this.roomService = roomService;
        this.availabilityService = availabilityService;
        this.defenseRepository = defenseRepository;
        this.supervisorRepository = supervisorRepository;
        this.attendeeGuestRepository = attendeeGuestRepository;
        this.studentService = studentService;
        this.attendeeGuestService = attendeeGuestService;
        this.supervisorStudentRepository = supervisorStudentRepository;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @GetMapping(value = "/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "404";
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value() || statusCode == HttpStatus.METHOD_NOT_ALLOWED.value()) {
                return "500";
            }
        }
        return "error";
    }

    @GetMapping(value = "/403")
    public String deny() {
        return "403";
    }

    @GetMapping(value = "/500")
    public String internal() {
        return "500";
    }

    /**
     *
     * @return retorune la page d'authentification
     */
    @GetMapping(value = "/login")
    public String login() {
        Object account = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!account.equals("anonymousUser")) {
            return "redirect:/";
        }
        return "login";
    }

    /**
     *
     * @return retourne la page de diffusion
     */
    @GetMapping(value = "/sendMail")
    public String sendMail() {
        return "sendMail";
    }

    /**
     *
     * @return retourne la page de récupération de mot de passe
     */
    @GetMapping(value = "/generatePassword")
    public String passwordGenerated() {
        return "passwordGenerated";
    }

    /**
     * retourne la page home en fonction du role de l'utilisateur
     * @param request
     * @return
     */
    @GetMapping(value = "/")
    public String home(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        boolean isAnythingElseOrgAdmin = false;
        Set<RoleEntity> roleEntities = account.getRoleEntities();
        for (RoleEntity roleEntity : roleEntities) {
            if (!roleEntity.getName().equals("Administrateur") && !roleEntity.getName().equals("Organisateur")) {
                isAnythingElseOrgAdmin = true;
                break;
            }
        }
        if (!isAnythingElseOrgAdmin) {
            if (roleEntities.stream().anyMatch(r -> r.getName().equals("Administrateur"))) {
                return "redirect:/accounts";
            }
            return "redirect:/campaign";
        }
        if (account.isRgpdAccept()) {
            StringBuilder sb = new StringBuilder();
            AccountEntity myAccount = accountService.findByEmail(account.getEmail());
            Set<RoleEntity> roles = myAccount.getRoleEntities();
            RoleEntity admin = roleRepository.findByName("Administrateur");
            RoleEntity org = roleRepository.findByName("Organisateur");
            RoleEntity student = roleRepository.findByName("Etudiant");
            if (roles.contains(admin)) {
                sb.append("<li class='nav-item active'><a  class=\"nav-link\"  href=\"/app/accounts\">Accueil administrateur</a></li>");
                roles.remove(admin);
            }
            if (roles.contains(org)) {
                sb.append("<li class='nav-item active'>'<a class=\"nav-link\" href=\"/app/campaign\">Accueil organisateur<span class=\"sr-only\">(current)</span></a></li>\n");
                roles.remove(org);
            }
            if (supervisorRepository.findByEmail(myAccount.getEmail()) != null && !roles.contains(student)) {
                sb.append("<li class='nav-item'><a  class=\"nav-link\"  href=\"/app/supervisor\">Ma liste d'étudiants</a></li>");
            }

            request.getSession().setAttribute("buttons", sb.toString());
            return "home";
        }
        return "conditions_rgpd";
    }

    @GetMapping(value = "/newPassword")
    public String newPassword() {
        return "newPassword";
    }

    @GetMapping(value = "/newPassword?error")
    public String newPasswordError() {
        return "newPassword?error";
    }

    /**
     * charge la page des campagnes lors de l'identification d'un organisateur
     * @param request
     * @return
     */
    @GetMapping(value = "/campaign")
    public String campaign(HttpServletRequest request) {
        AccountEntity account;
        StringBuilder sb = new StringBuilder();
        try {
            account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            AccountEntity myAccount = accountService.findByEmail(account.getEmail());
            Set<RoleEntity> roles = myAccount.getRoleEntities();
            RoleEntity admin = roleRepository.findByName("Administrateur");
            RoleEntity org = roleRepository.findByName("Organisateur");
            roles.remove(org);
            if (roles.contains(admin)) {
                sb.append("<li class='nav-item active'><a  class='nav-link'  href='/app/accounts'>Gérer les comptes</a></li>");
                roles.remove(admin);
            }
            if (!roles.isEmpty()) {
                sb.append("<li class='nav-item active'><a  class='nav-link' href='/app/'>Accueil participant</a></li>");
            }
        } catch (ClassCastException c) {
            return "redirect:/login";
        }
        if (account.isRgpdAccept()) {
            request.getSession().setAttribute("roleRepo", roleRepository);
            request.getSession().setAttribute("accountEntity", account);
            request.getSession().setAttribute("repo", campaignRepository);
            request.getSession().setAttribute("buttons", sb.toString());
            return "campaign_home";
        }
        return "conditions_rgpd";
    }

    /**
     * Récupération de la liste des étudiants  d'un superviasor
     * @param request
     * @return
     */
    @GetMapping(value = "/supervisor")
    public String supervisor(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity myAccount = accountService.findByEmail(account.getEmail());
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder buttonsBuilder = new StringBuilder();
        if (account.isRgpdAccept()) {
            SupervisorEntity supervisor = supervisorRepository.findByEmail(myAccount.getEmail());
            AttendeeGuestEntity attendeeGuestEntity = attendeeGuestRepository.findByEmail(myAccount.getEmail());
            if (supervisor != null) {
                List<SupervisorStudentEntity> sup = supervisorStudentRepository.findAllBySupervisor(supervisor);
                for (SupervisorStudentEntity s : sup) {
                    StudentEntity student = s.getStudent();
                    if (student != null) {
                        stringBuilder.append("<tr>");
                        stringBuilder.append("<td>").append(student.getCivility()).append("</td>")
                                .append("<td>").append(student.getLastname()).append("</td>")
                                .append("<td>").append(student.getFirstname()).append("</td>")
                                .append("<td>").append(student.getEmail()).append("</td>")
                                .append("<td>").append(student.getStudy()).append("</td>")
                                .append("<td>").append(s.getRoleEntity().getName()).append("</td>");
                        stringBuilder.append("</tr>");
                    }
                }
            }
            Set<RoleEntity> roles = myAccount.getRoleEntities();
            RoleEntity student = roleRepository.findByName("Etudiant");
            if (!roles.contains(student)) {
                buttonsBuilder.append("<li class='nav-item'><a  class=\"nav-link\"  href=\"/app/supervisor\">Ma liste d'étudiants</a></li>");
            }
            request.getSession().setAttribute("students", stringBuilder.toString());
            request.getSession().setAttribute("buttons", buttonsBuilder.toString());
            return "studentsList_for_supervisor";
        }
        return "conditions_rgpd";
    }

    /**
     *
     * @return retourne la page de rgpd
     */
    @GetMapping(value = "/rgpd")
    public String rgpd() {
        return "conditions_rgpd";
    }

    @CrossOrigin("*")
    @PostMapping(value = "/campaign/{id}/importAttendee", consumes = {"multipart/form-data"})
    public String importAttendee(HttpServletRequest request, @PathVariable(value = "id") int id, @RequestParam(value = "file") MultipartFile multipartFile) throws IOException {
        try {
            CsvParser.saveAttendee(multipartFile, id, pcTownRepository, civilityRepository, campaignRepository, attendeeService, studyRepository, roleRepository, accountRepository);
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("msg", "ERREUR : " + e.getMessage());
            return "redirect:/campaign/" + id;
        }
        CampaignEntity campaignEntity = campaignRepository.findById(id).get();
        if (campaignEntity.getPhases().isEmpty()) {
            CampaignStateEntity stateEntity = campaignStateRepository.findByName(State.READY_GENERATE_DEFAULT);
            if (stateEntity == null) {
                stateEntity = new CampaignStateEntity(State.READY_GENERATE_DEFAULT);
            }
            campaignEntity.setCampaignState(stateEntity);
        } else {
            CampaignStateEntity stateEntity = campaignStateRepository.findByName(State.WAIT_PHASE);
            if (stateEntity == null) {
                stateEntity = new CampaignStateEntity(State.WAIT_PHASE);
            }
            campaignEntity.setCampaignState(stateEntity);
        }
        campaignRepository.save(campaignEntity);
        request.getSession().setAttribute("msg", "Import des participants réussi");
        return "redirect:/campaign/" + id;
    }

    /**
     * importe un planning dans une campagne
     * @param request
     * @param id identifiant de la campagne
     * @param multipartFile le fichier
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @Transactional
    @PostMapping(value = "/campaign/{id}/importPlanning", consumes = {"multipart/form-data"})
    public String importPlanning(HttpServletRequest request, @PathVariable(value = "id") int id, @RequestParam(value = "file") MultipartFile multipartFile) throws IOException {
        CsvParser csvParser = new CsvParser();
        try {
            csvParser.saveDefense(multipartFile, id, defenseRepository, defenseTypeService, roomService, campaignRepository, studentService, attendeeGuestService);
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("msg", "ERREUR : " + e.getMessage());
            return "redirect:/campaign/" + id;
        }
        request.getSession().setAttribute("msg", "Import du planning réussi");
        return "redirect:/campaign/" + id;
    }

    /**
     * importation des disponibilités des participants dans une campagne
     * @param request
     * @param id id de la campagne
     * @param multipartFile le fichier de disponibilité
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @PostMapping(value = "/availability/{id}/uploadFile", consumes = {"multipart/form-data"})
    public String importAvailabiliies(HttpServletRequest request, @PathVariable(value = "id") Integer id, @RequestParam(value = "file") MultipartFile multipartFile) throws IOException {
        try {
            CsvParser.saveAvailabilityCSV(multipartFile, availabilityService, attendeeService);
            CampaignEntity campaignEntity = campaignRepository.findById(id).get();
            if (campaignEntity.getPhases().isEmpty()) {
                CampaignStateEntity stateEntity = campaignStateRepository.findByName(State.READY_GENERATE_ORG);
                if (stateEntity == null) {
                    stateEntity = new CampaignStateEntity(State.READY_GENERATE_ORG);
                }
                campaignEntity.setCampaignState(stateEntity);
            } else {
                CampaignStateEntity stateEntity = campaignStateRepository.findByName(State.WAIT_PHASE);
                if (stateEntity == null) {
                    stateEntity = new CampaignStateEntity(State.WAIT_PHASE);
                }
                campaignEntity.setCampaignState(stateEntity);
            }
            campaignRepository.save(campaignEntity);
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("msg", "ERREUR : " + e.getMessage());
            return "redirect:/campaign/" + id;
        }
        request.getSession().setAttribute("msg", "Import des disponibilités réussi");
        return "redirect:/campaign/" + id;
    }

    /**
     * importation d'un fichier de type de soutenance
     * @param request
     * @param multipartFile le fichier de type de soutenance
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @PostMapping(value = "campaign/import/defenseSettings", consumes = {"multipart/form-data"})
    public ModelAndView importDefenseSetting(HttpServletRequest request, @RequestParam(value = "file") MultipartFile multipartFile) throws IOException {
        request.getSession().setAttribute("defenseTypes", defenseTypeService.getDefenseTypes());
        try {
            List<DefenseTypeEntity> defenseTypeEntities = CsvParser.getDefenseSetting(multipartFile, roleRepository);
            CampaignEntity campaignEntity = (CampaignEntity) request.getSession().getAttribute("campaign");
            defenseTypeEntities.forEach(d -> {
                if (defenseTypeService.findByCode(d.getCode()) != null) {
                    request.getSession().setAttribute("msgImport", "ERREUR : Le type de soutenance existe déjà. Veuillez l'importer : " + d.getCode() + " : " + d.getDesc());
                } else {
                    campaignEntity.addDefenseType(d);
                }
            });
            request.getSession().setAttribute("campaign", campaignEntity);
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("msgImport", "ERREUR : " + e.getMessage());
            return new ModelAndView("newCampaign_3");
        }
        return new ModelAndView("newCampaign_3");
    }

    /**
     * charge la page d'import de campagne
     * @return
     */
    @GetMapping(value = "/campaign/import/campaign")
    public String importCampaign() {
        return "import_campaign";
    }

    @CrossOrigin("*")
    @PostMapping(value = "/campaign/importCampaign", consumes = {"multipart/form-data"})
    public ModelAndView importCampaignSetting(HttpServletRequest request, @RequestParam(value = "file") MultipartFile multipartFile) throws IOException {
        try {
            CampaignEntity campaignEntity = CsvParser.parseCampaignSetting(multipartFile, accountRepository);
            campaignEntity.setCampaignState(campaignStateRepository.findByName(State.CAMPAIGN_CREATE));
            campaignEntity.setCampaignBegin(null);
            campaignEntity.setCampaignEnd(null);
            request.getSession().setAttribute("campaign", campaignEntity);
        } catch (IllegalArgumentException e) {
            request.getSession().setAttribute("msgImportCampaign", "ERREUR : " + e.getMessage());
            return new ModelAndView("campaign_home");
        }
        return new ModelAndView("newCampaign_1");
    }

    /**
     * Lance la résolution du planning
     * @param id l'identifiant de la campagne
     * @return
     */
    @GetMapping(value = "/campaign/{id}/solve")
    public String solve(@PathVariable(value = "id") Integer id) {
        Optional<CampaignEntity> optional = campaignRepository.findById(id);
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            ScheduleCampaign scheduleCampaign = SolverManager.solve(ScheduleCampaign::create,
                    new SolverProdParam(campaignEntity, defenseRepository));

            for (Defense u : scheduleCampaign.getUnscheduledDefenses()) {
                Timestamp t = null;
                Optional<StudentEntity> studentEntity = campaignEntity.getStudents().stream().filter(e -> e.getId() == u.getStudent().getId()).findFirst();
                Optional<DefenseTypeEntity> defenseTypeEntity = campaignEntity.getDefensesType().stream().filter(e -> e.getCode().equals(u.getDefenseType().getCode())).findFirst();
                RoomEntity roomEntity = roomService.findByName(u.getRoom().getName());

                Set<AttendeeGuestEntity> attendeeGuestEntities = campaignEntity.getGuests();
                Set<AttendeeGuestEntity> attendeeGuestEntitiesDefense = new HashSet<>();
                Set<String> strings = attendeeGuestEntities.stream().map(AttendeeEntity::getEmail).collect(Collectors.toSet());
                for (Person p : u.getDefenseAssigns().stream().map(DefenseAssign::getOtherAttendee).collect(Collectors.toSet())) {
                    if (p != null && strings.contains(p.getEmail())) {
                        attendeeGuestEntitiesDefense.add(attendeeGuestRepository.findByEmail(p.getEmail()));
                    }
                }

                if (studentEntity.isPresent() && defenseTypeEntity.isPresent() && roomEntity != null) {
                    DefenseEntity defenseEntity = defenseRepository.findByStudent(studentEntity.get());
                    if (defenseEntity == null || defenseEntity.getCampaign().getId() != id) {
                        defenseEntity = new DefenseEntity(t, studentEntity.get(), false, defenseTypeEntity.get(), campaignEntity, roomEntity, attendeeGuestEntitiesDefense);
                    } else {
                        defenseEntity.setDefenseDate(t);
                    }
                    defenseRepository.save(defenseEntity);
                }
            }

            for (Defense d : scheduleCampaign.getDefenses()) {
                Timestamp t = Timestamp.valueOf(d.getTimeslot().getStartDateTime());
                Optional<StudentEntity> studentEntity = campaignEntity.getStudents().stream().filter(e -> Integer.valueOf(e.getId()).equals(d.getStudent().getId())).findFirst();
                Optional<DefenseTypeEntity> defenseTypeEntity = campaignEntity.getDefensesType().stream().filter(e -> e.getCode().equals(d.getDefenseType().getCode())).findFirst();
                RoomEntity roomEntity = roomService.findByName(d.getRoom().getName());

                Set<AttendeeGuestEntity> attendeeGuestEntities = campaignEntity.getGuests();
                Set<AttendeeGuestEntity> attendeeGuestEntitiesDefense = new HashSet<>();
                Set<String> strings = attendeeGuestEntities.stream().map(AttendeeEntity::getEmail).collect(Collectors.toSet());
                for (Person p : d.getDefenseAssigns().stream().map(DefenseAssign::getOtherAttendee).collect(Collectors.toSet())) {
                    if (p != null && strings.contains(p.getEmail())) {
                        attendeeGuestEntitiesDefense.add(attendeeGuestRepository.findByEmail(p.getEmail()));
                    }
                }

                if (studentEntity.isPresent() && defenseTypeEntity.isPresent() && roomEntity != null) {
                    DefenseEntity defenseEntity = defenseRepository.findByStudent(studentEntity.get());
                    if (defenseEntity == null || defenseEntity.getCampaign().getId() != id) {
                        defenseEntity = new DefenseEntity(t, studentEntity.get(), d.isPinned(), defenseTypeEntity.get(), campaignEntity, roomEntity, attendeeGuestEntitiesDefense);
                    } else {
                        defenseEntity.setDefenseDate(t);
                    }
                    defenseRepository.save(defenseEntity);
                }
            }
            campaignEntity.setCampaignState(campaignStateRepository.findByName(State.GENERATE));
            campaignRepository.save(campaignEntity);
            return "redirect:/campaign/" + id + "/scheduler";
        }
        return "redirect:/";
    }

    /**
     * charge l'interface d'ajout de compte
     * @return
     */
    @GetMapping(value = "/accounts/addAccount")
    public String addAccount() {
        return "addAccount";
    }

    /**
     * permet la modification d'un compte
     * @param id id du compte
     * @param request
     * @return
     */
    @GetMapping(value = "/accounts/modify/{id}")
    public String modifyAccount(@PathVariable(value = "id") int id, HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        Optional<AccountEntity> account = accountRepository.findById(id);
        if (!account.isPresent()) {
            request.getSession().setAttribute("msg", "Le compte n'existe pas ou n'existe plus.");
            return "redirect:/accounts";
        }
        RoleEntity admin = roleRepository.findByName("Administrateur");
        RoleEntity org = roleRepository.findByName("Organisateur");
        Set<RoleEntity> roles = account.get().getRoleEntities();
        sb.append("<div class='form-group col-md-12'><label for='administrateur'>Administrateur</label>");
        if (roles.contains(admin)) {
            sb.append("  <input type='checkbox' id='administrateur' name='role' value='Administrateur' checked='checked'>");
        } else {
            sb.append("  <input type='checkbox' id='administrateur' name='role' value='Administrateur'>");
        }
        sb.append(" </div><div class='form-group col-md-12'><label for='organisateur'>Organisateur</label>");

        if (roles.contains(org)) {
            sb.append("  <input type='checkbox' id='organisateur' name='role' value='Organisateur' checked='checked'>");
        } else {
            sb.append("  <input type='checkbox' id='organisateur' name='role' value='Organisateur'></div>");
        }
        request.getSession().setAttribute("email", account.get().getEmail());
        request.getSession().setAttribute("buttons", sb.toString());
        request.getSession().setAttribute("id", account.get().getId());
        return "modifyAccounts";
    }

    /**
     * modifie un compte
     * @param id id du compte
     * @param values les roles du compte
     * @param email le mail du compte
     * @param request
     * @return
     */
    @GetMapping(value = "accounts/modify/{id}/save")
    public String modifyAccountSave(@PathVariable(value = "id") int id, @RequestParam(name = "role", required = false) String[] values, @RequestParam(name = "email") String email, HttpServletRequest request) {
        Optional<AccountEntity> account = accountRepository.findById(id);
        if (account.isPresent()) {
            Set<RoleEntity> roles = account.get().getRoleEntities();
            RoleEntity admin = roleRepository.findByName("Administrateur");
            RoleEntity org = roleRepository.findByName("Organisateur");
            roles.remove(admin);
            roles.remove(org);
            if (values != null) {
                for (String i : values) {
                    roles.add(roleRepository.findByName(i));
                }
            }
            if (roles.isEmpty()) {
                request.getSession().setAttribute("msg", Utils.message("alert-danger", "Selectionnez au moins un rôle"));
                return "redirect:/accounts/modify/" + account.get().getId();
            }
            account.get().setRoleEntities(roles);
            accountService.saveAccount(account.get());
            request.getSession().setAttribute("msg", Utils.message("alert-success", "Le compte a été mis à jour."));
        }
        return "redirect:/accounts";
    }

    /**
     * sauvegarde un compte
     * @param values les roles du compte
     * @param email l'email du compte
     * @param request
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    @PostMapping(value = "accounts/addAccount/save")
    public String addAccountSave(@RequestParam(name = "role", required = false) String[] values, @RequestParam(name = "email") String email, HttpServletRequest request) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        AccountEntity account = accountRepository.findByEmail(email);

        if (values == null) {
            request.getSession().setAttribute("msg", Utils.message("alert-danger", "Veuillez sélectionner au moins un rôle."));
            request.getSession().setAttribute("email", email);
            return "addAccount";
        }
        if (account != null) {
            Set<RoleEntity> roles = account.getRoleEntities();
            if (roles.contains(roleRepository.findByName("Etudiant"))) {
                request.getSession().setAttribute("msg", Utils.message("alert-danger", "Vous ne pouvez pas donner de droit à " + account.getEmail() + " car cette personne est étudiante"));
                return "redirect:/accounts";
            }
            for (String i : values) {
                roles.add(roleRepository.findByName(i));
            }
            account.setRoleEntities(roles);
            accountService.saveAccount(account);
            request.getSession().setAttribute("msg", Utils.message("alert-success", "Le compte a été mis à jour."));
            return "redirect:/accounts";
        }

        Set<RoleEntity> setRole = new HashSet<>();
        for (String value : values) {
            setRole.add(roleRepository.findByName(value));
        }
        AccountEntity accountEntity = new AccountEntity(email.trim(), Password.encodePassword(Password.generatePassword()), setRole);
        accountService.saveAccount(accountEntity);
        String encryptedUrl = SymetricEncryption.encrypt(email + ";esipe2019");
        String url = "http://localhost:8080/app/generatePassword?hash=" + encryptedUrl;
        SendMail.sendAnEmail(MailBuilder.createPasswordAccount(url, email));
        request.getSession().setAttribute("msg", Utils.message("alert-success", "Le compte a été créé avec succès"));
        return "redirect:/accounts";
    }

    /**
     * permet l'affichage de tous les compte Organisateur ou Administrateur
     * @param request
     * @return
     */
    @GetMapping(value = "/accounts")
    public String displayAdminAndOrgAccounts(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AccountEntity myAccount = accountService.findByEmail(account.getEmail());
        StringBuilder buttonsBuilder = new StringBuilder();
        Set<RoleEntity> role = myAccount.getRoleEntities();
        RoleEntity admin = roleRepository.findByName("Administrateur");
        RoleEntity org = roleRepository.findByName("Organisateur");
        role.remove(admin);
        if (role.contains(org)) {
            buttonsBuilder.append("<li class='nav-item active'> <a class='nav-link' href='/app/campaign'>Mes campagnes <span class='sr-only'>(current)</span></a></li>");
            role.remove(org);
        }
        if (!role.isEmpty()) {
            buttonsBuilder.append("<li class='nav-item active'><a class='nav-link'  href='/app/'>Accueil participant</a></li>");
        }
        StringBuilder stringBuilder = new StringBuilder();
        List<AccountEntity> list = accountService.findAll();
        if (admin != null && org != null) {
            for (AccountEntity acc : list) {
                Set<RoleEntity> set = acc.getRoleEntities();
                if (set.contains(admin) || set.contains(org)) {
                    stringBuilder.append("<tr><td><input type='checkbox' name='account' value='").append(acc.getEmail()).append("'></td>");
                    stringBuilder.append("<td>").append(acc.getEmail()).append("</td>");
                    stringBuilder.append("<td>");
                    set.forEach(r -> stringBuilder.append(r.getName()).append(" "));
                    stringBuilder.append("</td><td>");
                    stringBuilder.append("<a href='/app/accounts/modify/").append(acc.getId()).append("' class='btn btn-light' >");
                    stringBuilder.append("<i class='fas fa-edit'></i></a></td></tr>");
                }
            }
        }
        request.getSession().setAttribute("accounts", stringBuilder.toString());
        request.getSession().setAttribute("buttons", buttonsBuilder.toString());
        return "admin_org_account";
    }

    /**
     * supprime un compte
     * @param accounts une liste de compte à supprimer
     * @param request
     * @return
     */
    @PostMapping(value = "/accounts/delete")
    public String deleteAdminOrg(@RequestParam(name = "account", required = false) String[] accounts, HttpServletRequest request) {
        if (accounts == null) {
            request.getSession().setAttribute("msg", Utils.message("alert-danger", "Veuillez selectionner au moins un compte."));
            return "redirect:/accounts";
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder updateBuilder = new StringBuilder();
        StringBuilder cannotDeleteBuilder = new StringBuilder();
        List<AccountEntity> list = new ArrayList<>();
        RoleEntity admin = roleRepository.findByName("Administrateur");
        RoleEntity org = roleRepository.findByName("Organisateur");
        AccountEntity myAccount = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        for (String email : accounts) {
            list.add(accountService.findByEmail(email));
        }

        boolean deleteMyAccount = false;
        boolean updateAccount = false;
        boolean deleteAccount = false;
        for(AccountEntity acc : list)
            if (acc.getEmail().equals(myAccount.getEmail())) {
                deleteMyAccount = true;
            }

        if(deleteMyAccount){
            request.getSession().setAttribute("msg", Utils.message("alert-danger", "Vous ne pouvez pas supprimer votre compte"));
            return "redirect:/accounts";
        }

        for(AccountEntity account : list){
            boolean delete = true;
            List<CampaignEntity> campaigns = campaignRepository.findByOrganizers(account);
            Set<RoleEntity> roles = account.getRoleEntities();
            for (CampaignEntity campaign : campaigns) {
                if (campaign.getOrganizers().size() > 1) {
                    Set<AccountEntity> set = campaign.getOrganizers();
                    set.remove(account);
                    campaign.setOrganizer(set);
                } else {
                    cannotDeleteBuilder.append("<br>").append(account.getEmail());
                    delete = false;
                    break;
                }
            }

            if (delete) {
                if(roles.contains(admin)){
                    roles.remove(admin);
                }
                if(roles.contains(org)){
                    roles.remove(org);
                }
                if(roles.size()>0){
                    campaignRepository.saveAll(campaigns);
                    account.setRoleEntities(roles);
                    accountService.saveAccount(account);
                    updateBuilder.append("<br>").append(account.getEmail());
                    updateAccount = true;
                }else{
                    campaignRepository.saveAll(campaigns);
                    accountRepository.delete(account);
                    deleteAccount =true;
                }
            }
        }

        if (deleteAccount) {
            request.getSession().setAttribute("msg", Utils.message("alert-success", "Les comptes ont bien été supprimés."));
        }
        if(updateAccount){
           updateBuilder.insert(0, "Les comptes suivants ont bien été mis à jour : ");
           request.getSession().setAttribute("msgUpdate", Utils.message("alert-success", updateBuilder.toString()));
        }

        if(cannotDeleteBuilder.length()>0){
            cannotDeleteBuilder.insert(0, "Il est impossible de supprimer ce/ces comptes : ");
            request.getSession().setAttribute("msgNoDelete", Utils.message("alert-danger", cannotDeleteBuilder.toString()));
        }

        return "redirect:/accounts";
    }

    @PostMapping("/accounts/backToAccounts")
    public String backToAccounts() {
        return "/accounts";
    }


    /**
     * Affiche les informations de l'attendee courant.
     *
     * @param request
     * @return
     */
    @GetMapping(value = "/profil")
    public String displayProfil(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AttendeeEntity attendee = attendeeService.findByEmail(account.getEmail());
        setMyAttribute(request, attendee);
        return "myProfil";
    }


    /**
     * Affiche la page de modification du profil avec les champs pré remplis
     *
     * @param request
     * @return
     */
    @PostMapping(value = "/profil/change")
    public String displayProfilForEdit(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AttendeeEntity attendee = attendeeService.findByEmail(account.getEmail());
        setMyAttribute(request, attendee);
        return "modifyProfil";
    }


    /**
     * Sauvegarde les nouvelles données de l'attendee en base et redirige l'utilisateur sur la page affichant son profil
     *
     * @param request
     * @param firstname prénom
     * @param lastname nom
     * @param address adresse
     * @param zipcode code postal
     * @param city ville
     * @return
     */
    @PostMapping(value = "/profil/change/save")
    public String modifyProfil(HttpServletRequest request, @RequestParam(name = "firstname") String firstname, @RequestParam(name = "lastname") String lastname, @RequestParam(name = "address") String address, @RequestParam(name = "zipcode") String zipcode, @RequestParam(name = "city") String city) {
        PcTownEntity townEntity = pcTownRepository.findByPostalCodeAndTown(zipcode.trim(), city.toUpperCase());
        if (townEntity == null) {

            if (zipcode.trim().matches("^(91|92|93|94|95|75|77|78)[0-9]{3}")) {
                townEntity = new PcTownEntity(zipcode, city.toUpperCase(), true);
            } else {
                townEntity = new PcTownEntity(zipcode, city.toUpperCase(), false);
            }
        }

        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        AttendeeEntity myAttendee = attendeeService.findByEmail(account.getEmail());
        myAttendee.setLastname(lastname);
        myAttendee.setFirstname(firstname);
        myAttendee.setAddress(address);
        myAttendee.setPcTown(townEntity);
        attendeeService.save(myAttendee);
        setMyAttribute(request, myAttendee);

        return "redirect:/profil";
    }

    private void setMyAttribute(HttpServletRequest request, AttendeeEntity attendee) {
        request.getSession().setAttribute("firstname", attendee.getFirstname().trim());
        request.getSession().setAttribute("lastname", attendee.getLastname().trim());
        if (attendee.getAddress().trim().equals("null")) {
            request.getSession().setAttribute("address", "");
        } else {
            request.getSession().setAttribute("address", attendee.getAddress().trim());
        }
        request.getSession().setAttribute("zipcode", attendee.getPostalCode().trim());
        request.getSession().setAttribute("city", attendee.getTown().trim());
    }
}
