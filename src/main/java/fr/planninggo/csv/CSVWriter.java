package fr.planninggo.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public class CSVWriter {
    private final List<String> list;
    private final Path path;

    public CSVWriter(Path path, List<String> list) {
        Objects.requireNonNull(path);
        if (!Pattern.matches("[a-zA-Z_0-9]*.csv", path.toString())) {
            throw new IllegalArgumentException("Wring extension for fileName !");
        }
        this.list = Objects.requireNonNull(list);
        this.path = path;
    }

    /**
     *
     * @return le path du fichier
     */
    public Path getPath() {
        return path;
    }


    /**
     * Construit l'inputStream contenant toutes les données du fichier
     * @return un imputStream
     * @throws IOException
     */
    public InputStream getData() throws IOException {
        String header = list.get(0);
        String[] tabHeader = header.split(";");
        StringBuilder writer = new StringBuilder();
        try (CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.EXCEL.withDelimiter(';')
                .withHeader(tabHeader))) {
            for (int i = 1; i < list.size(); i++) {
                String line = list.get(i);
                String[] tabLine = line.split(";");
                csvPrinter.printRecord(Arrays.asList(tabLine));
            }
            csvPrinter.flush();
        }
        return new ByteArrayInputStream(writer.toString().getBytes());
    }
}
