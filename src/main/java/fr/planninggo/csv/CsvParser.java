package fr.planninggo.csv;

import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.attendee.entity.CivilityEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.attendee.entity.PcTownEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.StudyEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.campaign.repository.CampaignRepository;
import fr.planninggo.spring.attendee.repository.CivilityRepository;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import fr.planninggo.spring.attendee.repository.PcTownRepository;
import fr.planninggo.spring.attendee.repository.StudyRepository;
import fr.planninggo.spring.attendee.service.AttendeeGuestService;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.attendee.service.AvailabilityService;
import fr.planninggo.spring.campaign.service.DefenseTypeService;
import fr.planninggo.spring.campaign.service.RoomService;
import fr.planninggo.spring.attendee.service.StudentService;
import fr.planninggo.scheduler.ScheduleCampaign;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.repository.AccountRepository;
import fr.planninggo.spring.user.repository.RoleRepository;
import fr.planninggo.utility.Password;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CsvParser {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    private static final String REGEX_AVAILABILITY_DATE = "(((0[1-9]|[1-2][0-9]|3[0-1])-(0[13578]|(10|12)))|((0[1-9]|[1-2][0-9])-02)|((0[1-9]|[1-2][0-9]|30)-(0[469]|11)))-[0-9]{4} (MATIN|APREM)";
    private static final String REGEX_MAIL = "(.+)@(.+)[.](.+)";
    private static final CSVFormat CSVFORMAT = CSVFormat.newFormat('\t');

    /**
     * Vérifie que l'orthographe des colonnes
     *
     * @param columns
     */
    private static void checkColumnsAvailability(String[] columns, File csvData) throws IOException {
        if (!(columns[0].equalsIgnoreCase("civilite") || columns[0].equalsIgnoreCase("civilité"))) {
            Files.delete(csvData.toPath());
            throw new IllegalArgumentException("La colonne 'CIVILITE' a été mal orthographiée.");
        }
        if (!columns[1].equalsIgnoreCase("nom")) {
            Files.delete(csvData.toPath());
            throw new IllegalArgumentException("La colonne 'NOM' a été mal orthographiée.");
        }
        if (!(columns[2].equalsIgnoreCase("prenom") || columns[2].equalsIgnoreCase("prénom"))) {
            Files.delete(csvData.toPath());
            throw new IllegalArgumentException("Une colonne 'PRENOM' a été mal orthographiée.");
        }
        if (!columns[3].equalsIgnoreCase("mail")) {
            Files.delete(csvData.toPath());
            throw new IllegalArgumentException("La colonne 'MAIL' a été mal orthographiée.");
        }
        for (int i = 4; i < columns.length - 1; i++) {
            if (!columns[i].matches(REGEX_AVAILABILITY_DATE)) {
                Files.delete(csvData.toPath());
                throw new IllegalArgumentException("Une date de soutenance n'est pas au bon format. Colonne " + i + " " + columns[i]);
            }
        }
    }

    /**
     * Retourne la première ligne du CSV, c'est à dire la ligne concernant les noms des colonnes
     *
     * @param records
     * @return Un tableau des noms de colonnes
     */
    private static String[] getColumns(List<CSVRecord> records) {
        CSVRecord record = records.get(0);
        return record.get(0).split(";");
    }

    /**
     * Convertit les dates en DD-MM-YYYY en YYY-MM-DD pour la base SQL
     *
     * @param s
     * @return
     */
    private static LocalDate toLocalDate(String s) {
        String year;
        String month;
        String day;
        String[] date = s.split("-");
        year = date[2];
        month = date[1];
        day = date[0];
        String jdbcDate = year + "-" + month + "-" + day;
        return LocalDate.parse(jdbcDate);
    }

    /**
     * Vérifie si les valeurs Oui/non pour les dates sont bien orthographiées
     *
     * @param records
     */
    private static void checkYesNo(List<CSVRecord> records, File csvData) throws IOException {
        List<CSVRecord> tmp = new ArrayList<>(records);
        tmp.remove(0);

        for (int index = 0; index < tmp.size(); index++) {
            CSVRecord record = tmp.get(index);
            String[] datas = record.get(0).split(";");
            for (int i = 4; i < datas.length - 1; i++) {
                String info = datas[i];
                int lineIndex = index + 1;
                if (checkYesNoCell(info)) {
                    Files.delete(csvData.toPath());
                    throw new IllegalArgumentException("Une valeur oui/non est incorrect.\n Position " + lineIndex + ", " + i + " : " + info);
                }
            }
        }
    }

    private static boolean checkYesNoCell(String info) {
        return !info.equalsIgnoreCase("oui") && !info.equalsIgnoreCase("non");
    }

    /**
     * Vérifie que les valeurs mail sont au bon format
     *
     * @param records
     */
    private static void checkMail(List<CSVRecord> records, File csvData) throws IOException {
        List<CSVRecord> tmp = new ArrayList<>(records);
        tmp.remove(0);
        for (int index = 0; index < tmp.size(); index++) {
            CSVRecord record = tmp.get(index);
            String[] datas = record.get(0).split(";");
            if (!datas[3].matches(REGEX_MAIL)) {
                int lineIndex = index + 1;
                Files.delete(csvData.toPath());
                throw new IllegalArgumentException("Le mail à la ligne " + lineIndex + " est au mauvais format");
            }
        }
    }


    /**
     * Copie les données du fichier uploadé vers un autre fichier
     *
     * @param inFile  Fichier entrant
     * @param outFile Fichier sortant
     */
    private static void transferToFile(MultipartFile inFile, File outFile) {
        try (FileOutputStream fos = new FileOutputStream(outFile)) {
            fos.write(inFile.getBytes());
        } catch (IOException e) {
            throw new IllegalArgumentException("Impossible de créer un fichier local.");
        }

    }

    /**
     * Enregistre un fichier csv de disponibilité en base
     *
     * @param inFile              le fichier à sauvegarder
     * @param availabilityService le service des disponibilités
     * @param attendeeService     le service des participants
     * @return
     * @throws IOException
     */
    public static void saveAvailabilityCSV(MultipartFile inFile, AvailabilityService availabilityService, AttendeeService attendeeService) throws IOException {
        Objects.requireNonNull(attendeeService);
        Objects.requireNonNull(availabilityService);
        Map<String, Set<AvailabilityEntity>> availabilityAttendee = parseAvailability(Objects.requireNonNull(inFile));
        List<AttendeeEntity> attendeeEntities = new ArrayList<>();
        for (Map.Entry<String, Set<AvailabilityEntity>> e : availabilityAttendee.entrySet()) {
            AttendeeEntity attendee = attendeeService.findByEmail(e.getKey());
            if (attendee == null) {
                throw new IllegalArgumentException("Le participant :  " + e.getKey() + " n'existe pas");
            }
            for (AvailabilityEntity availabilityEntity : attendee.getAvailabilities()) {
                for (AvailabilityEntity availability : e.getValue()) {
                    if (availability.getDate().equals(availabilityEntity.getDate())) {
                        availabilityEntity.setOnMorning(availability.isOnMorning());
                        availabilityEntity.setOnAfternoon(availability.isOnAfternoon());
                    }
                }
            }
            attendeeEntities.add(attendee);
        }
        attendeeService.saveAll(attendeeEntities);
    }

    /**
     * test si le fichier reçu n'est pas null et le copie en local
     *
     * @param inFile le fichier reçu
     * @return un fichier utilisable
     * @throws IOException
     */
    @NotNull
    private static File checkFileAndCreate(MultipartFile inFile) throws IOException {
        File csvData = new File(Objects.requireNonNull(inFile.getName()));
        if (!Files.exists(csvData.toPath())) {
            Files.createFile(csvData.toPath());
        }
        transferToFile(inFile, csvData);
        return csvData;
    }

    /**
     * Parse le fichier CSV lors de l'importation des disponibilités
     *
     * @param inFile
     * @return Une liste d'AvailabilityEntity
     * @throws IOException
     */
    private static Map<String, Set<AvailabilityEntity>> parseAvailability(MultipartFile inFile) throws IOException {
        File csvData = checkFileAndCreate(inFile);
        try (CSVParser parser = CSVParser.parse(csvData, UTF8, CSVFORMAT)) {
            List<CSVRecord> records = parser.getRecords();

            Map<String, Set<AvailabilityEntity>> map = new HashMap<>();
            String[] columns = getColumns(records);
            checkColumnsAvailability(columns, csvData);
            checkYesNo(records, csvData);
            checkMail(records, csvData);
            int columnSize;
            records.remove(0);
            LocalDate jdbcDate;
            boolean onMorning;
            boolean onAfternoon;

            for (CSVRecord record : records) {
                Set<AvailabilityEntity> listEntity = new HashSet<>();
                int i = 4;
                String[] information = record.get(0).split(";");
                String mail = information[3];
                columnSize = record.get(0).split(";").length;
                while (i <= columnSize - 1) {
                    String[] infoDate1 = columns[i].split(" ");
                    String date = infoDate1[0]; //La date
                    String onMorningAfternoon = infoDate1[1];
                    jdbcDate = toLocalDate(date);

                    if (i < columnSize - 1 && columns[i + 1].contains(date)) {
                        onMorning = information[i].equalsIgnoreCase("oui");
                        onAfternoon = information[i + 1].equalsIgnoreCase("oui");
                        i += 2;
                    } else {
                        switch (onMorningAfternoon) {
                            case "MATIN":
                                onMorning = information[i].equalsIgnoreCase("oui");
                                onAfternoon = false;
                                break;
                            case "APREM":
                                onMorning = false;
                                onAfternoon = information[i].equalsIgnoreCase("oui");
                                break;
                            default:
                                Files.delete(csvData.toPath());
                                throw new IllegalArgumentException("ERROR PARSING, fichier au mauvais format");
                        }
                        i++;
                    }
                    if (onMorning || onAfternoon) {
                        listEntity.add(new AvailabilityEntity(jdbcDate, onMorning, onAfternoon));
                    }
                }
                map.put(mail, listEntity);
            }

            Files.delete(csvData.toPath());
            return map;

        } catch (IOException e) {
            Files.delete(csvData.toPath());
            throw new IllegalArgumentException("Le fichier n'a pas pu être examiné.");
        }
    }


    /**
     * Sauvegarde les participants d'une campagne dans la base de données
     *
     * @param inFile             fichier CSV des participants
     * @param campaignId         l'identifiant de la campagne
     * @param pcTownRepository   le repository  de la table town
     * @param civilityRepository le repository  de la table civility
     * @param campaignRepository le repository  de la table campaign
     * @param attendeeService    le service de la table attendee
     * @param studyRepository    le repository  de la table study
     * @param roleRepository     le repository de la table role
     * @param accountRepository
     * @return
     * @throws IOException
     */
    public static void saveAttendee(MultipartFile inFile, int campaignId, PcTownRepository pcTownRepository, CivilityRepository civilityRepository, CampaignRepository campaignRepository, AttendeeService attendeeService, StudyRepository studyRepository, RoleRepository roleRepository, AccountRepository accountRepository) throws IOException {
        File csvData = checkFileAndCreate(inFile);
        RoleEntity roleStudentEntity = roleRepository.findByName("Etudiant");

        String[] columnsCSVAttendee = {"CIVILITE", "NOM", "PRENOM", "MAIL", "CP", "VILLE",
                "ADRESSE", "FILIERE", "ROLE", "CODE EXERCICE"};

        try (CSVParser parser = CSVParser.parse(csvData, UTF8, CSVFORMAT)) {
            List<CSVRecord> records = parser.getRecords();
            String[] columns = getColumns(records);
            checkColumnsCSV(columns, columnsCSVAttendee, 0);
            records.remove(0);
            CampaignEntity campaign = getCampaignWithId(campaignRepository, campaignId);

            Map<String, AttendeeEntity> attendees = new HashMap<>();
            Map<StudentEntity, List<String>> students = new HashMap<>();
            for (CSVRecord record : records) {
                String[] information = record.get(0).split(";");
                if (information.length < 9) {
                    continue;
                }
                if (information.length == 9) {
                    AttendeeEntity attendeeEntity = createAttendee(information, pcTownRepository, civilityRepository, campaign, roleStudentEntity, accountRepository);
                    attendees.put(attendeeEntity.getEmail(), attendeeEntity);
                } else {
                    students.putAll(createAttendeeStudent(columns, information, pcTownRepository, civilityRepository, campaign, studyRepository, roleStudentEntity, accountRepository));
                }
            }

            saveAttendee(attendeeService, campaignRepository, attendees, students, campaign);
            Files.delete(csvData.toPath());
        } catch (IOException e) {
            throw new IllegalArgumentException("Le fichier n'a pas pu être examiné.");
        }
    }

    /**
     * convertion d'une date en String en localDate
     * @param date date en string
     * @return localdate
     */
    private static LocalDate stringToLocalDate(String date) {
        String[] valuesDate = date.split("/");
        return LocalDate.of(Integer.valueOf(valuesDate[2]), Integer.valueOf(valuesDate[1]), Integer.valueOf(valuesDate[0]));
    }

    /**
     * Vérifie si la salle est rattaché à la campagne
     * @param room l'identifiant de la salle
     * @param campaignEntity la campagne Entitie
     * @param roomService le service des salles
     * @return true si la salle est déjà rattaché à la salle false sinon
     */
    private static boolean checkRoom(String room, CampaignEntity campaignEntity, RoomService roomService) {
        RoomEntity theRoom = roomService.findById(Integer.valueOf(room));
        if (theRoom == null) {
            return false;
        }
        Set<DefenseDayEntity> setDefenseDays = campaignEntity.getDefenseDays();
        Set<RoomEntity> roomEntities = new HashSet<>();
        for (DefenseDayEntity defenseDayEntity : setDefenseDays) {
            Set<RoomEntity> setRooms = defenseDayEntity.getRooms();
            roomEntities.addAll(setRooms);
        }
        return roomEntities.contains(theRoom);
    }

    /**
     * Vérifie si le type de soutenance est rattaché à la soutenance
     * @param type le code du type de soutenance
     * @param campaignEntity la campagne
     * @param defenseTypeService le service des types de doutenance
     * @return true si la campagne contient le type de soutenance false sinon
     */
    private static boolean checkDefenseType(String type, CampaignEntity campaignEntity, DefenseTypeService defenseTypeService) {
        DefenseTypeEntity typeEntity = defenseTypeService.findByCode(type);
        if (typeEntity == null) {
            return false;
        }
        Set<DefenseTypeEntity> defenseTypeEntities = campaignEntity.getDefensesType();
        return defenseTypeEntities.contains(typeEntity);
    }

    /**
     * convertion d'une date et une heure en texte en timeStamp
     * @param dateSoutenance date en texte
     * @param heureSoutenance heure en texte
     * @return un timestamp
     */
    private static Timestamp returnTimestamp(String dateSoutenance, String heureSoutenance) {
        String[] valuesDate = dateSoutenance.split("/");
        String[] valuesHeure = heureSoutenance.split(":");
        String heure = valuesHeure[0];
        String minutes = valuesHeure[1];
        LocalDateTime time = LocalDateTime.of(Integer.valueOf(valuesDate[2]), Integer.valueOf(valuesDate[1]), Integer.valueOf(valuesDate[0]), Integer.valueOf(heure), Integer.valueOf(minutes));
        return Timestamp.valueOf(time);
    }

    /**
     * Importe un planning en base
     * @param inFile le fichier csv
     * @param campaignId l'identifiant de la campagne
     * @param defenseRepository le répository des soutenances
     * @param defenseTypeService le service des defenses types
     * @param roomService le service des salles
     * @param campaignRepository le répository de la campagne
     * @param studentService le service des étudiants
     * @param attendeeGuestService le service des participants invités
     * @throws IOException
     */
    @Transactional
    public void saveDefense(MultipartFile inFile, int campaignId, DefenseRepository defenseRepository, DefenseTypeService defenseTypeService, RoomService roomService, CampaignRepository campaignRepository, StudentService studentService, AttendeeGuestService attendeeGuestService) throws IOException {
        File csvData = checkFileAndCreate(inFile);
        try (CSVParser parser = CSVParser.parse(csvData, UTF8, CSVFORMAT)) {
            List<CSVRecord> records = parser.getRecords();
            CampaignEntity campaign = getCampaignWithId(campaignRepository, campaignId);
            records.remove(0);
            List<DefenseEntity> listDefenseEntities = new ArrayList<>();
            for (CSVRecord record : records) {
                AttendeeGuestEntity attendeeGuestEntity = null;
                StudentEntity studentEntity = null;
                String[] infos = record.get(0).split(";");
                String nomCampagne = infos[0];
                if (!campaign.getName().equals(nomCampagne)) {
                    throw new IllegalArgumentException("Le nom de la campagne ne correspond pas à celle choisie : " + nomCampagne);
                }
                String dateSoutenance = infos[1];
                LocalDate localDate = stringToLocalDate(dateSoutenance);
                Date dateDebutCampagne = campaign.getCampaignBegin();
                LocalDate dateDebLoc = dateDebutCampagne.toLocalDate();
                Date dateFinCampagne = campaign.getCampaignEnd();
                LocalDate dateFin = dateFinCampagne.toLocalDate();
                if (localDate.isAfter(dateFin) || localDate.isBefore(dateDebLoc)) {
                    throw new IllegalArgumentException("La date n'est pas dans la période de la campagne : " + dateSoutenance);
                }
                String salle = infos[3];
                if (!checkRoom(salle, campaign, roomService)) {
                    throw new IllegalArgumentException("La salle n'est pas disponible pour la campagne : " + salle);
                }
                String defensetype = infos[4];
                if (!checkDefenseType(defensetype, campaign, defenseTypeService)) {
                    throw new IllegalArgumentException("Le type de soutenance n'existe pas pour cette campagne : " + defensetype);
                } else {
                    Timestamp timestamp = returnTimestamp(dateSoutenance, infos[2]);
                    for (int i = 0; i < infos.length; i++) {
                        if (i > 4) {
                            String participant = infos[i];
                            String[] infosParticipant = participant.split(",");
                            String mail = infosParticipant[0];
                            if (studentService.getStudent(mail) != null) {
                                studentEntity = studentService.getStudent(mail);
                            }
                            if (attendeeGuestService.getAttendeeGuest(mail) != null) {
                                attendeeGuestEntity = attendeeGuestService.getAttendeeGuest(mail);
                            }
                        }
                    }
                    if (studentEntity == null || attendeeGuestEntity == null) {
                        throw new IllegalArgumentException("L'étudiant ou l'enseignant de communication n'existe pas pour la soutenance à la date : " + dateSoutenance + " " + infos[2]);
                    } else {
                        Set<AttendeeGuestEntity> set = new HashSet<>();
                        set.add(attendeeGuestEntity);
                        DefenseEntity defenseEntity = new DefenseEntity(timestamp, studentEntity, false, defenseTypeService.findByCode(defensetype), campaign, roomService.findById(Integer.valueOf(salle)), set);
                        listDefenseEntities.add(defenseEntity);
                    }
                }
            }
            defenseRepository.deleteAllByCampaignId(campaignId);
            for (DefenseEntity defenseEntity : listDefenseEntities) {
                if (defenseRepository.findByStudent(defenseEntity.getStudent()) != null) {
                    throw new IllegalArgumentException("Soutenance déjà existante pour l'étudiant : " + defenseEntity.getStudent().getFirstname() + " " + defenseEntity.getStudent().getLastname());
                }
                defenseRepository.save(defenseEntity);
            }
            Files.delete(csvData.toPath());
        } catch (IOException e) {
            throw new IllegalArgumentException("Le fichier n'a pas pu être examiné.");
        }
    }

    /**
     * Sauvegarde tous les participants en base
     *
     * @param attendeeService    le service de la table attendee
     * @param campaignRepository le répository d'une campagne
     * @param attendees          la map des superviasor et des guest attendee
     * @param students           la map des students
     * @param campaign           la campagne
     */
    private static void saveAttendee(AttendeeService attendeeService, CampaignRepository campaignRepository, Map<String, AttendeeEntity> attendees, Map<StudentEntity, List<String>> students, CampaignEntity campaign) {
        /* Dispo par défaut */
        Set<AvailabilityEntity> availabilityEntities = CsvParser.createAvailabilities(campaign);

        /* Liste des tuteurs */
        List<String> allTutors = new ArrayList<>();
        students.values().forEach(allTutors::addAll);
        allTutors = allTutors.stream().map(t -> t.split(":")[1]).collect(Collectors.toList());

        /* Liste des guests */
        Set<AttendeeGuestEntity> attendeeGuestEntities = new HashSet<>();

        /* Liste des étudiants */
        Set<StudentEntity> studentEntities = new HashSet<>();

        Map<Integer, Set<AttendeeGuestEntity>> campaignsGuest = new HashMap<>();
        for (CampaignEntity campaignEntity : campaignRepository.findAll()) {
            campaignsGuest.put(campaignEntity.getId(), Set.copyOf(campaignEntity.getGuests()));
            campaignEntity.setGuests(new HashSet<>());
            campaignRepository.save(campaignEntity);
        }

        /* Sauvegarde des Guest Attendee */
        AttendeeGuestEntity attendeeGuestEntity1;
        for (AttendeeEntity attendeeEntity : attendees.values()) {
            if (!allTutors.contains(attendeeEntity.getEmail())) {
                attendeeGuestEntity1 = attendeeService.createAttendeeGuest(new AttendeeGuestEntity(attendeeEntity));
                for (AvailabilityEntity availabilityEntity3 : CsvParser.createAvailabilities(campaign)) {
                    attendeeGuestEntity1.addAvailabilities(availabilityEntity3);
                }
                attendeeService.saveAttendeeGuest(attendeeGuestEntity1);
                attendeeGuestEntities.add(attendeeGuestEntity1);
            }
        }

        /* Sauvegarde des étudiants + superviseurs */
        Set<SupervisorStudentEntity> supervisorsEntities = new HashSet<>();
        RoleEntity roleEntity = null;
        Set<JuryMemberEntity> juryMemberEntities = new HashSet<>();
        campaign.getDefensesType().forEach(d -> juryMemberEntities.addAll(d.getJuryMembers()));
        Set<RoleEntity> roleEntities = new HashSet<>();
        juryMemberEntities.forEach(j -> roleEntities.add(j.getRole()));
        StudentEntity studentEntity;
        SupervisorEntity supervisorEntity;
        for (Map.Entry<StudentEntity, List<String>> entry : students.entrySet()) {
            studentEntity = attendeeService.createStudent(entry.getKey());
            studentEntities.add(studentEntity);
            for (String supervisor : entry.getValue()) {
                String[] sup = supervisor.split(":");
                String mail = sup[1];
                String role = sup[0];
                if (attendees.get(mail) == null) {
                    throw new IllegalArgumentException("L'adresse mail : " + mail + " du rôle : " + role + " de l'étudiant " + studentEntity.getEmail() + " n'est pas présente dans le fichier csv.");
                }
                for (RoleEntity r : roleEntities) {
                    if (r.getName().equals(role)) {
                        roleEntity = r;
                    }
                }
                if (roleEntity == null) {
                    throw new IllegalArgumentException("Quelque chose s'est pas mal passé pour le role de " + mail + ". Merci de contacter le service technique");
                }
                supervisorEntity = attendeeService.createSupervisor(attendees.get(mail));
                for (AvailabilityEntity availabilityEntity3 : availabilityEntities) {
                    supervisorEntity.addAvailabilities(availabilityEntity3);
                }
                attendeeService.saveSupervisorAva(supervisorEntity);
                availabilityEntities = CsvParser.createAvailabilities(campaign);
                supervisorsEntities.add(new SupervisorStudentEntity(supervisorEntity, studentEntity, roleEntity));
            }
            attendeeService.removeSupervisor(studentEntity, supervisorsEntities);
        }
        supervisorsEntities.forEach(attendeeService::saveSupervisor);

        Set<AttendeeGuestEntity> newAttendeeGuestEntities = campaign.getGuests();
        newAttendeeGuestEntities.addAll(attendeeGuestEntities);
        campaign.setGuests(newAttendeeGuestEntities);

        Set<StudentEntity> newStudentEntities = campaign.getStudents();
        newStudentEntities.addAll(studentEntities);
        campaign.setStudents(newStudentEntities);

        campaignRepository.save(campaign);

        for (CampaignEntity campaignEntity : campaignRepository.findAll()) {
            Set<AttendeeGuestEntity> attendeeGuestEntities1 = new HashSet<>();
            if (campaign.getId() != campaignEntity.getId()) {
                for (AttendeeGuestEntity attendeeGuestEntity : campaignsGuest.get(campaignEntity.getId())) {
                    attendeeGuestEntities1.add(attendeeService.getGuest(attendeeGuestEntity.getEmail()));
                }
                campaignEntity.setGuests(attendeeGuestEntities1);
                campaignRepository.save(campaignEntity);
            }
        }

    }

    /**
     * création des toutes les disponibilités pour toute la période de la campagne
     * @param campaign la campagne
     * @return un set de disponibilité.
     */
    private static Set<AvailabilityEntity> createAvailabilities(CampaignEntity campaign) {
        Set<AvailabilityEntity> availabilityEntities = new HashSet<>();
        List<LocalDate> localDates = ScheduleCampaign.getDefenseLocalDates(campaign.getCampaignBegin().toLocalDate(), campaign.getCampaignEnd().toLocalDate());
        localDates.forEach(l -> availabilityEntities.add(new AvailabilityEntity(l, true, true)));
        return availabilityEntities;
    }

    /**
     * lecture de différents champs du fichier csv et création d'un students
     *
     * @param informations       les informations du fichier CSV
     * @param pcTownRepository   le repository  de la table town
     * @param civilityRepository le repository  de la table civility
     * @param campaign           la campagne concerné par l'import
     * @param studyRepository    le repository  de la table study
     * @param roleStudentEntity  le role d'étudiant
     * @param accountRepository
     * @return
     */
    private static Map<StudentEntity, List<String>> createAttendeeStudent(String[] columns, String[] informations, PcTownRepository pcTownRepository, CivilityRepository civilityRepository, CampaignEntity campaign, StudyRepository studyRepository, RoleEntity roleStudentEntity, AccountRepository accountRepository) {
        Map<StudentEntity, List<String>> attendees = new HashMap<>();
        AttendeeEntity attendeeEntity = createAttendee(informations, pcTownRepository, civilityRepository, campaign, roleStudentEntity, accountRepository);

        String filiere = informations[7];
        String codeEx = "";
        ArrayList<String> tutorEmail = new ArrayList<>();
        if (informations.length == 12) {
            codeEx = informations[9];
            tutorEmail.addAll(Arrays.asList(columns[10] + ":" + informations[10], columns[11] + ":" + informations[11]));
        }

        if (!tutorEmail.isEmpty()) {
            StudyEntity studyEntity = studyRepository.findByName(filiere);
            if (studyEntity == null) {
                studyEntity = studyRepository.save(new StudyEntity(filiere));
            }
            final String ex = codeEx;
            Optional<DefenseTypeEntity> optionalDefenseTypeEntity = campaign.getDefensesType().stream().filter(d -> d.getCode().equals(ex)).findFirst();
            if (!optionalDefenseTypeEntity.isPresent()) {
                throw new IllegalArgumentException("Le type de soutenance " + ex + " est invalide. Les types de soutenance disponibles sont : " + campaign.getDefensesType().stream().map(DefenseTypeEntity::getCode).collect(Collectors.joining(", ")));
            }
            DefenseTypeEntity defenseTypeEntity = optionalDefenseTypeEntity.get();
            StudentEntity studentEntity = new StudentEntity(attendeeEntity.getEmail(), attendeeEntity.getLastname(), attendeeEntity.getFirstname(), attendeeEntity.getAddress()
                    , attendeeEntity.getAccount(), attendeeEntity.getCivilityEntity(), attendeeEntity.getPcTown(), studyEntity, defenseTypeEntity, Set.of(campaign));
            Set<AvailabilityEntity> availabilityEntities = new HashSet<>();

            List<LocalDate> localDates = ScheduleCampaign.getDefenseLocalDates(campaign.getCampaignBegin().toLocalDate(), campaign.getCampaignEnd().toLocalDate());
            localDates.forEach(l -> availabilityEntities.add(new AvailabilityEntity(l, true, true)));
            studentEntity.setAvailabilities(availabilityEntities);
            attendees.put(studentEntity, tutorEmail);

        }
        return attendees;
    }

    /**
     * récupérer l'entity de la campagne en base à partir de son identifiant
     *
     * @param campaignRepository le repository  de la table campaign
     * @param campaignId         l'identifiant de la campagne
     * @return une campagnEntity ou bien lève une exception
     */
    private static CampaignEntity getCampaignWithId(CampaignRepository campaignRepository, int campaignId) {
        Optional<CampaignEntity> optionalCampaignEntity = campaignRepository.findById(campaignId);
        return optionalCampaignEntity.orElseThrow(() -> new IllegalArgumentException("La campagne " + campaignId + " n'existe pas."));
    }

    /**
     * lecture de différents champs du fichier csv et création d'un Attendee
     *
     * @param informations       les informations du fichier CSV
     * @param pcTownRepository   le repository  de la table town
     * @param civilityRepository le repository  de la table civility
     * @param campaign           la campagne concerné par l'import
     * @param roleStudentEntity  le role Etudiant
     * @param accountRepository
     * @return un AttendeeEntity
     */
    private static AttendeeEntity createAttendee(String[] informations, PcTownRepository pcTownRepository, CivilityRepository civilityRepository, CampaignEntity campaign, RoleEntity roleStudentEntity, AccountRepository accountRepository) {
        String civility = informations[0];
        String lastname = informations[1];
        String firstname = informations[2];
        String mail = informations[3];
        String postalCode = informations[4];
        String town = informations[5];
        String address = informations[6];
        String role = informations[8];

        PcTownEntity pcTownEntity = pcTownRepository.findByPostalCodeAndTown(postalCode, town);
        if (pcTownEntity == null) {
            boolean isParisArea = false;
            if (postalCode.startsWith("77") || postalCode.startsWith("75") || postalCode.startsWith("93") || postalCode.startsWith("94") || postalCode.startsWith("91")
                    || postalCode.startsWith("78") || postalCode.startsWith("92") || postalCode.startsWith("95")) {
                isParisArea = true;
            }
            pcTownEntity = pcTownRepository.save(new PcTownEntity(postalCode, town, isParisArea));
        }
        CivilityEntity civilityEntity = civilityRepository.findByName(civility);
        if (civilityEntity == null) {
            civilityEntity = civilityRepository.save(new CivilityEntity(civility));
        }
        List<String> roles = Arrays.asList(role.split(","));

        Set<RoleEntity> trueRoles = new HashSet<>();
        for (DefenseTypeEntity d : campaign.getDefensesType()) {
            Set<RoleEntity> roleDefenseType = d.getJuryMembers().stream().map(JuryMemberEntity::getRole).collect(Collectors.toSet());
            if (!trueRoles.containsAll(roleDefenseType)) {
                trueRoles.addAll(roleDefenseType);
            }
        }
        trueRoles.add(roleStudentEntity);
        Set<RoleEntity> roleEntities = trueRoles.stream().filter(r -> roles.stream().map(String::toLowerCase).collect(Collectors.toList()).contains(r.getName().toLowerCase())).collect(Collectors.toSet());
        if (roleEntities.size() != roles.size()) {
            throw new IllegalArgumentException("You specified a bad role Available roles are : " + trueRoles.stream().map(RoleEntity::getName).collect(Collectors.joining(", ")));
        }

        Set<AvailabilityEntity> availabilityEntities = new HashSet<>();

        List<LocalDate> localDates = ScheduleCampaign.getDefenseLocalDates(campaign.getCampaignBegin().toLocalDate(), campaign.getCampaignEnd().toLocalDate());

        localDates.forEach(l -> availabilityEntities.add(new AvailabilityEntity(l, true, true)));
        AccountEntity account = accountRepository.findByEmail(mail);
        if (account == null) {
            account = new AccountEntity(mail, Password.generatePassword(), roleEntities);
        } else {
            account.addRoles(roleEntities);
        }
        AttendeeEntity attendeeEntity = new AttendeeEntity(mail, lastname, firstname, address, account, civilityEntity, pcTownEntity);
        attendeeEntity.setAvailabilities(availabilityEntities);
        return attendeeEntity;
    }

    /**
     * Vérifie les colonnes du fichier d'import des participants
     *
     * @param columns            les colonnes du CSV des Participants
     * @param columnsCSVAttendee les colonnes attendues
     */
    private static void checkColumnsCSV(String[] columns, String[] columnsCSVAttendee, int indexStart) {
        for (int i = indexStart; i - indexStart < columnsCSVAttendee.length; i++) {
            if (!columns[i].equalsIgnoreCase(columnsCSVAttendee[i - indexStart])) {
                throw new IllegalArgumentException("La colonne " + columns[i] + " a été mal orthographiée." + columnsCSVAttendee[i - indexStart]);
            }
        }
    }

    /**
     * Création d'une liste de DefenseSetting à partir d'un fichier csv d'import de configuration de soutenance
     *
     * @param inFile         le fichier csv reçu
     * @param roleRepository le repository des roles
     * @return une liste de DefenseTypeEntity
     * @throws IOException
     */
    public static List<DefenseTypeEntity> getDefenseSetting(MultipartFile inFile, RoleRepository roleRepository) throws IOException {
        File csvData = checkFileAndCreate(inFile);
        String[] columnsCSVDefenseSeting = {"CODE EXERCICE", "DESCRIPTION EXERCICE", "DUREE"};
        String[] juryMemberCSV = {"ROLE JURY ", "LIEN ETUDIANT JURY ", "PRESIDENT JURY "};

        try (CSVParser parser = CSVParser.parse(csvData, UTF8, CSVFormat.EXCEL)) {
            List<CSVRecord> records = parser.getRecords();
            String[] columns = getColumns(records);

            int indexColumn = 0;
            checkColumnsCSV(columns, columnsCSVDefenseSeting, indexColumn);
            indexColumn += columnsCSVDefenseSeting.length;

            int indexJury = 1;
            while (indexColumn < columns.length) {
                String numJury = Integer.toString(indexJury);
                String[] headColumn = Arrays.stream(juryMemberCSV).map(j -> j + numJury).toArray(String[]::new);
                checkColumnsCSV(columns, headColumn, indexColumn);
                indexColumn += juryMemberCSV.length;
                indexJury++;
            }
            records.remove(0);
            List<DefenseTypeEntity> defenseEntities = new ArrayList<>();
            for (CSVRecord record : records) {
                String[] informations = record.get(0).split(";");
                DefenseTypeEntity defenseTypeEntity = getDefenseType(informations);
                Set<JuryMemberEntity> juryMembers = new HashSet<>();
                for (int i = 0; i < indexJury - 1; i++) {
                    juryMembers.add(getJuryMember(informations, i * 3 + 3, roleRepository));
                }
                defenseTypeEntity.setJuryMembers(juryMembers);
                defenseEntities.add(defenseTypeEntity);
            }
            Files.delete(csvData.toPath());
            return defenseEntities;

        } catch (IOException e) {
            throw new IllegalArgumentException("Le fichier n'a pas pu être examiné.");
        }
    }


    /**
     * Création d'un objet jury member à partir d'une ligne lue dans le fichier csv d'import des types de soutenance
     *
     * @param informations   les informations d'une ligne
     * @param columnIndex    l'index de la première colonne qui doit être traitée
     * @param roleRepository le répository des roles
     * @return un objet juryMember
     */
    private static JuryMemberEntity getJuryMember(String[] informations, int columnIndex, RoleRepository roleRepository) {
        String role = informations[columnIndex++];
        String linkStudent = StringUtils.stripAccents(informations[columnIndex++]).toUpperCase();
        if (checkYesNoCell(informations[columnIndex])) {
            throw new IllegalArgumentException("Une valeur oui/non est incorrect.\n colonne " + columnIndex);
        }
        boolean president = informations[columnIndex].equalsIgnoreCase("OUI");
        boolean studyLink;
        boolean studentLink;
        RoleEntity roleEntity = roleRepository.findByName(role);
        switch (linkStudent) {
            case "OUI":
                studyLink = true;
                studentLink = true;
                break;
            case "NON":
                studyLink = false;
                studentLink = false;
                break;
            case "FILIERE":
                studentLink = false;
                studyLink = true;
                break;
            default:
                throw new IllegalArgumentException("ERROR PARSING, fichier au mauvais format " + linkStudent);

        }
        if (roleEntity == null) {
            roleEntity = new RoleEntity(role);
        }
        return new JuryMemberEntity(studyLink, studentLink, president, roleEntity);

    }

    /**
     * récupére les informations d'une défenseType à partir d'une ligne lu edans le fichier csv d'import des types de soutenance
     *
     * @param informations les informations d'une ligne
     * @return un objet defenseType
     */
    private static DefenseTypeEntity getDefenseType(String[] informations) {
        String code = informations[0];
        String description = informations[1];
        String duration = informations[2];
        return new DefenseTypeEntity(code, description, duration);

    }

    /**
     * récupére les paramètres d'une campagne et crée une campagne entity
     *
     * @param multipartFile     le fichier csv des paramètres de la campagne
     * @param accountRepository le compt repository
     * @return une campagne entity
     * @throws IOException
     */
    public static CampaignEntity parseCampaignSetting(MultipartFile multipartFile, AccountRepository accountRepository) throws IOException {
        File csvData = checkFileAndCreate(multipartFile);
        String[] sectionCampagn = {"NOM CAMPAGNE", "DATE DEBUT SOUTENANCES", "DATE FIN SOUTENANCE", "STATUT", "NB PHASE RECOLTE"};


        try (CSVParser parser = CSVParser.parse(csvData, UTF8, CSVFormat.newFormat('\t'))) {
            List<CSVRecord> records = parser.getRecords();
            String[] columns = getColumns(records);
            String[] data = records.get(1).get(0).split(";");
            int indexColumn = 0;
            checkColumnsCSV(columns, sectionCampagn, indexColumn);
            CampaignEntity campaignEntity = createCampaign(data);
            indexColumn += sectionCampagn.length;
            campaignEntity.setPhases(getPhaseOfCampaign(campaignEntity, columns, data));
            int nbPhase = Integer.parseInt(data[4]);
            indexColumn += 5 * nbPhase;
            campaignEntity.setDefenseDays(createDefenseDayEntity(campaignEntity, indexColumn, columns, data));
            setOrganizer(campaignEntity, columns, data, accountRepository);
            Files.delete(csvData.toPath());
            return campaignEntity;

        } catch (IOException e) {
            throw new IllegalArgumentException("Le fichier n'a pas pu être examiné.");
        }
    }

    /**
     * Récupére les organisateurs associés à la campagne
     *
     * @param campaignEntity    l'objet campagne entity
     * @param columns           l'entête des colonnes
     * @param data              les données du fichier csv
     * @param accountRepository le répository d'account
     */
    private static void setOrganizer(CampaignEntity campaignEntity, String[] columns, String[] data, AccountRepository accountRepository) {
        String headColumn = "ORGANISTEUR SUP";
        int index = columns.length - 1;
        if (!columns[index].equalsIgnoreCase(headColumn)) {
            throw new IllegalArgumentException("La colonne " + columns[index] + " ne respecte pas le bon format.");
        }
        String[] organizer = data[index].split(",");
        Set<AccountEntity> accountEntities = new HashSet<>();
        for (String s : organizer) {
            AccountEntity accountEntity = accountRepository.findByEmail(s.trim());
            if (accountEntity == null) {
                throw new IllegalArgumentException("Le compte " + s + " n'existe pas.");
            }
            List<String> role = accountEntity.getRoleEntities().stream().map(RoleEntity::getName).collect(Collectors.toList());
            if (!role.contains("Organisateur")) {
                throw new IllegalArgumentException("Le compte " + s + " n'a pas le role d'organisateur");
            }
            accountEntities.add(accountEntity);
        }
        campaignEntity.setOrganizer(accountEntities);
    }


    /**
     * création d'une campagne avec la section campagne du fichier csv
     *
     * @param data la ligne des données du fichier csv
     * @return
     */
    private static CampaignEntity createCampaign(String[] data) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String nameCampaign = data[0];
        try {
            Date startDateCampaign = new Date(dateFormat.parse(data[1]).getTime());
            Date endDateCampaign = new Date(dateFormat.parse(data[2]).getTime());

            String status = data[3];

            return new CampaignEntity(nameCampaign, startDateCampaign, endDateCampaign, new CampaignStateEntity(status));
        } catch (ParseException e) {
            throw new IllegalArgumentException("Les dates de la campagne ne sont pas au bon format");
        }

    }

    /**
     * récupére tous les phases de la campagne
     *
     * @param campaignEntity la campagne entity
     * @param columns        l'entête des colonnes
     * @param data           les données
     * @return un Set de phaseEntity
     */
    private static Set<PhaseEntity> getPhaseOfCampaign(CampaignEntity campaignEntity, String[] columns, String[] data) {
        String[] headOfPhase = {"DATE DEBUT PHASE ", "DATE FIN PHASE ", "NB DISPO MIN PHASE ", "SAISIE PAR PHASE ", "DISPO DE PHASE "};
        int nbPhase = Integer.parseInt(data[4]);
        Set<PhaseEntity> phaseEntities = new HashSet<>();
        int index = 5;
        for (int i = 1; i <= nbPhase; i++) {
            int numPhase = i;
            String[] headColumn = Arrays.stream(headOfPhase).map(j -> j + numPhase).toArray(String[]::new);
            checkColumnsCSV(columns, headColumn, index);
            phaseEntities.add(createPhaseEntity(campaignEntity, data, index));
            index += headColumn.length;
        }
        return phaseEntities;
    }

    /**
     * création d'une phase Entity
     *
     * @param campaignEntity la capagneEntity
     * @param data           les données
     * @param index          le prochaine index à lire
     * @return une phase entity
     */
    private static PhaseEntity createPhaseEntity(CampaignEntity campaignEntity, String[] data, int index) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date startPhase = new Date(dateFormat.parse(data[index++]).getTime());
            Date endPhase = new Date(dateFormat.parse(data[index++]).getTime());
            int nbDispo = Integer.parseInt(data[index++]);
            String writtenBy = data[index++];
            String writtenFor = data[index];
            return new PhaseEntity(startPhase, endPhase, campaignEntity, nbDispo, writtenBy, writtenFor);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Problème lors de l'importation des phases");
        }
    }

    /**
     * Création des day defense pour une campagna à partir du fichier csv de configuration de la campagne
     *
     * @param campaignEntity la campagne entity
     * @param indexColumn    l'index de la colonne à lire
     * @param columns        les entêtes des colonnes
     * @param data           les données du fichier csv
     * @return un Set de DefenseDayType
     */
    private static Set<DefenseDayEntity> createDefenseDayEntity(CampaignEntity campaignEntity, int indexColumn, String[] columns, String[] data) {
        LocalDate dateEnd = campaignEntity.getCampaignEnd().toLocalDate().plusDays(1);
        Set<DefenseDayEntity> defenseDayEntitySet = new HashSet<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            if (checkColumnsDateRoom(columns[indexColumn])) {
                throw new IllegalArgumentException("La colonne " + columns[indexColumn] + " ne respecte pas le bon format.");
            }
            LocalDate columnsDate = new Date(dateFormat.parse(columns[indexColumn].split(" - ")[1]).getTime()).toLocalDate();
            while (columnsDate.isBefore(dateEnd)) {
                int nbRoom = Integer.parseInt(data[indexColumn]);
                defenseDayEntitySet.add(new DefenseDayEntity(Date.valueOf(columnsDate), campaignEntity, createRooms(nbRoom)));
                indexColumn++;
                if (checkColumnsDateRoom(columns[indexColumn])) {
                    break;
                }
                columnsDate = new Date(dateFormat.parse(columns[indexColumn].split(" - ")[1]).getTime()).toLocalDate();

            }
            return createTimeSlot(defenseDayEntitySet, indexColumn, columns, data);
        } catch (ParseException e) {
            throw new IllegalArgumentException("La colonne " + columns[indexColumn] + " ne respecte pas le bon format.");
        }

    }

    /**
     * création des time slot pour une les defenseDayEntity
     *
     * @param defenseDayEntitySet Les defenses day entity
     * @param indexColumn         l'index de la prochaine colonnes à lire
     * @param columns             les entêtes de colonnes
     * @param data                les données du fichier csv
     * @return un set de DefenseTypeEntity
     */
    private static Set<DefenseDayEntity> createTimeSlot(Set<DefenseDayEntity> defenseDayEntitySet, int indexColumn, String[] columns, String[] data) {
        String headColumn = "PLAGE HORAIRES";
        if (!headColumn.equalsIgnoreCase(columns[indexColumn])) {
            throw new IllegalArgumentException("La colonne " + columns[indexColumn] + " ne respecte pas le bon format.");
        }
        Set<TimeslotEntity> timeslotEntities = createTimeSlot(data[indexColumn++]);
        defenseDayEntitySet.forEach(d -> d.setSlots(timeslotEntities));
        headColumn += " JOUR -";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        while (indexColumn < columns.length && columns[indexColumn].startsWith(headColumn)) {
            try {
                Date columnsDate = new Date(dateFormat.parse(columns[indexColumn].split(" - ")[1]).getTime());
                Set<TimeslotEntity> timeslotDate = createTimeSlot(data[indexColumn++]);
                defenseDayEntitySet.stream().filter(d -> d.getDate().equals(columnsDate)).forEach(d -> d.setSlots(timeslotDate));
            } catch (ParseException e) {
                throw new IllegalArgumentException("La colonne " + columns[indexColumn] + " ne respecte pas le bon format.");
            }
        }
        return defenseDayEntitySet;
    }

    /**
     * création d'un timeSlot
     *
     * @param data les heures lus dans le fichier csv
     * @return un Set de deux timeSlot
     */
    private static Set<TimeslotEntity> createTimeSlot(String data) {
        String[] times = data.split(",");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        return Arrays.asList(times).stream().map(t -> {
            try {
                String[] timeSlot = t.split("-");
                Time start = new Time(timeFormat.parse(makeFormatTime(timeSlot[0])).getTime());
                Time end = new Time(timeFormat.parse(makeFormatTime(timeSlot[1])).getTime());
                return new TimeslotEntity(start, end);
            } catch (ParseException e) {
                throw new IllegalArgumentException("La colonne des heures ne respecte pas le bon format.");
            }
        }).collect(Collectors.toSet());
    }

    /**
     * vérification du bon format de l'heure
     *
     * @param s l'heure lu
     * @return l'heure correctement formatée
     */
    private static String makeFormatTime(String s) {
        String[] time = s.split(":");
        if (Integer.parseInt(time[0]) > 23) {
            throw new IllegalArgumentException("Les heures renseignées ne sont pas correctes");
        }
        if (time.length == 1) {
            return time[0] + ":00";
        }
        if (Integer.parseInt(time[1]) > 60) {
            throw new IllegalArgumentException("Les heures renseignées ne sont pas correctes");
        }
        return time[0] + ":" + time[1];

    }

    /**
     * Vérifie les entête de colonne des salles disponibles
     *
     * @param column la colonne lu
     * @return true si la colonnes est correcte non sinon
     */
    private static boolean checkColumnsDateRoom(String column) {
        String columnsName = "NOMBRE DE SALLES DISPO - ";
        return !column.startsWith(columnsName);
    }

    /**
     * création des salles
     *
     * @param nbRoom le nombre de salles à créer
     * @return un set de RoomEntity
     */
    private static SortedSet<RoomEntity> createRooms(int nbRoom) {
        return IntStream.range(1, nbRoom + 1).mapToObj(n -> new RoomEntity(String.valueOf(n))).collect(Collectors.toCollection(TreeSet::new));
    }
}
