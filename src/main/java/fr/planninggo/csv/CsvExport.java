package fr.planninggo.csv;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.user.entity.AccountEntity;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CsvExport {
    private CsvExport() {}
    /**
     * Export une campagne dans un fichier csv
     *
     * @param campaignEntity la campagne à exporter
     * @return un csvWriter
     */
    public static CSVWriter exportCampaign(CampaignEntity campaignEntity) {
        ArrayList<String> columnsName = new ArrayList<>();
        ArrayList<String> data = new ArrayList<>();
        addSectionCampaign(campaignEntity, columnsName, data);
        addPhase(campaignEntity, columnsName, data);
        addNbrRoom(campaignEntity, columnsName, data);
        addTime(campaignEntity, columnsName, data);
        columnsName.add("ORGANISTEUR SUP");
        data.add(campaignEntity.getOrganizers().stream().map(AccountEntity::getEmail).collect(Collectors.joining(",")));
        Path path = Paths.get("Export_parametre.csv");
        return new CSVWriter(path, Arrays.asList(String.join(";", columnsName), String.join(";", data)));
    }

    /**
     * ajout la session time lors de l'export d'une campagne
     *
     * @param campaignEntity la campagne entity
     * @param columnsName    les entêtes des colonnes
     * @param data           les données
     */
    private static void addTime(CampaignEntity campaignEntity, ArrayList<String> columnsName, ArrayList<String> data) {
        String timeColumn = "PLAGE HORAIRES";
        String timeSpecifique = "PLAGE HORAIRES JOUR - ";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        HashMap<String, Integer> mapTime = new HashMap<>();
        for (DefenseDayEntity day : campaignEntity.getDefenseDays()) {
            String time = getFormatTime(day.getSlots());
            mapTime.put(time, mapTime.getOrDefault(time, 0) + 1);
        }
        columnsName.add(timeColumn);
        String timeGlobal = Collections.max(mapTime.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
        data.add(timeGlobal);
        for (DefenseDayEntity day : campaignEntity.getDefenseDays()) {
            String time = getFormatTime(day.getSlots());
            if (!time.equals(timeGlobal)) {
                columnsName.add(timeSpecifique + dateFormat.format(day.getDate()));
                data.add(time);
            }
        }
    }

    /**
     * formate les heures
     *
     * @param slots un set de slot
     * @return les heures formaté
     */
    private static String getFormatTime(Set<TimeslotEntity> slots) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        StringBuilder slotString = new StringBuilder();
        for (TimeslotEntity time : slots) {
            slotString.append(timeFormat.format(time.getBegin())).append("-").append(timeFormat.format(time.getEnd())).append(",");
        }
        slotString.setLength(slotString.length() - 1);
        return slotString.toString();
    }

    private static void addNbrRoom(CampaignEntity campaignEntity, ArrayList<String> columnsName, ArrayList<String> data) {
        String columnsRoom = "NOMBRE DE SALLES DISPO - ";
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        for (DefenseDayEntity day : campaignEntity.getDefenseDays()) {
            columnsName.add(columnsRoom + dateFormat.format(day.getDate()));
            data.add(Integer.toString(day.getRooms().size()));
        }
    }

    /**
     * ajout les phase de récolte
     *
     * @param campaignEntity la campagne entity
     * @param columnsName    les entêtes des colonnes
     * @param data           les données
     */
    private static void addPhase(CampaignEntity campaignEntity, ArrayList<String> columnsName, ArrayList<String> data) {
        List<String> headOfPhase = Arrays.asList("DATE DEBUT PHASE ", "DATE FIN PHASE ", "NB DISPO MIN PHASE ", "SAISIE PAR PHASE ", "DISPO DE PHASE ");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        int comptPhase = 1;
        for (PhaseEntity phase : campaignEntity.getPhases()) {
            final int indexPhase = comptPhase;
            columnsName.addAll(headOfPhase.stream().map(h -> h + indexPhase).collect(Collectors.toList()));
            data.add(dateFormat.format(phase.getBegin()));
            data.add(dateFormat.format(phase.getEnd()));
            data.add(Integer.toString(phase.getAvailabilityNumber()));
            data.add(phase.getWrittenBy());
            data.add(phase.getWrittenFor());
            comptPhase++;
        }

    }

    /**
     * ajoute la session campagne
     *
     * @param campaignEntity la campagne entity
     * @param columnsName    les entêtes des colonnes
     * @param data           les données
     */
    private static void addSectionCampaign(CampaignEntity campaignEntity, ArrayList<String> columnsName, ArrayList<String> data) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        columnsName.addAll(Arrays.asList("NOM CAMPAGNE", "DATE DEBUT SOUTENANCES", "DATE FIN SOUTENANCE", "STATUT", "NB PHASE RECOLTE"));
        data.add(campaignEntity.getName());
        data.add(dateFormat.format(campaignEntity.getCampaignBegin()));
        data.add(dateFormat.format(campaignEntity.getCampaignEnd()));
        data.add(campaignEntity.getCampaignState());
        data.add(Integer.toString(campaignEntity.getPhases().size()));
    }
}
