package fr.planninggo.spring.user.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Cette classe permet de récupérer les informations de la table "account"
 */
@Entity
@Table(name="account")
public class AccountEntity implements UserDetails, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "rgpdAccept")
    private boolean rgpdAccept;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "account_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private Set<RoleEntity> roleEntities = new HashSet<>();

    public AccountEntity(String email, String password, Set<RoleEntity> roleEntities) {
        this.email = Objects.requireNonNull(email);
        Objects.requireNonNull(password);
        if (!password.substring(0, 6).equals("$2a$10")) {
            password = new BCryptPasswordEncoder().encode(password);
        }
        this.password = password;
        this.roleEntities = Objects.requireNonNull(roleEntities);
        this.rgpdAccept = false;
    }

    public AccountEntity(String email, String password, boolean rgpdAccept, Set<RoleEntity> roleEntities) {
        this(email, password, roleEntities);
        this.rgpdAccept = rgpdAccept;
        this.roleEntities = new HashSet<>();
    }

    public AccountEntity() {
    }

    public static AccountEntity create(CSVRecord csvRecord) {
        return new AccountEntity(csvRecord.get(1),
                csvRecord.get(2),
                csvRecord.get(3).equals("t"),
                new HashSet<>());
    }

    @Override
    @Generated
    public int hashCode() {
        return email.hashCode() ^ password.hashCode() ^ roleEntities.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof AccountEntity)) {
            return false;
        }
        AccountEntity a = (AccountEntity) obj;
        return email.equals(a.email) && roleEntities.containsAll(a.roleEntities) && a.roleEntities.containsAll(roleEntities);
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", rgpdAccept=" + rgpdAccept +
                ", roleEntities=" + roleEntities.stream().map(RoleEntity::getName).collect(Collectors.joining()) +
                '}';
    }

    public int getId() {
        return id;
    }



    public Set<RoleEntity> getRoleEntities() {
        return roleEntities;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isRgpdAccept() {
        return rgpdAccept;
    }

    public void setRgpdAccept(boolean rgpdAccept) {
        this.rgpdAccept = rgpdAccept;
    }

    @Override
    @Generated
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roleEntities;
    }

    @Override
    @Generated
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    @Override
    @Generated
    public String getUsername() {
        return email;
    }

    @Override
    @Generated
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    @Generated
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    @Generated
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    @Generated
    public boolean isEnabled() {
        return true;
    }

    public void addRole(RoleEntity roleEntity) {
        this.roleEntities.add(roleEntity);
    }

    public void setRoleEntities(Set<RoleEntity> roleEntity) {
        this.roleEntities = roleEntity;
    }

    public void addRoles(Set<RoleEntity> roleEntities) {
        this.roleEntities.addAll(roleEntities);
    }
}
