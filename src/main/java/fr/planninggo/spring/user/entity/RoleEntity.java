package fr.planninggo.spring.user.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "role"
 */
@Entity
@Table(name="role")
public class RoleEntity implements GrantedAuthority, Serializable, Comparable<RoleEntity> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;

    @Column(name = "role_name")
    private String name;

    public RoleEntity() {
    }

    public RoleEntity(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public static RoleEntity create(CSVRecord csvRecord) {
        return new RoleEntity(csvRecord.get(1));
    }

    @Override
    @Generated
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof RoleEntity)) {
            return false;
        }
        RoleEntity e = (RoleEntity) obj;
        return this.name.equals(e.name);
    }

    public String getName() {
        return name;
    }

    @Override
    @Generated
    public String toString() {
        return name;
    }

    @Override
    @Generated
    public String getAuthority() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(@NotNull RoleEntity o) {
        return name.compareTo(o.name);
    }
}
