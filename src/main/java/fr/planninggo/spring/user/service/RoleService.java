package fr.planninggo.spring.user.service;

import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.repository.RoleRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Cette classe permet de requêter la table role
 */
@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Inject
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     * Cette méthode permet de récupérer toutes les roles présents en base
     *
     * @return une liste de roles
     */
    public List<RoleEntity> getAllRoles() {
        return (List<RoleEntity>) roleRepository.findAll();
    }

    /**
     * Cette méthode permet de récupérer un role présent  en base
     *
     * @return un role
     */
    public RoleEntity findByName(String name) {
        return roleRepository.findByName(name);
    }

    public RoleEntity findByNameLowercase(String currentRolename) {
        return roleRepository.findByNameLowercase(currentRolename);
    }
}
