package fr.planninggo.spring.user.service;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.repository.AccountRepository;
import fr.planninggo.spring.user.repository.RoleRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * cette classe permet de requêter la table account
 */
@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;

    @Inject
    public AccountService(RoleRepository roleRepository, AccountRepository accountRepository) {
        this.roleRepository = roleRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Cette méthode permet de sauvegarder ou de mettre à jour un compte en base
     *
     * @param accountEntity le compte à sauvegarder ou à modifier
     */
    public void saveAccount(AccountEntity accountEntity) {
        accountRepository.save(accountEntity);
    }

    /**
     * Cette méthode permet de sauvegarder ou de mettre à jour des comptes en base
     */
    public void saveAccounts(CampaignEntity campaignEntity) {
        for(AccountEntity accountEntity : campaignEntity.getOrganizers()) {
            Set<RoleEntity> roleEntities = new HashSet<>();
            for (RoleEntity roleEntity : accountEntity.getRoleEntities()) {
                RoleEntity role = this.roleRepository.findByName(roleEntity.getName());
                if (role == null) {
                    role = this.roleRepository.save(roleEntity);
                }
                roleEntities.add(role);
            }
            accountEntity.setRoleEntities(roleEntities);
        }

        campaignEntity.getOrganizers().stream().filter(organizer -> accountRepository.findByEmail(organizer.getEmail()) == null).forEach(accountRepository::save);
    }

    /**
     * Cette méthode permet de récupérer toutes les comptes présents en base
     *
     * @return une liste de comptes
     */
    public List<AccountEntity> findAll() {
        return (List<AccountEntity>) accountRepository.findAll();
    }

    /**
     * Cette méthode permet de récupérer un compte avec un email
     *
     * @param email email correspondant au compte
     * @return le compte retrouvé en base
     */
    public AccountEntity findByEmail(String email) {
        return accountRepository.findByEmail(email);
    }

}
