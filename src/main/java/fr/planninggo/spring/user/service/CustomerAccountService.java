package fr.planninggo.spring.user.service;

import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.repository.AccountRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe sert lors de l'authentification de l'utilisateur via Spring security
 * elle permet de récupérer un UserDetail en base avec l'email saisi.
 */
@Service("CustomAccountDetailService")
@Transactional
public class CustomerAccountService implements UserDetailsService {
    private static final Logger LOGGER = Logger.getLogger(CustomerAccountService.class.getName());
    private final AccountRepository accountRepository;

    @Inject
    public CustomerAccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * cette méthode permet de récupérer un userDetail avec un email
     *
     * @param email email saisi
     * @return un objet UserDetail correspondant à l'email
     */
    @Override
    public UserDetails loadUserByUsername(String email) {
        AccountEntity accountEntity = accountRepository.findByEmail(email);
        if (accountEntity == null) {
            LOGGER.log(Level.INFO, "AccountEntity not found !");
            return new AccountEntity();
        }
        return accountEntity;
    }
}
