package fr.planninggo.spring.user.controller;

import fr.planninggo.spring.user.service.RoleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;


/**
 * Cette classe traite toutes les requêtes en lien avec les roles
 */
@RestController
@RequestMapping(name = "/role")
public class RoleController {
    private final RoleService roleService;

    @Inject
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }
}
