package fr.planninggo.spring.user.controller;

import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.service.AccountService;
import fr.planninggo.spring.user.service.RoleService;
import fr.planninggo.utility.MailBuilder;
import fr.planninggo.utility.Password;
import fr.planninggo.utility.SendMail;
import fr.planninggo.utility.SymetricEncryption;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe traite toutes les requêtes en lien avec un compte utilisateur
 */
@RestController
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;
    private final RoleService roleService;

    @Inject
    public AccountController(AccountService accountService, RoleService roleService) {
        this.accountService = accountService;
        this.roleService = roleService;
    }

    /**
     * Cette méthode permet d'accepter les conditions du RGPD
     * Elle peut être appelée seulement lorsqu'un utilisateur est authentifié.
     */
    @PostMapping(value = "/acceptRgpd")
    public void acceptRgpd(HttpServletResponse response) throws IOException {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        account.setRgpdAccept(true);
        accountService.saveAccount(account);
        if (account.getRoleEntities().stream().map(RoleEntity::getName).anyMatch(e -> e.equals("Organisateur"))) {
            response.sendRedirect("/app/campaign");
            return;
        }
        response.sendRedirect("/app/");
    }

    /**
     * Générère un URL en cryptant le mail et en l'envoyant par mail
     *
     * @param email    email
     * @param response redirection vers la page associée
     * @throws IllegalBlockSizeException e
     * @throws InvalidKeyException e
     * @throws BadPaddingException e
     * @throws NoSuchAlgorithmException e
     * @throws NoSuchPaddingException e
     */
    @PostMapping(value = "/generateURL")
    public ModelAndView generateURL(@RequestParam(value = "mail") String email, HttpServletResponse response, HttpSession session) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        if (accountService.findByEmail(email) == null) {
            return new ModelAndView("redirect:/newPassword?error");
        } else {
            String encryptedUrl = SymetricEncryption.encrypt(email + ";esipe2019");
            String url = "https://163.172.87.149/app/generatePassword?hash=" + encryptedUrl;
            session.setAttribute("mail", SendMail.sendAnEmailWithResponse(MailBuilder.createPasswordMail(url, email)));
            return new ModelAndView("redirect:/sendMail");
        }
    }

    /**
     * Decrypte le mail, génère un nouveau mot de passe, le crypte et l'enregistre en base avant de l'envoyer à l'utilisateur.
     *
     * @param hash hash
     * @return mot de passe
     * @throws IllegalBlockSizeException e
     * @throws InvalidKeyException e
     * @throws BadPaddingException e
     * @throws NoSuchAlgorithmException e
     * @throws NoSuchPaddingException e
     */
    @CrossOrigin("*")
    @GetMapping(value = "/generatePasswordJson")
    public List<String> generatePassword(@RequestParam(value = "hash") String hash) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        String decryptedUrl = SymetricEncryption.decrypt(hash);
        String email = decryptedUrl.split(";")[0];

        String password = Password.generatePassword();

        AccountEntity accountEntity = accountService.findByEmail(email);
        accountEntity.setPassword(password);
        accountService.saveAccount(accountEntity);

        List<String> list = new ArrayList<>();
        list.add(password);
        return list;
    }
}
