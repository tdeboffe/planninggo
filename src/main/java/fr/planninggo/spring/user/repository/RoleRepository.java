package fr.planninggo.spring.user.repository;

import fr.planninggo.spring.user.entity.RoleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Cette interface permet de requêter la table role
 */
public interface RoleRepository extends CrudRepository<RoleEntity, String> {
    RoleEntity findByName(String name);

    @Query(value = "SELECT * FROM role WHERE UPPER(role_name) = UPPER(?1) LIMIT 1", nativeQuery = true)
    RoleEntity findByNameLowercase(@Param("name") String name);
}
