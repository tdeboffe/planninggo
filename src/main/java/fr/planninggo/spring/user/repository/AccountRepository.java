package fr.planninggo.spring.user.repository;

import fr.planninggo.spring.user.entity.AccountEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table account
 */
public interface AccountRepository extends CrudRepository<AccountEntity, Integer> {
    AccountEntity findByEmail(String username);
}
