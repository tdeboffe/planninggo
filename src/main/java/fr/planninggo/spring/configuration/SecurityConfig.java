package fr.planninggo.spring.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.inject.Inject;

/**
 * Cette classe permet de configurer SpringSecurity
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    @Qualifier(value = "CustomAccountDetailService")
    private UserDetailsService accountDetailsService;

    /**
     * donne l'accès à un service de la base à  AuthenticationManagerBuilder
     * ceci dans le but de pouvoir accèder aux comptes enregistrés en base
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.eraseCredentials(false).userDetailsService(accountDetailsService);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Permet de retourner un objet gérant les redirections après que l'authentification ait réussi
     *
     * @return un  objet AuthentificationSuccessHandler
     */
    @Bean
    public AuthenticationSuccessHandler customHandler() {
        return new CustomAuthenticationSuccessHandler();
    }

    /**
     * Configure les pages qui doivent faire l'objet d'une authentification
     *
     * @param httpSecurity
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers(HttpMethod.POST).permitAll();

        httpSecurity
                .authorizeRequests()
                .antMatchers("/account/generateURL" ,"/defense/own/**", "/campaign/**/scheduler", "/campaign/**/getDefenses", "/defense/scheduler", "/campaign/**/getBeginDate", "/defense/schedule").permitAll()
                .antMatchers("/profil/**", "/rgpd").authenticated()
                .antMatchers("/", "/supervisor", "/attendee/**", "/attendeeGuest/**", "/availability/**", "campaign/**/updateStateAvailabilities").authenticated()
                .antMatchers("/campaign/**", "/defense/**", "/constraint/**", "/defense/**", "/defenseDay/**", "/defenseType/**", "/phase/**", "/room/**", "/student/**", "/timeslot/**").hasAuthority("Organisateur")
                .antMatchers("/accounts/**").hasAuthority("Administrateur")
                .and().exceptionHandling().accessDeniedPage("/403");

        httpSecurity
                .formLogin()
                .loginPage("/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll()
                .successHandler(customHandler());

        httpSecurity.csrf().disable();
    }
}
