package fr.planninggo.spring.configuration;

import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.spring.campaign.service.CampaignStateService;
import fr.planninggo.utility.MailBuilder;
import fr.planninggo.utility.SendMail;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Cette classe permet d'effectuer des taches automatiquements tous les jours à 1h00
 */
@Component
public class ScheduledTasks {
    private final CampaignService campaignService;
    private final CampaignStateService campaignStateService;

    @Inject
    public ScheduledTasks(CampaignService campaignService, CampaignStateService campaignStateService) {
        this.campaignService = campaignService;
        this.campaignStateService = campaignStateService;
    }

    /**
     * Permet de lancer une phase de récolte
     */
    @Scheduled(cron = "0 0 1 * * *")
    public void launchPhase() {
        List<CampaignEntity> campaignEntities = campaignService.getCampaigns();
        for (CampaignEntity campaignEntity : campaignEntities) {
            if (!campaignEntity.getPhases().isEmpty()) {
                treatmentStart(campaignEntity, campaignEntity.getPhases());
            }
        }
    }

    /**
     * permet de cloturer une phase de récolte
     */
    @Scheduled(cron = "0 0 1 * * *")
    public void endPhase() {
        List<CampaignEntity> campaignEntities = campaignService.getCampaigns();
        for (CampaignEntity campaignEntity : campaignEntities) {
            if (!campaignEntity.getPhases().isEmpty()) {
                treatmentEnd(campaignEntity, campaignEntity.getPhases());
            }
        }
    }


    private void treatmentEnd(CampaignEntity campaignEntity, Set<PhaseEntity> phases) {
        for (PhaseEntity phaseEntity : phases) {
            if (phaseEntity.getEnd().equals(Date.valueOf(LocalDate.now().minusDays(1)))) {
                campaignEntity.setCampaignState(campaignStateService.findByName(State.READY_GENERATE));
                for (PhaseEntity phaseEntity1 : phases) {
                    if (!phaseEntity1.equals(phaseEntity)) {
                        if (phaseEntity1.isStart()) {
                            return;
                        } else if (phaseEntity1.getBegin().toLocalDate().isAfter(LocalDate.now())){
                            CampaignStateEntity stateEntity = campaignStateService.findByName(State.WAIT_PHASE);
                            if (stateEntity == null) {
                                stateEntity = new CampaignStateEntity(State.WAIT_PHASE);
                            }
                            campaignEntity.setCampaignState(stateEntity);
                            campaignService.save(campaignEntity);
                            return;
                        }
                    }
                }
            }
        }
        campaignService.save(campaignEntity);
    }

    private void treatmentStart(CampaignEntity campaignEntity, Set<PhaseEntity> phases) {
        String writtenBy;
        String writtenFor;
        for (PhaseEntity phaseEntity : phases) {
            if (phaseEntity.getBegin().equals(Date.valueOf(LocalDate.now()))) {
                CampaignStateEntity stateEntity = campaignStateService.findByName(State.PHASE_IN_PROGRESS);
                campaignEntity.setCampaignState(stateEntity);
                writtenBy = phaseEntity.getWrittenBy();
                writtenFor = phaseEntity.getWrittenFor();
                if (writtenFor.equals(writtenBy)) {
                    writtenFor = "";
                }

                if (writtenBy.equals("Etudiant")) {
                    for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                        SendMail.sendAnEmail(MailBuilder.createAvailabilitiesMail("http://localhost:8080/app", studentEntity.getEmail(), writtenFor, new SimpleDateFormat("dd/MM/yyyy").format(phaseEntity.getEnd())));
                    }
                } else {
                    boolean sendToSupervisor = false;
                    final String wb = writtenBy;
                    for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                        Optional<String> tutor = studentEntity.getSupervisors().stream().filter(s -> s.getRoleEntity().getName().equals(wb)).map(ss -> ss.getSupervisor().getEmail()).findAny();
                        if (tutor.isPresent()) {
                            sendToSupervisor = true;
                            SendMail.sendAnEmail(MailBuilder.createAvailabilitiesMail("http://localhost:8080/app", tutor.get(), writtenFor, new SimpleDateFormat("dd/MM/yyyy").format(phaseEntity.getEnd())));
                        }
                    }
                    if (!sendToSupervisor) {
                        for (AttendeeGuestEntity attendeeGuestEntity : campaignEntity.getGuests()) {
                            SendMail.sendAnEmail(MailBuilder.createAvailabilitiesMail("http://localhost:8080/app", attendeeGuestEntity.getEmail(), writtenFor, new SimpleDateFormat("dd/MM/yyyy").format(phaseEntity.getEnd())));
                        }
                    }
                }
            }
        }
        campaignService.save(campaignEntity);
    }
}
