package fr.planninggo.spring.configuration;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;


/**
 * Cette classe gére les redirections après que l'autentification ait réussi
 * cette redirection doit se faire avec le rôle le plus élevé du compte.
 */
@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {
        Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
        if (roles.contains("Organisateur")) {
            httpServletResponse.sendRedirect("/app/campaign");
        } else if (roles.contains("Administrateur")) {
            httpServletResponse.sendRedirect("/app/accounts");
        } else {
            httpServletResponse.sendRedirect("/app/");
        }
    }
}
