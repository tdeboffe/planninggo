package fr.planninggo.spring.configuration;

/**
 * Les différents états d'une campagne
 */
public class State {
    private State() {}
    public static final String CAMPAIGN_CREATE = "En attente d'import de participants";
    public static final String READY_GENERATE_DEFAULT = "Prêt à être généré (ATTENTION : Disponible tout le temps)";
    public static final String READY_GENERATE = "Prêt à être généré";
    public static final String READY_GENERATE_ORG = "Prêt à être généré (Disponibilités données par l'organisateur)";
    public static final String WAIT_PHASE = "En attente du lancement d'une phase de récolte";
    public static final String PHASE_IN_PROGRESS = "Phase de récolte en cours";
    public static final String GENERATE = "Planning généré";
    public static final String BROADCAST = "Planning diffusé";
    public static final String PARTIAL_BROADCAST = "Planning diffusé partiellement";
    public static final String ARCHIVE = "Archivé";
}
