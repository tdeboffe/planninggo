package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.repository.PhaseRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class PhaseService {
    private final PhaseRepository phaseRepository;

    @Inject
    public PhaseService(PhaseRepository phaseRepository) {
        this.phaseRepository = phaseRepository;
    }

    /**
     * Permet la récupération de toutes les phases
     */
    public List<PhaseEntity> getPhases() {
        return (List<PhaseEntity>) this.phaseRepository.findAll();
    }

    /**
     * retourne une phase par son identifiant
     * @param id l'identifiant de la phase
     * @return une phase entity
     */
    public PhaseEntity getPhase(Integer id) {
        return this.phaseRepository.findById(id).get();
    }
}
