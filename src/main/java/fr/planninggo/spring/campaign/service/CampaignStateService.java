package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.campaign.repository.CampaignStateRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class CampaignStateService {
    private final CampaignStateRepository campaignStateRepository;

    @Inject
    public CampaignStateService(CampaignStateRepository campaignStateRepository) {
        this.campaignStateRepository = campaignStateRepository;
    }

    /**
     * retrouve un état de la campagne
     * @param name nom de l'état
     * @return un CampaignStateEntity
     */
    public CampaignStateEntity findByName(String name) {
        return campaignStateRepository.findByName(name);
    }

    /**
     * Sauvegarde un CampaignStateEntity
     * @param campaignStateEntity le CampaignStateEntity à sauvegarder
     * @return un CampaignStateEntity
     */
    public CampaignStateEntity save(CampaignStateEntity campaignStateEntity) {
        return campaignStateRepository.save(campaignStateEntity);
    }

}
