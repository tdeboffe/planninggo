package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.repository.CampaignRepository;
import fr.planninggo.spring.campaign.repository.CampaignStateRepository;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import fr.planninggo.spring.campaign.repository.PhaseRepository;
import fr.planninggo.spring.configuration.State;
import fr.planninggo.spring.user.entity.RoleEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * cette classe permet de requêter plusieurs tables comme campagne,
 */
@Service
public class CampaignService {
    private final CampaignRepository campaignRepository;
    private final DefenseRepository defenseRepository;
    private final CampaignStateRepository campaignStateRepository;
    private final PhaseRepository phaseRepository;

    @Inject
    public CampaignService(PhaseRepository phaseRepository, CampaignRepository campaignRepository, DefenseRepository defenseRepository, CampaignStateRepository campaignStateRepository) {
        this.campaignRepository = campaignRepository;
        this.phaseRepository = phaseRepository;
        this.defenseRepository = defenseRepository;
        this.campaignStateRepository = campaignStateRepository;
    }

    /**
     * Permet la récupération de toutes les campagnes
     */
    public List<CampaignEntity> getCampaigns() {
        return (List<CampaignEntity>) this.campaignRepository.findAll();
    }

    /**
     * Permet de récupérer une campagne par son id
     * @param id  identifiant de la campagne
     */
    public Optional<CampaignEntity> getCampaign(int id) {
        return this.campaignRepository.findById(id);
    }


    private Timestamp getEndingDate(Timestamp beginningDate, String duration) {
        String[] durations = duration.split("h");
        int hour = Integer.parseInt(durations[0].replaceAll(" ", ""));
        int min = Integer.parseInt(durations[1].replaceAll(" ", ""));
        LocalDateTime localDateTime = beginningDate.toLocalDateTime();

        localDateTime = localDateTime.plus(hour, ChronoUnit.HOURS);
        localDateTime = localDateTime.plus(min, ChronoUnit.MINUTES);
        return Timestamp.valueOf(LocalDateTime.of(localDateTime.getYear(), localDateTime.getMonth(), localDateTime.getDayOfMonth(),
                localDateTime.getHour(), localDateTime.getMinute()));
    }

    /**
     * Renvoi un JSON contenant les informations sur la soutenance,avec les dates si la soutenance est placée.
     * @param defense la soutenance
     * @param id
     * @param dates les dates de la soutenance
     */
    private String getDefenseInformation(DefenseEntity defense, int id, String... dates) {
        //Membres du jury
        List<AttendeeEntity> attendees = new ArrayList<>();
        attendees.addAll(defense.getAttendeeGuests());

        attendees.addAll(defense.getStudent().getSupervisorsSet());

        String attendeeEntities = attendees.stream().map(attendeeEntity -> attendeeEntity.getFirstname()
                + " " + attendeeEntity.getLastname()).collect(Collectors.joining(", "));


        //Présidents du jury
        List<RoleEntity> rolePresident = defense.getDefenseType().getJuryMembers().stream()
                .filter(JuryMemberEntity::isPresident).map(JuryMemberEntity::getRole).collect(Collectors.toList());
        List<AttendeeEntity> presidents = new ArrayList<>();
        for(AttendeeEntity attendeeEntity : attendees) {
            if(attendeeEntity.getAccount().getRoleEntities().stream().anyMatch(rolePresident::contains)) {
                presidents.add(attendeeEntity);
            }
        }
        String presidentsEntities = presidents.stream().map(attendeeEntity -> attendeeEntity.getFirstname()
                + " " + attendeeEntity.getLastname()).collect(Collectors.joining(", "));

        if (dates.length == 2) {
            return "{\"text\":\"Etudiant: " + defense.getStudent().getFirstname() + " " + defense.getStudent().getLastname() + "/"
                    + "Filière: " + defense.getStudent().getStudy() + "/"
                    + "Membre(s) du Jury: " + attendeeEntities + "/"
                    + "Président(s) du Jury: " + presidentsEntities + "/"
                    + "Exercice: " + defense.getDefenseType().getDesc() + "/"
                    + "Lieu: " + defense.getRoom()
                    + "\", \"hold\":\"" + defense.isHold()
                    + "\", \"start_date\":\"" + dates[0]
                    + "\", \"end_date\":\"" + dates[1]
                    + "\", \"firstName\":\"" + defense.getStudent().getFirstname()
                    + "\", \"lastName\":\"" + defense.getStudent().getLastname()
                    + "\", \"duration\":\"" + defense.getDefenseType().getDurationToInt()
                    + "\", \"room\":\"" + defense.getRoom().getName()
                    + "\"}";
        }
        return "{\"id\": \"" + id+ "\", " +
                "\"text\":\"Etudiant: " + defense.getStudent().getFirstname() + " " + defense.getStudent().getLastname() + "/"
                + "Filière: " + defense.getStudent().getStudy() + "/"
                + "Membre(s) du Jury: " + attendeeEntities + "/"
                + "Président(s) du Jury: " + presidentsEntities + "/"
                + "Exercice: " + defense.getDefenseType().getDesc() + "/"
                + "Lieu: " + defense.getRoom()
                + "\", \"userdata\":[" +
                "{\"name\": \"hold\", \"content\" : \"" + defense.isHold()
                + "\"}, {\"name\" : \"firstName\", \"content\" : \"" + defense.getStudent().getFirstname()
                + "\"}, {\"name\" : \"lastName\", \"content\" : \"" + defense.getStudent().getLastname()
                + "\"}, {\"name\" : \"duration\", \"content\" : \"" + defense.getDefenseType().getDurationToInt()
                + "\"}, {\"name\" : \"room\", \"content\" : \"" + defense.getRoom().getName()
                + "\"}, {\"name\" : \"fromTree\", \"content\" : \"" + true
                + "\"}]"
                + "}";
    }

    /**
     * Cette méthode permet de récupérer toutes les soutenances présentes en base
     * @param id l'identifiant de la campagne
     * @return une liste de soutenances
     */
    public List<String> findById(int id) {
        List<DefenseEntity> list = defenseRepository.findAllWithCampaignId(id);
        List<String> toReturn = new ArrayList<>();
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        int i = 1;
        for (DefenseEntity defense : list) {
            String beginningDate = dateFormat.format(defense.getDefenseDate());
            String endingDate = dateFormat.format(getEndingDate(defense.getDefenseDate(), defense.getDefenseType().getDuration()));
            toReturn.add(getDefenseInformation(defense, i, beginningDate, endingDate));
            i++;
        }
        return toReturn;
    }


    /**
     * Cette méthode permet de récupérer toutes les soutenances présentes en base
     * @param id l'identifiant de la campagne
     * @return une liste de soutenances
     */
    public List<String> findNullDefensesById(int id) {
        List<DefenseEntity> list = defenseRepository.findNullDefensesWithCampaignId(id);
        List<String> toReturn = new ArrayList<>();

        int i = 1;
        for (DefenseEntity defense : list) {
            toReturn.add(getDefenseInformation(defense, i));
            i++;
        }
        return toReturn;
    }

    /**
     * permet de créer une nouvelle campagne
     * @param campaignEntity une campagne entity à suvegarder
     * @return
     */
    public CampaignEntity createCampaign(CampaignEntity campaignEntity) {
        Set<PhaseEntity> phaseEntities = campaignEntity.getPhases();
        CampaignStateEntity campaignStateEntity;
        int endPhase = 0;
        if (!campaignEntity.getAttendees().isEmpty()) {
            if (!campaignEntity.getPhases().isEmpty()) {
                String state = State.WAIT_PHASE;
                for (PhaseEntity phaseEntity : campaignEntity.getPhases()) {
                    if (phaseEntity.getEnd().toLocalDate().isBefore(LocalDate.now())) {
                        endPhase++;
                    } else if (phaseEntity.isStart()) {
                        state = State.PHASE_IN_PROGRESS;
                        break;
                    }
                }
                campaignStateEntity = campaignStateRepository.findByName(state);
                if (campaignStateEntity == null) {
                    campaignStateEntity = new CampaignStateEntity(state);
                }
            } else {
                campaignStateEntity = campaignStateRepository.findByName(State.READY_GENERATE_DEFAULT);
                if (campaignStateEntity == null) {
                    campaignStateEntity = new CampaignStateEntity(State.READY_GENERATE_DEFAULT);
                }
            }
            if (endPhase == phaseEntities.size()) {
                campaignStateEntity = campaignStateRepository.findByName(State.READY_GENERATE);
            }
            campaignEntity.setCampaignState(campaignStateEntity);
        }
        campaignEntity.setPhases(new HashSet<>());
        campaignEntity = this.campaignRepository.save(campaignEntity);
        final CampaignEntity campaign = campaignEntity;
        phaseEntities.forEach(s -> {
            s.setCampaign(campaign);
            phaseRepository.save(s);
        });
        List<PhaseEntity> allPhases = (List<PhaseEntity>) phaseRepository.findAll();
        for (PhaseEntity phase : allPhases) {
            if (phase.getCampaign().getId() == campaignEntity.getId() && !phaseEntities.contains(phase)) {
                phaseRepository.delete(phase);
            }
        }
        campaignEntity.setPhases(phaseEntities);
        return campaignRepository.save(campaignEntity);
    }

    /**
     * mets à jour une campagne entity
     * @param campaignEntity une campagne entity
     */
    public void save(CampaignEntity campaignEntity) {
        campaignRepository.save(campaignEntity);
    }
}
