package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.campaign.repository.TimeslotRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class TimeslotService {
    private final TimeslotRepository timeslotRepository;

    @Inject
    public TimeslotService(TimeslotRepository timeslotRepository) {
        this.timeslotRepository = timeslotRepository;
    }

    /**
     * Permet la création ou la mise à jour de timeslots
     * @param timeslotEntity1 premier timeslots
     * @param timeslotEntity2 deuxième timeslots
     */
    public Set<TimeslotEntity> createTimeslots(TimeslotEntity timeslotEntity1, TimeslotEntity timeslotEntity2) {
        List<TimeslotEntity> timeslotEntities = (List<TimeslotEntity>) timeslotRepository.findAll();
        Set<TimeslotEntity> toRet = new TreeSet<>();
        boolean insert1 = true;
        boolean insert2 = true;
        for (TimeslotEntity timeslotEntity : timeslotEntities) {
            if (timeslotEntity1.getBegin().equals(timeslotEntity.getBegin()) && timeslotEntity1.getEnd().equals(timeslotEntity.getEnd())) {
                insert1 = false;
                toRet.add(timeslotEntity);
            }
            if (timeslotEntity2.getBegin().equals(timeslotEntity.getBegin()) && timeslotEntity2.getEnd().equals(timeslotEntity.getEnd())) {
                toRet.add(timeslotEntity);
                insert2 = false;
            }
        }
        if (insert1) {
            toRet.add(timeslotEntity1);
        }
        if (insert2) {
            toRet.add(timeslotEntity2);
        }
        return toRet;
    }

    /**
     * Permet la récupération de tous les timelots
     */
    public List<TimeslotEntity> getTimeslots() {
        return (List<TimeslotEntity>) this.timeslotRepository.findAll();
    }
}
