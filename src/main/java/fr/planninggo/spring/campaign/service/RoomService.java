package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.campaign.repository.RoomRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class RoomService {
    private final RoomRepository roomRepository;

    @Inject
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    /**
     * Permet la création ou la mise à jour de salles depuis une campagne.
     * @param campaignEntity une campagne entity
     */
    public SortedSet<RoomEntity> createRooms(CampaignEntity campaignEntity) {
        Set<RoomEntity> roomEntities = IntStream.range(1,campaignEntity.getRoomNumber()+1).mapToObj(n -> new RoomEntity(String.valueOf(n))).collect(Collectors.toSet());
        Set<RoomEntity> noExist = roomEntities.stream().filter(roomEntity -> this.roomRepository.findByName(roomEntity.getName()) == null)
                .map(this.roomRepository::save).collect(Collectors.toSet());
        Set<RoomEntity> exist = roomEntities.stream().filter(roomEntity -> this.roomRepository.findByName(roomEntity.getName()) != null)
                .map(e -> this.roomRepository.findByName(e.getName())).collect(Collectors.toSet());
        exist.addAll(noExist);
        TreeSet<RoomEntity> sort = new TreeSet<>();
        sort.addAll(exist);
        sort.addAll(noExist);
        return sort;
    }

    /**
     * recherche une salle par son nom
     * @param name le nom de la salle
     * @return une RoomEntity
     */
    public RoomEntity findByName(String name) {
        return this.roomRepository.findByName(name);
    }

    /**
     * recherche une salle par son id
     * @param id l'identifiant de la salle
     * @return une RoomEntity
     */
    public RoomEntity findById(int id) { return this.roomRepository.findById(id); }
}
