package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import fr.planninggo.spring.campaign.repository.ConstraintRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class ConstraintService {
    private final ConstraintRepository constraintRepository;

    @Inject
    public ConstraintService(ConstraintRepository constraintRepository) {
        this.constraintRepository = constraintRepository;
    }

    /**
     * Permet la récupération de tous les contraintes
     */
    public List<ConstraintEntity> getConstraints() {
        return (List<ConstraintEntity>) this.constraintRepository.findAll();
    }

}
