package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.repository.DefenseDayRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class DefenseDayService {
    private final DefenseDayRepository defenseDayRepository;

    @Inject
    public DefenseDayService(DefenseDayRepository defenseDayRepository) {
        this.defenseDayRepository = defenseDayRepository;
    }

    /**
     * Permet la création ou la mise à jour de defenseDays
     * @param defenseDayEntities un set de defenseDay à sauvegarder
     */
    public void createDefenseDays(Set<DefenseDayEntity> defenseDayEntities) {
        defenseDayEntities.forEach(d -> {
            if (defenseDayRepository.findDefenseDayEntityByDate(d.getDate()) == null) {
                this.defenseDayRepository.save(d);
            }
        });
    }

    /**
     * Permet la récupération de tous les defenseDay
     */
    public List<DefenseDayEntity> getDefenseDays() {
        return (List<DefenseDayEntity>) this.defenseDayRepository.findAll();
    }
}
