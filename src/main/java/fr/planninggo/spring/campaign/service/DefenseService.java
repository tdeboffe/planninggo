package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class DefenseService {
    private final DefenseRepository defenseRepository;

    @Inject
    public DefenseService(DefenseRepository defenseRepository) {
        this.defenseRepository = defenseRepository;
    }

    /**
     * Permet la récupération de toutes les soutenances
     * @param campaignEntity  une campagne
     */
    public List<DefenseEntity> getDefensesByCampaign(CampaignEntity campaignEntity) {
        return this.defenseRepository.findByCampaign(campaignEntity);
    }

    /**
     * sauvegarde une soutenance
     * @param defenseEntity la soutenance à sauvegarder
     */
    public void save(DefenseEntity defenseEntity) {
        defenseRepository.save(defenseEntity);
    }

    /**
     * retrouve une soutenance d'un étudiant par son prénom et nom et sa campagne
     * @param firstName le prénom de l'étudiant
     * @param lastName le nom de l'étudiant
     * @param campaignEntity la campagne
     * @return un optional de la défense de l'étudiant
     */
    public Optional<DefenseEntity> findByFirstNameAndLastNameAndCampaign(String firstName, String lastName, CampaignEntity campaignEntity) {
        return defenseRepository.findByFirstNameAndLastNameAndCampaign(firstName, lastName, campaignEntity.getId());
    }

    /**
     * retrouve toutes soutenance d'une campagne correspondant à un type de soutenance
     * @param campaignEntity la campagne
     * @param defenseTypeEntity le type de soutenance
     * @return une liste de defense entity
     */
    public List<DefenseEntity> findByCampaignAndDefenseType(CampaignEntity campaignEntity, DefenseTypeEntity defenseTypeEntity) {
        return defenseRepository.findByCampaignAndDefenseType(campaignEntity, defenseTypeEntity);
    }

    /**
     * supprime une soutenance par son identifiant
     * @param id l'identifiant de la soutenance
     */
    public void removeById(int id) {
        defenseRepository.deleteById(id);
    }

    /**
     * récupérer toutes les soutenances
     * @return
     */
    public List<DefenseEntity> getDefenses() {
        return (List<DefenseEntity>)  defenseRepository.findAll();
    }
}
