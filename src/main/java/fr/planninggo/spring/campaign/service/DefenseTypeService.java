package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.repository.DefenseTypeRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class DefenseTypeService {
    private final DefenseTypeRepository defenseTypeRepository;

    @Inject
    public DefenseTypeService(DefenseTypeRepository defenseTypeRepository) {
        this.defenseTypeRepository = defenseTypeRepository;
    }


    /**
     * Permet la création ou la mise à jour d'un nouveau type de soutenances depuis une campagne
     * @param campaignEntity une campagne entity
     */
    public void createDefensesType(CampaignEntity campaignEntity) {
        campaignEntity.getDefensesType().forEach(this.defenseTypeRepository::save);
    }

    /**
     * Permet la récupération de tous les types de soutenances
     *
     */
    public List<DefenseTypeEntity> getDefenseTypes() {
        return (List<DefenseTypeEntity>) this.defenseTypeRepository.findAll();
    }

    /**
     * recherche un type de soutenance par son code
     * @param code le code du type de soutenance
     * @return le type de soutenance
     */
    public DefenseTypeEntity findByCode(String code) { return defenseTypeRepository.findByCode(code); }

    /**
     * Vérifie si le code d'un type de soutenance est déjà présent en base
     * @param code le code du type de soutenance
     * @return true si le code existe false sinon
     */
    public boolean isCodePresent(String code) {
        return defenseTypeRepository.findByCode(code) != null;
    }
}
