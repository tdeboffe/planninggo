package fr.planninggo.spring.campaign.service;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.campaign.repository.JuryMemberRepository;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.repository.RoleRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Cette classe offre des méthodes simplifiant l'enregistrement des informations
 */

@Service
public class JuryMemberService {
    private final JuryMemberRepository juryMemberRepository;
    private final RoleRepository roleRepository;

    @Inject
    public JuryMemberService(JuryMemberRepository juryMemberRepository, RoleRepository roleRepository) {
        this.juryMemberRepository= juryMemberRepository;
        this.roleRepository = roleRepository;
    }


    /**
     * Permet la création ou la mise à jour de membres du jury depuis une campagne
     * @param campaignEntity une campagne entity
     */
    public void createJuryMembers(CampaignEntity campaignEntity) {
        campaignEntity.getDefensesType().forEach(defenseType ->
            defenseType.getJuryMembers().forEach(juryMember -> {
                RoleEntity roleEntity = this.roleRepository.findByName(juryMember.getRole().getName());
                if (roleEntity == null) {
                    roleEntity = this.roleRepository.save(juryMember.getRole());
                }
                juryMember.setRole(roleEntity);
            })
        );

        for(DefenseTypeEntity defenseType: campaignEntity.getDefensesType()) {
            for(JuryMemberEntity juryMember : defenseType.getJuryMembers()) {
                this.juryMemberRepository.save(juryMember);
            }
        }
    }
}
