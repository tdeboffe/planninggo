package fr.planninggo.spring.campaign.controller;

import fr.planninggo.csv.CSVWriter;
import fr.planninggo.csv.CsvExport;
import fr.planninggo.scheduler.ScheduleCampaign;
import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.service.AttendeeGuestService;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.attendee.service.StudentService;
import fr.planninggo.spring.attendee.service.SupervisorService;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.campaign.entity.RoomEntity;
import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.spring.campaign.service.CampaignStateService;
import fr.planninggo.spring.campaign.service.ConstraintService;
import fr.planninggo.spring.campaign.service.DefenseDayService;
import fr.planninggo.spring.campaign.service.DefenseService;
import fr.planninggo.spring.campaign.service.DefenseTypeService;
import fr.planninggo.spring.campaign.service.JuryMemberService;
import fr.planninggo.spring.campaign.service.RoomService;
import fr.planninggo.spring.campaign.service.TimeslotService;
import fr.planninggo.spring.configuration.State;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.service.AccountService;
import fr.planninggo.spring.user.service.RoleService;
import fr.planninggo.utility.MailBuilder;
import fr.planninggo.utility.Password;
import fr.planninggo.utility.SendMail;
import fr.planninggo.utility.SymetricEncryption;
import fr.planninggo.utility.Utils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Cette classe traite toutes les requêtes en lien avec une campagne
 */
@RestController
@RequestMapping("/campaign")
public class CampaignController {
    private final CampaignService campaignService;
    private final DefenseTypeService defenseTypeService;
    private final RoleService roleService;
    private final DefenseService defenseService;
    private final CampaignStateService campaignStateService;
    private final TimeslotService timeslotService;
    private final RoomService roomService;
    private final ConstraintService constraintService;
    private final AccountService accountService;
    private final DefenseDayService defenseDayService;
    private final JuryMemberService juryMemberService;
    private final SupervisorService supervisorService;
    private final StudentService studentService;
    private final AttendeeGuestService attendeeGuestService;
    private final AttendeeService attendeeService;

    @Inject
    public CampaignController(AttendeeService attendeeService, CampaignService campaignService, DefenseTypeService defenseTypeService, RoleService roleService, DefenseService defenseService, CampaignStateService campaignStateService, TimeslotService timeslotService, RoomService roomService, ConstraintService constraintService, AccountService accountService, DefenseDayService defenseDayService, JuryMemberService juryMemberService, SupervisorService supervisorService, StudentService studentService, AttendeeGuestService attendeeGuestService) {
        this.campaignService = campaignService;
        this.attendeeService = attendeeService;
        this.defenseTypeService = defenseTypeService;
        this.roleService = roleService;
        this.defenseService = defenseService;
        this.campaignStateService = campaignStateService;
        this.timeslotService = timeslotService;
        this.roomService = roomService;
        this.constraintService = constraintService;
        this.accountService = accountService;
        this.defenseDayService = defenseDayService;
        this.juryMemberService = juryMemberService;
        this.supervisorService = supervisorService;
        this.studentService = studentService;
        this.attendeeGuestService = attendeeGuestService;
    }

    /**
     * Récupérer toutes les soutenances d'une campagne
     * @param id l'identifiant de la campagne
     * @return une liste de soutenance au format texte
     */
    @CrossOrigin("*")
    @GetMapping(value = "/{id}/getDefenses")
    public List<String> getDefenses(@PathVariable int id) {
        return campaignService.findById(id);
    }

    /**
     * sauvegarde un soutenance sur le planning
     * @param firstName le présnom de l'étudiant
     * @param lastName le nom de l'étudiant
     * @param beginDate la date de la soutenance
     * @param hold si elle est bloquée ou non
     */
    @CrossOrigin("*")
    @GetMapping(value = "/saveDefense")
    @ResponseStatus(value = HttpStatus.OK)
    public void saveDefense(HttpSession session, @RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName, @RequestParam(name = "beginDate") String beginDate, @RequestParam(name = "hold") String hold) {
        CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
        Optional<DefenseEntity> defenseEntity = defenseService.findByFirstNameAndLastNameAndCampaign(firstName, lastName, campaignEntity);
        if (defenseEntity.isPresent()) {
            DefenseEntity defenseEntity1 = defenseEntity.get();
            defenseEntity1.setDefenseDate(new Timestamp(Long.valueOf(beginDate)));
            defenseEntity1.setHold(Boolean.valueOf(hold));
            defenseEntity1.setShare(false);
            defenseService.save(defenseEntity1);
        }
    }

    /**
     * Vérifie si le planning est valide
     * @param id l'identifiant de la campagne
     * @return true si le planning est valide false sinon
     */
    @GetMapping("/verify/{id}")
    public List<String> verifyPlanning(@PathVariable("id") Integer id) {
        return controlPlanning(id);
    }

    @NotNull
    private List<String> controlPlanning(Integer id) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        boolean defenseWithOutDate = false;
        if (optional.isEmpty()) {
            return List.of("ERREUR : Cette erreur ne doit pas se produire : optional");
        }
        CampaignEntity campaignEntity = optional.get();
        Set<DefenseEntity> defenseEntities = campaignEntity.getDefenses();
        if (defenseEntities.size() != campaignEntity.getStudents().size()) {
            return List.of("ERREUR :  Toutes les soutenances ne sont pas placées");
        }

        for (DefenseEntity defenseEntity : defenseEntities) {
            if (defenseEntity.getDefenseDate() == null) {
                defenseWithOutDate = true;
                continue;
            }
            Optional<AvailabilityEntity> optionalAvailabilityEntity = defenseEntity.getStudent().getAvailabilities().stream().filter(a -> a.getDate().toLocalDate().equals(defenseEntity.getDefenseDate().toLocalDateTime().toLocalDate())).findAny();
            if (optionalAvailabilityEntity.isEmpty()) {
                return List.of("ERREUR : Cette erreur ne doit pas se produire : dispoStudent");
            }
            AvailabilityEntity availabilityEntity = optionalAvailabilityEntity.get();
            if (defenseEntity.getDefenseDate().toLocalDateTime().toLocalTime().isBefore(LocalTime.NOON)) {
                if (!availabilityEntity.isOnMorning()) {
                    return List.of("ERREUR :  Etudiant : " + defenseEntity.getStudent().getEmail() + " non disponible pour la soutenance : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter));
                }
            } else {
                if (!availabilityEntity.isOnAfternoon()) {
                    return List.of("ERREUR :  Etudiant : " + defenseEntity.getStudent().getEmail() + " non disponible pour la soutenance : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter));
                }
            }

            for (SupervisorEntity supervisorEntity : defenseEntity.getStudent().getSupervisorsSet()) {
                Optional<AvailabilityEntity> optionalAvailabilityEntity1 = supervisorEntity.getAvailabilities().stream().filter(a -> a.getDate().toLocalDate().equals(defenseEntity.getDefenseDate().toLocalDateTime().toLocalDate())).findAny();
                if (optionalAvailabilityEntity1.isEmpty()) {
                    return List.of("ERREUR : Cette erreur ne doit pas se produire : dispoSup");
                }
                AvailabilityEntity availabilityEntity1 = optionalAvailabilityEntity1.get();
                if (defenseEntity.getDefenseDate().toLocalDateTime().toLocalTime().isBefore(LocalTime.NOON)) {
                    if (!availabilityEntity1.isOnMorning()) {
                        return List.of("ERREUR :  Superviseur : " + supervisorEntity.getEmail() + " non disponible pour la soutenance : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter));
                    }
                } else {
                    if (!availabilityEntity1.isOnAfternoon()) {
                        return List.of("ERREUR :  Superviseur : " + supervisorEntity.getEmail() + " non disponible pour la soutenance : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter));
                    }
                }
            }

            for (AttendeeGuestEntity attendeeGuestEntity : defenseEntity.getAttendeeGuests()) {
                Optional<AvailabilityEntity> optionalAvailabilityEntity1 = attendeeGuestEntity.getAvailabilities().stream().filter(a -> a.getDate().toLocalDate().equals(defenseEntity.getDefenseDate().toLocalDateTime().toLocalDate())).findAny();
                if (optionalAvailabilityEntity1.isEmpty()) {
                    return List.of("ERREUR : Cette erreur ne doit pas se produire : dispoAttendee");
                }
                AvailabilityEntity availabilityEntity1 = optionalAvailabilityEntity1.get();
                if (defenseEntity.getDefenseDate().toLocalDateTime().toLocalTime().isBefore(LocalTime.NOON)) {
                    if (!availabilityEntity1.isOnMorning()) {
                        return List.of("ERREUR :  Invité : " + attendeeGuestEntity.getEmail() + " non disponible pour la soutenance : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter));
                    }
                } else {
                    if (!availabilityEntity1.isOnAfternoon()) {
                        return List.of("ERREUR :  Invité : " + attendeeGuestEntity.getEmail() + " non disponible pour la soutenance : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter));
                    }
                }
            }

            for (DefenseEntity defenseEntityCompare : defenseEntities) {
                if (defenseEntity.getId() != defenseEntityCompare.getId() && overlap(defenseEntity, defenseEntityCompare)) {
                    if (defenseEntity.getRoom().equals(defenseEntityCompare.getRoom())) {
                        return List.of("ERREUR :  Même salle pour les deux soutenances suivantes : " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter) + " : " + defenseEntity.getStudent().getEmail() + " et " + defenseEntityCompare.getDefenseDate().toLocalDateTime().format(formatter) + " : " + defenseEntityCompare.getStudent().getEmail() + ".");
                    }
                    if (defenseEntity.getStudent().getEmail().equals(defenseEntityCompare.getStudent().getEmail())) {
                        return List.of("ERREUR :  Les soutenances " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter) + " et " + defenseEntityCompare.getDefenseDate().toLocalDateTime().format(formatter) + " ont le même étudiant. Très étrange");
                    }
                    for (AttendeeGuestEntity attendeeGuestEntity : defenseEntity.getAttendeeGuests()) {
                        for (AttendeeGuestEntity attendeeGuestEntity1 : defenseEntityCompare.getAttendeeGuests()) {
                            if (attendeeGuestEntity.getEmail().equals(attendeeGuestEntity1.getEmail())) {
                                return List.of("ERREUR :  Les soutenances " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter) + " et " + defenseEntityCompare.getDefenseDate().toLocalDateTime().format(formatter) + " ont le même invité : " + attendeeGuestEntity.getEmail() + ".");
                            }
                        }
                    }
                    for (SupervisorEntity supervisorEntity : defenseEntity.getStudent().getSupervisorsSet()) {
                        for (SupervisorEntity supervisorEntity1 : defenseEntityCompare.getStudent().getSupervisorsSet()) {
                            if (supervisorEntity.getEmail().equals(supervisorEntity1.getEmail())) {
                                return List.of("ERREUR :  Les soutenances " + defenseEntity.getDefenseDate().toLocalDateTime().format(formatter) + " et " + defenseEntityCompare.getDefenseDate().toLocalDateTime().format(formatter) + " ont le même superviseur : " + supervisorEntity.getEmail() + ".");
                            }
                        }
                    }
                }
            }
        }
        if (defenseWithOutDate) {
            return List.of("Le planning est correct, mais des soutenances ne sont pas encore placées.");
        }
        return List.of("true");
    }

    private static boolean overlap(DefenseEntity defenseEntity, DefenseEntity defenseEntity1) {
        if (defenseEntity.getDefenseDate() == null || defenseEntity1.getDefenseDate() == null) {
            return false;
        }
        int duration1 = Integer.parseInt(defenseEntity.getDefenseType().getDurationToInt());
        int duration2 = Integer.parseInt(defenseEntity1.getDefenseType().getDurationToInt());

        LocalDateTime end1 = defenseEntity.getDefenseDate().toLocalDateTime().plusMinutes(duration1);
        LocalDateTime end2 = defenseEntity1.getDefenseDate().toLocalDateTime().plusMinutes(duration2);

        return !end1.minusMinutes(1).isBefore(defenseEntity1.getDefenseDate().toLocalDateTime())
                && !defenseEntity.getDefenseDate().toLocalDateTime().plusMinutes(1).isAfter(end2);
    }

    /**
     * récupérer toutes les soutenances non planifiées d'une campagne
     * @param id l'identifiant de la campagne
     * @return une liste de défense
     */
    @CrossOrigin("*")
    @GetMapping(value = "/{id}/getNullDefenses")
    public List<String> getNullDefenses(@PathVariable int id) {
        return campaignService.findNullDefensesById(id);
    }

    /**
     * récupére la date de début d'une campagne
     * @param id l'identifiant de la campagne
     * @return un json contenant la date de la campagne
     */
    @CrossOrigin("*")
    @GetMapping(value = "/{id}/getBeginDate")
    public List<String> getBeginDate(@PathVariable("id") Integer id) {
        Optional<CampaignEntity> campaignEntity = campaignService.getCampaign(id);
        List<String> toReturn = new ArrayList<>();
        if (campaignEntity.isPresent()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            toReturn.add("{\"date\":\"" + simpleDateFormat.format(campaignEntity.get().getCampaignBegin().getTime()) + "\"}");
        }
        return toReturn;
    }

    /**
     * récupère tous les roles présents dans la table role de la base
     * excepté les role organisateur, administrateur et étudiant
     * @return une liste de role
     */
    @GetMapping(value = "/roles")
    public List<RoleEntity> getRoles() {
        return new TreeSet<>(roleService.getAllRoles()).stream().filter(e -> !e.getName().equals("Administrateur") && !e.getName().equals("Organisateur") && !e.getName().equals("Etudiant")).collect(Collectors.toList());
    }

    /**
     * export une campagne au format csv
     * @param response http response
     * @param id l'identifiant de la campagne
     * @return un imputStream correspondant au fichier csv
     * @throws IOException
     */
    @RequestMapping(value = "/{id}/exportCampaign", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public InputStreamSource exportCampaignSetting(HttpServletResponse response, @PathVariable(value = "id") int id) throws IOException {
        Optional<CampaignEntity> campaignEntity = campaignService.getCampaign(id);
        if (campaignEntity.isPresent()) {
            CSVWriter writer = CsvExport.exportCampaign(campaignEntity.get());
            response.setHeader("Content-Disposition", "attachment; filename=" + writer.getPath().toString());
            return new InputStreamResource(writer.getData());
        } else {
            throw new IllegalArgumentException("L'identifiant de la campagne ne correspond à aucune campagne en base");
        }
    }

    /**
     * Diffuse le planning auprès des participants de la campagne
     * @param session
     * @param otherMail les mails qui ne correspondent pas à des participants de la campagne
     * @param typeCode les types de soutenance de concerné par la diffusion
     * @param roles les roles concernés par la diffusion
     * @param msg le message
     * @param objet l'objet du message
     * @return retour sur la vue du planning
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    @PostMapping(value = "sendMail")
    public ModelAndView sharePlanning(HttpSession session, @RequestParam(value = "otherMails") String otherMail, @RequestParam(value = "type_code", required = false) String[] typeCode, @RequestParam(value = "role", required = false) String[] roles, @RequestParam(value = "message") String msg, @RequestParam(value = "objet") String objet) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
        if (!verifyPlanning(campaignEntity.getId()).get(0).equals("true")) {
            return new ModelAndView("redirect:/campaign/" + campaignEntity.getId() + "/scheduler");
        }

        Set<AttendeeEntity> allAttendee = new HashSet<>();
        Set<RoleEntity> roleEntities = new HashSet<>();
        List<String> mails = new ArrayList<>();
        if (otherMail.length() > 0) {
            mails = Arrays.asList(otherMail.split(";"));
        }
        List<DefenseTypeEntity> defenseTypes = new ArrayList<>();
        if (roles != null) {
            for (String role : roles) {
                roleEntities.add(roleService.findByName(role));
            }
        }
        if (typeCode != null) {
            for (String s1 : typeCode) {
                DefenseTypeEntity defenseTypeEntity = defenseTypeService.findByCode(s1);
                if (defenseTypeEntity == null) {
                    throw new IllegalArgumentException("le type de soutenance n'existe pas. ");
                }
                List<DefenseEntity> defenseEntities = defenseService.findByCampaignAndDefenseType(campaignEntity, defenseTypeEntity);

                allAttendee.addAll(defenseEntities.stream().filter(d -> !d.isShare()).map(DefenseEntity::getStudent)
                        .filter(s -> s.getAccount().getRoleEntities().stream().anyMatch(roleEntities::contains))
                        .collect(Collectors.toSet()));

                allAttendee.addAll(defenseEntities.stream().filter(d -> !d.isShare()).
                        map(d -> d.getStudent().getSupervisors().stream().filter(ss -> roleEntities.contains(ss.getRoleEntity()))
                                .map(SupervisorStudentEntity::getSupervisor).collect(Collectors.toSet())).
                        flatMap(Collection::stream).collect(Collectors.toSet()));

                allAttendee.addAll(defenseEntities.stream().filter(d -> !d.isShare()).map(DefenseEntity::getAttendeeGuests)
                        .flatMap(Collection::stream)
                        .filter(g -> g.getAccount().getRoleEntities().stream().anyMatch(roleEntities::contains))
                        .collect(Collectors.toSet()));
                defenseTypes.add(defenseTypeEntity);

                defenseEntities.forEach(d -> {
                    d.setShare(true);
                    defenseService.save(d);
                });
            }
        }
        List<String> errorMessages = sendEmailDiffusion(msg, objet, campaignEntity, allAttendee, mails);
        checkStateCampaign(campaignEntity, defenseTypes);
        session.setAttribute("errorBroadcast", errorMessages);
        return new ModelAndView("redirect:/campaign/" + campaignEntity.getId() + "/scheduler");
    }

    private void checkStateCampaign(CampaignEntity campaignEntity, List<DefenseTypeEntity> defenseTypes) {
        if (!defenseTypes.containsAll(campaignEntity.getDefensesType())) {
            campaignEntity.setCampaignState(campaignStateService.findByName(State.PARTIAL_BROADCAST));
            campaignService.save(campaignEntity);
            return;
        }
        campaignEntity.setCampaignState(campaignStateService.findByName(State.BROADCAST));
        campaignService.save(campaignEntity);
    }

    private List<String> sendEmailDiffusion(String msg, String objet, CampaignEntity campaignEntity, Set<AttendeeEntity> allAttendee, List<String> mails) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        List<String> errorMessage = new ArrayList<>();
        for (AttendeeEntity attendeeEntity : allAttendee) {
            String encryptedUrl = "https://163.172.87.149/app/defense/own/" + SymetricEncryption.encrypt(attendeeEntity.getEmail() + ";" + campaignEntity.getId());
            errorMessage.add(SendMail.sendAnEmailWithResponse(MailBuilder.sharePlanning(encryptedUrl, attendeeEntity.getEmail(), msg, objet)));
        }
        for (String mail : mails) {
            String encryptedUrl = "https://163.172.87.149/app/campaign/" + campaignEntity.getId() + "/scheduler";
            errorMessage.add(SendMail.sendAnEmailWithResponse(MailBuilder.sharePlanning(encryptedUrl, mail, msg, objet)));
        }
        return errorMessage;
    }

    /**
     * Envoie un mail de relance pour une phase
     * @param phaseId identifiant de la phase
     * @param campId l'identifiant de la campagne
     * @return
     */
    @GetMapping("relance")
    public List<String> broadcast(@RequestParam("idPhase") Integer phaseId, @RequestParam("idCamp") Integer campId) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(campId);
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            Optional<PhaseEntity> optionalPhaseEntity = campaignEntity.getPhases().stream().filter(p -> p.getId() == phaseId).findFirst();
            if (optionalPhaseEntity.isPresent()) {
                PhaseEntity phaseEntity = optionalPhaseEntity.get();
                String writtenBy = phaseEntity.getWrittenBy();
                String writtenFor = phaseEntity.getWrittenFor();
                if (writtenFor.equals(writtenBy)) {
                    writtenFor = "";
                }
                List<String> errorMessages = new ArrayList<>();
                if (writtenBy.equals("Etudiant")) {
                    for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                        errorMessages.add(SendMail.sendAnEmailWithResponse(MailBuilder.createAvailabilitiesMailRelance("http://localhost:8080/app", studentEntity.getEmail(), writtenFor, new SimpleDateFormat("dd/MM/yyyy").format(phaseEntity.getEnd()))));
                    }
                } else {
                    boolean sendToSupervisor = false;
                    final String wb = writtenBy;
                    for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                        Optional<String> tutor = studentEntity.getSupervisors().stream().filter(s -> s.getRoleEntity().getName().equals(wb)).map(ss -> ss.getSupervisor().getEmail()).findAny();
                        if (tutor.isPresent()) {
                            sendToSupervisor = true;
                            errorMessages.add(SendMail.sendAnEmailWithResponse(MailBuilder.createAvailabilitiesMailRelance("http://localhost:8080/app", tutor.get(), writtenFor, new SimpleDateFormat("dd/MM/yyyy").format(phaseEntity.getEnd()))));
                        }
                    }
                    if (!sendToSupervisor) {
                        for (AttendeeGuestEntity attendeeGuestEntity : campaignEntity.getGuests()) {
                            errorMessages.add(SendMail.sendAnEmailWithResponse(MailBuilder.createAvailabilitiesMailRelance("http://localhost:8080/app", attendeeGuestEntity.getEmail(), writtenFor, new SimpleDateFormat("dd/MM/yyyy").format(phaseEntity.getEnd()))));
                        }
                    }
                }
                return errorMessages;
            }
        }
        return List.of("true");
    }

    /**
     * mets à jour l'état de la campagne
     * @param id l'identifiant de la campagne
     */
    @GetMapping("{id}/updateStateAvailabilities")
    public void setCampaignState(@PathVariable("id") Integer id) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            if (!campaignEntity.getCampaignState().equals(State.GENERATE) && !campaignEntity.getCampaignState().equals(State.BROADCAST) && !campaignEntity.getCampaignState().equals(State.PARTIAL_BROADCAST) && !campaignEntity.getCampaignState().equals(State.ARCHIVE)) {
                campaignEntity.setCampaignState(campaignStateService.findByName(State.READY_GENERATE_ORG));
                campaignService.save(campaignEntity);
            }
        }
    }

    /**
     * permet de déplannifier une soutenance dans une campagne
     * @param session la session de l'utilisateur
     * @param firstName le prénom de l'étudiant
     * @param lastName le nom de l'étudiant
     */
    @CrossOrigin("*")
    @GetMapping(value = "saveDefenseOnTree")
    @ResponseStatus(value = HttpStatus.OK)
    public void saveDefenseOnTree(HttpSession session, @RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName) {
        CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
        Optional<DefenseEntity> defenseEntity = defenseService.findByFirstNameAndLastNameAndCampaign(firstName, lastName, campaignEntity);
        if (defenseEntity.isPresent()) {
            DefenseEntity defenseEntity1 = defenseEntity.get();
            defenseEntity1.setDefenseDate(null);
            defenseEntity1.setHold(false);
            defenseEntity1.setShare(false);
            defenseService.save(defenseEntity1);
        }
    }

    /**
     * fige ou défige toutes les soutenances d'une campagne
     * @param isHold true ou false
     */
    @CrossOrigin("*")
    @GetMapping(value = "saveDefenses")
    @ResponseStatus(value = HttpStatus.OK)
    public void saveDefenses(HttpSession session, @RequestParam(name = "isHold") String isHold) {
        CampaignEntity campaignEntity = (CampaignEntity) session.getAttribute("campaign");
        List<DefenseEntity> defenseEntities = defenseService.getDefensesByCampaign(campaignEntity);
        defenseEntities.stream().filter(defenseEntity -> defenseEntity.getDefenseDate() != null).forEach(defenseEntity -> {
            defenseEntity.setHold(Boolean.valueOf(isHold));
            defenseService.save(defenseEntity);
        });
    }

    /**
     * Affiche le planning
     * @param id l'identifiant de la campagne
     * @param request
     * @return
     */
    @GetMapping(value = "{id}/scheduler")
    public ModelAndView scheduler(@PathVariable("id") Integer id, HttpServletRequest request) {
        Optional<CampaignEntity> campaignEntity = campaignService.getCampaign(id);
        if (campaignEntity.isPresent()) {
            if (request.getSession().getAttribute("campaign") == null) {
                request.getSession().setAttribute("campaign", campaignEntity.get());
            }
            request.getSession().setAttribute("authenticated", !SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser"));
            return new ModelAndView("scheduler");
        }
        return new ModelAndView("campaign_home");
    }

    /**
     * retour à l'interface des campagnes
     * @return
     */
    @PostMapping(value = "backToCampaign")
    public ModelAndView backToCampaigns() {
        return new ModelAndView("redirect:/campaign");
    }

    /**
     * permet de charger l'interface d'une campagne
     * @param id l'identifiant d'une campagne
     * @param request
     * @return
     */
    @GetMapping(value = "{id}")
    public ModelAndView getCampaign(@PathVariable("id") Integer id, HttpServletRequest request) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        if (optional.isPresent()) {
            request.getSession().setAttribute("campaign", optional.get());
            return new ModelAndView("campaign");
        }
        return new ModelAndView("redirect:/campaign");
    }

    /**
     * lance la création d'une campagne
     * @return
     */
    @GetMapping(value = "/create")
    public ModelAndView campaignCreate() {
        return new ModelAndView("newCampaign_1");
    }

    /**
     *
     * @param request
     * @param map
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/defensetype")
    public ModelAndView defensetype(HttpServletRequest request, Map<String, Object> map) {
        request.getSession().setAttribute("defenseTypes", defenseTypeService.getDefenseTypes());
        return new ModelAndView("newCampaign_3", map);
    }

    /**
     * Ajout un type existant à une campagne
     * @param code le code du type de soutenance
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @GetMapping("/addExistType/{code}")
    public ModelAndView addExistType(@PathVariable("code") String code, HttpServletRequest request) {
        CampaignEntity campaignEntity = (CampaignEntity) request.getSession().getAttribute("campaign");
        if (campaignEntity.getDefensesType().stream().noneMatch(d -> d.getCode().equals(code))) {
            campaignEntity.addDefenseType(defenseTypeService.findByCode(code));
        } else {
            request.getSession().setAttribute("msgImport", "ERREUR : Le type de soutenance existe déjà. Veuillez l'importer : " + code);
        }
        request.getSession().setAttribute("campaign", campaignEntity);
        return new ModelAndView("newCampaign_3");
    }

    /**
     * création d'une nouvelle campagne
     * @param httpServletRequest
     * @param campaignName nom de la campagne
     * @param campaignBegin date du début de la campagne
     * @param campaignEnd date de fin de la campagne
     * @param organizers les organisateurs de la campagne
     * @param roomNumber le nombre de salle de la campagne
     * @return
     * @throws ParseException
     */
    @CrossOrigin("*")
    @PostMapping(value = "/type")
    public ModelAndView newCampaignType(HttpServletRequest httpServletRequest, @RequestParam(name = "campaignName") String campaignName, @RequestParam(name = "campaignBegin") String campaignBegin, @RequestParam(name = "campaignEnd") String campaignEnd, @RequestParam(name = "orgList") String organizers, @RequestParam(name = "roomNumber") Integer roomNumber) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date beginDate = simpleDateFormat.parse(campaignBegin);
        java.util.Date endingDate = simpleDateFormat.parse(campaignEnd);
        CampaignStateEntity campaignStateEntity = campaignStateService.findByName(State.CAMPAIGN_CREATE);
        if (campaignStateEntity == null) {
            campaignStateEntity = campaignStateService.save(new CampaignStateEntity(State.CAMPAIGN_CREATE));
        }
        Set<AccountEntity> accountEntities = new HashSet<>();
        AccountEntity currentAccount;
        RoleEntity roleEntity = roleService.findByName("Organisateur");
        if (roleEntity == null) {
            roleEntity = new RoleEntity("Organisateur");
        }
        accountEntities.add((AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        for (String s : organizers.split(",")) {
            currentAccount = accountService.findByEmail(s);
            if (currentAccount == null) {
                currentAccount = new AccountEntity(s, Password.generatePassword(), Set.of(roleEntity));
            }
            if (!s.equals("")) {
                accountEntities.add(currentAccount);
            }
        }
        CampaignEntity campaignEntity = (CampaignEntity) httpServletRequest.getSession().getAttribute("campaign");
        if (campaignEntity == null) {
            campaignEntity = new CampaignEntity(campaignName, Date.valueOf(campaignBegin), Date.valueOf(campaignEnd), accountEntities, campaignStateEntity, roomNumber);
        } else {
            campaignEntity.setName(campaignName);
            campaignEntity.setCampaignBegin(Date.valueOf(campaignBegin));
            campaignEntity.setCampaignEnd(Date.valueOf(campaignEnd));
            campaignEntity.setOrganizer(accountEntities);
            campaignEntity.setRoomNumber(roomNumber);
        }
        httpServletRequest.getSession().setAttribute("campaign", campaignEntity);
        if (beginDate.after(endingDate)) {
            httpServletRequest.getSession().setAttribute("msg", "ERREUR : La date de début doit être avant la date de fin.");
            return new ModelAndView("newCampaign_1");
        }
        httpServletRequest.getSession().setAttribute("defenseTypes", defenseTypeService.getDefenseTypes());
        return new ModelAndView("newCampaign_3");
    }

    /**
     * retour à la page de création de la campagne
     * @return
     */
    @CrossOrigin("*")
    @PostMapping(value = "/create")
    public ModelAndView backToCreate() {
        return new ModelAndView("newCampaign_1");
    }

    /**
     * création d'une campagne à partir d'une campagne existante
     * @param id l'identifiant de la campagne modèle
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @GetMapping(value = "/createFrom/{id}")
    public ModelAndView createFrom(@PathVariable("id") Integer id, HttpServletRequest request) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        CampaignStateEntity campaignStateEntity = campaignStateService.findByName(State.CAMPAIGN_CREATE);
        if (campaignStateEntity == null) {
            campaignStateEntity = new CampaignStateEntity(State.CAMPAIGN_CREATE);
        }
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = new CampaignEntity(optional.get(), campaignStateEntity);
            request.getSession().setAttribute("campaign", campaignEntity);
            return new ModelAndView("newCampaign_1");
        }
        return new ModelAndView("campaign_home");
    }

    /**
     * création d'une nouvelle campagne
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @PostMapping(value = "/createNewCampaign")
    public ModelAndView createNewCampaign(HttpServletRequest request) {
        CampaignEntity campaignEntity = (CampaignEntity) request.getSession().getAttribute("campaign");

        TimeslotEntity morning = new TimeslotEntity(Time.valueOf(LocalTime.of(9, 0)), Time.valueOf(LocalTime.NOON));
        TimeslotEntity afternoon = new TimeslotEntity(Time.valueOf(LocalTime.of(13, 45)), Time.valueOf(LocalTime.of(17, 0)));

        Set<TimeslotEntity> timeslotEntities = timeslotService.createTimeslots(morning, afternoon);

        SortedSet<RoomEntity> roomsEntities = roomService.createRooms(campaignEntity);

        List<ConstraintEntity> constraintEntities = constraintService.getConstraints();

        accountService.saveAccounts(campaignEntity);
        juryMemberService.createJuryMembers(campaignEntity);
        defenseTypeService.createDefensesType(campaignEntity);

        if (campaignEntity.getConstraintEntities().isEmpty()) {
            for (ConstraintEntity constraintEntity : constraintEntities) {
                campaignEntity.addConstraint(constraintEntity);
            }
        }

        CampaignEntity campaignEntity1 = campaignService.createCampaign(campaignEntity);

        List<LocalDate> localDates = ScheduleCampaign.getDefenseLocalDates(campaignEntity.getCampaignBegin().toLocalDate(), campaignEntity.getCampaignEnd().toLocalDate());
        Set<DefenseDayEntity> defenseDayEntities = localDates.stream().map(localDate -> new DefenseDayEntity(Date.valueOf(localDate),
                campaignEntity1, roomsEntities)).collect(Collectors.toSet());

        defenseDayEntities.forEach(defenseDayEntity -> {
            defenseDayEntity.setSlots(timeslotEntities);
            defenseDayEntity.setRooms(roomsEntities);
        });
        defenseDayService.createDefenseDays(defenseDayEntities);

        return new ModelAndView("redirect:/campaign/" + campaignEntity1.getId());
    }

    /**
     * permet d'ajouté un type de soutenance
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/addType")
    public ModelAndView addType() {
        return new ModelAndView("newCampaign_3_addtype1");
    }

    /**
     * permet de revenir à la création d'une campagne  à partir de l'interface de création des types
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/backToType")
    public ModelAndView backToType(HttpServletRequest request) {
        request.getSession().setAttribute("defenseTypes", defenseTypeService.getDefenseTypes());
        return new ModelAndView("newCampaign_3");
    }

    /**
     *  permet de revenir à la création d'une campagne à partir de l'interface des phases
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/backToPhase")
    public ModelAndView backToPhase() {
        return new ModelAndView("newCampaign_2");
    }

    @CrossOrigin("*")
    @PostMapping("/steps")
    public ModelAndView steps() {
        return new ModelAndView("newCampaign_2");
    }

    private static boolean codeAlreadyAffected(CampaignEntity campaignEntity, String code) {
        return campaignEntity.getPhases().stream().anyMatch(phase -> phase.getCode().equals(code));
    }

    /**
     * création d'une phase pour la campagne
     * @param request
     * @param map
     * @param campaignId l'identifiant de la campagne
     * @param dateBegin date de début de la phase
     * @param dateEnd date de fin de la phase
     * @param rolePar saisie par un role spécifique
     * @param rolePour saisie pour un role spécifique
     * @param availabilityNumber nombre minimal de disponibilité demandées
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/createPhase")
    public ModelAndView createPhase(HttpServletRequest request, Map<String, Object> map, @RequestParam(name = "campaign") String campaignId, @RequestParam(name = "dateBegin") Date dateBegin, @RequestParam(name = "dateEnd") Date dateEnd, @RequestParam(name = "rolePar") String rolePar, @RequestParam(name = "rolePour") String rolePour, @RequestParam(name = "availabilityNumber") int availabilityNumber) {
        CampaignEntity campaignEntity = (CampaignEntity) request.getSession().getAttribute("campaign");
        map.put("campaign", campaignId);
        Random random = new Random();
        int code = random.nextInt(50);
        while (codeAlreadyAffected(campaignEntity, String.valueOf(code))) {
            code = random.nextInt(50);
        }
        PhaseEntity phaseEntity = new PhaseEntity(code, dateBegin, dateEnd, campaignEntity, rolePar, rolePour, availabilityNumber);

        if (dateBegin.after(dateEnd)) {
            request.getSession().setAttribute("phase", phaseEntity);
            request.getSession().setAttribute("msg", "ERREUR : La date de début doit être avant la date de fin.");
            return new ModelAndView("newCampaign_2_addphase");
        }

        if (dateEnd.after(campaignEntity.getCampaignBegin())) {
            request.getSession().setAttribute("phase", phaseEntity);
            request.getSession().setAttribute("msg", "ERREUR : La phase doit avoir lieu avant le début de la campagne.");
            return new ModelAndView("newCampaign_2_addphase");
        }

        campaignEntity.addPhaseEntity(phaseEntity);
        request.getSession().setAttribute("campaign", campaignEntity);
        return new ModelAndView("newCampaign_2", map);
    }

    /**
     * ajout d'une phase pour une campagne
     * @param map les informations de la phase
     * @param campaignId l'identifiant de la campagne
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/addStep")
    public ModelAndView addStep(Map<String, Object> map, @RequestParam(name = "campaign") String campaignId) {
        map.put("campaign", campaignId);
        return new ModelAndView("newCampaign_2_addphase", map);
    }

    /**
     * Création d'un type et passage à l'étape suivante
     * @param request
     * @param map map contenant toutes les informations de création du type
     * @param campaignId l'identifiant de la campagne
     * @param typeCode le code du type
     * @param typeDesc la description du type
     * @param typeHour le nombre d'heure de la soutenance
     * @param typeMin le nombre de minute de soutenance
     * @param juryNumber le nombre de jury
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/addTypeNext")
    public ModelAndView addTypeNext(HttpServletRequest request, Map<String, Object> map, @RequestParam(name = "campaign") String campaignId, @RequestParam(name = "typeCode") String typeCode, @RequestParam(name = "typeDesc") String typeDesc, @RequestParam(name = "typeHour") Integer typeHour, @RequestParam(name = "typeMin") Integer typeMin, @RequestParam(name = "juryNumber") Integer juryNumber) {
        map.put("campaign", campaignId);
        map.put("typeHour", typeHour);
        map.put("typeMin", typeMin);
        map.put("typeCode", typeCode);
        map.put("typeDesc", typeDesc);
        map.put("juryNumber", juryNumber);

        if (typeHour == 0 && typeMin == 0) {
            request.getSession().setAttribute("msg", "ERREUR : Durée de la soutenance invalide.");
            return new ModelAndView("newCampaign_3_addtype1", map);
        }

        request.getSession().setAttribute("juryMember", juryNumber);
        return new ModelAndView("newCampaign_3_addtype2", map);
    }

    /**
     * permet de revenir en arrière lors de la crétion d'un type
      * @param map map contenant toutes les informations de création du type
     * @param campaignId l'identifiant de la campagne
     * @param typeCode le code du type
     * @param typeDesc la description du type
     * @param typeHour le nombre d'heure de la soutenance
     * @param typeMin le nombre de minute de soutenance
     * @param juryNumber le nombre de jury
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/backToAddType")
    public ModelAndView backToAddType(Map<String, Object> map, @RequestParam(name = "campaign") String campaignId, @RequestParam(name = "typeCode") String typeCode, @RequestParam(name = "typeDesc") String typeDesc, @RequestParam(name = "typeHour") Integer typeHour, @RequestParam(name = "typeMin") Integer typeMin, @RequestParam(name = "juryNumber") Integer juryNumber) {
        map.put("campaign", campaignId);
        map.put("typeHour", typeHour);
        map.put("typeMin", typeMin);
        map.put("typeCode", typeCode);
        map.put("typeDesc", typeDesc);
        map.put("juryNumber", juryNumber);
        return new ModelAndView("newCampaign_3_addtype1", map);
    }

    /**
     * Création d'un nouveau type de soutenance
     * @param request
     * @param map
     * @param campaignId l'identifiant de la campagne
     * @param typeCode le code du type de doutenance
     * @param typeDesc la description du type de soutenance
     * @param typeHour le nombre d'heure du type de soutenance
     * @param typeMin le nombre de minute du type de soutenance
     * @param roleName la liste des roles faissant partie du type de soutenance
     * @param isStudent si le jury est lié à l'étudiant
     * @param isStudy si le jury encadre d'autre étudiant de la même filière que l'étudiant
     * @param isPresident si le jury est président de la soutenance
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/createType")
    public ModelAndView createType(HttpServletRequest request, Map<String, Object> map, @RequestParam(name = "campaign") String campaignId, @RequestParam(name = "typeCode") String typeCode, @RequestParam(name = "typeDesc") String typeDesc, @RequestParam(name = "typeHour") Integer typeHour, @RequestParam(name = "typeMin") Integer typeMin, @RequestParam(name = "roleName") List<String> roleName, @RequestParam(name = "isStudent") List<String> isStudent, @RequestParam(name = "isStudy") List<String> isStudy, @RequestParam(name = "isPresident") List<String> isPresident) {
        map.put("campaign", campaignId);
        request.getSession().setAttribute("defenseTypes", defenseTypeService.getDefenseTypes());
        DefenseTypeEntity defenseTypeEntity = new DefenseTypeEntity(typeCode, typeDesc, typeHour + "h" + typeMin);
        RoleEntity roleEntity;
        Set<JuryMemberEntity> juryMemberEntities = new HashSet<>();
        String currentRolename;
        for (int i = 0; i < roleName.size(); i++) {
            currentRolename = roleName.get(i);
            roleEntity = roleService.findByNameLowercase(currentRolename);
            if (roleEntity == null) {
                roleEntity = new RoleEntity(currentRolename);
            }
            boolean isSt = isStudent.get(i).equals("true");
            boolean isSy = isStudy.get(i).equals("true");
            boolean isPr = isPresident.get(i).equals("true");
            juryMemberEntities.add(new JuryMemberEntity(isSt, isSy, isPr, roleEntity));
        }
        defenseTypeEntity.addJuryMembers(juryMemberEntities);
        if (defenseTypeService.isCodePresent(defenseTypeEntity.getCode())) {
            request.getSession().setAttribute("msgImport", "ERREUR : Le code utilisé existe déjà. Veuillez changer le code ou le sélectionner depuis la liste déroulante");
            return new ModelAndView("newCampaign_3", map);
        }
        CampaignEntity campaignEntity = (CampaignEntity)request.getSession().getAttribute("campaign");
        campaignEntity.addDefenseType(defenseTypeEntity);
        request.getSession().setAttribute("campaign", campaignEntity);
        request.getSession().setAttribute("dTypes", "yes");
        return new ModelAndView("newCampaign_3", map);
    }

    /**
     * supprime un type de soutenance d'une campagne
     * @param code le code du type de soutenance à supprimer
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/removeTempDType")
    public ModelAndView removeTempDType(@RequestParam(value = "code") String code, HttpServletRequest request) {
        CampaignEntity campaignEntity = (CampaignEntity) request.getSession().getAttribute("campaign");
        campaignEntity.removeDefenseTypeByCode(code);
        request.getSession().setAttribute("campaign", campaignEntity);
        request.getSession().setAttribute("defenseTypes", defenseTypeService.getDefenseTypes());
        return new ModelAndView("newCampaign_3", "campaign", campaignEntity);
    }

    /**
     * supprime une phase de récolte
     * @param code le code de la phase
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @PostMapping("/removeTempPhase")
    public ModelAndView removeTempPhase(@RequestParam(value = "code") String code, HttpServletRequest request) {
        CampaignEntity campaignEntity = (CampaignEntity) request.getSession().getAttribute("campaign");
        campaignEntity.removePhaseByCode(code);
        request.getSession().setAttribute("campaign", campaignEntity);
        return new ModelAndView("newCampaign_2", "campaign", campaignEntity);
    }

    /**
     * charge la page d'import des participants
     * @return
     */
    @GetMapping(value = "/import")
    public ModelAndView importAttendee() {
        return new ModelAndView("import");
    }

    /**
     * charge la page d'import des participants
     * @param id l'identifiant de la campgne
     * @return
     */
    @GetMapping("/{id}/import/attendee")
    public ModelAndView importAttendee(@PathVariable(name = "id") Integer id) {
        if (campaignService.getCampaign(id).isPresent()) {
            return new ModelAndView("import_attendee");
        }
        return new ModelAndView("campaign_home");
    }

    /**
     * charge la page d'import des disponibilités
     * @param id identifiant de la campagne
     * @return
     */
    @GetMapping("/{id}/import/availability")
    public ModelAndView importAvailabilities(@PathVariable(name = "id") Integer id) {
        if (campaignService.getCampaign(id).isPresent()) {
            return new ModelAndView("import_availability");
        }
        return new ModelAndView("campaign_home");
    }

    /**
     * charge la page d'import du planning
     * @param id identifiant de la campagne
     * @return
     */
    @GetMapping("/{id}/import/planning")
    public ModelAndView importPlanning(@PathVariable(name = "id") Integer id) {
        if (campaignService.getCampaign(id).isPresent()) {
            return new ModelAndView("import_planning");
        }
        return new ModelAndView("campaign_home");
    }

    /**
     * charge la page d'import des types de soutenance
     * @return
     */
    @GetMapping(value = "/import/defenseType")
    public ModelAndView formDefense() {
        return new ModelAndView("import_defense_type");
    }

    /**
     * charge la page de modification des diponibilités
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}/availability/change")
    public ModelAndView changeAvailabilities(@PathVariable String id) {
        return new ModelAndView("availabilities");
    }

    /**
     * Permet d'archiver une campagne
     * @param id l'identifiant de la campagne
     * @return
     */
    @GetMapping(value = "/{id}/archive")
    public ModelAndView archive(@PathVariable("id") Integer id) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            if (campaignEntity.getCampaignState().equals(campaignStateService.findByName(State.BROADCAST).getName())) {
                campaignEntity.setCampaignState(campaignStateService.findByName(State.ARCHIVE));
                Date begin = campaignEntity.getCampaignBegin();
                Date end = campaignEntity.getCampaignEnd();

                LocalDate dateBegin = begin.toLocalDate();
                dateBegin = dateBegin.minusDays(1);
                begin = Date.valueOf(dateBegin);

                LocalDate dateEnd = end.toLocalDate();
                dateEnd = dateEnd.plusDays(1);
                end = Date.valueOf(dateEnd);

                Date curDate;
                Set<AvailabilityEntity> availabilityEntities;
                Set<AvailabilityEntity> newAva = new HashSet<>();
                /* STUDENT */
                for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                    availabilityEntities = studentEntity.getAvailabilities();
                    for (AvailabilityEntity availabilityEntity : availabilityEntities) {
                        curDate = availabilityEntity.getDate();
                        if (!curDate.after(begin) || !curDate.before(end)) {
                            newAva.add(availabilityEntity);
                        }
                    }
                    studentEntity.setAvailabilities(newAva);
                    studentService.save(studentEntity);
                }
                /* SUPERVISOR */
                for (SupervisorEntity supervisorEntity : campaignEntity.getSupervisors()) {
                    availabilityEntities = supervisorEntity.getAvailabilities();
                    for (AvailabilityEntity availabilityEntity : availabilityEntities) {
                        curDate = availabilityEntity.getDate();
                        if (!curDate.after(begin) || !curDate.before(end)) {
                            newAva.add(availabilityEntity);
                        }
                    }
                    supervisorEntity.setAvailabilities(newAva);
                    supervisorService.save(supervisorEntity);
                }
                /* ATTENDEEGUEST */
                for (AttendeeGuestEntity attendeeGuestEntity : campaignEntity.getGuests()) {
                    availabilityEntities = attendeeGuestEntity.getAvailabilities();
                    for (AvailabilityEntity availabilityEntity : availabilityEntities) {
                        curDate = availabilityEntity.getDate();
                        if (!curDate.after(begin) || !curDate.before(end)) {
                            newAva.add(availabilityEntity);
                        }
                    }
                    attendeeGuestEntity.setAvailabilities(newAva);
                    attendeeGuestService.save(attendeeGuestEntity);
                }
                campaignService.save(campaignEntity);
            }
        }
        return new ModelAndView("redirect:/campaign");
    }

    /**
     * Charge la page de diffusion
     * @return
     */
    @GetMapping(value = "/diffusion")
    public ModelAndView diffusion() {
        return new ModelAndView("diffusionMail");
    }

    /**
     * Affiche tous les participants d'une campagne
     * @param id l'identifiant d'une campagne
     * @param request
     * @return
     */
    @GetMapping("/{id}/attendees")
    public ModelAndView displayAttendees(@PathVariable(value = "id") Integer id, HttpServletRequest request) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        if (optional.isPresent()) {
            request.getSession().setAttribute("campaign", optional.get());
        } else {
            return new ModelAndView("redirect:/campaign");
        }
        CampaignEntity campaignEntity = optional.get();

        List<StudentEntity> studentsList = new ArrayList<>(campaignEntity.getStudents());
        List<AttendeeGuestEntity> guestsList = new ArrayList<>(campaignEntity.getGuests());

        studentsList.sort(Comparator.comparing(StudentEntity::getEmail));
        guestsList.sort(Comparator.comparing(AttendeeGuestEntity::getEmail));

        StringBuilder stringBuilder = new StringBuilder();

        studentsList.forEach(student -> {
            stringBuilder.append("<tr><td><input type='checkbox' name='account' value='").append(student.getId()).append("'></td>");
            stringBuilder.append("<td>").append(student.getFirstname()).append("</td>");
            stringBuilder.append("<td>").append(student.getLastname()).append("</td>");
            stringBuilder.append("<td>").append(student.getEmail()).append("</td>");
            stringBuilder.append("<td>Etudiant</td></tr>");
        });

        guestsList.forEach(attendeeGuest -> {
            stringBuilder.append("<tr><td><input type='checkbox' name='account' value='").append(attendeeGuest.getId()).append("'></td>");
            stringBuilder.append("<td>").append(attendeeGuest.getFirstname()).append("</td>");
            stringBuilder.append("<td>").append(attendeeGuest.getLastname()).append("</td>");
            stringBuilder.append("<td>").append(attendeeGuest.getEmail()).append("</td>");
            Set<RoleEntity> roles = accountService.findByEmail(attendeeGuest.getEmail()).getRoleEntities();
            stringBuilder.append("<td>");
            roles.forEach(role -> stringBuilder.append(role.getName()).append(" "));
            stringBuilder.append("</td></tr>");
        });

        request.getSession().setAttribute("accounts", stringBuilder.toString());

        return new ModelAndView("org_participant_accounts");
    }

    /**
     * supprimer un participant d'une campagne
     * @param id id de la campagne
     * @param accounts liste des comptes à supprimer
     * @param request
     * @return
     */
    @PostMapping(value = "/{id}/attendees/delete")
    public ModelAndView deleteParticipant(@PathVariable(value = "id") Integer id, @RequestParam(name = "account", required = false) int[] accounts, HttpServletRequest request) {
        AccountEntity myAccount = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilder2 = new StringBuilder();
        if (accounts == null) {
            request.getSession().setAttribute("msg", Utils.message("alert-danger", "Veuillez selectionner au moins un compte."));
            return new ModelAndView("redirect:/campaign/" + id + "/attendees");
        }

        for (Integer i : accounts) {
            AttendeeEntity account = attendeeService.getAttendee(i);
            if (account == null) {
                stringBuilder2.append(accounts[i]).append("\n");
            } else if (myAccount.getEmail().equals(account.getEmail())) {
                stringBuilder.append("Vous ne pouvez pas supprimer votre compte \n");
            } else {
                Optional<CampaignEntity> campaignEntity = campaignService.getCampaign(id);
                if (campaignEntity.isPresent()) {
                    campaignEntity.get().getStudents().remove(account);
                    campaignEntity.get().getGuests().remove(account);
                    defenseService.removeById(attendeeService.findByEmail(account.getEmail()).getId());
                    campaignService.save(campaignEntity.get());
                }
            }
        }

        if (stringBuilder2.toString().length() > 0) {
            stringBuilder2.insert(0, "Ces comptes n'existent pas ou n'existent plus : \n");
            if (stringBuilder.length() > 0) {
                stringBuilder.insert(0, "Attention ! \n");
                stringBuilder2.insert(0, stringBuilder.toString());
                request.getSession().setAttribute("msg", Utils.message("alert-danger", stringBuilder.toString()));
                return new ModelAndView("redirect:/campaign/" + id + "/attendees");
            }
        }
        request.getSession().setAttribute("msg", Utils.message("alert-success", "Les comptes ont bien été supprimés."));
        return new ModelAndView("redirect:/campaign/" + id + "/attendees");
    }
}
