package fr.planninggo.spring.campaign.controller;

import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import fr.planninggo.spring.campaign.service.ConstraintService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Cette classe traite toutes les requêtes en lien avec les contraintes
 */

@Controller
@RequestMapping("/constraint")
public class ConstraintController {
    private final ConstraintService constraintService;

    public ConstraintController(ConstraintService constraintService) {
        this.constraintService = constraintService;
    }

    /**
     * Permet la récupération de toutes les contraintes
     */

    @GetMapping(value = "/getConstraints")
    public List<ConstraintEntity> getConstraints() {
        return constraintService.getConstraints();
    }
}
