package fr.planninggo.spring.campaign.controller;

import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import fr.planninggo.spring.campaign.service.TimeslotService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Cette classe traite toutes les requêtes en lien avec les timeslots
 */

@Controller
@RequestMapping("/timeslot")
public class TimeslotController {
    private final TimeslotService timeslotService;

    public TimeslotController(TimeslotService timeslotService) {
        this.timeslotService = timeslotService;
    }

    /**
     * Permet la récupération de tous les timeslots
     */
    @GetMapping(value = "/getTimeslots")
    public List<TimeslotEntity> getTimeslots() {
        return timeslotService.getTimeslots();
    }
}
