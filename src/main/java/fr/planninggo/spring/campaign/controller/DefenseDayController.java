package fr.planninggo.spring.campaign.controller;

import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import fr.planninggo.spring.campaign.service.DefenseDayService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Cette classe traite toutes les requêtes en lien avec les jours de soutenances
 */

@Controller
@RequestMapping("/defenseDay")
public class DefenseDayController {
    private final DefenseDayService defenseDayService;

    public DefenseDayController(DefenseDayService defenseDayService) {
        this.defenseDayService = defenseDayService;
    }

    /**
     * Permet la récupération de tous les jours de soutenances
     */
    @GetMapping(value = "/getDefenseDays")
    public List<DefenseDayEntity> getDefenseDays() {
        return defenseDayService.getDefenseDays();
    }
}
