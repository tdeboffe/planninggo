package fr.planninggo.spring.campaign.controller;

import fr.planninggo.csv.CSVWriter;
import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.service.AttendeeGuestService;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.attendee.service.StudentService;
import fr.planninggo.spring.attendee.service.SupervisorService;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.repository.CampaignRepository;
import fr.planninggo.spring.campaign.repository.DefenseRepository;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.spring.campaign.service.DefenseService;
import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;
import fr.planninggo.spring.user.service.AccountService;
import fr.planninggo.spring.user.service.RoleService;
import fr.planninggo.utility.SymetricEncryption;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Cette classe traite toutes les requêtes en lien avec les soutenances
 */

@Controller
@RequestMapping("/defense")
public class DefenseController {
    private final DefenseService defenseService;
    private final AttendeeService attendeeService;
    private final StudentService studentService;
    private final AttendeeGuestService attendeeGuestService;
    private final SupervisorService supervisorService;
    private final CampaignRepository campaignRepository;
    private final DefenseRepository defenseRepository;
    private final CampaignService campaignService;
    private final AccountService accountService;
    private final RoleService roleService;

    @Inject
    public DefenseController(RoleService roleService, CampaignService campaignService, AccountService accountService, DefenseRepository defenseRepository, SupervisorService supervisorService, DefenseService defenseService, AttendeeService attendeeService, StudentService studentService, AttendeeGuestService attendeeGuestService, CampaignRepository campaignRepository) {
        this.defenseService = defenseService;
        this.accountService = accountService;
        this.attendeeService = attendeeService;
        this.studentService = studentService;
        this.attendeeGuestService = attendeeGuestService;
        this.supervisorService = supervisorService;
        this.campaignRepository = campaignRepository;
        this.defenseRepository = defenseRepository;
        this.campaignService = campaignService;
        this.roleService = roleService;
    }

    /**
     * Permet la récupération de toutes les soutenances
     */
    @GetMapping(value = "/getDefenses")
    public List<DefenseEntity> getDefenses() {
        return defenseService.getDefenses();
    }

    /**
     * permet de récupérer toutes les soutenances d'un participant
     * @param account le compte du participant
     * @param request
     * @return
     */
    private ModelAndView defensesByPerson(AccountEntity account, HttpServletRequest request) {
        StudentEntity studentEntity = studentService.getStudent(account.getEmail());

        StringBuilder sb = new StringBuilder();
        AccountEntity myAccount = accountService.findByEmail(account.getEmail());
        Set<RoleEntity> roles = myAccount.getRoleEntities();
        RoleEntity etudiant = roleService.findByName("Etudiant");

        if (!roles.contains(etudiant)) {
            sb.append("<li class='nav-item'><a  class=\"nav-link\"  href=\"/app/supervisor\">Ma liste d'étudiants</a></li>");
        }

        request.getSession().setAttribute("buttons", sb.toString());

        if (studentEntity != null) {
            request.getSession().setAttribute("defenses", studentEntity.getDefenses());
            return new ModelAndView("own_planning");
        }
        AttendeeGuestEntity attendeeGuestEntity = attendeeGuestService.getAttendeeGuest(account.getEmail());
        if (attendeeGuestEntity != null) {
            request.getSession().setAttribute("defenses", attendeeGuestEntity.getDefenses());
            return new ModelAndView("own_planning");
        }
        SupervisorEntity supervisorEntity = supervisorService.getSupervisor(account.getEmail());
        if (supervisorEntity != null) {
            Set<DefenseEntity> defenseEntities = new HashSet<>();
            supervisorEntity.getStudents().stream().map(StudentEntity::getDefenses).forEach(defenseEntities::addAll);
            request.getSession().setAttribute("defenses", defenseEntities);
            return new ModelAndView("own_planning");
        }
        return new ModelAndView("own_planning");
    }

    /**
     * permet de récupérer toutes les soutenances du participant connecté
     * @param request
     * @return
     */
    @GetMapping(value = "/schedule")
    public ModelAndView getDefensesByPerson(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return defensesByPerson(account, request);
    }

    /**
     * permet d'ouvrir une page à partir de l'url de diffusion
     * @param request
     * @param hash le hash de l'url
     * @return
     * @throws IllegalBlockSizeException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    @RequestMapping(value = "/own/{hash}")
    public ModelAndView ownDefenseWithHash(HttpServletRequest request, @PathVariable(value = "hash") String hash) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        String decryptedUrl = SymetricEncryption.decrypt(hash);
        System.out.println(decryptedUrl);
        String[] values = decryptedUrl.split(";");
        if (values.length != 2) {
            throw new IllegalArgumentException("L'url est mal formatée");
        }
        AttendeeEntity attendeeEntity = attendeeService.findByEmail(values[0]);
        if (attendeeEntity == null) {
            Optional<CampaignEntity> campaignEntity = campaignRepository.findById(Integer.parseInt(values[1]));

            request.getSession().setAttribute("campaign", campaignEntity.orElseThrow(() -> new IllegalArgumentException("La campagne n'existe pas en base")));
            request.getSession().setAttribute("authenticated", !SecurityContextHolder.getContext().getAuthentication().getPrincipal().equals("anonymousUser"));
            return new ModelAndView("scheduler");
        }


        return defensesByPerson(attendeeEntity.getAccount(), request);
    }

    /**
     * Exporte le planning complet d'une campagne
     * @param id l'identifiant de la campagne
     * @param response
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @GetMapping(value = "{id}/exportPlanning", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public InputStreamSource exportPlanning(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
        String fileName = "exportPlanning.csv";
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

        String header = "NOM CAMPAGNE;DATE SOUTENANCE;HEURE;SALLE;TYPE SOUTENANCE";

        List<DefenseEntity> defensesEntities = defenseRepository.findAllWithCampaignId(id);
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);

        List<String> listForCSV = new ArrayList<>();
        List<String> listLines = new ArrayList<>();
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            String nomCampagne = campaignEntity.getName();
            for (DefenseEntity defenseEntity : defensesEntities) {
                String line = nomCampagne;
                String date = defenseEntity.getFormattedDefenseDate();
                String[] tabDate = date.split(" ");
                String addToheader = ";ETUDIANT";
                if (!header.contains(addToheader)) {
                    header = header + addToheader;
                }
                line = line+";"+tabDate[0]+";"+tabDate[1]+";"+defenseEntity.getRoom().getName()+";"+defenseEntity.getDefenseType().getCode()+";"+defenseEntity.getStudent().getEmail() + "," + defenseEntity.getStudent().getLastname() + "," + defenseEntity.getStudent().getFirstname();
                Set<SupervisorStudentEntity> supervisorEntities = defenseEntity.getStudent().getSupervisors();
                int i = 1;
                Map<String, Set<SupervisorEntity>> supervisorEntityMap = new HashMap<>();
                for (SupervisorStudentEntity sup : supervisorEntities) {
                    Set<SupervisorEntity> supervisorEntities1 = supervisorEntityMap.computeIfAbsent(sup.getRoleEntity().getName(), __ ->  new HashSet<>());
                    supervisorEntities1.add(sup.getSupervisor());
                }

                for (Map.Entry<String, Set<SupervisorEntity>> entry : supervisorEntityMap.entrySet()) {
                    header = header + ";" + entry.getKey();
                    Set<SupervisorEntity> supervisorEntities2 = new TreeSet<>(entry.getValue());
                    for (SupervisorEntity supervisorEntity : supervisorEntities2) {
                        line = line + ";" + supervisorEntity.getEmail() + "," + supervisorEntity.getLastname() + "," + supervisorEntity.getFirstname();
                    }
                }

                Set<AttendeeGuestEntity> attendeeGuestEntities = defenseEntity.getAttendeeGuests();
                for (AttendeeGuestEntity attendeeGuestEntity : attendeeGuestEntities) {
                    addToheader = ";INVITE " + i;
                    if (!header.contains(addToheader)) {
                        header = header + addToheader;
                    }
                    line = line + ";" + attendeeGuestEntity.getEmail() + "," + attendeeGuestEntity.getLastname() + "," + attendeeGuestEntity.getFirstname();
                    i++;
                }
                listLines.add(line);
            }
        }
        listForCSV.add(header);
        listForCSV = Stream.concat(listForCSV.stream(), listLines.stream()).collect(Collectors.toList());
        Path path = Paths.get(fileName);
        CSVWriter cw = new CSVWriter(path, listForCSV);
        return new InputStreamResource(cw.getData());
    }
}

