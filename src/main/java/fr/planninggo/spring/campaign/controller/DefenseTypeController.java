package fr.planninggo.spring.campaign.controller;

import fr.planninggo.csv.CSVWriter;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.spring.campaign.service.DefenseTypeService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Cette classe traite toutes les requêtes en lien avec les types de soutenances
 */

@Controller
@RequestMapping("/defenseType")
public class DefenseTypeController {
    private final DefenseTypeService defenseTypeService;
    private final CampaignService campaignService;

    public DefenseTypeController (CampaignService campaignService, DefenseTypeService defenseTypeService) {
        this.defenseTypeService = defenseTypeService;
        this.campaignService = campaignService;
    }

    /**
     * Export des configurations de soutenance d'une campagne au format csv
     * @param id l'identifiant de la campagne
     * @param response
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @GetMapping(value = "{id}/exportConfigSoutenances", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public InputStreamSource exportConfigSoutenances(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
        String fileName = "ConfigSoutenances.csv";
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        if(!Pattern.matches("[a-zA-Z_0-9]*.csv",fileName)){
            throw new IllegalArgumentException("Wrong extension for fileName !");
        }
        String header= "CODE EXERCICE;DESCRIPTION EXERCICE;DUREE";
        Optional<CampaignEntity> optional = campaignService.getCampaign(id);
        List<String> listForCSV = new ArrayList<>();
        List<String> listLines = new ArrayList<>();

        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            Set<DefenseTypeEntity> list = campaignEntity.getDefensesType();

            for (DefenseTypeEntity entity : list){
                String line = entity.getCode()+";"+entity.getDesc()+";"+entity.getDuration();

                Set<JuryMemberEntity> set = entity.getJuryMembers();
                int i = 1;
                for(JuryMemberEntity ent : set){
                    String pres = "non";
                    String addToHeader = ";ROLE JURY "+i+";LIEN ETUDIANT JURY "+i+";PRESIDENT JURY "+i;
                    if(!header.contains(addToHeader)){
                        header = header + addToHeader;
                    }
                    if(ent.isPresident()){
                        pres = "oui";
                    }
                    if(ent.isStudentLink()){
                        line = line + ";" + ent.getRolename()+";oui;"+pres;
                    }
                    else if(ent.isStudyLink()){
                        line = line + ";" + ent.getRolename()+";filiere;"+pres;
                    }
                    else{
                        line = line + ";" + ent.getRolename()+";non;"+pres;
                    }
                    i++;
                }
                System.out.println(line);
                listLines.add(line);
            }
        }
        listForCSV.add(header);
        listForCSV = Stream.concat(listForCSV.stream(), listLines.stream())
                .collect(Collectors.toList());
        Path path = Paths.get(fileName);
        CSVWriter cw = new CSVWriter(path, listForCSV);
        return new InputStreamResource(cw.getData());
    }
}
