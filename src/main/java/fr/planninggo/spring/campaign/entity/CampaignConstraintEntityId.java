package fr.planninggo.spring.campaign.entity;

import lombok.Generated;

import java.io.Serializable;
/**
 * Cette classe sert de clé primaire de la table constraint entity
 */
public class CampaignConstraintEntityId implements Serializable {
    private int campaign;
    private int constraint;

    @Override
    @Generated
    public int hashCode() {
        return campaign ^ constraint ^ super.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof CampaignConstraintEntityId)) {
            return false;
        }
        CampaignConstraintEntityId c = (CampaignConstraintEntityId) obj;
        return campaign == c.campaign && constraint == c.constraint;
    }
}
