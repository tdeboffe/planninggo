package fr.planninggo.spring.campaign.entity;

import fr.planninggo.spring.user.entity.RoleEntity;
import org.jetbrains.annotations.NotNull;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "jury_member"
 */
@Entity
@Table(name = "jury_member")
public class JuryMemberEntity implements Serializable, Comparable<JuryMemberEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "jury_member_id")
    private int id;

    @Column(name = "student_link")
    private boolean isStudentLink;

    @Column(name = "study_link")
    private boolean isStudyLink;

    @Column(name = "president")
    private boolean isPresident;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private RoleEntity role;

    public JuryMemberEntity() {}

    public JuryMemberEntity(boolean isStudentLink, boolean isStudyLink, boolean isPresident, RoleEntity role) {
        this.isStudentLink = isStudentLink;
        this.isStudyLink = isStudyLink;
        this.isPresident = isPresident;
        this.role = Objects.requireNonNull(role);
    }

    public String getRolename() {
        return role.getName();
    }

    public void setRole(RoleEntity role) { this.role = role; }

    public boolean isPresident() {
        return isPresident;
    }

    public boolean isStudentLink() {
        return isStudentLink;
    }

    public boolean isStudyLink() {
        return isStudyLink;
    }


    public RoleEntity getRole() {
        return role;
    }

    @Override
    public int compareTo(@NotNull JuryMemberEntity o) {
        return getRolename().compareTo(o.getRolename());
    }
}
