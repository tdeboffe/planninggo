package fr.planninggo.spring.campaign.entity;

import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * Cette classe permet de récupérer les informations de la table "defense"
 */
@Entity
@Table(name = "defense")
public class DefenseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "defense_id")
    private int id;

    @Column(name = "defense_date")
    private Timestamp defenseDate;

    @Column(name = "is_hold")
    private boolean isHold;

    @Column(name = "share")
    private Boolean share = false;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "attendee_id")
    private StudentEntity student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_code")
    private DefenseTypeEntity defenseType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "campaign_id")
    private CampaignEntity campaign;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private RoomEntity room;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "defense_attendee_guest", joinColumns = {@JoinColumn(name = "defense_id")}, inverseJoinColumns = {@JoinColumn(name = "attendee_id")})
    private Set<AttendeeGuestEntity> attendeeGuests;

    public DefenseEntity() {
    }

    public DefenseEntity(Timestamp defenseDate, StudentEntity student, boolean isHold, DefenseTypeEntity defenseType, CampaignEntity campaign, RoomEntity room, Set<AttendeeGuestEntity> attendeeGuests) {
        this.defenseDate = defenseDate;
        this.student = Objects.requireNonNull(student);
        this.defenseType = Objects.requireNonNull(defenseType);
        this.campaign = Objects.requireNonNull(campaign);
        this.room = Objects.requireNonNull(room);
        this.attendeeGuests = Objects.requireNonNull(attendeeGuests);
        this.isHold = isHold;
    }

    public DefenseEntity(Timestamp defenseDate, StudentEntity student, DefenseTypeEntity defenseType, CampaignEntity campaign, RoomEntity room, Set<AttendeeGuestEntity> attendeeGuests) {
        this.defenseDate = defenseDate;
        this.student = Objects.requireNonNull(student);
        this.defenseType = Objects.requireNonNull(defenseType);
        this.campaign = Objects.requireNonNull(campaign);
        this.room = Objects.requireNonNull(room);
        this.attendeeGuests = Objects.requireNonNull(attendeeGuests);
        this.isHold = true;
    }

    public void setDefenseDate(Timestamp defenseDate) {
        this.defenseDate = defenseDate;
    }

    public Timestamp getDefenseDate() {
        return defenseDate;
    }

    public String getFormattedDefenseDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date date = new Date();
        date.setTime(this.defenseDate.getTime());
        return simpleDateFormat.format(date);
    }

    public int getId() {
        return id;
    }

    public boolean isHold() {
        return isHold;
    }

    public void setHold(boolean isHold) {
        this.isHold = isHold;
    }

    public StudentEntity getStudent() {
        return student;
    }

    public DefenseTypeEntity getDefenseType() {
        return defenseType;
    }

    public CampaignEntity getCampaign() {
        return campaign;
    }

    public RoomEntity getRoom() {
        return room;
    }

    public Set<AttendeeGuestEntity> getAttendeeGuests() {
        return attendeeGuests;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public boolean isShare() {
        return share;
    }

    @Override
    public String toString() {
        return "DefenseEntity{" +
                "id=" + id +
                ", defenseDate=" + defenseDate +
                ", isHold=" + isHold +
                ", student=" + student +
                ", defenseType=" + defenseType +
                ", campaign=" + campaign +
                ", room=" + room +
                ", attendeeGuests=" + attendeeGuests +
                '}';
    }
}
