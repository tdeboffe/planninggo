package fr.planninggo.spring.campaign.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "campaign_state"
 */
@Entity
@Table(name = "campaign_state")
public class CampaignStateEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "state_id")
    private int id;

    @Column(name = "state_name")
    private String name;

    public CampaignStateEntity() {
    }

    public CampaignStateEntity(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public CampaignStateEntity(int id, String name) {
        this(name);
        this.id = id;
    }

    public static CampaignStateEntity create(CSVRecord csvRecord) {
        return new CampaignStateEntity(csvRecord.get(1));
    }

    public String getName() {
        return name;
    }

    @Override
    @Generated
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof CampaignStateEntity)) {
            return false;
        }
        return this.name.equals(((CampaignStateEntity) obj).name);
    }

    @Override
    public String toString() {
        return "CampaignStateEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
