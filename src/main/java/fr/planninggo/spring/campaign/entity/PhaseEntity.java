package fr.planninggo.spring.campaign.entity;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "phase"
 */
@Entity
@Table(name = "phase")
public class PhaseEntity implements Serializable, Comparable<PhaseEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "phase_id")
    private int id;

    @Column(name = "phase_begin")
    private Date begin;

    @Column(name = "phase_end")
    private Date end;

    @Column(name = "written_by")
    private String writtenBy;

    @Column(name = "written_for")
    private String writtenFor;

    @Transient
    private String code;

    @Column(name = "availability_number")
    private int availabilityNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "campaign_id")
    private CampaignEntity campaignEntity;

    public PhaseEntity() {
    }

    public PhaseEntity(Date begin, Date end, CampaignEntity campaignEntity, int availabilityNumber, String writtenFor, String writtenBy) {
        this.begin = Objects.requireNonNull(begin);
        this.end = Objects.requireNonNull(end);
        this.campaignEntity = Objects.requireNonNull(campaignEntity);
        this.availabilityNumber = availabilityNumber;
        this.writtenBy = writtenBy;
        this.writtenFor = writtenFor;
    }

    public PhaseEntity(int code, Date begin, Date end, CampaignEntity campaignEntity, String writtenBy, String writtenFor, int availabilityNumber) {
        this.code = String.valueOf(code);
        this.begin = Objects.requireNonNull(begin);
        this.end = Objects.requireNonNull(end);
        this.writtenBy = Objects.requireNonNull(writtenBy);
        this.writtenFor = Objects.requireNonNull(writtenFor);
        this.campaignEntity = Objects.requireNonNull(campaignEntity);
        this.availabilityNumber = availabilityNumber;
    }

    public CampaignEntity getCampaign() {
        return campaignEntity;
    }

    public void setCampaign(CampaignEntity campaignEntity) {
        this.campaignEntity = campaignEntity;
    }

    public Date getBegin() {
        return begin;
    }

    public int getAvailabilityNumber() {
        return availabilityNumber;
    }

    public Date getEnd() {
        return end;
    }

    public String getWrittenBy() {
        return writtenBy;
    }

    public String getWrittenFor() {
        return writtenFor;
    }

    public String getCode() {
        if (code == null) {
            return "";
        }
        return code;
    }

    @Override
    public String toString() {
        return "PhaseEntity{" +
                "id=" + id +
                ", begin=" + begin +
                ", end=" + end +
                ", availabilityNumber=" + availabilityNumber +
                ", writtenBy='" + writtenBy + '\'' +
                ", writtenFor='" + writtenFor + '\'' +
                '}';
    }

    public boolean isStart() {
        return !begin.toLocalDate().isAfter(LocalDate.now()) && !end.toLocalDate().isBefore(LocalDate.now());
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public int hashCode() {
        return begin.hashCode() ^ end.hashCode() ^ campaignEntity.hashCode() ^ writtenFor.hashCode() ^ writtenBy.hashCode() ^ availabilityNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PhaseEntity)) {
            return false;
        }
        PhaseEntity phaseEntity = (PhaseEntity) obj;
        return phaseEntity.begin.equals(begin)
                && phaseEntity.end.equals(end)
                && phaseEntity.campaignEntity.equals(campaignEntity)
                && phaseEntity.writtenBy.equals(writtenBy)
                && phaseEntity.writtenFor.equals(writtenFor)
                && phaseEntity.availabilityNumber == availabilityNumber;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(@NotNull PhaseEntity o) {
        return begin.compareTo(o.begin);
    }
}
