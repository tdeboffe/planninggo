package fr.planninggo.spring.campaign.entity;

import lombok.Generated;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "timeslot"
 */
@Entity
@Table(name = "timeslot")
public class TimeslotEntity implements Comparable<TimeslotEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timeslot_id")
    private int id;

    @Column(name = "timeslot_begin")
    private Time begin;

    @Column(name = "timeslot_end")
    private Time end;

    public TimeslotEntity() {
    }

    public TimeslotEntity(Time begin, Time end) {
        this.begin = Objects.requireNonNull(begin);
        this.end = Objects.requireNonNull(end);
    }

    public Time getBegin() {
        return begin;
    }

    public Time getEnd() {
        return end;
    }

    @Override
    @Generated
    public int hashCode() {
        return begin.hashCode() ^ end.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof TimeslotEntity)) {
            return false;
        }
        TimeslotEntity t = (TimeslotEntity) obj;
        return begin.equals(t.begin) && end.equals(t.end);
    }

    @Override
    public String toString() {
        return "TimeslotEntity{" +
                "id=" + id +
                ", begin=" + begin +
                ", end=" + end +
                '}';
    }

    @Override
    public int compareTo(@NotNull TimeslotEntity o) {
        if (o.id == 0 || id == 0) { return 1; }
        return Integer.compare(id, o.id);
    }
}
