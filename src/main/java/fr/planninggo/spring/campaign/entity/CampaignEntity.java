package fr.planninggo.spring.campaign.entity;

import fr.planninggo.scheduler.ConstraintWeight;
import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.user.entity.AccountEntity;
import lombok.Generated;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Cette classe permet de récupérer les informations de la table "campaign"
 */
@Entity
@Table(name = "campaign")
public class CampaignEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "campaign_id")
    private int id;

    @Column(name = "campaign_name")
    private String name;

    @Column(name = "campaign_begin")
    private Date campaignBegin;

    @Column(name = "campaign_end")
    private Date campaignEnd;

    @ManyToOne
    @JoinColumn(name = "state_id")
    private CampaignStateEntity campaignState;

    @Column(name = "room_number")
    private int roomNumber;

    @ManyToMany
    @JoinTable(name = "campaign_defense_type", joinColumns = {@JoinColumn(name = "campaign_id")}, inverseJoinColumns = {@JoinColumn(name = "type_code")})
    private Set<DefenseTypeEntity> defensesType = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "student_campaign", joinColumns = {@JoinColumn(name = "campaign_id")}, inverseJoinColumns = {@JoinColumn(name = "attendee_id")})
    private Set<StudentEntity> students = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "attendee_guests_campaign", joinColumns = {@JoinColumn(name = "campaign_id")}, inverseJoinColumns = {@JoinColumn(name = "attendee_id")})
    private Set<AttendeeGuestEntity> guests = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "account_campaign", joinColumns = {@JoinColumn(name = "campaign_id")}, inverseJoinColumns = {@JoinColumn(name = "user_id")})
    private Set<AccountEntity> organizers;

    @OneToMany(mappedBy = "campaign", cascade = CascadeType.ALL)
    private Set<CampaignConstraintEntity> constraintEntities = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "campaignEntity", cascade = CascadeType.ALL)
    private Set<PhaseEntity> phases = new HashSet<>();

    @OneToMany(mappedBy = "campaign")
    private Set<DefenseDayEntity> defenseDays;

    @OneToMany(mappedBy = "campaign")
    private Set<DefenseEntity> defenses = new HashSet<>();


    public CampaignEntity() {
    }

    public CampaignEntity(CampaignEntity campaignEntity, CampaignStateEntity campaignStateEntity) {
        this.name = campaignEntity.name;
        this.campaignState = campaignStateEntity;
        this.roomNumber = campaignEntity.roomNumber;
        this.organizers = campaignEntity.organizers;
        this.defensesType = campaignEntity.defensesType;
    }

    public CampaignEntity(String name, Date campaignBegin, Date campaignEnd, Set<AccountEntity> organizers, CampaignStateEntity campaignState, int roomNumber) {
        this.name = Objects.requireNonNull(name);
        this.campaignBegin = Objects.requireNonNull(campaignBegin);
        this.campaignEnd = Objects.requireNonNull(campaignEnd);
        this.organizers = Objects.requireNonNull(organizers);
        this.roomNumber = roomNumber;
        this.campaignState = campaignState;
        this.defenseDays = new HashSet<>();
        this.guests = new HashSet<>();
        this.students = new HashSet<>();
        this.phases = new HashSet<>();
    }

    public CampaignEntity(String name, Date campaignBegin, Date campaignEnd, CampaignStateEntity campaignState) {
        this.name = Objects.requireNonNull(name);
        this.campaignBegin = Objects.requireNonNull(campaignBegin);
        this.campaignEnd = Objects.requireNonNull(campaignEnd);
        this.organizers = new HashSet<>();
        this.campaignState = campaignState;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public String getName() {
        return name;
    }

    public Date getCampaignBegin() {
        return campaignBegin;
    }

    public Date getCampaignEnd() {
        return campaignEnd;
    }

    public Set<AccountEntity> getOrganizers() {
        return organizers;
    }

    public String getCampaignState() {
        return campaignState.getName();
    }

    public void setStudents(Set<StudentEntity> students) {
        this.students = students;
    }

    public void setGuests(Set<AttendeeGuestEntity> guests) {
        this.guests = guests;
    }

    public void setPhases(Set<PhaseEntity> phases) {
        this.phases = phases;
    }

    public void setDefenseDays(Set<DefenseDayEntity> defenseDays) {
        this.defenseDays = defenseDays;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void setCampaignBegin(Date campaignBegin) {
        this.campaignBegin = campaignBegin;
    }

    public void setCampaignEnd(Date campaignEnd) {
        this.campaignEnd = campaignEnd;
    }

    public Set<AttendeeGuestEntity> getGuests() {
        return guests;
    }

    public Set<StudentEntity> getStudents() {
        return students;
    }

    public Set<SupervisorEntity> getSupervisors() {
        Set<SupervisorEntity> supervisorEntities = new HashSet<>();
        students.forEach(s -> s.getSupervisors().forEach(e -> supervisorEntities.add(e.getSupervisor())));
        return supervisorEntities;
    }

    public Set<DefenseDayEntity> getDefenseDays() {
        return defenseDays;
    }

    public int getId() {
        return id;
    }

    @Override
    @Generated
    public int hashCode() {
        return name.hashCode() ^ campaignBegin.hashCode() ^ campaignEnd.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof CampaignEntity)) {
            return false;
        }
        CampaignEntity c = (CampaignEntity) obj;
        return this.organizers.containsAll(c.organizers)
                && c.organizers.containsAll(this.organizers) && this.campaignEnd.equals(c.campaignEnd)
                && this.campaignBegin.equals(c.campaignBegin) && this.campaignState.equals(c.campaignState)
                && this.name.equals(c.name);
    }

    public void setCampaignState(CampaignStateEntity campaignState) {
        this.campaignState = campaignState;
    }

    public void addDefenseType(DefenseTypeEntity defenseTypeEntity) {
        this.defensesType.add(defenseTypeEntity);
    }

    public Set<DefenseTypeEntity> getDefensesType() {
        return defensesType;
    }

    public void removeDefenseTypeByCode(String code) {
        DefenseTypeEntity toRemove = null;
        for (DefenseTypeEntity d : defensesType) {
            if (d.getCode().equals(code)) {
                toRemove = d;
            }
        }
        if (toRemove != null) {
            defensesType.remove(toRemove);
        }
    }

    public void removePhaseByCode(String code) {
        PhaseEntity toRemove = null;
        for (PhaseEntity p : phases) {
            if (p.getCode().equals(code)) {
                toRemove = p;
            }
        }
        if (toRemove != null) {
            phases.remove(toRemove);
        }
    }

    public void addPhaseEntity(PhaseEntity phaseEntity) {
        this.phases.add(phaseEntity);
    }

    public Set<PhaseEntity> getPhases() {
        return phases;
    }

    @Override
    public String toString() {
        return "CampaignEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", campaignBegin=" + campaignBegin +
                ", campaignEnd=" + campaignEnd +
                ", campaignState=" + campaignState +
                ", phases=" + phases +
                ", defenseDays=" + defenseDays +
                 ", organizers= " + organizers +
                '}';
    }


    public List<AttendeeEntity> getAttendees() {
        List<AttendeeEntity> attendeeEntities = new ArrayList<>();
        attendeeEntities.addAll(guests);
        attendeeEntities.addAll(students);
        attendeeEntities.addAll(getSupervisors());
        return attendeeEntities;
    }

    public void setOrganizer(Set<AccountEntity> accountEntities) {
        organizers = accountEntities;
    }

    public Set<CampaignConstraintEntity> getConstraintEntities() {
        return constraintEntities;
    }

    public void addConstraint(ConstraintEntity constraintEntity) {
        if(constraintEntity.getId() == ConstraintWeight.MINIMIZE_OTHER_ATTENDEE_PER_HALF_DAY ||
            constraintEntity.getId() == ConstraintWeight.MINIMIZE_OVERLAPPED_DEFENSES){
            this.constraintEntities.add(new CampaignConstraintEntity(constraintEntity, this, 10, true, false, true, 0));
        }else if(constraintEntity.getId() == ConstraintWeight.MINIMIZE_DISPERSED_OTHER_ATTENDEE_DEFENSES_DAY ||
                 constraintEntity.getId() == ConstraintWeight.MIDDLE_DAY_DEFENSES){
            this.constraintEntities.add(new CampaignConstraintEntity(constraintEntity, this, 4, true, false, true, 0));
        }else {
            this.constraintEntities.add(new CampaignConstraintEntity(constraintEntity, this, 4, true, true, false, 0));
        }
    }

    public Set<DefenseEntity> getDefenses() {
        return defenses;
    }
}
