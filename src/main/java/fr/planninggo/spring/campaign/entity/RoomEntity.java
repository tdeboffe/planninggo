package fr.planninggo.spring.campaign.entity;

import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Cette classe permet de récupérer les informations de la table "room"
 */
@Entity
@Table(name = "room")
public class RoomEntity implements Comparable<RoomEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id")
    private int id;

    @Column(name = "room_name")
    private String name;

    public RoomEntity() {}

    public RoomEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Salle " + name;
    }

    @Override
    public int compareTo(@NotNull RoomEntity o) {
        return Integer.valueOf(name).compareTo(Integer.valueOf(o.getName()));
    }

    public void setId(int id) {
        this.id = id;
    }
}
