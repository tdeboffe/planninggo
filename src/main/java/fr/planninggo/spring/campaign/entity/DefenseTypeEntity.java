package fr.planninggo.spring.campaign.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Cette classe permet de récupérer les informations de la table "defense_type"
 */
@Entity
@Table(name = "defense_type")
public class DefenseTypeEntity implements Serializable {
    @Id
    @Column(name = "type_code")
    private String code;

    @Column(name = "type_desc")
    private String desc;

    @Column(name = "duration")
    private String duration;

    public static DefenseTypeEntity create(CSVRecord r) {
        return new DefenseTypeEntity(r.get(0), r.get(1), r.get(2).trim());
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getDuration() {
        return duration;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "jury_member_defense_type", joinColumns = {@JoinColumn(name = "type_code")}, inverseJoinColumns = {@JoinColumn(name = "jury_member_id")})
    private Set<JuryMemberEntity> juryMembers = new HashSet<>();

    public DefenseTypeEntity() {}

    public DefenseTypeEntity(String code, String desc, String duration) {
        this.code = code;
        this.desc = desc;
        this.duration = duration;
    }

    public Set<JuryMemberEntity> getJuryMembers() {
        return juryMembers;
    }

    public void setJuryMembers(Set<JuryMemberEntity> juryMembers) {
        this.juryMembers = juryMembers;
    }

    @Override
    @Generated
    public int hashCode() {
        return code.hashCode() ^ desc.hashCode() ^ duration.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object o) {
        if (!(o instanceof DefenseTypeEntity)) {
            return false;
        }
        DefenseTypeEntity that = (DefenseTypeEntity) o;
        return code.equals(that.code) && desc.equals(that.desc) && duration.equals(that.duration);
    }

    public void addJuryMembers(Set<JuryMemberEntity> juryMemberEntities) {
        this.juryMembers.addAll(juryMemberEntities);
    }

    @Override
    public String toString() {
        return "DefenseTypeEntity{" +
                "code='" + code + '\'' +
                ", desc='" + desc + '\'' +
                ", duration='" + duration + '\'' +
                ", juryMembers=" + juryMembers +
                '}';
    }

    public String getDurationToInt() {
        String[] durations = this.duration.split("h");
        return String.valueOf((Integer.valueOf(durations[0])*60)+Integer.valueOf(durations[1].replaceAll(" ", "")));
    }

}
