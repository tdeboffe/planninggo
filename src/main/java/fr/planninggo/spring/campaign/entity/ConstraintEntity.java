package fr.planninggo.spring.campaign.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.*;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "constraint_"
 */
@Entity
@Table(name = "constraint_")
public class ConstraintEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "constraint_id")
    private int id;

    @Column(name = "constraint_desc")
    private String desc;

    public ConstraintEntity() {
    }

    public ConstraintEntity(String desc) {
        this.desc = Objects.requireNonNull(desc);
    }

    public static ConstraintEntity create(CSVRecord csvRecord) {
        return new ConstraintEntity(csvRecord.get(1));
    }

    public String getDesc() {
        return desc;
    }

    public int getId() {
        return id;
    }

    @Override
    @Generated
    public int hashCode() {
        return desc.hashCode() ^ id;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof ConstraintEntity)) {
            return false;
        }
        ConstraintEntity c = (ConstraintEntity) obj;
        return c.desc.equals(this.desc);
    }

    @Override
    public String toString() {
        return "ConstraintEntity{" +
                "id=" + id +
                ", desc='" + desc + '\'' +
                '}';
    }
}
