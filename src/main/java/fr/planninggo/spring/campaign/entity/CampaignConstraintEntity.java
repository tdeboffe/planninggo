package fr.planninggo.spring.campaign.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "campaign_containt"
 */
@Entity
@Table(name = "campaign_constraint")
@IdClass(CampaignConstraintEntityId.class)
public class CampaignConstraintEntity implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "constraint_id")
    private ConstraintEntity constraint;

    @Id
    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private CampaignEntity campaign;

    private int weight;
    private boolean active;
    private boolean valide;
    private boolean optimal;
    private int score;

    public CampaignConstraintEntity() {}

    public CampaignConstraintEntity(ConstraintEntity constraintEntity, CampaignEntity campaign) {
        this.constraint = Objects.requireNonNull(constraintEntity);
        this.campaign = Objects.requireNonNull(campaign);
    }

    CampaignConstraintEntity(ConstraintEntity constraint, CampaignEntity campaign, int weight, boolean active, boolean valide, boolean optimal, int score) {
        this.constraint = constraint;
        this.campaign = campaign;
        this.weight = weight;
        this.active = active;
        this.valide = valide;
        this.optimal = optimal;
        this.score = score;
    }

    public CampaignEntity getCampaign() {
        return campaign;
    }

    public ConstraintEntity getConstraint() {
        return constraint;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setOptimal(boolean optimal) {
        this.optimal = optimal;
    }

    @Override
    public String toString() {
        return "CampaignConstraintEntity{" +
                "constraint=" + constraint +
                ", campaign=" + campaign +
                ", weight=" + weight +
                ", active=" + active +
                ", valide=" + valide +
                ", optimal=" + optimal +
                ", score=" + score +
                '}';
    }
}
