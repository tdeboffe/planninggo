package fr.planninggo.spring.campaign.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;

/**
 * Cette classe permet de récupérer les informations de la table "defense_day"
 */
@Entity
@Table(name = "defense_day")
public class DefenseDayEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "defense_day_id")
    private int id;

    @Column(name = "defense_day_date")
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "campaign_id")
    private CampaignEntity campaign;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "defense_day_timeslot", joinColumns = {@JoinColumn(name = "defense_day_id")}, inverseJoinColumns = {@JoinColumn(name = "timeslot_id")})
    private Set<TimeslotEntity> slots;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "room_defense_day", joinColumns = {@JoinColumn(name = "defense_day_id")}, inverseJoinColumns = {@JoinColumn(name = "room_id")})
    private Set<RoomEntity> rooms;

    public DefenseDayEntity() {}

    public DefenseDayEntity(Date date, CampaignEntity campaignEntity) {
        this.date = date;
        this.campaign = campaignEntity;
        this.rooms = new HashSet<>();
        this.slots = new HashSet<>();
    }

    public DefenseDayEntity(Date date, CampaignEntity campaign, Set<RoomEntity> roomEntity) {
        this.date = Objects.requireNonNull(date);
        this.campaign = Objects.requireNonNull(campaign);
        this.slots = new HashSet<>();
        this.rooms = Objects.requireNonNull(roomEntity);
    }

    public Date getDate() {
        return date;
    }

    public CampaignEntity getCampaign() {
        return campaign;
    }

    public Set<TimeslotEntity> getSlots() {
        return slots;
    }

    public Set<RoomEntity> getRooms() {
        return rooms;
    }

    public void setRooms(SortedSet<RoomEntity> rooms) {
        this.rooms = rooms;
    }

    public void setSlots(Set<TimeslotEntity> slots) {
        this.slots = slots;
    }

    @Override
    public String toString() {
        return "DefenseDayEntity{" +
                "date=" + date +
                ", slots=" + slots +
                ", rooms=" + rooms +
                '}';
    }
}
