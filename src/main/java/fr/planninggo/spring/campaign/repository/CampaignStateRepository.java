package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.CampaignStateEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table CampaignState
 */

public interface CampaignStateRepository extends CrudRepository<CampaignStateEntity, Integer> {
    CampaignStateEntity findByName(String name);
}
