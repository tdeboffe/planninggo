package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.PhaseEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Phase
 */

public interface PhaseRepository extends CrudRepository<PhaseEntity, Integer> {
}
