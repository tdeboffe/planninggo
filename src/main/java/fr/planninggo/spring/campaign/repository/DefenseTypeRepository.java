package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table DefenseType
 */

public interface DefenseTypeRepository extends CrudRepository<DefenseTypeEntity, Integer> {
    DefenseTypeEntity findByCode(String code);
}
