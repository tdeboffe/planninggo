package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.ConstraintEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Constraint_
 */

public interface ConstraintRepository extends CrudRepository<ConstraintEntity, Integer> {
}
