package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Cette interface permet de requêter la table Defense
 */
public interface DefenseRepository extends CrudRepository<DefenseEntity, Integer> {

    @Query(value = "SELECT * FROM defense WHERE campaign_id = ?1 AND defense_date is not NULL", nativeQuery = true)
    List<DefenseEntity> findAllWithCampaignId(@Param("id") int id);

    @Query(value = "SELECT * FROM defense WHERE campaign_id = ?1 AND defense_date is NULL", nativeQuery = true)
    List<DefenseEntity> findNullDefensesWithCampaignId(@Param("id") int id);

    DefenseEntity findByStudent(StudentEntity studentEntity);

    @Query(value = "select * from defense d inner join attendee a on d.attendee_id = a.attendee_id where a.firstname= ?1 and a.lastname= ?2 and d.campaign_id=?3", nativeQuery = true)
    Optional<DefenseEntity> findByFirstNameAndLastNameAndCampaign(String firstName, String lastName, Integer id);

    List<DefenseEntity> findByCampaignAndDefenseType(CampaignEntity campaignEntity, DefenseTypeEntity type_code);

    @Query(value = "DELETE FROM defense WHERE campaign_id = ?1", nativeQuery = true)
    @Modifying
    void deleteAllByCampaignId(@Param("id") int id);

    @Query(value = "DELETE FROM defense WHERE attendee_id = ?1", nativeQuery = true)
    @Modifying
    void deleteById(@Param("id") int id);

    List<DefenseEntity> findByCampaign(CampaignEntity campaignEntity);
}
