package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.TimeslotEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Timeslot
 */

public interface TimeslotRepository extends CrudRepository<TimeslotEntity, Integer> {
}
