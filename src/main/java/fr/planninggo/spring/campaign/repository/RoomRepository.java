package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.RoomEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Room
 */

public interface RoomRepository extends CrudRepository<RoomEntity, Integer> {
    RoomEntity findByName(String name);
    RoomEntity findById(int id);
}
