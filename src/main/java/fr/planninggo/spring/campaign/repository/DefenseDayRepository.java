package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.DefenseDayEntity;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;

/**
 * Cette interface permet de requêter la table DefenseDay
 */

public interface DefenseDayRepository extends CrudRepository<DefenseDayEntity, Integer> {
    DefenseDayEntity findDefenseDayEntityByDate(Date date);
}
