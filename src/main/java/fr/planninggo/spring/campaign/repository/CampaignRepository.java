package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.user.entity.AccountEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Cette interface permet de requêter la table campaign
 */
public interface CampaignRepository extends CrudRepository<CampaignEntity, Integer> {
    List<CampaignEntity> findByOrganizers(AccountEntity accountEntity);
}
