package fr.planninggo.spring.campaign.repository;

import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table JuryMember
 */

public interface JuryMemberRepository extends CrudRepository<JuryMemberEntity, Integer> {
}
