package fr.planninggo.spring.attendee.controller;

import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Cette classe traite toutes les requêtes en lien avec les étudiants
 */

@Controller
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * Permet la récupération de tous les étudiants
     */
    @GetMapping(value = "/getStudents")
    public List<StudentEntity> getStudents() {
        return studentService.getStudents();
    }
}
