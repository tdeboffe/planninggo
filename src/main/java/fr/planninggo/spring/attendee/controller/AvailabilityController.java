package fr.planninggo.spring.attendee.controller;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.PhaseEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.attendee.service.AvailabilityService;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.spring.campaign.service.PhaseService;
import fr.planninggo.spring.attendee.service.SupervisorService;
import fr.planninggo.csv.CSVWriter;
import fr.planninggo.spring.user.entity.AccountEntity;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value = "/availability")
public class AvailabilityController {
    private final AvailabilityService availabilityService;
    private final AttendeeService attendeeService;
    private final CampaignService campaignService;
    private final SupervisorService supervisorService;
    private final PhaseService phaseService;

    @Inject
    public AvailabilityController(PhaseService phaseService, AvailabilityService availabilityService, AttendeeService attendeeService, CampaignService campaignService, SupervisorService supervisorService) {
        this.availabilityService = availabilityService;
        this.attendeeService = attendeeService;
        this.campaignService = campaignService;
        this.supervisorService = supervisorService;
        this.phaseService = phaseService;
    }

    /**
     * récupération des diponibilités d'un participant pendant la période d'une campagne
     * @param id l'identifiant du participant
     * @param campaignId l'identifiant de la campagne
     * @return un set de diponibilité
     */
    @GetMapping(value = "/{id}/{camp}")
    public Set<DataAvailability> getAvailabilities(@PathVariable("id") Integer id, @PathVariable("camp") Integer campaignId) {
        Optional<CampaignEntity> optional = campaignService.getCampaign(campaignId);
        Set<AvailabilityEntity> availabilityEntities = new TreeSet<>(attendeeService.getAvailabilitiesById(id));
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            availabilityEntities = availabilityEntities.stream()
                    .filter(s -> s.getDate().toLocalDate().isBefore(campaignEntity.getCampaignEnd().toLocalDate().plusDays(1)))
                    .filter(s -> s.getDate().toLocalDate().isAfter(campaignEntity.getCampaignBegin().toLocalDate().minusDays(1)))
                    .collect(Collectors.toSet());
        }
        Set<DataAvailability> dataAvailabilities = new TreeSet<>();
        Optional<StudentEntity> optional1 = attendeeService.findStudentById(id);
        Optional<SupervisorEntity> optional2 = attendeeService.findSupervisorById(id);
        Set<AttendeeEntity> linkAttendees = new HashSet<>();
        if (optional1.isPresent()) {
            linkAttendees.addAll(optional1.get().getSupervisorsSet());
        } else if (optional2.isPresent() && optional.isPresent()) {
            Set<SupervisorEntity> supervisorEntities = new HashSet<>();
            optional2.get().getStudents().stream()
                    .filter(s -> optional.get().getStudents().contains(s))
                    .map(StudentEntity::getSupervisorsSet)
                    .forEach(supervisorEntities::addAll);
            supervisorEntities.stream().filter(s -> !s.getEmail().equals(optional2.get().getEmail()))
                    .forEach(linkAttendees::add);
        }

        for (AvailabilityEntity availabilityEntity : availabilityEntities) {
            StringBuilder morning = new StringBuilder("mor:");
            StringBuilder afternoon = new StringBuilder("&aft:");
            for (AttendeeEntity attendeeEntity : linkAttendees) {
                for (AvailabilityEntity availabilityEntity1 : attendeeEntity.getAvailabilities()) {
                    if (availabilityEntity.getDate().equals(availabilityEntity1.getDate())) {
                        if (availabilityEntity1.isOnMorning()) {
                            morning.append(attendeeEntity.getLastname()).append(" ").append(attendeeEntity.getFirstname()).append(", ");
                        }
                        if (availabilityEntity1.isOnAfternoon()) {
                            afternoon.append(attendeeEntity.getLastname()).append(" ").append(attendeeEntity.getFirstname()).append(", ");
                        }
                    }
                }
            }
            dataAvailabilities.add(new DataAvailability(availabilityEntity, morning.toString().substring(0, morning.toString().length() - 2) + afternoon.toString().substring(0, afternoon.toString().length() - 2 )));
        }
        return dataAvailabilities;
    }

    /**
     * cette classe contient les diponibilités d'une personne
     */
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    private class DataAvailability implements Comparable<DataAvailability> {
        private AvailabilityEntity availabilityEntity;
        private String availablePerson;

        private DataAvailability(AvailabilityEntity availabilityEntity, String availablePerson) {
            this.availabilityEntity = availabilityEntity;
            this.availablePerson = availablePerson;
        }

        @Override
        public int compareTo(@NotNull AvailabilityController.DataAvailability o) {
            return availabilityEntity.getDate().compareTo(o.availabilityEntity.getDate());
        }

        @Override
        public String toString() {
            return availabilityEntity.toString();
        }
    }

    /**
     * Vérifie si un participant à renseigné suffisament de disponibilité
     * @param id l'identifiant du participant
     * @param campaignId l'indentifiant de la campagne
     * @param phaseId l'identifiant de la phase
     * @return une liste de String avec true ou le nombre de disponibilité obligatoire pour la phase
     */
    @GetMapping("checkNumber")
    public List<String> checkNumberAvailability(@RequestParam("id") Integer id, @RequestParam("campaign") Integer campaignId, @RequestParam("phase") String phaseId) {
        if (phaseId.equals("undefined")) {
            return List.of("true");
        }
        int pId = Integer.parseInt(phaseId);
        Optional<CampaignEntity> optional = campaignService.getCampaign(campaignId);
        Set<AvailabilityEntity> availabilityEntities = new TreeSet<>(attendeeService.getAvailabilitiesById(id));
        if (optional.isPresent()) {
            CampaignEntity campaignEntity = optional.get();
            availabilityEntities = availabilityEntities.stream()
                    .filter(s -> s.getDate().toLocalDate().isBefore(campaignEntity.getCampaignEnd().toLocalDate().plusDays(1)))
                    .filter(s -> s.getDate().toLocalDate().isAfter(campaignEntity.getCampaignBegin().toLocalDate().minusDays(1)))
                    .filter(s -> s.isOnAfternoon() || s.isOnMorning())
                    .collect(Collectors.toSet());
        }
        int nbAvailabilities = 0;
        for (AvailabilityEntity availabilityEntity : availabilityEntities) {
            if (availabilityEntity.isOnAfternoon()) { nbAvailabilities++; }
            if (availabilityEntity.isOnMorning()) { nbAvailabilities++; }
        }
        if (nbAvailabilities <= phaseService.getPhase(pId).getAvailabilityNumber()) {
            return List.of(String.valueOf(phaseService.getPhase(pId).getAvailabilityNumber()));
        }
        return List.of("true");
    }

    /**
     * Recherche les diponibilités d'un superviasor
     * @param id l'identifiant du superviasor
     * @return la liste de diponibilité d'un superviasor
     */
    @GetMapping(value = "/supervisor/{id}")
    public Set<AvailabilityEntity> getAvailabilitiesSupervisors(@PathVariable("id") Integer id) {
        return new TreeSet<>(supervisorService.getAvailabilitiesById(id));
    }

    /**
     * Mets à jour les disponibilitsé d'un participant
     * @param id l'identifiant d'un participant
     * @param dispo la disponibilité à identifier
     */
    @GetMapping(value = "/updateAvailabilities")
    public void update(@RequestParam("id") Integer id, @RequestParam("dispo") String dispo) {
        AvailabilityEntity availabilityEntity = availabilityService.findById(id);
        String[] dispos = dispo.split(":");
        if (dispos[0].equals("mor")) {
            availabilityEntity.setOnMorning(Boolean.parseBoolean(dispos[1]));
        } else {
            availabilityEntity.setOnAfternoon(Boolean.parseBoolean(dispos[1]));
        }
        availabilityService.save(availabilityEntity);
    }

    /**
     * Export les disponibilité de tous les participants d'une campagne
     * @param id l'identifiant d'une campagne
     * @param response
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @GetMapping(value = "{id}/exportAvailabilities", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public InputStreamSource exportAvailabilities(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
        String fileName = "exportDispos.csv";
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        CampaignEntity campaignEntity = campaignService.getCampaign(id).get();
        List<AttendeeEntity> sups = campaignEntity.getAttendees();
        List<String> listForCSV = new ArrayList<>();
        List<String> listLines = new ArrayList<>();
        StringBuilder header = new StringBuilder("Civilite;Nom;Prenom;Mail");

        StringBuilder line;
        for(AttendeeEntity sup : sups) {
            line = new StringBuilder(sup.getCivility()+";"+sup.getLastname()+";"+sup.getFirstname()+";"+sup.getEmail());
            Set<AvailabilityEntity> set = sup.getAvailabilities();
            TreeSet<AvailabilityEntity> treeSet = new TreeSet<>(set);

            final String OLD_FORMAT = "yyyy-MM-dd";
            final String NEW_FORMAT = "dd-MM-yyyy";

            SimpleDateFormat sdf = new SimpleDateFormat();

            for(AvailabilityEntity entity : treeSet) {
                sdf.applyPattern(OLD_FORMAT);
                Date d;
                try {
                    d = sdf.parse(entity.getDate().toString());
                } catch (ParseException e) {
                    throw new IllegalArgumentException("La date n'est pas au bon forma : " + entity.getDate());
                }
                sdf.applyPattern(NEW_FORMAT);
                String addToHeader = ";" + sdf.format(d) + " MATIN;"+ sdf.format(d) +" APREM";
                if(!header.toString().contains(addToHeader)){
                    header.append(addToHeader);
                }
                if(entity.isOnMorning()){
                    line.append(";oui");
                }
                else{
                    line.append(";non");
                }
                if(entity.isOnAfternoon()){
                    line.append(";oui");
                }
                else{
                    line.append(";non");
                }

            }
            listLines.add(line.toString());
        }
        listForCSV.add(header.toString());
        listForCSV = Stream.concat(listForCSV.stream(), listLines.stream())
                .collect(Collectors.toList());
        Path path = Paths.get(fileName);
        CSVWriter cw = new CSVWriter(path, listForCSV);
        return new InputStreamResource(cw.getData());
    }

    /**
     * Permet de renseigner en base que le participants à renseignés ses disponibilités
     * @param mail le mail du participant
     */
    @GetMapping("setFlag")
    public void setFlag(@RequestParam("mail") String mail) {
        AttendeeEntity attendeeEntity = attendeeService.findByEmail(mail);
        attendeeEntity.setAvailabilityGiven(true);
        attendeeService.save(attendeeEntity);
    }

    /**
     *
     * @return renvoie la page import
     */
    @GetMapping(value = "/import")
    public ModelAndView importPage() {
        return new ModelAndView("import");
    }

    /**
     * Permet au participant de saisir ces disponibilités
     * @param request
     * @return
     */
    @GetMapping(value = "/attendee")
    public ModelAndView availabilityAttendee(HttpServletRequest request) {
        AccountEntity account = (AccountEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<CampaignEntity> campaignEntities = campaignService.getCampaigns();

        Map<Integer, List<Map<Integer, List<String>>>> campaignPhase = new HashMap<>();
        Map<Integer, String> campaignData = new HashMap<>();
        List<Map<Integer, List<String>>> currentList;
        Map<Integer, List<String>> currentMap;
        List<String> currentData;
        for (CampaignEntity campaignEntity : campaignEntities) {
            campaignData.put(campaignEntity.getId(), campaignEntity.getName());
            for (PhaseEntity phase : campaignEntity.getPhases()) {
                if (!isPhaseInProgress(phase)) {
                    continue;
                }
                currentList = campaignPhase.computeIfAbsent(campaignEntity.getId(), __ -> new ArrayList<>());
                currentMap = new HashMap<>();
                currentData = currentMap.computeIfAbsent(phase.getId(), __ -> new ArrayList<>());
                currentList.add(currentMap);
                if (phase.getWrittenBy().equals("Etudiant")) {
                    for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                        if (studentEntity.getAccountId() == account.getId()) {
                            if (phase.getWrittenBy().equals(phase.getWrittenFor())) {
                                currentData.add("idDispo::" + attendeeService.findByEmail(account.getEmail()).getId());
                                currentData.add("mailDispo::" + account.getEmail());
                            } else {
                                Optional<SupervisorEntity> optional = studentEntity.getSupervisors().stream()
                                        .filter(ss -> ss.getRoleEntity().getName().equals(phase.getWrittenFor())).map(SupervisorStudentEntity::getSupervisor).findAny();
                                if (optional.isPresent()) {
                                    currentData.add("idDispo::" + optional.get().getId());
                                    currentData.add("mailDispo::" + optional.get().getEmail());
                                    currentData.add("currentAttendee::" + account.getEmail());
                                }
                            }
                        }
                    }
                } else {
                    for (StudentEntity studentEntity : campaignEntity.getStudents()) {
                        Optional<SupervisorEntity> tutor = studentEntity.getSupervisors().stream().filter(s -> s.getRoleEntity().getName().equals(phase.getWrittenBy())).map(ss -> ss.getSupervisor()).findAny();
                        if (tutor.isPresent() && tutor.get().getAccountId() == account.getId()) {
                            currentData.add("idDispo::" + attendeeService.findByEmail(account.getEmail()).getId());
                            currentData.add("mailDispo::" + account.getEmail());
                        }
                    }
                    for (AttendeeGuestEntity attendeeGuestEntity : campaignEntity.getGuests()) {
                        if (attendeeGuestEntity.getAccountId() == account.getId()) {
                            currentData.add("idDispo::" + account.getId());
                            currentData.add("mailDispo::" + account.getEmail());
                        }
                    }
                }
            }
        }
        if (campaignPhase.isEmpty()) {
            request.getSession().setAttribute("errorDispo", "ERREUR : Vous n'avez pas de disponibilités à saisir");
            return new ModelAndView("redirect:/");
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<Integer, List<Map<Integer, List<String>>>> entry : campaignPhase.entrySet()) {
            stringBuilder.append("campaignName::").append(campaignData.get(entry.getKey())).append("|");
            stringBuilder.append("campaignId::").append(entry.getKey()).append(";D");
            for (Map<Integer, List<String>> map : entry.getValue()) {
                stringBuilder.append("&phase[");
                for (Map.Entry<Integer, List<String>> entry1 : map.entrySet()) {
                    stringBuilder.append("phaseId::").append(entry1.getKey()).append(",");
                    stringBuilder.append(String.join(",", entry1.getValue()));
                }
                stringBuilder.append("]");
            }
            stringBuilder.append("||");
        }

        request.getSession().setAttribute("map", stringBuilder.toString().substring(0, stringBuilder.toString().length() - 2));
        return new ModelAndView("availabilities_attendee");
    }

    /**
     * vérifie si une phase est en progression
     * @param phase la phase entity
     * @return true si la phase est en cours non sinon
     */
    private static boolean isPhaseInProgress(PhaseEntity phase) {
        LocalDate begin = phase.getBegin().toLocalDate().minusDays(1);
        LocalDate end = phase.getEnd().toLocalDate().plusDays(1);
        return java.sql.Date.valueOf(LocalDate.now()).after(java.sql.Date.valueOf(begin)) && java.sql.Date.valueOf(LocalDate.now()).before(java.sql.Date.valueOf(end));
    }
}
