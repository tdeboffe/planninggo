package fr.planninggo.spring.attendee.controller;

import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.JuryMemberEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.service.AttendeeService;
import fr.planninggo.spring.campaign.service.CampaignService;
import fr.planninggo.csv.CSVWriter;
import fr.planninggo.spring.user.entity.RoleEntity;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Cette classe traite toutes les requêtes en lien avec les attendees
 */

@Controller
@RequestMapping("/attendee")
public class AttendeeController {

    private final AttendeeService attendeeService;
    private final CampaignService campaignService;

    @Inject
    public AttendeeController(AttendeeService attendeeService, CampaignService campaignService) {
        this.attendeeService = attendeeService;
        this.campaignService = campaignService;
    }

    /**
     * permet d'exporter tous les particpants d'une campagne dans un fichier csv
     * @param id  l'identifiant de la campagne
     * @param response
     * @return
     * @throws IOException
     */
    @CrossOrigin("*")
    @GetMapping(value = "{id}/exportParticipants", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public InputStreamResource exportParticipants(@PathVariable("id") Integer id, HttpServletResponse response) throws IOException {
        String fileName = "exportParticipants.csv";
        response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
        CampaignEntity campaignEntity = campaignService.getCampaign(id).get();
        List<String> listCSV = new ArrayList<>();
        String header = "CIVILITE;NOM;PRENOM;MAIL;CP;VILLE;ADRESSE;FILIERE;ROLE;CODE EXERCICE;";
        listCSV.add(header);
        String line = "";
        String lineTI = "";
        String lineTE = "";
        String lne = "";
        String lneTI = "";
        String lneTE = "";
        String te = "";
        String ti = "";

        List<String> roleSup = new ArrayList<>();

        Set<StudentEntity> students = campaignEntity.getStudents();
        for(StudentEntity student : students){
            line = student.getCivility() +";"+student.getLastname()+";"+student.getFirstname()+";"+student.getEmail()+";"+student.getPostalCode();
            lne = ";"+student.getTown()+";"+student.getAddress()+";"+student.getStudy()+";Etudiant;"+student.getDefenseType().getCode();
            Set<SupervisorStudentEntity> set = student.getSupervisors();
            for(SupervisorStudentEntity sup : set) {
                if (!roleSup.contains(sup.getRoleEntity().getName())) {
                    roleSup.add(sup.getRoleEntity().getName());
                    header += sup.getRoleEntity() + ";";
                }
                if (roleSup.get(0).equals(sup.getRoleEntity().getName())) {
                    te = ";" + sup.getSupervisor().getEmail();
                    lineTE = sup.getSupervisor().getCivility() + ";" + sup.getSupervisor().getLastname() + ";" + sup.getSupervisor().getFirstname() + ";" + sup.getSupervisor().getEmail() + ";" + sup.getSupervisor().getPostalCode();
                    lneTE = ";" + sup.getSupervisor().getTown() + ";" + sup.getSupervisor().getAddress() + ";;" + sup.getRoleEntity().getName() + ";;;";
                }
                else if (roleSup.get(1).equals(sup.getRoleEntity().getName())) {
                    ti = ";" +  sup.getSupervisor().getEmail();
                    lineTI = sup.getSupervisor().getCivility() + ";" + sup.getSupervisor().getLastname() + ";" + sup.getSupervisor().getFirstname() + ";" + sup.getSupervisor().getEmail() + ";" + sup.getSupervisor().getPostalCode();
                    lneTI = ";" + sup.getSupervisor().getTown() + ";" + sup.getSupervisor().getAddress() + ";;" + sup.getRoleEntity().getName() + ";;;";
                }
            }
            listCSV.add(line+lne+te+ti);
            listCSV.add(lineTI+lneTI);
            listCSV.add(lineTE+lneTE);
        }

        Set<AttendeeGuestEntity> attendeeGuestEntities = campaignEntity.getGuests();
        for (AttendeeGuestEntity attendeeGuestEntity : attendeeGuestEntities) {

            Set<String> roles = attendeeGuestEntity.getAccount().getRoleEntities().stream().map(RoleEntity::getName).collect(Collectors.toSet());
            Set<JuryMemberEntity> juryMemberEntities = new HashSet<>();
            Set<String> roleDType = new HashSet<>();
            campaignEntity.getDefensesType().forEach(d -> juryMemberEntities.addAll(d.getJuryMembers()));
            juryMemberEntities.forEach(j -> roleDType.add(j.getRolename()));

            String role = setRole(roles, roleDType);

            if (role.isEmpty()) {
                throw new IllegalArgumentException("Le rôle n'est pas dispo");
            }

            line = attendeeGuestEntity.getCivility() +";"+attendeeGuestEntity.getLastname()+";"
                    +attendeeGuestEntity.getFirstname()+";"+attendeeGuestEntity.getEmail()+";"
                    +attendeeGuestEntity.getPostalCode()+";"+attendeeGuestEntity.getTown()+";"
                    +attendeeGuestEntity.getAddress()+";;"+role;
            listCSV.add(line);
        }

        listCSV.set(0, header);
        Path path = Paths.get(fileName);
        CSVWriter cw = new CSVWriter(path, listCSV);
        return new InputStreamResource(cw.getData());
    }

    /**
     * permet de retrouver un role présent dans deux set
     * @param roles tous les roles des guest attendee
     * @param roleDType les roles du defense type
     * @return
     */
    private static String setRole(Set<String> roles, Set<String> roleDType) {
        for (String r : roles) {
            if (roleDType.contains(r)) {
                return r;
            }
        }
        return "";
    }
}
