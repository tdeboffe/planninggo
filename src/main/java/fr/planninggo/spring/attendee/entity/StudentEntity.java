package fr.planninggo.spring.attendee.entity;

import fr.planninggo.spring.campaign.entity.CampaignEntity;
import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.campaign.entity.DefenseTypeEntity;
import fr.planninggo.spring.user.entity.AccountEntity;
import lombok.Generated;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Cette classe permet de récupérer les informations de la table "student"
 */
@Entity
@Table(name = "student")
public class StudentEntity extends AttendeeEntity implements Serializable {
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "study_id")
    private StudyEntity study;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student", cascade = CascadeType.ALL)
    private Set<SupervisorStudentEntity> supervisors = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "student_campaign", joinColumns = {@JoinColumn(name = "attendee_id")}, inverseJoinColumns = {@JoinColumn(name = "campaign_id")})
    private Set<CampaignEntity> campaigns;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "student", cascade = CascadeType.ALL)
    private Set<DefenseEntity> defenses = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "type_code")
    private DefenseTypeEntity defenseType;

    public StudentEntity() {
        super();
    }

    public StudentEntity(String email, String lastname, String firstname, String address, AccountEntity account, CivilityEntity civility, PcTownEntity pcTown, StudyEntity study, DefenseTypeEntity defenseType, Set<CampaignEntity> campaigns) {
        super(email, lastname, firstname, address, account, civility, pcTown);
        this.defenseType = Objects.requireNonNull(defenseType);
        this.study = Objects.requireNonNull(study);
        this.campaigns = Objects.requireNonNull(campaigns);
    }

    public StudentEntity(AttendeeEntity attendeeEntity) {
        super(attendeeEntity.getEmail(), attendeeEntity.getLastname(), attendeeEntity.getFirstname(), attendeeEntity.getAddress(), attendeeEntity.getAccount(), attendeeEntity.getCivilityEntity(), attendeeEntity.getPcTown());
        this.supervisors = new HashSet<>();
    }
    public StudentEntity(StudentEntity studentEntity) {
        this(studentEntity.getEmail(), studentEntity.getLastname(), studentEntity.getFirstname(), studentEntity.getAddress(), studentEntity.getAccount(), studentEntity.getCivilityEntity(), studentEntity.getPcTown(), studentEntity.getStudyEntity(), studentEntity.getDefenseType(), studentEntity.getCampaigns());
        super.setId(studentEntity.getId());
        this.supervisors = studentEntity.supervisors;
        this.defenses = studentEntity.defenses;
    }


    public StudentEntity(String email, String lastname, String firstname, String address, AccountEntity account, CivilityEntity civilityEntity, PcTownEntity pcTown, StudyEntity studyEntity, DefenseTypeEntity defenseType, Set<CampaignEntity> campaigns, Set<SupervisorStudentEntity> supervisorEntities) {
        this(email, lastname, firstname, address, account, civilityEntity, pcTown, studyEntity, defenseType, campaigns);
        this.supervisors = supervisorEntities;
    }

    public static StudentEntity create(AttendeeEntity attendeeEntity) {
        StudentEntity studentEntity = new StudentEntity(attendeeEntity);
        studentEntity.setAvailabilities(attendeeEntity.getAvailabilities());
        return studentEntity;
    }

    public String getStudy() {
        return study.getName();
    }

    public void setStudy(StudyEntity study) {
        this.study = study;
    }

    public DefenseTypeEntity getDefenseType() {
        return defenseType;
    }

    public void setDefenseType(DefenseTypeEntity defenseType) {
        this.defenseType = defenseType;
    }

    @Override
    @Generated
    public int hashCode() {
        return super.hashCode() ^ study.hashCode() ^ defenseType.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof StudentEntity)) {
            return false;
        }
        StudentEntity studentEntity = (StudentEntity) obj;
        return super.equals(studentEntity) && studentEntity.defenseType.equals(this.defenseType)
                && studentEntity.study.equals(this.study);
    }

    public Set<SupervisorStudentEntity> getSupervisors() {
        return supervisors;
    }

    public Set<SupervisorEntity> getSupervisorsSet() {
        return supervisors.stream().map(SupervisorStudentEntity::getSupervisor).collect(Collectors.toSet());
    }

    public void setSupervisors(Set<SupervisorStudentEntity> supervisors) {
        this.supervisors = supervisors;
    }

    public Set<CampaignEntity> getCampaigns() {
        return campaigns;
    }

    public StudyEntity getStudyEntity() {
        return study;
    }

    public Set<DefenseEntity> getDefenses() {
        return defenses;
    }

    @Override
    public String toString() {
        return super.toString() + " : " + study.getName() + supervisors.toString();
    }
}
