package fr.planninggo.spring.attendee.entity;

import fr.planninggo.spring.campaign.entity.DefenseEntity;
import fr.planninggo.spring.user.entity.AccountEntity;
import lombok.Generated;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Cette classe permet de récupérer les informations de la table "attendee_guest"
 */
@Entity
@Table(name = "attendee_guest")
public class AttendeeGuestEntity extends AttendeeEntity {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "defense_attendee_guest", joinColumns = {@JoinColumn(name = "attendee_id")}, inverseJoinColumns = {@JoinColumn(name = "defense_id")})
    private Set<DefenseEntity> defenses = new HashSet<>();


    public AttendeeGuestEntity() {}

    public AttendeeGuestEntity(String email, String lastname, String firstname, String address, AccountEntity account, CivilityEntity civility, PcTownEntity pcTown) {
        super(email, lastname, firstname, address, account, civility, pcTown);
    }

    public AttendeeGuestEntity(AttendeeEntity attendeeEntity) {
        super(attendeeEntity.getEmail(), attendeeEntity.getLastname(), attendeeEntity.getFirstname(), attendeeEntity.getAddress(), attendeeEntity.getAccount(), attendeeEntity.getCivilityEntity(), attendeeEntity.getPcTown());
        this.setId(attendeeEntity.getId());
        this.setAvailabilities(attendeeEntity.getAvailabilities());
    }

    public static AttendeeGuestEntity create(AttendeeEntity attendeeEntity) {
        AttendeeGuestEntity attendeeGuestEntity = new AttendeeGuestEntity(attendeeEntity);
        attendeeGuestEntity.setAvailabilities(attendeeEntity.getAvailabilities());
        return attendeeGuestEntity;
    }

    @Override
    @Generated
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof AttendeeGuestEntity)) {
            return false;
        }
        AttendeeGuestEntity a = (AttendeeGuestEntity) obj;
        return super.equals(a);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public Set<DefenseEntity> getDefenses() {
        return defenses;
    }

    public void addAvailabilities(AvailabilityEntity availabilityEntity) {
        for (AvailabilityEntity availabilityEntity1 : this.getAvailabilities()) {
            if (availabilityEntity.getDate().equals(availabilityEntity1.getDate())) {
                return;
            }
        }
        this.getAvailabilities().add(availabilityEntity);
    }
}
