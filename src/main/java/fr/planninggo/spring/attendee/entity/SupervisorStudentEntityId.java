package fr.planninggo.spring.attendee.entity;

import lombok.Generated;

import java.io.Serializable;

/**
 * cette classe représente l'identifiant de la classe SuperviasorStudentEntity
 */
public class SupervisorStudentEntityId implements Serializable {
    private int supervisor;
    private int student;

    @Override
    @Generated
    public int hashCode() {
        return supervisor ^ student;
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof SupervisorStudentEntityId)) {
            return false;
        }
        SupervisorStudentEntityId s = (SupervisorStudentEntityId) obj;
        return s.supervisor == supervisor && s.student == student;
    }
}
