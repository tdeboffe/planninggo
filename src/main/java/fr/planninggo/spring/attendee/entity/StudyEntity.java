package fr.planninggo.spring.attendee.entity;

import org.apache.commons.csv.CSVRecord;

import javax.persistence.*;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "study"
 */
@Entity
@Table(name = "study")
public class StudyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "study_id")
    private int id;

    @Column(name = "study_name")
    private String name;

    public StudyEntity() {}

    public StudyEntity(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public static StudyEntity create(CSVRecord csvRecord) {
        return new StudyEntity(csvRecord.get(1));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
