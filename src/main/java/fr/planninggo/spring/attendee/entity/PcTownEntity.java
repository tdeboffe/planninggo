package fr.planninggo.spring.attendee.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.*;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "pc_town"
 */
@Entity
@Table(name = "pc_town")
public class PcTownEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "town")
    private String town;

    @Column(name = "is_paris_area")
    private boolean parisArea;

    public PcTownEntity() {
    }

    public PcTownEntity(String postalCode, String town, boolean parisArea) {
        this.postalCode = Objects.requireNonNull(postalCode);
        this.town = Objects.requireNonNull(town);
        this.parisArea = parisArea;
    }

    public static PcTownEntity create(CSVRecord csvRecord) {
        return new PcTownEntity(csvRecord.get(1).trim(),
                csvRecord.get(2),
                csvRecord.get(3).equals("t"));
    }

    public int getId() {
        return id;
    }

    public boolean isParisArea() {
        return parisArea;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getTown() {
        return town;
    }

    @Override
    @Generated
    public int hashCode() {
        return postalCode.hashCode() ^ town.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof PcTownEntity)) {
            return false;
        }
        PcTownEntity p = (PcTownEntity) obj;
        return p.town.equals(this.town) && p.postalCode.equals(this.postalCode);
    }

    @Override
    public String toString() {
        return "PcTownEntity{" +
                "id=" + id +
                ", postalCode='" + postalCode + '\'' +
                ", town='" + town + '\'' +
                ", parisArea=" + parisArea +
                '}';
    }
}
