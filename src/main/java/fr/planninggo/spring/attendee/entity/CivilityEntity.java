package fr.planninggo.spring.attendee.entity;

import lombok.Generated;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.*;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "civility"
 */
@Entity
@Table(name = "civility")
public class CivilityEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "civility_id")
    private int id;

    @Column(name = "civility_name")
    private String name;

    public CivilityEntity() {}

    public CivilityEntity(String name) {
        this.name = Objects.requireNonNull(name);
    }

    public static CivilityEntity create(CSVRecord csvRecord) {
        return new CivilityEntity(csvRecord.get(1));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    @Generated
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof CivilityEntity)) {
            return false;
        }
        CivilityEntity c = (CivilityEntity) obj;
        return c.name.equals(this.name);
    }
}
