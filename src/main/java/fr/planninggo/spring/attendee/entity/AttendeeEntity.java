package fr.planninggo.spring.attendee.entity;

import fr.planninggo.spring.user.entity.AccountEntity;
import lombok.Generated;
import org.apache.commons.csv.CSVRecord;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Cette classe permet de récupérer les informations de la table "attendee"
 */
@Entity
@Table(name = "attendee")
@Inheritance(
        strategy = InheritanceType.JOINED
)
public class AttendeeEntity implements Comparable<AttendeeEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "attendee_id")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "address")
    private String address;

    @Column(name = "availability_given")
    private Boolean availabilityGiven;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private AccountEntity account;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "civility_id")
    private CivilityEntity civility;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pc_town_id")
    private PcTownEntity pcTown;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "attendee_availability",
            joinColumns = {@JoinColumn(name = "attendee_id")},
            inverseJoinColumns = {@JoinColumn(name = "availability_id")})
    private Set<AvailabilityEntity> availabilities = new HashSet<>();

    public AttendeeEntity() {
    }

    public AttendeeEntity(String email, String lastname, String firstname, String address, AccountEntity account, CivilityEntity civility, PcTownEntity pcTown) {
        this.email = Objects.requireNonNull(email);
        this.lastname = Objects.requireNonNull(lastname);
        this.firstname = Objects.requireNonNull(firstname);
        this.address = Objects.requireNonNull(address);
        this.account = Objects.requireNonNull(account);
        this.civility = Objects.requireNonNull(civility);
        this.pcTown = Objects.requireNonNull(pcTown);
        this.availabilityGiven = false;
    }

    public AttendeeEntity(String email, String lastname, String firstname, String address) {
        this.email = Objects.requireNonNull(email);
        this.lastname = Objects.requireNonNull(lastname);
        this.firstname = Objects.requireNonNull(firstname);
        this.address = Objects.requireNonNull(address);
        this.availabilityGiven = false;
    }

    public static AttendeeEntity create(CSVRecord csvRecord) {
        return new AttendeeEntity(csvRecord.get(1),
                csvRecord.get(2),
                csvRecord.get(3),
                csvRecord.get(4));
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        account.setEmail(email);
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCivility() {
        return civility.getName();
    }

    public void setCivility(CivilityEntity civility) {
        this.civility = civility;
    }

    public CivilityEntity getCivilityEntity() {
        return civility;
    }

    public String getTown() {
        return pcTown.getTown();
    }

    public String getPostalCode() {
        return pcTown.getPostalCode();
    }

    public AccountEntity getAccount() {
        return account;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    public PcTownEntity getPcTown() {
        return pcTown;
    }

    public void setPcTown(PcTownEntity pcTown) {
        this.pcTown = pcTown;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    @Generated
    public int hashCode() {
        return Integer.hashCode(id) ^ email.hashCode() ^ lastname.hashCode() ^ firstname.hashCode() ^ account.hashCode() ^ civility.hashCode() ^ pcTown.hashCode();
    }

    @Override
    @Generated
    public boolean equals(Object obj) {
        if (!(obj instanceof AttendeeEntity)) {
            return false;
        }
        AttendeeEntity a = (AttendeeEntity) obj;
        return a.email.equals(this.email) && this.lastname.equals(a.lastname) && this.firstname.equals(a.firstname)
                && a.account.equals(this.account) && this.civility.equals(a.civility)
                && this.pcTown.equals(a.pcTown);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        availabilities.forEach(sb::append);
        return "AttendeeEntity{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", address='" + address + '\'' +
                ", account=" + account +
                ", civility=" + civility.getName() +
                ", pcTown=" + pcTown.getPostalCode() + " : " + pcTown.getTown() +
                ", avails=" + sb.toString() +
                '}';
    }

    public int getAccountId() {
        return account.getId();
    }

    public Set<AvailabilityEntity> getAvailabilities() {
        return availabilities;
    }

    public void setAvailabilities(Set<AvailabilityEntity> availabilities) {
        this.availabilities = availabilities;
    }

    public void setAvailabilityGiven(boolean availabilityGiven) {
        this.availabilityGiven = availabilityGiven;
    }

    public boolean isAvailabilityGiven() {
        return availabilityGiven;
    }

    @Override
    public int compareTo(@NotNull AttendeeEntity o) {
        return email.compareTo(o.getEmail());
    }
}
