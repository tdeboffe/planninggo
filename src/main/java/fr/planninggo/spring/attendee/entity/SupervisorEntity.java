package fr.planninggo.spring.attendee.entity;

import fr.planninggo.spring.user.entity.AccountEntity;
import fr.planninggo.spring.user.entity.RoleEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * Cette classe permet de récupérer les informations de la table "superviasor"
 */
@Entity
@Table(name = "supervisor")
public class SupervisorEntity extends AttendeeEntity implements Serializable {
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "supervisor", cascade = CascadeType.ALL)
    private Set<SupervisorStudentEntity> students = new HashSet<>();

    public SupervisorEntity() { super(); }

    public SupervisorEntity(String email, String lastname, String firstname, String address, AccountEntity account, CivilityEntity civility, PcTownEntity pcTown) {
        super(email, lastname, firstname, address, account, civility, pcTown);
    }

    public SupervisorEntity(AttendeeEntity attendeeEntity) {
        super(attendeeEntity.getEmail(), attendeeEntity.getLastname(), attendeeEntity.getFirstname(), attendeeEntity.getAddress(), attendeeEntity.getAccount(), attendeeEntity.getCivilityEntity(), attendeeEntity.getPcTown());
        super.setId(attendeeEntity.getId());
    }

    public static SupervisorEntity create(AttendeeEntity attendeeEntity) {
        SupervisorEntity supervisorEntity = new SupervisorEntity(attendeeEntity);
        supervisorEntity.setAvailabilities(attendeeEntity.getAvailabilities());
        return supervisorEntity;
    }

    public Set<StudentEntity> getStudents() {
        return students.stream().map(SupervisorStudentEntity::getStudent).collect(Collectors.toSet());
    }


    public void addStudent(StudentEntity student, RoleEntity role) {
        this.students.add(new SupervisorStudentEntity(this, student, role));
    }

    public void addAvailabilities(AvailabilityEntity availabilityEntity) {
        for (AvailabilityEntity availabilityEntity1 : this.getAvailabilities()) {
            if (availabilityEntity.getDate().equals(availabilityEntity1.getDate())) {
                return;
            }
        }
        this.getAvailabilities().add(availabilityEntity);
    }

    @Override
    public String toString() {
        return "Supervisor : "  + super.toString();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
