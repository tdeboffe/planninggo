package fr.planninggo.spring.attendee.entity;

import fr.planninggo.spring.user.entity.RoleEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "superviasor_student"
 */
@Entity
@Table(name = "supervisor_student")
@IdClass(SupervisorStudentEntityId.class)
public class SupervisorStudentEntity implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "attendee_id_supervisor")
    private SupervisorEntity supervisor;

    @Id
    @ManyToOne
    @JoinColumn(name = "attendee_id_student")
    private StudentEntity student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private RoleEntity roleEntity;

    public SupervisorStudentEntity() {}

    public SupervisorStudentEntity(SupervisorEntity supervisorEntity, StudentEntity studentEntity, RoleEntity roleEntity) {
        this.student = Objects.requireNonNull(studentEntity);
        this.supervisor = Objects.requireNonNull(supervisorEntity);
        this.roleEntity = Objects.requireNonNull(roleEntity);
    }

    public StudentEntity getStudent() {
        return student;
    }

    public SupervisorEntity getSupervisor() {
        return supervisor;
    }

    public RoleEntity getRoleEntity() {
        return roleEntity;
    }

    @Override
    public String toString() {
        return student.getEmail() + " : " + supervisor.getEmail();
    }

    @Override
    public int hashCode() {
        return student.getEmail().hashCode() ^ supervisor.getEmail().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SupervisorStudentEntity)) {
            return false;
        }
        SupervisorStudentEntity s = (SupervisorStudentEntity) obj;
        return s.supervisor.getEmail().equals(supervisor.getEmail()) && s.student.getEmail().equals(student.getEmail());
    }
}
