package fr.planninggo.spring.attendee.entity;

import lombok.Generated;
import org.jetbrains.annotations.NotNull;
import org.apache.commons.csv.CSVRecord;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Cette classe permet de récupérer les informations de la table "availability"
 */
@Entity
@Table(name="availability")
public class AvailabilityEntity implements Comparable<AvailabilityEntity> {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="availability_id")
    private int id;

    @Column(name = "availability_date")
    private Date date;

    @Column(name = "morning")
    private boolean onMorning;

    @Column(name = "afternoon")
    private boolean onAfternoon;

    public AvailabilityEntity(LocalDate date, boolean onMorning, boolean onAfternoon) {
        this.date = Date.valueOf(date);
        this.onMorning = onMorning;
        this.onAfternoon = onAfternoon;
    }

    public AvailabilityEntity() {
    }

    public static AvailabilityEntity create(CSVRecord csvRecord) {
        return new AvailabilityEntity(strToLocalDate(csvRecord.get(1)),
                strToBoolean(csvRecord.get(2)),
                strToBoolean(csvRecord.get(3)));
    }

    private static LocalDate strToLocalDate(String strDate) {
        String[] dates = strDate.split("-");
        return LocalDate.of(Integer.parseInt(dates[0]), Integer.parseInt(dates[1]), Integer.parseInt(dates[2]));
    }

    private static boolean strToBoolean(String strBoolean) {
        return strBoolean.equals("t");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(LocalDate newDate) {
        date = Date.valueOf(newDate);
    }

    public boolean hasAvailabilities() {
        return (onMorning || onAfternoon);
    }

    public boolean isOnAfternoon() {
        return onAfternoon;
    }

    public void setOnAfternoon(boolean newOnAfternoon) {
        onAfternoon = newOnAfternoon;
    }

    public boolean isOnMorning() {
        return onMorning;
    }

    public void setOnMorning(boolean newOnMorning) {
        onMorning = newOnMorning;
    }

    @Override
    @Generated
    public boolean equals(Object o){
        if (!(o instanceof AvailabilityEntity)) {
            return false;
        }
        AvailabilityEntity a = (AvailabilityEntity) o;
        return this.date.equals(a.getDate()) && this.onAfternoon==a.isOnAfternoon() && this.onMorning==a.isOnMorning();
    }

    @Override
    @Generated
    public int hashCode() {
        return Objects.hash(date, onMorning, onAfternoon);
    }

    @Override
    @Generated
    public String toString() {
        return "AvailabilityEntity{" +
                "id=" + id +
                ", date=" + date +
                ", onMorning=" + onMorning +
                ", onAfternoon=" + onAfternoon +
                '}';
    }

    @Override
    public int compareTo(@NotNull AvailabilityEntity o) {
        return date.compareTo(o.date);
    }

}
