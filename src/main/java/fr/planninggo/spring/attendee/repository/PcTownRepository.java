package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.PcTownEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table PcTown
 */

public interface PcTownRepository extends CrudRepository<PcTownEntity, Integer> {
    PcTownEntity findByPostalCodeAndTown(String postalCode, String town);
}
