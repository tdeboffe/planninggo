package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntityId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Cette interface permet de requêter la table SupervisorStudent
 */

public interface SupervisorStudentRepository extends CrudRepository<SupervisorStudentEntity, SupervisorStudentEntityId> {
    @Modifying
    @Query(value = "DELETE FROM supervisor_student WHERE attendee_id_student = ?1 AND attendee_id_supervisor = ?2", nativeQuery = true)
    void deleteByStudentAndSupervisor(@Param("id") Integer id, @Param("id_sup") Integer idSup);

    List<SupervisorStudentEntity> findAllByStudent(StudentEntity studentEntity);

    List<SupervisorStudentEntity> findAllBySupervisor(SupervisorEntity supervisorEntity);
}
