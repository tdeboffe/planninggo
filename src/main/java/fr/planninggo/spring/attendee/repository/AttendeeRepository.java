package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Cette incterface permet de requêter la table Attendee
 */
public interface AttendeeRepository extends CrudRepository<AttendeeEntity, Integer> {
    AttendeeEntity findByEmail(String email);

    List<AttendeeEntity> findAllByEmail(String email);
}
