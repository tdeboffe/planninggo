package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.StudentEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Student
 */

public interface StudentRepository extends CrudRepository<StudentEntity, Integer> {
    StudentEntity findByEmail(String username);
}
