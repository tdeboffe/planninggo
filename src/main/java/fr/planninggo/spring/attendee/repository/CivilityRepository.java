package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.CivilityEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Civility
 */

public interface CivilityRepository extends CrudRepository<CivilityEntity, Integer> {
    CivilityEntity findByName(String name);
}
