package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.StudyEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Study
 */

public interface StudyRepository extends CrudRepository<StudyEntity, Integer> {
    StudyEntity findByName(String study);
}
