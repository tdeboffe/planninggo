package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;

/**
 * Cette interface permet de requêter la table availability
 */
@Repository
public interface AvailabilityRepository extends CrudRepository<AvailabilityEntity, Integer> {
    AvailabilityEntity findByDateAndOnMorningAndOnAfternoon(Date date, boolean onMorning, boolean onAfternoon);
}
