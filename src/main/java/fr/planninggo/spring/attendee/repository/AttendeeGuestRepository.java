package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table AttendeeGuest
 */

public interface AttendeeGuestRepository extends CrudRepository<AttendeeGuestEntity, Integer> {
    AttendeeGuestEntity findByEmail(String email);
}
