package fr.planninggo.spring.attendee.repository;

import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Cette interface permet de requêter la table Supervisor
 */

public interface SupervisorRepository extends CrudRepository<SupervisorEntity, Integer> {
    SupervisorEntity findByEmail(String name);
}
