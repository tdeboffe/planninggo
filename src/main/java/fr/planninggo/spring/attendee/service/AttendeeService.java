package fr.planninggo.spring.attendee.service;

import fr.planninggo.spring.attendee.entity.AttendeeEntity;
import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.entity.SupervisorStudentEntity;
import fr.planninggo.spring.attendee.repository.AttendeeGuestRepository;
import fr.planninggo.spring.attendee.repository.AttendeeRepository;
import fr.planninggo.spring.attendee.repository.StudentRepository;
import fr.planninggo.spring.attendee.repository.SupervisorRepository;
import fr.planninggo.spring.attendee.repository.SupervisorStudentRepository;
import fr.planninggo.spring.campaign.repository.CampaignRepository;
import fr.planninggo.spring.user.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;


/**
 * cette classe offre des méthodes simplifiant l'enregistrement des informations dans la table "attendee"
 */
@Service
public class AttendeeService {
    private final AttendeeRepository attendeeRepository;
    private final SupervisorRepository supervisorRepository;
    private final AttendeeGuestRepository attendeeGuestRepository;
    private final StudentRepository studentRepository;
    private final SupervisorStudentRepository supervisorStudentRepository;
    private final CampaignRepository campaignRepository;
    private final AccountRepository accountRepository;

    @Inject
    public AttendeeService(SupervisorStudentRepository supervisorStudentRepository, AttendeeRepository attendeeRepository, SupervisorRepository supervisorRepository, AttendeeGuestRepository attendeeGuestRepository, StudentRepository studentRepository, CampaignRepository campaignRepository, AccountRepository accountRepository) {
        this.attendeeRepository = attendeeRepository;
        this.supervisorRepository = supervisorRepository;
        this.attendeeGuestRepository = attendeeGuestRepository;
        this.studentRepository = studentRepository;
        this.supervisorStudentRepository = supervisorStudentRepository;
        this.campaignRepository = campaignRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * enregistre un nouveau gestAttendee en base de données le supprime de la table supervisor s'il est déjà enregistré.
     *
     * @param attendeeGuestEntity le gest Attendee à enregistrer.
     */
    public AttendeeGuestEntity createAttendeeGuest(AttendeeGuestEntity attendeeGuestEntity) {
        List<AttendeeEntity> dbGuest = attendeeRepository.findAllByEmail(attendeeGuestEntity.getEmail());
        if (!dbGuest.isEmpty()) {
            AttendeeGuestEntity attendeeGuestEntity1 = attendeeGuestRepository.findByEmail(attendeeGuestEntity.getEmail());
            if (attendeeGuestEntity1 == null) {
                attendeeGuestEntity1 = new AttendeeGuestEntity(dbGuest.get(0));
                attendeeRepository.delete(dbGuest.get(0));
                if(attendeeRepository.findAllByEmail(dbGuest.get(0).getEmail()) == null && campaignRepository.findByOrganizers(dbGuest.get(0).getAccount()) == null){
                    accountRepository.delete(dbGuest.get(0).getAccount());
                }
            }
            return new AttendeeGuestEntity(updateGuestBase(attendeeGuestEntity1, attendeeGuestEntity));
        }
        return attendeeGuestRepository.save(attendeeGuestEntity);
    }

    /**
     * Mise à jour des informations du guest attendee si il est déjà enregistré.
     *
     * @param dbGuest             Entity retrouver en base
     * @param attendeeGuestEntity l'entity qui doit être enregistrée
     */
    private AttendeeEntity updateGuestBase(AttendeeGuestEntity dbGuest, AttendeeGuestEntity attendeeGuestEntity) {
        return attendeeGuestRepository.save(updateGuest(dbGuest, attendeeGuestEntity));
    }

    /**
     * Mise à jour des informations des champs du guest attendee à enregistrer
     *
     * @param dbAttendee     Entity retrouver en base
     * @param attendeeEntity l'entity qui doit être enregistrée
     * @return
     */
    private AttendeeGuestEntity updateGuest(AttendeeGuestEntity dbAttendee, AttendeeGuestEntity attendeeEntity) {
        dbAttendee.setEmail(attendeeEntity.getEmail());
        dbAttendee.setLastname(attendeeEntity.getLastname());
        dbAttendee.setFirstname(attendeeEntity.getFirstname());
        dbAttendee.setAddress(attendeeEntity.getAddress());
        dbAttendee.setCivility(attendeeEntity.getCivilityEntity());
        dbAttendee.setPcTown(attendeeEntity.getPcTown());
        return dbAttendee;
    }

    /**
     * enregistre un nouveau Superviasor en base de données le supprime de la table GuestAttendee s'il est déjà enregistré.
     * @param attendeeEntity le guest Attendee à enregistrer.
     */
    public SupervisorEntity createSupervisor(AttendeeEntity attendeeEntity) {
        List<AttendeeEntity> dbSupervisor = attendeeRepository.findAllByEmail(attendeeEntity.getEmail());
        if (!dbSupervisor.isEmpty()) {
            SupervisorEntity supervisorEntity = supervisorRepository.findByEmail(attendeeEntity.getEmail());
            if (supervisorEntity == null) {
                supervisorEntity = new SupervisorEntity(dbSupervisor.get(0));
                if (attendeeGuestRepository.findByEmail(attendeeEntity.getEmail()) != null) {
                    attendeeGuestRepository.deleteById(dbSupervisor.get(0).getId());
                    if(attendeeRepository.findAllByEmail(dbSupervisor.get(0).getEmail())==null && campaignRepository.findByOrganizers(dbSupervisor.get(0).getAccount())==null){
                        accountRepository.delete(dbSupervisor.get(0).getAccount());
                    }
                    attendeeGuestRepository.save(new AttendeeGuestEntity(dbSupervisor.get(0)));
                } else {
                    attendeeRepository.delete(dbSupervisor.get(0));
                }
            }
            return updateSupervisorBase(supervisorEntity, attendeeEntity);
        }
        return supervisorRepository.save(new SupervisorEntity(attendeeEntity));
    }

    /**
     * Mise à jour des informations du Superviasor attendee si il est déjà enregistré.
     *
     * @param dbSupervisor   Entity retrouver en base
     * @param attendeeEntity l'entity qui doit être enregistrée
     */
    private SupervisorEntity updateSupervisorBase(SupervisorEntity dbSupervisor, AttendeeEntity attendeeEntity) {
        return supervisorRepository.save(updateSupervisor(dbSupervisor, attendeeEntity));
    }

    /**
     * Mise à jour des informations des champs du Superviasor attendee à enregistrer
     *
     * @param dbAttendee     Entity retrouver en base
     * @param attendeeEntity l'entity qui doit être enregistrée
     * @return
     */
    private SupervisorEntity updateSupervisor(SupervisorEntity dbAttendee, AttendeeEntity attendeeEntity) {
        dbAttendee.setEmail(attendeeEntity.getEmail());
        dbAttendee.setLastname(attendeeEntity.getLastname());
        dbAttendee.setFirstname(attendeeEntity.getFirstname());
        dbAttendee.setAddress(attendeeEntity.getAddress());
        dbAttendee.setCivility(attendeeEntity.getCivilityEntity());
        dbAttendee.setPcTown(attendeeEntity.getPcTown());
        return dbAttendee;
    }

    /**
     * Mise à jour des informations des champs du Student attendee à enregistrer
     *
     * @param dbAttendee     Entity retrouver en base
     * @param attendeeEntity l'entity qui doit être enregistrée
     * @return
     */
    private StudentEntity updateStudent(StudentEntity dbAttendee, StudentEntity attendeeEntity) {
        dbAttendee.setEmail(attendeeEntity.getEmail());
        dbAttendee.setLastname(attendeeEntity.getLastname());
        dbAttendee.setFirstname(attendeeEntity.getFirstname());
        dbAttendee.setAddress(attendeeEntity.getAddress());
        dbAttendee.setCivility(attendeeEntity.getCivilityEntity());
        dbAttendee.setPcTown(attendeeEntity.getPcTown());
        dbAttendee.setStudy(attendeeEntity.getStudyEntity());
        dbAttendee.setDefenseType(attendeeEntity.getDefenseType());
        dbAttendee.setSupervisors(attendeeEntity.getSupervisors());
        dbAttendee.getAvailabilities().addAll(attendeeEntity.getAvailabilities());
        attendeeEntity.setId(dbAttendee.getId());
        return dbAttendee;
    }

    /**
     * Mise à jour des informations du Student attendee si il est déjà enregistré.
     *
     * @param dbStudent     Entity retrouver en base
     * @param studentEntity l'entity qui doit être enregistrée
     */
    private StudentEntity updateStudentBase(StudentEntity dbStudent, StudentEntity studentEntity) {
        return studentRepository.save(updateStudent(dbStudent, studentEntity));
    }

    /**
     * enregistre un nouveau Strudent en base de données ou mise à jour de ses informations s'il est déjà enregistré.
     *
     * @param studentEntity le gest Attendee à enregistrer.
     */
    public StudentEntity createStudent(StudentEntity studentEntity) {
        StudentEntity dbStudent = studentRepository.findByEmail(studentEntity.getEmail());
        if (dbStudent != null) {
            return updateStudentBase(dbStudent, studentEntity);
        }
        return studentRepository.save(studentEntity);
    }

    /**
     * retrouve un attendee en base avec son email
     *
     * @param s l'email de l'attendee
     * @return un attendee Entity
     */
    public AttendeeEntity findByEmail(String s) {
        return attendeeRepository.findByEmail(s);
    }

    /**
     * Suvegarde une liste d'attendee en base de données
     *
     * @param attendeeEntities la liste d'entity à sauvegarder
     */
    public void saveAll(List<AttendeeEntity> attendeeEntities) {
        attendeeRepository.saveAll(attendeeEntities);
    }

    /**
     * Permet la récupération de tous les attendees
     */
    public List<AttendeeEntity> getAttendees() {
        return (List<AttendeeEntity>) this.attendeeRepository.findAll();
    }

    /**
     * Permet de récupérer un attendee par son id
     */
    public AttendeeEntity getAttendee(int id) {
        Optional<AttendeeEntity> optionalAttendeeEntity = this.attendeeRepository.findById(id);
        return optionalAttendeeEntity.orElse(null);
    }

    public Set<AvailabilityEntity> getAvailabilitiesById(Integer id) {
        Optional<AttendeeEntity> optionalAttendeeEntity = attendeeRepository.findById(id);
        if (optionalAttendeeEntity.isPresent()) {
            return optionalAttendeeEntity.get().getAvailabilities();
        }
        return new HashSet<>();
    }

    public void save(AttendeeEntity attendeeEntity) {
        attendeeRepository.save(attendeeEntity);
    }

    public void delete(AttendeeEntity attendeeEntity) { attendeeRepository.delete(attendeeEntity); }

    public AttendeeGuestEntity getGuest(String email) {
        return attendeeGuestRepository.findByEmail(email);
    }

    public void saveSupervisor(SupervisorStudentEntity supervisorStudentEntity) {
        supervisorStudentRepository.save(supervisorStudentEntity);
    }

    @Transactional
    public void removeSupervisor(StudentEntity studentEntity, Set<SupervisorStudentEntity> supervisorStudentEntities) {
        List<SupervisorStudentEntity> current = supervisorStudentRepository.findAllByStudent(studentEntity);
        for (SupervisorStudentEntity supervisorStudentEntity : current) {
            if (!supervisorStudentEntities.contains(supervisorStudentEntity)) {
                supervisorStudentRepository.deleteByStudentAndSupervisor(studentEntity.getId(), supervisorStudentEntity.getSupervisor().getId());
            }
        }
    }

    public void saveSupervisorAva(SupervisorEntity supervisorEntity) {
        supervisorRepository.save(supervisorEntity);
    }

    public void saveAttendeeGuest(AttendeeGuestEntity attendeeGuestEntity) {
        attendeeGuestRepository.save(attendeeGuestEntity);
    }

    public Optional<StudentEntity> findStudentById(Integer id) {
        return studentRepository.findById(id);
    }

    public Optional<SupervisorEntity> findSupervisorById(Integer id) {
        return supervisorRepository.findById(id);
    }
}
