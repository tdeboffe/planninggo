package fr.planninggo.spring.attendee.service;

import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.attendee.entity.SupervisorEntity;
import fr.planninggo.spring.attendee.repository.SupervisorRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


/**
 * cette classe offre des méthodes simplifiant l'enregistrement des informations dans la table "superviasor"
 */
@Service
public class SupervisorService {
    private final SupervisorRepository supervisorRepository;

    @Inject
    public SupervisorService(SupervisorRepository supervisorRepository) {
        this.supervisorRepository = supervisorRepository;
    }

    /**
     * récupére un superviasor avec son email
     * @param email l'email du superviasor
     * @return un superviasor entity
     */
    public SupervisorEntity getSupervisor(String email) {
        return supervisorRepository.findByEmail(email);
    }

    /**
     * récupérer toutes les diponibilités d'un superviasor
     * @param id l'identifiant d'un superviasor
     * @return les disponibilités du superviasor
     */
    public Set<AvailabilityEntity> getAvailabilitiesById(Integer id) {
        Optional<SupervisorEntity> optional = supervisorRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get().getAvailabilities();
        }
        return new HashSet<>();
    }

    /**
     * suavegarde un superviasor entity
     * @param supervisorEntity un superviasor entity
     */
    public void save(SupervisorEntity supervisorEntity) {
        supervisorRepository.save(supervisorEntity);
    }
}
