package fr.planninggo.spring.attendee.service;

import fr.planninggo.spring.attendee.entity.AttendeeGuestEntity;
import fr.planninggo.spring.attendee.repository.AttendeeGuestRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * cette classe offre des méthodes simplifiant l'enregistrement des informations dans la table "attendee_guest"
 */

@Service
public class AttendeeGuestService {
    private final AttendeeGuestRepository attendeeGuestRepository;

    @Inject
    public AttendeeGuestService(AttendeeGuestRepository attendeeGuestRepository) {
        this.attendeeGuestRepository = attendeeGuestRepository;
    }

    /**
     * récupération d'un attendee guest par son mail
     * @param email le mail du participant
     * @return
     */
    public AttendeeGuestEntity getAttendeeGuest(String email) {
        return attendeeGuestRepository.findByEmail(email);
    }

    /**
     * sauvegarde d'un attendee guest en base
     * @param attendeeGuestEntity
     */
    public void save(AttendeeGuestEntity attendeeGuestEntity) {
        attendeeGuestRepository.save(attendeeGuestEntity);
    }
}
