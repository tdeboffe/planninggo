package fr.planninggo.spring.attendee.service;

import fr.planninggo.spring.attendee.entity.StudentEntity;
import fr.planninggo.spring.attendee.repository.StudentRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;


/**
 * cette classe offre des méthodes simplifiant l'enregistrement des informations dans la table "student"
 */
@Service
public class StudentService {
    private final StudentRepository studentRepository;

    @Inject
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    /**
     * Permet la récupération de tous les étudiants
     */
    public List<StudentEntity> getStudents() {
        return (List<StudentEntity>) this.studentRepository.findAll();
    }

    /**
     * Permet de récupérer un student par son id
     */
    public StudentEntity getStudent(String mail) {
        return this.studentRepository.findByEmail(mail);
    }

    public void save(StudentEntity studentEntity) {
        studentRepository.save(studentEntity);
    }

}
