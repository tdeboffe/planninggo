package fr.planninggo.spring.attendee.service;

import fr.planninggo.spring.attendee.entity.AvailabilityEntity;
import fr.planninggo.spring.attendee.repository.AvailabilityRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.transaction.Transactional;



/**
 * cette classe offre des méthodes simplifiant l'enregistrement des informations dans la table "availability"
 */
@Service
@Transactional
public class AvailabilityService {
    private AvailabilityRepository availabilityRepository;

    @Inject
    public AvailabilityService(AvailabilityRepository availabilityRepository) {
        this.availabilityRepository = availabilityRepository;
    }

    /**
     * trouve une disponibilité avec son identifiant
     * @param id l'identifiant de la disponibilité
     * @return une availability entity
     */
    public AvailabilityEntity findById(Integer id) {
        return availabilityRepository.findById(id).get();
    }

    /**
     * sauvegarde une diponibilité en base
     * @param availabilityEntity la diponibilité a sauvegarder
     */
    public void save(AvailabilityEntity availabilityEntity) {
        availabilityRepository.save(availabilityEntity);
    }
}
