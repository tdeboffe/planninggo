"use strict";

let planning = document.getElementById("scheduler_here");
let authenticated = document.getElementById("authenticated").value;
let tree;
let msgIndicator;

planning.addEventListener('contextmenu', function(evt) {
    evt.preventDefault();
});

let changeMode = function () {
    let button = document.getElementById("figer");
    if (scheduler.config.blocked === true) {
        scheduler.config.blocked = false;
        scheduler.getEvents().forEach(function (evt) {
            evt.hold = "true";
            evt.color = "gray";
        });
        button.innerText = "Défiger";
        saveDefenses(true);
    } else {
        scheduler.config.blocked = true;
        scheduler.getEvents().forEach(function (evt) {
            evt.hold = "false";
            evt.color = undefined;
        });
        button.innerText = "Figer";
        saveDefenses(false);
    }
    scheduler.update_view();
};

let saveDefenses = function(isHold) {
    providesJson(
        "http://localhost:8080/app/campaign/saveDefenses?isHold="+isHold.toString(),
        //"http://localhost:8080/app/campaign/saveDefenses?isHold="+isHold.toString(),
        good => {},
        bad => {}
    );
    return true;
};

let saveDefense = function (id, evt) {
    let firstName = evt.firstName;
    let lastName = evt.lastName;
    let hold = evt.hold ? evt.hold : "false";
    providesJson(
        "http://localhost:8080/app/campaign/saveDefense?firstName="+firstName+"&lastName="+lastName
        +"&beginDate="+evt.start_date.getTime().toString()+"&hold="+hold.toString(),
        //"http://localhost:8080/app/campaign/saveDefense?firstName="+firstName+"&lastName="+lastName
        //+"&beginDate="+evt.start_date.getTime().toString()+"&hold="+hold,
        good => {},
        bad => {}
    );
    return true;
};

let saveDefenseOnTree = function (id, evt) {
    let firstName = evt.firstName;
    let lastName = evt.lastName;
    providesJson(
        "http://localhost:8080/app/campaign/saveDefenseOnTree?firstName="+firstName+"&lastName="+lastName,
        //"http://localhost:8080/app/campaign/saveDefenseOnTree?firstName="+firstName+"&lastName="+lastName,
        good => {},
        bad => {}
    );
    return true;
};

let isRegistered = function(evt) {
    return scheduler.getEvents().filter(function(e) { return evt === e;}).length !== 0;
};

let provideTree = function(defenses) {
    let campaignId = getCampaignId();
    if(campaignId === undefined) {
        return;
    }
    providesJson(
        //"http://localhost:8080/app/campaign/"+campaignId+"/getNullDefenses",
        "http://localhost:8080/app/campaign/"+campaignId+"/getNullDefenses",
        good => {
            let nullDefenses = {id:0, item:[]};
            good.forEach(function (el) {
                nullDefenses.item.push(JSON.parse(el));
            });
            initializePlanning(defenses, nullDefenses);
        },
        bad => {
            console.log(bad);
        }
    );
};

let getCurrentDate = function() {
    let campaignId = getCampaignId();
    if(campaignId === undefined) {
        return;
    }
    providesJson(
        //"http://localhost:8080/app/campaign/"+campaignId+"/getBeginDate",
        "http://localhost:8080/app/campaign/"+campaignId+"/getBeginDate",
        good => {
            if(good.length !== 0) {
                let tmp = JSON.parse(good[0]);
                scheduler.setCurrentView(new Date(tmp.date));
            }
        },
        bad => {}
    );
};

let checkMsgIndicator = function() {
    setTimeout(function () {
        providesJson(
            "http://localhost:8080/app/campaign/verify/" + getCampaignId(),
            good => {
            if (tree.getAllLeafs().split(",").filter(function(e) { return e !=="" }).length === 0 && good[0] === "true") {
            msgIndicator.innerText = "Planning Valide";
            msgIndicator.classList.remove("btn-danger");
            msgIndicator.className += " btn-success" ;
            document.getElementById("diffBtn").classList.remove("hide");
            $("#error").remove();
        } else {
            msgIndicator.innerText = "Planning Invalide";
            msgIndicator.classList.remove("btn-success");
            msgIndicator.className += " btn-danger";
            document.getElementById("diffBtn").classList.add("hide");
            $("#error").remove();
            $("#diff").after("<div class=\"alert alert-danger alert-dismissible fade show   \" role=\"alert\" id=\"error\">" +
                "                " + good[0] +
                "                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                "                    <span aria-hidden='true'>&times;</span>" +
                "                </button>" +
                "            </div>");
        }

    },
        bad => { console.log(bad) }
    )
    }, 300);
};

let isDifferentDuration = function(event) {
  let begin = (event.start_date.getHours()*60) + event.start_date.getMinutes();
  let end = (event.end_date.getHours()*60) + event.end_date.getMinutes();
  return (end-begin) === parseInt(event.duration);
};

let fillEvent = function(event) {
    event.hold = tree.getUserData(tree.getSelectedItemId(),"hold");
    event.a1 = tree.getUserData(tree.getSelectedItemId(), "a1");
    event.firstName = tree.getUserData(tree.getSelectedItemId(),"firstName");
    event.lastName = tree.getUserData(tree.getSelectedItemId(),"lastName");
    event.duration = tree.getUserData(tree.getSelectedItemId(),"duration");
    event.room = tree.getUserData(tree.getSelectedItemId(),"room");
};

let insertNewItem = function(event) {
    let id = tree.getAllLeafs().split(",").length + 1;
    tree.insertNewItem(0, id, event.text);
    tree.setUserData(id, "a1", event.a1);
    tree.setUserData(id, "hold", "false");
    tree.setUserData(id, "firstName", event.firstName);
    tree.setUserData(id, "lastName", event.lastName);
    tree.setUserData(id, "duration", event.duration);
    tree.setUserData(id, "room", event.room);
    checkMsgIndicator();
};

let isNotSameRoom = function(event) {
    let events = scheduler.getEvents(event.start_date, event.end_date);
    return events.filter(function(evt) { return evt!==event && evt.room === event.room; }).length === 0;
};

function highlight(filter) {
    let events = scheduler.getEvents();
    for (let event of events) {
        if (event.a1.toLowerCase().includes(filter.toLowerCase()) && filter.length >= 3) {
            event.color = "#e60000";
        } else {
            if (event.hold === "false") {
                event.color = "#1796b0";
            } else {
                event.color = "gray"
            }
        }
    }
    scheduler.updateView();
}

let initializePlanning = function (defenses, nullDefenses) {

    scheduler.config.select = false;
    scheduler.config.drag_move = false;
    scheduler.config.drag_resize = false;
    scheduler.config.dblclick_create = false;
    scheduler.config.first_hour = 8;
    scheduler.config.last_hour = 19;
    scheduler.config.hour_size_px = 66;
    scheduler.locale.labels.agenda_tab = "Agenda";
    scheduler.config.lightbox.sections = [ {name:"description", height:30, map_to:"text", type:"textarea" , focus:true}, {name:"master", height:200, map_to:"a1", type:"textarea"}];

    defenses.forEach(function(defense) {
        defense.a1 = defense.text.split("/").join("\n");
        defense.text = defense.text.split("/")[0];
        defense.text += "<p></p>Président " + defense.a1.split("Président(s) du Jury")[1].split("\n")[0];
    });

    nullDefenses.item.forEach(function(defense) {
        defense.a1 = defense.text.split("/").join("\n");
        defense.text = defense.text.split("/")[0];
        defense.text += "<p></p>Président " + defense.a1.split("Président(s) du Jury")[1].split("\n")[0];
        $("#treeBox").show();
        $("#scheduler_here").toggleClass("col-sm-9");
        $("#scheduler_here").toggleClass("col-sm-12");
    });

    let date = getCurrentDate();
    scheduler.init('scheduler_here', date, "week");
    scheduler.parse(defenses, "json");

    scheduler.attachEvent("onLightbox", function () {
        $('.dhx_delete_btn').next().remove()
        $('.dhx_delete_btn').after("<div>Retirer du planning</div>");
        $('.dhx_save_btn').next().remove()
        $('.dhx_save_btn').after("<div>Ajouter au planning</div>");
        $('.dhx_cancel_btn').next().remove()
        $('.dhx_cancel_btn').after("<div>Retour au planning</div>");
    });

    scheduler.attachEvent("onBeforeLightbox", function (id, e) {
        if (authenticated === "true") {
            scheduler.config.buttons_right = (scheduler.getState().new_event) ? [] : ["dhx_delete_btn"];
            scheduler.config.buttons_left =  (scheduler.getState().new_event) ? ["dhx_save_btn", "dhx_cancel_btn"] : ["dhx_cancel_btn"];
        }
        else {
            scheduler.config.buttons_right = (scheduler.getState().new_event) ? [] : [];
            scheduler.config.buttons_left =  (scheduler.getState().new_event) ? ["dhx_cancel_btn"] : ["dhx_cancel_btn"];
        }
        scheduler.config.lightbox.sections = [{name:"master", height:200, map_to:"a1", type:"textarea"}];
        scheduler.resetLightbox();
        return true;
    });
    scheduler.attachEvent("onAfterLightbox", function () {
        scheduler.config.lightbox.sections = [{name:"description", height:30, map_to:"text", type:"textarea" , focus:true}]
        scheduler.resetLightbox();
        return true;
    })
    scheduler.attachEvent("onConfirmedBeforeEventDelete", function (id, event) {
        return event.hold !== "true" && authenticated === "true";
    });
    scheduler.getEvents().forEach(function(evt) {
        if(evt.hold === "true") {
           evt.color = "gray";
        } else {
           evt.color = undefined;
        }
    });

    if(authenticated === "true") {
        scheduler.config.drag_move = true;
        tree.setImagePath("/app/codebase/imgs/dhxtree_terrace/");
        tree.enableDragAndDrop(true);
        tree.parse(nullDefenses, "json");
        checkMsgIndicator();

        scheduler.config.blocked = true;
        scheduler.config.collision_limit = parseInt(document.getElementById("roomNumber").value);

        scheduler.attachEvent("onEventLoading", function(ev) {
            return scheduler.checkCollision(ev);
        });
        scheduler.attachEvent("onContextMenu", function (id) {
            let event = scheduler.getEvent(id);
            if (event.hold === "true") {
                event.hold = "false";
                event.color = undefined;
            } else {
                event.hold = "true";
                event.color = "gray";
            }
            scheduler.update_view();
            saveDefense(id, event);
            return true;
        });

        scheduler.attachEvent("onBeforeEventDelete", function (id, event) {
            if(isRegistered(event) === false || event.hold === "true") {
                return false;
            }

            saveDefenseOnTree(id, event);
            insertNewItem(event);
            $("#treeBox").show();
            $("#scheduler_here").toggleClass("col-sm-9");
            $("#scheduler_here").toggleClass("col-sm-12");
            scheduler.update_view();
            return true;
        });
        scheduler.attachEvent("onEventCreated", function(e) {
            let event = scheduler.getEvent(e);
            event.text = tree.getSelectedItemText();
            event.a1 = tree.getUserData(tree.getSelectedItemId(), "a1");
            event.end_date.setMinutes(event.start_date.getMinutes() + parseInt(tree.getUserData(tree.getSelectedItemId(),"duration"), 10));
            scheduler.config.lightbox.sections = [{name:"master", height:200, map_to:"a1", type:"textarea"}];
            scheduler.resetLightbox();
            return true;
        });
        scheduler.attachEvent("onEventAdded", function (id, evt) {
            evt.hold = "false";
            tree.deleteItem(tree.getSelectedItemId(), true);
            if (tree.getSubItems(0).length === 0) {
                $("#treeBox").hide();
                $("#scheduler_here").toggleClass("col-sm-9");
                $("#scheduler_here").toggleClass("col-sm-12");
                scheduler.update_view();
            }
            saveDefense(id, evt);
            checkMsgIndicator();
            return true;
        });
        scheduler.attachEvent("onEventSave", function(id, event) {
            if (event.start_date.getDay() === 6 || event.start_date.getDay() === 0) {
                return false;
            }
            if(event.duration === undefined) {
                fillEvent(event);
            }
            if (isDifferentDuration(event) === false) {
                return false;
            }
            saveDefense(id,event);
            checkMsgIndicator();
            return true;
        });
        scheduler.attachEvent("onEventChanged", function(id, event) {
            if (event.start_date.getDay() === 6 || event.start_date.getDay() === 0) {
                return false;
            }

            if(isDifferentDuration(event) === false) {
                return false;
            }

            saveDefense(id,event);
            checkMsgIndicator();
            return true;
        });
        scheduler.attachEvent("onBeforeDrag", function (id) {
            let event = scheduler.getEvent(id);
            return !(event === undefined || event.hold === "true");
        });
    }

    scheduler.ignore_day = function(date) {
        return date.getDay() === 6 || date.getDay() === 0;
    };
    scheduler.ignore_week = function(date) {
        return date.getDay() === 6 || date.getDay() === 0;
    };
    scheduler.ignore_month = function(date) {
        return date.getDay() === 6 || date.getDay() === 0;
    };
};

let getCampaignId = function() {
    let array = (window.location.href).split("/");
    for(let i=3; i<array.length; i++) {
        if(!isNaN(parseInt(array[i]))) {
            return array[i];
        }
    }
    return undefined;
};

window.onload = function () {
    let campaignId = getCampaignId();
    if(campaignId === undefined) {
        return;
    }
    providesJson(
        //"http://localhost:8080/app/campaign/"+campaignId+"/getDefenses",
        "http://localhost:8080/app/campaign/"+campaignId+"/getDefenses",
        good => {
            let defenses = [];
            good.forEach(function (el) {
                defenses.push(JSON.parse(el))
            });
            if(authenticated === "true") {
                msgIndicator = document.getElementById("indicator");
                tree = new dhtmlXTreeObject("treeBox", "100%", "100%", 0);
                provideTree(defenses);
            } else {
                initializePlanning(defenses, {id:0, item:[]});
            }
        },
        bad => {
            console.log(bad);
        }
    );
};
