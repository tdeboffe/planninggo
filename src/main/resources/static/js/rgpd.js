"use strict"

function enableButton() {
    if(document.getElementById("accept").checked) {
        document.getElementById("accept_button").disabled = false;
    } else {
        document.getElementById("accept_button").disabled = true;
    }
}
