"use strict";

$("#attendee").combobox();

function changeClass(elem, id) {
    if (!window.location.href.split("/").includes("attendee")) {
        providesJson("http://localhost:8080/app/campaign/" + idCamp + "/updateStateAvailabilities", good => {}, bad => {})
    }
    providesJson(
        "http://localhost:8080/app/availability/checkNumber?id=" + mail + "&campaign=" + idCamp + "&phase=" + idPhase,
        good => {
            if (good[0] === "true" || $(elem).hasClass("bg-red")) {
                $(elem).toggleClass("bg-green");
                $(elem).toggleClass("bg-red");

                var dispo;

                if ($(elem).hasClass("afternoon")) {
                    if ($(elem).hasClass("bg-green")) {
                        dispo="aft:true";
                    } else {
                        dispo="aft:false";
                    }
                } else {
                    if ($(elem).hasClass("bg-green")) {
                        dispo="mor:true";
                    } else {
                        dispo="mor:false";
                    }
                }
                $("#error").remove();
                providesJson(
                    "http://localhost:8080/app/availability/updateAvailabilities?id=" + id + "&dispo=" + dispo,
                    good => {},
                    bad => {}
                );
            } else {
                $("#error").remove();
                $("#title").after(" <div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\" id=\"error\">" +
                    "        Vous ne pouvez pas saisir moins de " + good[0] + " disponibilités" +
                    "        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                    "            <span aria-hidden='true'>&times;</span>" +
                    "        </button>" +
                    "    </div>")
            }
    },
        bad => {}
    )


}

function setFlag(writtenFor, writtenBy) {
    if (writtenBy != "null") {
        providesJson("http://localhost:8080/app/availability/setFlag?mail=" + writtenBy, good => {}, bad => {})
    }
    providesJson("http://localhost:8080/app/availability/setFlag?mail=" + writtenFor, good => {}, bad => {})
}

function dayDiff(d1,d2) {
    var timeDiff = Date.parse(d2) - Date.parse(d1);
    var daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
    return daysDiff;
}

function toDate(str){
    var infos = str.split('-');
    return new Date(infos[0], infos[1], infos[2]);
}

var idCamp;
var mail;
var idPhase;

function printAvailabilities(tab, nb) {
    if (nb === undefined) {
        nb = "";
    }
    var index = 0;
    var nbDayDisplay=5;
    var nbTab = Math.ceil(tab.length/nbDayDisplay);

    console.log(nbTab);

    $('.tab').empty();
    for(let i = 1; i<=nbTab;i++){
        $('.tab' + nb).append("<button class=\"tablink\" onclick=openCalendar(event,'week"+nb+i+"')>Semaine" + i + "</button>");
    }


    for(let i = 1; i<= nbTab;i++){
        $('div.content' + nb).append("<div id='week"+nb+""+i+"' class='tabcontent' style='display: none'><table id='availabilities' class=\"table table-striped text-center\">\n" +
            "    <thead id = \"availabilities_head\">\n" +
            "    <tbody>\n" +
            "        <tr id=\"morning\"></tr>\n" +
            "        <tr id=\"afternoon\"></tr>\n" +
            "    </tbody>\n" +
            "    </thead>\n" +
            "</table></div>");

        var head = $("div#week"+nb+""+i+" thead#availabilities_head").empty();
        var morning = $("div#week"+nb+""+i+" tr#morning").empty();
        var afternoon = $("div#week"+nb+""+i+" tr#afternoon").empty();
        morning.append("<td style='color:black; font-size: 1rem; width: 4vw; font-weight: bold'>Matin</td>");
        afternoon.append("<td style='color:black; font-size: 1rem; width: 4vw; font-weight: bold'>Après-midi</td>");
        head.append("<td></td>");
        var clazz;
        for (var j = 0; j < nbDayDisplay && index < tab.length; j++) {
            let textM = tab[index].availablePerson.split("mor:");
            if (textM.length !== 1) {
                textM = textM[1].split("&")[0]
            } else {
                textM = "";
            }
            let textA = tab[index].availablePerson.split("aft:");
            if (textA.length === 1) {
                textA = "";
            } else {
                textA = textA[1];
            }
            head.append("<th scope='col' >" + frenchDateFormat(tab[index].availabilityEntity.date) + "</th>");
            clazz = setClazz(tab[index].availabilityEntity.onMorning);
            morning.append("<td class='morning " + clazz + "' onclick='changeClass(this, \""+ tab[index].availabilityEntity.id + "\")'>" + textM + "</td>");
            clazz = setClazz(tab[index].availabilityEntity.onAfternoon);
            afternoon.append("<td class='afternoon " + clazz + "' onclick='changeClass(this, \""+ tab[index].availabilityEntity.id + "\")'>" + textA + "</td>");
            index++;
        }
    }

}


function setClazz(bool) {
    if (bool) {
        return "bg-green";
    }
    return "bg-red";
}


function getAvailabilities(value, campaignId, phaseId, nb) {
    if (value === "") {
        return;
    }
    idCamp = campaignId;
    mail = value;
    idPhase = phaseId;
    providesJson(
        "http://localhost:8080/app/availability/" + value + "/" + campaignId,
        good => {
            printAvailabilities(good, nb);
        },
        bad => {
            console.log(bad)
        }
    );
}

function getAvailabilitiesPerso(map) {
    var campaigns = map.split("||");
    var nb = 0;
    for (const campaign of campaigns) {
        var campaignName = campaign.split("campaignName::")[1].split("|")[0];
        var campaignId = campaign.split("campaignId::")[1].split(";D")[0];
        var phases = campaign.split("&");
        phases.shift();
        for (const phase of phases) {
            var phaseId = phase.split("phaseId::")[1].split(",")[0];
            var dispoId = phase.split("idDispo::")[1].split(",")[0];
            var dispoMail = phase.split("mailDispo::")[1].split(",")[0].split("]")[0];
            var currentAttendee = "null";
            if (phase.split("currentAttendee::")[1] != undefined) {
                currentAttendee = phase.split("currentAttendee::")[1].split(",")[0].split("]")[0];
            }
            setFlag(dispoMail, currentAttendee);
            $(".container-fluid").append("<span>Dans le cadre de la campagne de soutenances : " + campaignName + ", il vous est demandé de saisir les disponibilités pour : " + dispoMail + "</span>");
            $(".container-fluid").append("<div class='tab" + nb + "'></div>");
            $(".container-fluid").append("<div class='content" + nb + "'></div>");
            getAvailabilities(dispoId, campaignId, phaseId, nb);
            nb++;
        }
    }
}


function frenchDateFormat(d){
    var infos =d.split('-');
    return infos[2]+ "-" + infos[1]+ "-" +infos[0];
}


function openCalendar(evt, tab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " active";
}
