"use strict";

function updateValue(elem, value) {
    if (value === "false") {
        $(elem).val("true");
    } else {
        $(elem).val("false");
    }
}

$(document).ready(function () {
    $("input:checkbox").on('click', function () {
        if ($(this).prop('checked')) {
            $(this).val("true");
        } else {
            $(this).val("false");
        }
    });
});

function beforeSubmit() {
    $(':checkbox').each(function() {
        this.checked = true;
    });
    setTimeout(function () {
        $("#form").submit();
    }, 1000);
}

setTimeout(function () {
    $('[data-toggle="tooltip"]').tooltip();
}, 3000);

$(function() {
    $('#add').on('click',function(){
        $("#roleNew").show();
        $("#validateNewRole").show();
    });

    $("#validateNewRole").on('click', function () {
        if ($("#roleNew").val() === "") {
            $("#errorRole").show();
        } else {
            $("select").each(function () {
                $(this).append("<option value='" + $("#roleNew").val() +"'>" + $("#roleNew").val() + "</option>");
            });
            $("#errorRole").hide();
            $(this).hide();
            $("#roleNew").hide();
        }
    });
});
