"use strict";

function setRelance(id, idCamp) {
    $("#launchRelance").attr("title", id);
    $("#launchRelance").attr("name", idCamp);
}

function relance(id, idCamp) {
    providesJson(
        "http://localhost:8080/app/campaign/relance?idPhase=" + id + "&idCamp=" + idCamp,
        good => {
            if (good[0] === "true") {
                $("#errorAfter").after("<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\" id=\"error\">" +
                    "                Relances envoyées" +
                    "                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                    "                    <span aria-hidden='true'>&times;</span>" +
                    "                </button>" +
                    "            </div>");
                return;
            }
            for (const elem of good) {
                $("#errorAfter").after("<div class=\"alert alert-danger alert-dismissible fade show\" role=\"alert\" id=\"error\">" +
                    "                " + elem +
                    "                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                    "                    <span aria-hidden='true'>&times;</span>" +
                    "                </button>" +
                    "            </div>");
            }
            if (good.length > 6) {
                $(".alert").delay(4000).slideUp(200, function() {
                    $(this).alert('close');
                });
            }
        },
        bad => {}
    )
}
