"use strict";
let getTomorrowDate = function() {
    let date = new Date();
    date.setDate(date.getDate() + 1);
    return date.getFullYear().toString()+"-"+("0"+(date.getMonth()+1).toString()).slice(-2)+"-"+("0"+(date.getDate()).toString()).slice(-2);
};

let beginDate = document.getElementById("inputBegin");
beginDate.setAttribute("min", getTomorrowDate());

let endingDate = document.getElementById("inputEnd");
endingDate.setAttribute("min", getTomorrowDate());

let selectPar = document.getElementById("rolePar");
let selectPour = document.getElementById("rolePour");

if (selectPour !== null && selectPar !== null) {
    if(selectPour.value !== "Etudiant") {
        selectPar.options[selectPar.options.length] = new Option(selectPour.value, selectPour.value);
    }
    selectPar.options[selectPar.options.length] = new Option("Etudiant", "Etudiant");

    selectPour.onchange = function() {
        let length = selectPar.options.length;
        while(length >=0) {
            selectPar.options[length] = null;
            length--;
        }

        if(selectPour.value !== "Etudiant") {
            selectPar.options[selectPar.options.length] = new Option(selectPour.value, selectPour.value);
        }
        selectPar.options[selectPar.options.length] = new Option("Etudiant", "Etudiant");
    };
}
