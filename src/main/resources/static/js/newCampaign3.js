"use strict";

function showForm() {
    $("#select").show();
    $("#select").css("display", "inline-block")
    $("#select_btn").show();
    $("#label").show();
    $("#addexisting").attr("disabled", "disabled");
}

function addDefenseType() {
    window.location.href = "/app/campaign/addExistType/" + $("#select").val();
}
