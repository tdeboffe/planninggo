"use strict";

$(function(){
    $('#add').on('click',function(){
        var table = $('#dates >tbody:last-child');
        var tableLength = $('#dates tr').length;
        table.append('<tr>\n' +
            '                <td><input type="Date" id='+tableLength+'></td>\n' +
            '                <td><select id="hourFrom'+tableLength+'"></select>H <select id="minuteFrom'+tableLength+'"></select>M</td>\n' +
            '                <td><select id="hourTo'+tableLength+'"></select>H <select id="minuteTo'+tableLength+'"></select>M</td>\n' +
            '            </tr>');
        var selectFrom =  $('#hourFrom'+tableLength);
        var selectFrom2 = $('#minuteFrom'+tableLength);
        var selectTo =  $('#hourTo'+tableLength);
        var selectTo2 = $('#minuteTo'+tableLength);
        var o;

        for (var i =0; i < 24; i++){
            o = new Option(i,i);
            $(o).html(i);
            selectFrom.append(o);
        }

        for(var i=0; i<60;i++){
            o = new Option(i,i);
            $(o).html(i);
            selectFrom2.append(o);
        }

        for (var i =0; i < 24; i++){
            o = new Option(i,i);
            $(o).html(i);
            selectTo.append(o);
        }

        for(var i=0; i<60;i++){
            o = new Option(i,i);
            $(o).html(i);
            selectTo2.append(o);
        }




        return false;
    });

    $('#delete').on('click',function(){
        var table = $('#dates tr:last');
        var tableLength = $('#dates tr').length;
        if(tableLength>2) {
            table.remove();
        }
        console.log(tableLength);
        return false;
    })

    var selectFrom =  $('#hourFrom1');
    var selectFrom2 = $('#minuteFrom1');
    var selectTo =  $('#hourTo1');
    var selectTo2 = $('#minuteTo1');
    var o;

    for (var i =0; i < 24; i++){
        o = new Option(i,i);
        $(o).html(i);
        selectFrom.append(o);
    }

    for(var i=0; i<60;i++){
        o = new Option(i,i);
        $(o).html(i);
        selectFrom2.append(o);
    }

    for (var i =0; i < 24; i++){
        o = new Option(i,i);
        $(o).html(i);
        selectTo.append(o);
    }

    for(var i=0; i<60;i++){
        o = new Option(i,i);
        $(o).html(i);
        selectTo2.append(o);
    }

});
