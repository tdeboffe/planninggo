"use strict";

window.onload = function () {
    providesJson(
        "http://localhost:8080/app/campaign/roles",
        good => {
        let vue = new Vue({
            el: "#roles",
            data: {
                options : good,
            }
        });
},
    bad => {
        console.log(bad);
    });
};
