package fr.planninggo.scheduler;

dialect "java"

import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScoreHolder;

import fr.planninggo.scheduler.Campaign;
import fr.planninggo.scheduler.ConstraintWeight;
import fr.planninggo.scheduler.Room;
import fr.planninggo.scheduler.Defense;
import fr.planninggo.scheduler.DefenseAssign;
import fr.planninggo.scheduler.OtherAttendee;
import fr.planninggo.scheduler.Timeslot;

global HardSoftScoreHolder scoreHolder;

// ############################################################################
// Contraintes de validité
// ############################################################################

rule "Room unavailable"
    when
        Campaign($weight : getConstraintWeight(3) != 0)
        Defense(hasUnavailableRoom())
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight);
end

rule "Room conflict"
    when
        Campaign($weight : getConstraintWeight(4) != 0)
        $defense : Defense(timeslot != null,
                room != null, $room : room,
                $leftId : id)
        Defense(timeslot != null, overlaps($defense),
                room == $room,
                id != $leftId)
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight);
end


rule "Supervisor unavailable"
    when
        Campaign($weight : getConstraintWeight(1) != 0)
        Defense($nbUnavail : getNbUnavailableAttendees() != 0)
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight*$nbUnavail);
end


rule "Supervisor conflict"
    when
        Campaign($weight : getConstraintWeight(2) != 0)
        $nbDefense : Defense(timeslot != null,
                $leftId : id)
        $nbDefense2 : Defense(timeslot != null, overlaps($nbDefense),
                id != $leftId)
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight*$nbDefense2.getNbAttendeesSameDefenseFromCache($nbDefense));
end

rule "Other attendee unavailable"
    when
        Campaign($weight : getConstraintWeight(7) != 0)
        DefenseAssign(otherAttendee != null && defense != null && hasUnavailableAttendee())
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight);
end

rule "Other attendee conflict"
    when
        Campaign($weight : getConstraintWeight(8) != 0)
        $otherAttAssign : DefenseAssign(otherAttendee != null, defense != null, $leftId : id)
        DefenseAssign(otherAttendee != null, defense != null, overlaps($otherAttAssign),
                id != $leftId,
                hasSameAttendee($otherAttAssign))
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight);
end


rule "Same attendee on defense"
    when
        Campaign($weight : getConstraintWeight(9) != 0)
        Defense($nbSame : sameAttendee()!= 0)
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight * $nbSame);
end

rule "Defense Type with good TimeSlot"
    when
        Campaign($weight : getConstraintWeight(10) != 0)
        Defense(!goodTimeSlot())
    then
        scoreHolder.addHardConstraintMatch(kcontext, - $weight);
end
// ############################################################################
// Contraintes d'optimalité
// ############################################################################

rule "Avoid multiple other attendees on half day"
    when
        Campaign($weight : getConstraintWeight(11) != 0)
        $otherAttAssign : DefenseAssign()
        DefenseAssign(hasDifferentOtherAttendeeFromHalfDayDefenses($otherAttAssign))
    then
        scoreHolder.addSoftConstraintMatch(kcontext, - $weight);
end

rule "Avoid overlapped defenses"
    when
        Campaign($weight : getConstraintWeight(12) != 0)
        $otherAttAssign : DefenseAssign(otherAttendee != null, defense != null, $leftId : id)
        DefenseAssign(otherAttendee != null, defense != null, overlaps($otherAttAssign),
                        id != $leftId)
    then
        scoreHolder.addSoftConstraintMatch(kcontext, - $weight);
end

rule "Avoid dispersed otherAttendee defenses on day"
    when
        Campaign($weight : getConstraintWeight(5) != 0)
        $otherAttAssign : DefenseAssign($otherAttendee : otherAttendee, $day : day)
        DefenseAssign(otherAttendee == $otherAttendee, day == $day, isNotAdjacentDefense($otherAttAssign))
    then
        scoreHolder.addSoftConstraintMatch(kcontext, - $weight);
end

rule "Middle day defenses"
    when
        Campaign($weight : getConstraintWeight(6) != 0)
        DefenseAssign($otherAttendee : otherAttendee, hasNotOtherAttendeeLivingParisArea())
        DefenseAssign(otherAttendee == $otherAttendee, $distance : getTimeslotDistanceWithNoonHour() != 0)
    then
        scoreHolder.addSoftConstraintMatch(kcontext, - $weight*$distance);
end
