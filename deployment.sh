#!/bin/bash

docker pull ggorret/planninggo_db:app
docker run --rm -d -p 5432:5432 --name planninggo_db ggorret/planninggo_db:app
mkdir /home/planninggodata
touch /home/planninggodata/relance.txt
echo "Bonjour ! Dans le cadre de la plannification d'une soutenance vous êtes invités à saisir vos disponibilités avant le <DATEEND>. Pour cela rendez-vous à cette adresse : <URL>. Si vous ne connaissez pas vos identifiants, passez par la récupération de mot de passe !" > /home/planninggodata/relance.txt
mvn clean install -DskipTests
java -jar target/Planninggo-alpha.jar &