# Description des méthodes de travail

## Description de la syntaxe du code

On utilise la syntaxe par défaut d'IntelliJ.
Quelques règles importantes :
* Espace après chaque virgule dans les paramètres. (Exemple : toto.tutu(param1, param2))
* Un saut de ligne entre chaque méthode.
* Pas de saut de ligne entre les champs.
* Je vous en supplie : Pas d'accolade d'ouverture solo sur une ligne. Moi je veux ça : if () {
* Quand on code on commente !!!

## Gestion des branches

Une branche par *feature*.
Chaque *feature* devra être déclarée dans une *issue*.

## Gestion des commits

Il y a une branche *develop* et une branche *master*.
On ne *push* jamais directment sur aucune de ces deux branches.
Chaque *push* doit faire l'objet d'une *merge request* qui devra être validée par une personne.

## Branche master

Chaque feature terminée sera mergée sur la branche master suite à un merge request.

## Fonctionnement de la chaine d'intégration

Step-by-Step Development :

1. &Eacute;crire son code (et le commenter :))
2. Via GitKraken : 
    1. Sélection des fichiers à push
    2. Titre du commit 
    3. Push
3. Sur GitLab seront ensuite testés, l'application web, la qualité du code, la couverture des tests, et les tests unitaires.
4. Une fois que le chef de projet aura déterminé qu'il est temps de faire une release, il s'en chargera en créant un tag.
5. La création d'une release entrainera le push sur le docker hub et la récupération via ssh de l'image à jour.

  
## SonarLint sur IntelliJ : Améliorer la qualité de code tout en codant
Installer le plugin SonarLint sur IntelliJ :
* File > Settings > Plugins > (Rechercher "SonarLint")
* Avec l'onglet SonarLint, accéder aux différentes issues dans votre code
* Appliquez les solutions proposées par SonarLint
  
## Déploiement continu

### Base de données

Sur le serveur :
* docker pull postgres
* docker run --name postgres -e POSTGRES_PASSWORD=pwd -e POSTGRES_USER=usr -e POSTGRES_DB=dbname -d postgres --> Se cale automatiquement sur le port 5432
* docker container exec -i postgres psql dbname < file.sql -U usr --> Lance un script SQL dans une BDD (Par exemple celui qui crée la BDD :))

### Application (JAR)

* docker pull ggorret/planninggo
* docker run ggorret/planninggo

## Optaplanner (Composant Solveur)

[Documentation Optaplanner 7.14](https://docs.optaplanner.org/7.14.0.Final/optaplanner-docs/html_single/index.html)

Lire au minimum les parties :
* Première partie de l'intro (1.1 What is Optaplanner ?)
* 4. Planner Configuration
* 5.1.1. What is a Score ?
* 5.3. Calculate the Score
* 5.6. Testing score constraints with JUnit

[Exemples disponibles -> zip à télécharger et ouvrir dans IntelliJ](https://download.jboss.org/optaplanner/release/7.14.0.Final/optaplanner-distribution-7.14.0.Final.zip)

