FROM openjdk:latest
WORKDIR /
ADD target/*.jar PlanningGo.jar
EXPOSE 8080
CMD java -jar PlanningGo.jar