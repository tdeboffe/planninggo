#!/bin/bash

if [ $# = 0 ] 
then	
	#TU
	mvn clean compile test

	#Selenium
	mvn clean spring-boot:run &
	mvn test -Dtest="AppTest" -DURL_HOME=http://localhost:8080/app


	#Sonar
	mvn clean jacoco:prepare-agent install
	mvn -DskipTests --batch-mode verify sonar:sonar -Dsonar.host.url=http://163.172.87.149/sonarqube -Dsonar.login=deec49e85509a64842c6929445e85429cf7bed90

else
	for param in "$*"
	do
		if [ $param = "-s" ]
		then
			echo "Lancement des tests sonarqube"
			mvn clean jacoco:prepare-agent install
			mvn --batch-mode verify sonar:sonar -Dsonar.host.url=http://163.172.87.149/sonarqube -Dsonar.login=deec49e85509a64842c6929445e85429cf7bed90
		elif [ $param = "-t" ] 
		then
			echo "Lancement des tests unitaires"
			mvn clean compile test
		elif [ $param = "-w" ]
		then
			echo "Lancement des tests Selenium"
			mvn clean compile test -Dtest="AppTest" -DURL_HOME=http://localhost:8080/app
		else
			echo -e "./test.sh [-t] [-w] [-s]\n-t = Tests unitaires\n-w = Tests Selenium\n-s = Tests Sonarqube"
		fi
	done
fi
